let row = $(".subjectRow");
let id = 1;

//  ---------------------------------------------------------------------------
//   should get back to normal url wiyhout # when click add tab
$("#structure-add").click(function (e) {
    e.preventDefault();
    document.location.href = String(document.location.href).replace("?#list", "");
});
$(function () {
    $('select.styled').customSelect();
});

//   open/close accordion in role access Privileges:
//----------------------------------------------------
$(function () {
    $('#toggleAccordionShow').on('click', function (e) {
        $('#accordionitem .collapse').removeAttr("data-parent").collapse('show');
    });
    $('#toggleAccordionHide').on('click', function (e) {
        $('#accordionitem .collapse').attr("data-parent", "#accordionitem").collapse('hide');
    });
    $('#toggleAccordionShow2').on('click', function (e) {
        $('#accordionitem2 .collapse').removeAttr("data-parent").collapse('show');
    });
    $('#toggleAccordionHide2').on('click', function (e) {
        $('#accordionitem2 .collapse').attr("data-parent", "#accordionitem2").collapse('hide');
    });
});



// checkbox sytle
//checkbox and radio btn

var d = document;
var safari = (navigator.userAgent.toLowerCase().indexOf('safari') != -1) ? true : false;
var gebtn = function (parEl, child) {
    return parEl.getElementsByTagName(child);
};
onload = function () {

    var body = gebtn(d, 'body')[0];
    body.className = body.className && body.className != '' ? body.className + ' has-js' : 'has-js';

    if (!d.getElementById || !d.createTextNode) return;
    var ls = gebtn(d, 'label');
    for (var i = 0; i < ls.length; i++) {
        var l = ls[i];
        if (l.className.indexOf('label_') == -1) continue;
        var inp = gebtn(l, 'input')[0];
        if (l.className == 'label_check') {
            l.className = (safari && inp.checked == true || inp.checked) ? 'label_check c_on' : 'label_check c_off';
            l.onclick = check_it;
        };
        if (l.className == 'label_radio') {
            l.className = (safari && inp.checked == true || inp.checked) ? 'label_radio r_on' : 'label_radio r_off';
            l.onclick = turn_radio;
        };
    };
};
var check_it = function () {
    var inp = gebtn(this, 'input')[0];
    if (this.className == 'label_check c_off' || (!safari && inp.checked)) {
        this.className = 'label_check c_on';
        if (safari) inp.click();
    } else {
        this.className = 'label_check c_off';
        if (safari) inp.click();
    };
};
var turn_radio = function () {
    var inp = gebtn(this, 'input')[0];
    if (this.className == 'label_radio r_off' || inp.checked) {
        var ls = gebtn(this.parentNode, 'label');
        for (var i = 0; i < ls.length; i++) {
            var l = ls[i];
            if (l.className.indexOf('label_radio') == -1) continue;
            l.className = 'label_radio r_off';
        };
        this.className = 'label_radio r_on';
        if (safari) inp.click();
    } else {
        this.className = 'label_radio r_off';
        if (safari) inp.click();
    };
};



$(document).ready(function () {



    // filter for programme/ module 
    $(".btn-search").click(function (e) {
        e.preventDefault();
        $('.intro-card').hide();
        $('.table-result').show();
    });
    $(".btn-reset").click(function (e) {
        e.preventDefault();
        $('.intro-card').show();
        $('.table-result').hide();
    });

    //  -------------------------
    // submit programme/module

    $("#submit-import").click(function () {
        $('#import-tu').hide();
        $('#btn-mod-ok').hide();
        $('#message').show();
    });

    $("#importData").click(function () {

        // $('#importData').addClass('running');
        $('#test').addClass('running');
        $('#btn-mod-cancel').hide();
        setTimeout(function () {
            $('#test').removeClass('running');
            $('#message').hide();
            $('#import-msg').show();
            $('#btn-mod-ok').show();
        }, 5000);
    });
    // ----------------------

    // to go to module listing tabs 
    var url = document.location.toString();
    if (url.match('#')) {
        $('.nav-tabs a[href="#' + url.split('#')[1] + '"]').tab('show');
    }


    $("#complete-module").click(function () {
        var url = document.location.toString();
        if (url.match('#')) {
            $('.nav-tabs a[href="#' + url.split('#')[1] + '"]').tab('show');
        }
        // Change hash for page-reload
        $('.nav-tabs a').on('shown.bs.tab', function (e) {
            window.location.hash = e.target.hash;
        })
    });


    // datepicker
    $('.helper-datepicker').datepicker({

        todayBtn: "linked",
        clearBtn: true,
        autoclose: true,
        todayHighlight: true,
        orientation: "top",
        templates: {
            leftArrow: '<i class="fa fa-angle-left"></i>',
            rightArrow: '<i class="fa fa-angle-right"></i>'
        }
    });

    // select2
    $(".form-control select2").select2();

    $(".js-example-form-control select2").select2({
        dropdownParent: $(".form-container")
    });

    $(".select2-multiple").select2();


    $("#owl-demo").owlCarousel({
        navigation: true,
        slideSpeed: 300,
        paginationSpeed: 400,
        singleItem: true,
        autoPlay: true

    });
    $("#checkAll").click(function () {
        $('input:checkbox').not(this).prop('checked', this.checked);
    });


    $(".remove").on('click', function () {
        if ($("#subjectTable tr").length === 3) {
            $(".remove").hide();
        } else if ($("#subjectTable tr").length - 1 == 3) {
            $(".remove").hide();
            removeRow($(this));
        } else {
            removeRow($(this));
        }
    });

    $(".add").on('click', function () {
        addRow();

        $(this).closest("tr").appendTo("#subjectTable");

        if ($("#subjectTable tr").length === 3) {
            $(".remove").hide();
        } else {
            $(".remove").show();
        }
    });
    //    });

    function removeRow(button) {
        button.closest("tr").remove();
    }

    function addRow() {
        var clonedDiv = row.clone(true, true);
        clonedDiv.appendTo("#subjectTable");
    }








});