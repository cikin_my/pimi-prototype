var i = 0;
var j = 0;
var k = 0;
$(document).ready(function () {
    // table 1
    // PLO01
    $("#add-td1").click(function (e) {
        e.preventDefault();
        $("#pl01-1").show();
    });
    $("#sub-td1").click(function (e) {
        e.preventDefault();
        $("#pl01-1").hide();
    });
    $("#add-td2").click(function (e) {
        e.preventDefault();
        $("#pl01-2").show();
    });
    $("#sub-td2").click(function (e) {
        e.preventDefault();
        $("#pl01-2").hide();
    });
    $("#add-td3").click(function (e) {
        e.preventDefault();
        $("#pl01-3").show();
    });
    $("#sub-td3").click(function (e) {
        e.preventDefault();
        $("#pl01-3").hide();
    });
    $("#add-td4").click(function (e) {
        e.preventDefault();
        $("#pl01-4").show();
    });
    $("#sub-td4").click(function (e) {
        e.preventDefault();
        $("#pl01-4").hide();
    });
    $("#add-td5").click(function (e) {
        e.preventDefault();
        $("#pl01-5").show();
    });
    $("#sub-td5").click(function (e) {
        e.preventDefault();
        $("#pl01-5").hide();
    });

    // PL02
    $("#2add-td1").click(function (e) {
        // e.preventDefault();
        $("#pl02-1").show();
    });
    $("#2sub-td1").click(function (e) {
        e.preventDefault();
        $("#pl02-1").hide();
    });
    $("#2add-td2").click(function (e) {
        e.preventDefault();
        $("#pl02-2").show();
    });
    $("#2sub-td2").click(function (e) {
        e.preventDefault();
        $("#pl02-2").hide();
    });
    $("#2add-td3").click(function (e) {
        e.preventDefault();
        $("#pl02-3").show();
    });
    $("#2sub-td3").click(function (e) {
        e.preventDefault();
        $("#pl02-3").hide();
    });
    $("#2add-td4").click(function (e) {
        e.preventDefault();
        $("#pl02-4").show();
    });
    $("#2sub-td4").click(function (e) {
        e.preventDefault();
        $("#pl02-4").hide();
    });
    $("#2add-td5").click(function (e) {
        e.preventDefault();
        $("#pl02-5").show();
    });
    $("#2sub-td5").click(function (e) {
        e.preventDefault();
        $("#pl02-5").hide();
    });

    // PLO3
    $("#3add-td1").click(function (e) {
        e.preventDefault();
        $("#pl03-1").show();
    });
    $("#3sub-td1").click(function (e) {
        e.preventDefault();
        $("#pl03-1").hide();
    });
    $("#3add-td2").click(function (e) {
        e.preventDefault();
        $("#pl03-2").show();
    });
    $("#3sub-td2").click(function (e) {
        e.preventDefault();
        $("#pl03-2").hide();
    });
    $("#3add-td3").click(function (e) {
        e.preventDefault();
        $("#pl03-3").show();
    });
    $("#3sub-td3").click(function (e) {
        e.preventDefault();
        $("#pl03-3").hide();
    });
    $("#3add-td4").click(function (e) {
        e.preventDefault();
        $("#pl03-4").show();
    });
    $("#3sub-td4").click(function (e) {
        e.preventDefault();
        $("#pl03-4").hide();
    });
    $("#3add-td5").click(function (e) {
        e.preventDefault();
        $("#pl03-5").show();
    });
    $("#3sub-td5").click(function (e) {
        e.preventDefault();
        $("#pl03-5").hide();
    });

    // PLO04
    $("#4add-td1").click(function (e) {
        e.preventDefault();
        $("#pl04-1").show();
    });
    $("#4sub-td1").click(function (e) {
        e.preventDefault();
        $("#pl04-1").hide();
    });
    $("#4add-td2").click(function (e) {
        e.preventDefault();
        $("#pl04-2").show();
    });
    $("#4sub-td2").click(function (e) {
        e.preventDefault();
        $("#pl04-2").hide();
    });
    $("#4add-td3").click(function (e) {
        e.preventDefault();
        $("#pl04-3").show();
    });
    $("#4sub-td3").click(function (e) {
        e.preventDefault();
        $("#pl04-3").hide();
    });
    $("#4add-td4").click(function (e) {
        e.preventDefault();
        $("#pl04-4").show();
    });
    $("#4sub-td4").click(function (e) {
        e.preventDefault();
        $("#pl04-4").hide();
    });
    $("#4add-td5").click(function (e) {
        e.preventDefault();
        $("#pl04-5").show();
    });
    $("#4sub-td5").click(function (e) {
        e.preventDefault();
        $("#pl04-5").hide();
    });

    // PL05
    $("#5add-td1").click(function (e) {
        e.preventDefault();
        $("#pl05-1").show();
    });
    $("#5sub-td1").click(function (e) {
        e.preventDefault();
        $("#pl05-1").hide();
    });
    $("#5add-td2").click(function (e) {
        e.preventDefault();
        $("#pl05-2").show();
    });
    $("#5sub-td2").click(function (e) {
        e.preventDefault();
        $("#pl05-2").hide();
    });
    $("#5add-td3").click(function (e) {
        e.preventDefault();
        $("#pl05-3").show();
    });
    $("#5sub-td3").click(function (e) {
        e.preventDefault();
        $("#pl05-3").hide();
    });
    $("#5add-td4").click(function (e) {
        e.preventDefault();
        $("#pl05-4").show();
    });
    $("#5sub-td4").click(function (e) {
        e.preventDefault();
        $("#pl05-4").hide();
    });
    $("#5add-td5").click(function (e) {
        e.preventDefault();
        $("#pl05-5").show();
    });
    $("#5sub-td5").click(function (e) {
        e.preventDefault();
        $("#pl05-5").hide();
    });

    // table2
    //==== mapping table 2 mlo-plo 
    $(".btn-dropdown-cell").on('click', function () {
        let row = this.id;
        var btn = document.getElementById(row);

        $(".plo-item").on('click', function () {
            let select = this.id;

            btn.innerHTML = 'PLO 1 <i class="fa fa-caret-down pull-right" aria-hidden="true"></i>';
            $("#" + row + ".btn-outline-purple").addClass('active');

            // alert('row:' + row + '--selected item:' + select);
            // if (row == 'row1') {
            //     btn.innerHTML = 'PLO 1 <i class="fa fa-caret-down pull-right" aria-hidden="true"></i>';
            //     $("#" + row + ".btn-outline-purple").addClass('active');
            // }
            // if (row == 'row2') {
            //     btn.innerHTML = 'PLO 2 <i class="fa fa-caret-down pull-right" aria-hidden="true"></i>';
            //     $("#" + row + ".btn-outline-purple").addClass('active');
            // }
            // if (row == 'row3') {
            //     btn.innerHTML = 'PLO 3 <i class="fa fa-caret-down pull-right" aria-hidden="true"></i>';
            //     $("#" + row + ".btn-outline-purple").addClass('active');
            // }
            // if (row == 'row4') {
            //     btn.innerHTML = 'PLO 4 <i class="fa fa-caret-down pull-right" aria-hidden="true"></i>';
            //     $("#" + row + ".btn-outline-purple").addClass('active');
            // }

        });




    });

    $('tr .addbtn').click(function () {

        var buttonId = this.id,
            tableId = $(this).closest('.table').attr('id');
        console.log(tableId);
        // Save to modal Data
        $('#addtgc').data('tableId', tableId);
    });


    $('.add-row').click(function () {
        var currentRowID = $('#addtgc').data('tableId');
        console.log('the table id is : ' + currentRowID + $('#addtgc').data('tableId'));

        if (currentRowID == 'table1') {
            i++;
            newrow = '<tr id="tgc-' + i + '"><td><buton class="btn btn-success btn-space btn-xs" width="10%"> TGC-' + i +
                '</button></td><td><a class="btn btn-light btn-xs btn-block" data-toggle="modal" data-target="#desc">Description</a></td>' +
                '<td><button id="alink" class="btnselect btn btn-light btn-block btn-xs"data-toggle="modal" data-target="#selection" >select</button></td></tr>';
            $('#table1 tr:last').before(newrow);

        }
        if (currentRowID == "table2") {
            j++;
            newrow = '<tr id="tgc-' + j + '"><td><buton class="btn btn-success btn-space btn-xs" width="10%"> TGC-' + j +
                '</button></td><td><a class="btn btn-light btn-xs btn-block" data-toggle="modal" data-target="#desc">Description</a></td>' +
                '<td><button class="btnselect btn btn-light btn-block btn-xs" data-toggle="modal" data-target="#selection">select</button></td></tr>';
            $('#table2 tr:last').before(newrow);
        }
        if (currentRowID == "table3") {
            k++;
            newrow = '<tr id="tgc-' + k + '"><td><buton class="btn btn-success btn-space btn-xs" width="10%"> TGC-' + k +
                '</button></td><td><a class="btn btn-light btn-xs btn-block" data-toggle="modal" data-target="#desc">Description</a></td>' +
                '<td><button class="btnselect btn btn-light btn-block btn-xs" data-toggle="modal" data-target="#selection">select</button></td></tr>';
            $('#table3 tr:last').before(newrow);
        }

    });



    $('#addselection').click(function () {
        var myID = $('#selection').data('tableId');
        //   console.log('the table id is : ' + $('#addtgc').data('parentID'));
        //  alert($('#selection').data('myID'))
    });


    //  ----------
    $('.addplo').click(function () {

        var buttonId = this.id;
        var tdid = $(this).closest('td').attr('id');
        console.log('button id: ' + buttonId);
        console.log('td id:' + tdid);
        $('#dropdown').data('tdID', tdid);
    });

    $('.ploitem').click(function () {
        var currentTdID = $('#dropdown').data('tdID');
        console.log('the td id is : ' + $('#dropdown').data('tdID'));

        switch (currentTdID) {
            case 'plo1':
                $('#plotext1').show();
                $('#dropplo1').hide();
                break;
            case 'plo2':
                $('#plotext2').show();
                $('#dropplo2').hide();
                break;
            case 'plo3':
                $('#plotext3').show();
                $('#dropplo3').hide();
                break;
        }
    });
    // ==============================================
    // mapping table no 3
    // ==============================================

    $('.ploaddbtn').click(function () {

        let tdid = $(this).closest('td').attr('id');
        console.log('tdId:' + tdid);
        // Save to modal Data
        $('#addplo').data('tdId', tdid);
    });
    $('#okplo').click(function () {
        // retrieve current td ID 
        let currentTdID = $('#addplo').data('tdId');
        // get the selected options 
        let selected = $('#ploNo option:selected');
        let SelectedPlo = selected.html();
        // change & display button PLO#       
        $('#' + currentTdID).find('.ploaddbtn').hide();
        $('#' + currentTdID).html('<div class="selectedtd"><button class="btn btn-block btn-xs btn-purple" data-toggle="modal" data-target="#addplo">' + SelectedPlo + '</button></div>');

    });


    // ==============================================
    // table from step 10 module info form : /moduleinfo/edit
    // ==============================================
    $('.add1').click(function () {


        let tdid = $(this).closest('th').attr('id');


        //  console.log('tdId:' + tdid);
        // Save to modal Data
        $('#additem').data('tdId', tdid);
    });

    $('#okadditem').click(function () {
        // retrieve current td ID 
        let currentTdID = $('#additem').data('tdId');
        console.log(currentTdID);
        // get the selected options 
        let selected = $('#ploNo option:selected');
        let SelectedPlo = selected.html();
        // add to table th 
        $('#' + currentTdID).html('<table class="week-item">' +
            '<tr><td><small>Guided Learning F2F (Lecture)</small></td></tr>' +
            '<tr><td><small>Guided Learning F2F (Tutorial)</small></td></tr>' +
            '<tr><td><small>Guided Learning F2F (Practical)</small></td></tr>' +
            '</table>');

    });

    //   =====MLO add====
    $('.addmlo').click(function () {

        var buttonId = this.id;
        var tdid = $(this).closest('td').attr('id');
        console.log('button id: ' + buttonId);
        console.log('td id:' + tdid);
        $('#dropdownMLO').data('tdID', tdid);
    });

    $('.mloitem').click(function () {
        var currentTdID = $('#dropdownMLO').data('tdID');
        console.log('the td id is : ' + $('#dropdownMLO').data('tdID'));

        switch (currentTdID) {
            case 'mlo1':
                $('#mlotext1').hide();
                $('#dropmlo1').show();
                break;
            case 'mlo2':
                $('#mlotext2').show();
                $('#dropmlo2').hide();
                break;
            case 'mlo3':
                $('#mlotext3').show();
                $('#dropmlo3').hide();
                break;
            case 'mlo4':
                $('#mlotext4').show();
                $('#dropmlo4').hide();
                break;
            case 'mlo5':
                $('#mlotext5').show();
                $('#dropmlo5').hide();
                break;
        }
           
          
    });

     $('#savetable10').click(function () {
        
         // add to table th 
          $('.addtotal').addClass("is-active");
         $('.addtotal').html('<span> 8 </span>');
          $('.totalall').html('40');

     });



}); //end of doc ready 