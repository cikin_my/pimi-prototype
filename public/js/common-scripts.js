/*---LEFT BAR ACCORDION----*/
$(function() {
    $('#nav-accordion').dcAccordion({
        eventType: 'click',
        autoClose: true,
        saveState: true,
        disableLink: true,
        speed: 'slow',
        showCount: false,
        autoExpand: true,
//        cookie: 'dcjq-accordion-1',
        classExpand: 'dcjq-current-parent'
    });
    $('.dpDays').datepicker({
        format: "dd-mm-yyyy",
        // viewMode: "months",
        // minViewMode: "months",
        autoclose: true,
        orientation: "bottom",
        templates: {
            leftArrow: '<i class="fa fa-angle-left"></i>',
            rightArrow: '<i class="fa fa-angle-right"></i>'
        }
    });
    $('.dpMonths').datepicker({
        format: "MM",
        viewMode: "months",
        minViewMode: "months",
        autoclose: true,
        orientation: "bottom",
        templates: {
            leftArrow: '<i class="fa fa-angle-left"></i>',
            rightArrow: '<i class="fa fa-angle-right"></i>'
        }
    });
    $('.dpYears').datepicker({
        startDate: new Date(),
        format: "yyyy",
        viewMode: "years",
        minViewMode: "years",
        autoclose: true,
        orientation: "bottom",
        templates: {
            leftArrow: '<i class="fa fa-angle-left"></i>',
            rightArrow: '<i class="fa fa-angle-right"></i>'
        }
    });

});


// for mobile check
function mobilecheck() {
    var check = false;
    (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check = true})(navigator.userAgent||navigator.vendor||window.opera);
    return check;
};

var Script = function () {

//    sidebar dropdown menu auto scrolling

    jQuery('#sidebar .sub-menu > a').on('click', function () {
        var o = ($(this).offset());
        diff = 250 - o.top;
        if(diff>0)
            $("#sidebar").scrollTo("-="+Math.abs(diff),500);
        else
            $("#sidebar").scrollTo("+="+Math.abs(diff),500);
    });

//    sidebar toggle

    $('.sidebar-toggle-box').on('click', function () {
        $(document.body).toggleClass('is-sidebar-nav-open');

        var owl = $("#owl-demo").data("owlCarousel");
        owl.reinit();
    });

// custom scrollbar
    $("#sidebar").niceScroll({styler:"fb",cursorcolor:"#e8403f", cursorwidth: '3', cursorborderradius: '10px', background: '#404040', spacebarenabled:false, cursorborder: '', scrollspeed: 60});

    //$("html").niceScroll({styler:"fb",cursorcolor:"#e8403f", cursorwidth: '6', cursorborderradius: '10px', background: '#404040', spacebarenabled:false,  cursorborder: '', zindex: '1000', scrollspeed: 100, mousescrollstep: 60});

    $(".table-responsive").niceScroll({styler:"fb",cursorcolor:"#e8403f", cursorwidth: '6', cursorborderradius: '10px', background: '#404040', spacebarenabled:false,  cursorborder: '', zindex: '1000', horizrailenabled: true });



// widget tools

    jQuery('.card .tools .fa-chevron-down').on('click', function () {
        var el = jQuery(this).parents(".card").children(".card-body");
        if (jQuery(this).hasClass("fa-chevron-down")) {
            jQuery(this).removeClass("fa-chevron-down").addClass("fa-chevron-up");
            el.slideUp(200);
        } else {
            jQuery(this).removeClass("fa-chevron-up").addClass("fa-chevron-down");
            el.slideDown(200);
        }
    });


    jQuery('.card .tools .fa-times').on('click', function () {
        jQuery(this).parents(".card").parent().remove();
    });


//    tool tips

    $('.tooltips').tooltip();

//    popovers

    $('.popovers').popover();


// custom bar chart

    if ($(".custom-bar-chart")) {
        $(".bar").each(function () {
            var i = $(this).find(".value").html();
            $(this).find(".value").html("");
            $(this).find(".value").animate({
                height: i
            }, 2000)
        })
    }

}();


// right slidebar

$(function(){
    $.slidebars();
});


//add new programme
$('.award').hide()
$('input[name="award"]').change(function(){
    let value = $(this).val();
    if(value=="award"){
        $('.award').show()
    }else{
        $('.award').hide()
    }
})

$('.other-type').hide()
$('#award_type').change(function(){
    let value = $(this).val();
    if(value=="other"){
        $('.other-type').show()
    }else{
        $('.other-type').hide()
    }
})

$('.awarding-body').hide()
$('input[name="awarding_body"]').change(function(){
    let value = $(this).val();
    console.log(value)
    if(value=="other"){
        $('.awarding-body').show()
    }else{
        $('.awarding-body').hide()
    }
})


$('.other-programme-type').hide()
$('#programme_type').change(function(){
    let value = $(this).val();
    if(value=="other"){
        $('.other-programme-type').show()
    }else{
        $('.other-programme-type').hide()
    }
})

$('.accreditation').hide()
$('input[name="accreditation"]').change(function(){
    let value = $(this).val();
    console.log(value)
    if(value=="other"){
        $('.accreditation').show()
    }else{
        $('.accreditation').hide()
    }
})

$('.other-learning-mode').hide()
$('#learning_mode').change(function(){
    let value = $(this).val();
    if(value=="others"){
        $('.other-learning-mode').show()
    }else{
        $('.other-learning-mode').hide()
    }
})

//dynamic table
var $TABLE = $('#table');
var $TABLE2 = $('#table2');
var $TABLE3 = $('#table3');
var $TABLE4 = $('#table4');
var $TABLE5 = $('#table5');
var $TABLE6 = $('#table6');
var $TABLE7 = $('#table7');

var $BTN = $('#export-btn');
var $EXPORT = $('#export');



$('.table-add').click(function () {
    var $clone = $TABLE.find('tr.hide').clone(true).removeClass('hide table-line');
    $TABLE.find('table').append($clone);
});

$('.table-add2').click(function () {
    var $clone = $TABLE2.find('tr.hide').clone(true).removeClass('hide table-line');
    $TABLE2.find('table').append($clone);
});
$('.table-add3').click(function () {
    var $clone = $TABLE3.find('tr.hide').clone(true).removeClass('hide table-line');
    $TABLE3.find('table').append($clone);
});
$('.table-add4').click(function () {
    var $clone = $TABLE4.find('tr.hide').clone(true).removeClass('hide table-line');
    $TABLE4.find('table').append($clone);
});
$('.table-add5').click(function () {
    var $clone = $TABLE5.find('tr.hide').clone(true).removeClass('hide table-line');
    $TABLE5.find('table').append($clone);
});
$('.table-add6').click(function () {
    var $clone = $TABLE6.find('tr.hide').clone(true).removeClass('hide table-line');
    $TABLE6.find('table').append($clone);
});
$('.table-add7').click(function () {
    var $clone = $TABLE7.find('tr.hide').clone(true).removeClass('hide table-line');
    $TABLE7.find('table').append($clone);
});


$('.table-remove').click(function () {
    $(this).parents('tr').detach();
});

$('.table-up').click(function () {
    var $row = $(this).parents('tr');
    if ($row.index() === 1) return; // Don't go above the header
    $row.prev().before($row.get(0));
});

$('.table-down').click(function () {
    var $row = $(this).parents('tr');
    $row.next().after($row.get(0));
});

// A few jQuery helpers for exporting only
jQuery.fn.pop = [].pop;
jQuery.fn.shift = [].shift;

$BTN.click(function () {
    var $rows = $TABLE.find('tr:not(:hidden)');
    var headers = [];
    var data = [];

// Get the headers (add special header logic here)
    $($rows.shift()).find('th:not(:empty)').each(function () {
        headers.push($(this).text().toLowerCase());
    });

// Turn all existing rows into a loopable array
    $rows.each(function () {
        var $td = $(this).find('td');
        var h = {};

// Use the headers from earlier to name our hash keys
        headers.forEach(function (header, i) {
            h[header] = $td.eq(i).text();
        });

        data.push(h);
    });

// Output the result
    $EXPORT.text(JSON.stringify(data));
});


// $('.add-more').click(function(){
//     console.log('cliked')
//     var clone = $('#approval').clone('.form-row');
//     $('#approval').append(clone)
// })
$('.add-more').on('click',function(){
    console.log('cliked')
    var clone = $('#approval').clone('.form-row');
    $('#approval-add').append(clone)
})
