<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Route::get('/', function () {
// return view('dashboard.staff');
// });
Route::get('/', function () {
return view('landing.index');
});

//--------------- dashboard
Route::get('/dashboard', function () {$userType = 'staff'; return view('dashboard.staff', ['usertype' =>$userType]);})->name('dashboard');
//--------------- user & role
Route::get('/role', function () { ;return view('user.role');})->name('role');
Route::get('/user', function () { ;return view('user.user');})->name('user');
Route::get('/pd', function () { ;return view('user.pd');})->name('pd');
Route::get('/ml', function () { ;return view('user.ml');})->name('ml');
Route::get('/hos', function () { ;return view('user.hos');})->name('hos');
Route::get('/ed', function () { ;return view('user.ed');})->name('ed');
//--------------- system setup
Route::get('/tgc', function () { ;return view('setup.tgc');})->name('tgc');
Route::get('/mqf', function () { ;return view('setup.mqf');})->name('mqf');
Route::get('/mapping', function () { ;return view('setup.mapping');})->name('mapping');
Route::get('/nec', function () { ;return view('setup.nec');})->name('nec');
Route::get('/discipline', function () { ;return view('setup.discipline');})->name('discipline');
Route::get('/viewtgc', function () { ;return view('setup.tgc-table');})->name('tgc-table');
Route::get('/viewmqf', function () { ;return view('setup.mqf-table');})->name('mqf-table');
Route::get('/viewmapping', function () { ;return view('setup.mapping-table');})->name('mapping-table');
//--------------- programme mgt
// Route::get('/add', function () { ;return view('module.add');})->name('add');
// Route::get('/list', function () { ;return view('module.list');})->name('list');

Route::get('/new-programme',function(){$userType='staff';return view('programme.new',['usertype'=>$userType]);})->name('programme-new');
Route::get('/info', function () { ;return view('programme.programme-info');})->name('programme-info');
Route::get('/structure', function () { ;return view('programme.structure');})->name('structure');
Route::get('/view-programme', function () { ;return view('programme.view');})->name('programme-view');

//--------------- programme -> module info
// Route::get('/infomodule', function () { ;return view('programme.module-info');})->name('module-info');
Route::get('/moduleinfo', function () { ;return view('programme.module-info.list');})->name('module-info');
Route::get('/moduleinfo/edit', function () { ;return view('programme.module-info.edit');})->name('module-info.edit');
Route::get('/add-rubrics',function(){return view('programme.module-info.rubric');})->name('add-rubrics');
//--------------- view Programme
Route::get('/search-programme', function () { ;return view('view-programme.programme');})->name('search-programme');
Route::get('/search-module', function () { ;return view('view-programme.module');})->name('search-module');

//--------------- Approval & Review
Route::get('/review', function () { ;return view('review-prog.review');})->name('review');
Route::get('/reviewpi', function () { ;return view('review-prog.reviewpi');})->name('review-pi');

//--------------- Setting
Route::get('/setting', function () { ;return view('setting.general');})->name('setting');

//--------------- tracking
Route::get('/tracking', function () { ;return view('track.tracking');})->name('tracking');
Route::get('/changes', function () { ;return view('track.changes');})->name('changes');


//--------------- Extra
Route::get('/mapping1', function () { ;return view('mapping1');})->name('mapping1');
Route::get('/mapping2', function () { ;return view('mapping2');})->name('mapping2');
Route::get('/mapping3', function () { ;return view('mapping3');})->name('mapping3');
Route::get('/step10', function () { ;return view('step10');})->name('step10');
Route::get('/test', function () { ;return view('setup.test');})->name('test');



