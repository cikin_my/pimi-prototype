@extends('layouts.main.master')
@section('content')


<header class="card-header">
    <i class="fa fa-th mr-3"></i> PLO -Module Mapping
    <span id="spnText"></span>
</header>
<div class="bg-light plo-mapping">
    <table class="table table-bordered table-responsive table-small-text text-center tgc bg-2"
        style="padding:5px !important; table-layout: fixed; width:100%;border-collapse: collapse;" id="tablePLOmap">
        <tr class="thead">
            <th rowspan="3">No</th>
            <th rowspan="3">Stream</th>
            <th rowspan="3">
                <div class="longtext">Module code &amp; Name</div>
            </th>
            <th rowspan="3">Classification</th>
            <th rowspan="3">Creadit Value</th>
            <th rowspan="3">
                <div class="longtext">Micro-Credential</div>
            </th>
            <th rowspan="3">Year Level</th>
            <th rowspan="3">
                <div class="longtext">Set KPI/Target(Optional)</div>
            </th>
            <th colspan="10">Programme Learning Outcomes(PLOs)</th>
            <th rowspan="3">Count by Module</th>
        </tr>
        <tr class="plo-numbers">
            <td>1</td>
            <td>2</td>
            <td>3</td>
            <td>4</td>
            <td>5</td>
            <td>6</td>
            <td>7</td>
            <td>8</td>
            <td>9</td>
            <td>10</td>
        </tr>
        <tr class="plo-tgc">
            <td>TGC1</td>
            <td>TGC2</td>
            <td>TGC3</td>
            <td>TGC4</td>
            <td>TGC5</td>
            <td>TGC6</td>
            <td>TGC7</td>
            <td>TGC8</td>
            <td>TGC9</td>
            <td>TGC10</td>
        </tr>
        <tr>
            <td>1</td>
            <td></td>
            <td class="text-left">ABC123-Arch Design 1</td>
            <td></td>
            <td>Core</td>
            <td></td>
            <td>1</td>
            <td></td>
            <td id="row1_tgc1">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row1_tgc2">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row1_tgc3">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row1_tgc4">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row1_tgc5">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row1_tgc6">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row1_tgc7">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row1_tgc8">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row1_tgc9">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row1_tgc10">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td class="count-by-module">2</td>
        </tr>
        <tr>
            <td>2</td>
            <td></td>
            <td class="text-left">ABC123-History</td>
            <td></td>
            <td>Core</td>
            <td></td>
            <td>1</td>
            <td></td>
            <td id="row2_tgc1">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row2_tgc2">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row2_tgc3">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row2_tgc4">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row2_tgc5">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row2_tgc6">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row2_tgc7">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row2_tgc8">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row2_tgc9">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row2_tgc10">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td class="count-by-module">3</td>
        </tr>
        <tr>
            <td>3</td>
            <td></td>
            <td class="text-left">ABC123-Projects</td>
            <td></td>
            <td>Core</td>
            <td></td>
            <td>1</td>
            <td></td>
            <td id="row3_tgc1">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row3_tgc2">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row3_tgc3">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row3_tgc4">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row3_tgc5">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row3_tgc6">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row3_tgc7">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row3_tgc8">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row3_tgc9">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row3_tgc10">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td class="count-by-module">2</td>
        </tr>
        <tr>
            <td>4</td>
            <td></td>
            <td class="text-left">ABC123-Sys Analysis</td>
            <td></td>
            <td>Core</td>
            <td></td>
            <td>1</td>
            <td></td>
            <td id="row4_tgc1">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row4_tgc2">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row4_tgc3">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row4_tgc4">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row4_tgc5">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row4_tgc6">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row4_tgc7">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row4_tgc8">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row4_tgc9">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row4_tgc10">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td class="count-by-module">2</td>
        </tr>
        <tr>
            <td>5</td>
            <td></td>
            <td class="text-left">ABC123-Software Engineering</td>
            <td></td>
            <td>Core</td>
            <td></td>
            <td>1</td>
            <td></td>
            <td id="row5_tgc1">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row5_tgc2">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row5_tgc3">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row5_tgc4">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row5_tgc5">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row5_tgc6">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row5_tgc7">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row5_tgc8">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row5_tgc9">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row5_tgc10">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td class="count-by-module">2</td>
        </tr>
        <tr>
            <td>6</td>
            <td></td>
            <td class="text-left">ABC123-Computer Apps</td>
            <td></td>
            <td>Core</td>
            <td></td>
            <td>1</td>
            <td></td>
            <td id="row6_tgc1">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row6_tgc2">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row6_tgc3">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row6_tgc4">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row6_tgc5">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row6_tgc6">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row6_tgc7">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row6_tgc8">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row6_tgc9">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row6_tgc10">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td class="count-by-module">2</td>
        </tr>
        <tr>
            <td>7</td>
            <td></td>
            <td class="text-left">ABC123-PC Design 1</td>
            <td></td>
            <td>Core</td>
            <td></td>
            <td>2</td>
            <td></td>
            <td id="row7_tgc1">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row7_tgc2">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row7_tgc3">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row7_tgc4">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row7_tgc5">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row7_tgc6">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row7_tgc7">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row7_tgc8">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row7_tgc9">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row7_tgc10">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td class="count-by-module">2</td>
        </tr>
        <tr>
            <td>8</td>
            <td></td>
            <td class="text-left">ABC123-Module Name1</td>
            <td></td>
            <td>MPU</td>
            <td></td>
            <td>2</td>
            <td></td>
            <td id="row8_tgc1">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row8_tgc2">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row8_tgc3">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row8_tgc4">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row8_tgc5">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row8_tgc6">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row8_tgc7">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row8_tgc8">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row8_tgc9">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row8_tgc10">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td class="count-by-module">2</td>
        </tr>
        <tr>
            <td>9</td>
            <td></td>
            <td class="text-left">ABC123-Module Name2</td>
            <td></td>
            <td>MPU</td>
            <td></td>
            <td>2</td>
            <td></td>
            <td id="row9_tgc1">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row9_tgc2">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row9_tgc3">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row9_tgc4">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row9_tgc5">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row9_tgc6">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row9_tgc7">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row9_tgc8">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row9_tgc9">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row9_tgc10">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td class="count-by-module">4</td>
        </tr>
        <tr>
            <td>10</td>
            <td></td>
            <td class="text-left">ABC123-Module Name3</td>
            <td></td>
            <td>UCM</td>
            <td></td>
            <td>2</td>
            <td></td>
            <td id="row9_tgc1">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row9_tgc2">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row9_tgc3">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row9_tgc4">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row9_tgc5">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row9_tgc6">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row9_tgc7">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row9_tgc8">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row9_tgc9">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row9_tgc10">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td class="count-by-module">4</td>
        </tr>
        <tr>
            <td>11</td>
            <td></td>
            <td class="text-left">ABC123-Module Name4</td>
            <td></td>
            <td>MPU</td>
            <td></td>
            <td>2</td>
            <td></td>
           <td id="row10_tgc1">
            <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                <i class="fa fa-plus text-success"></i>
            </button>
        </td>
        <td id="row10_tgc2">
            <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                <i class="fa fa-plus text-success"></i>
            </button>
        </td>
        <td id="row10_tgc3">
            <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                <i class="fa fa-plus text-success"></i>
            </button>
        </td>
        <td id="row10_tgc4">
            <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                <i class="fa fa-plus text-success"></i>
            </button>
        </td>
        <td id="row10_tgc5">
            <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                <i class="fa fa-plus text-success"></i>
            </button>
        </td>
        <td id="row10_tgc6">
            <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                <i class="fa fa-plus text-success"></i>
            </button>
        </td>
        <td id="row10_tgc7">
            <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                <i class="fa fa-plus text-success"></i>
            </button>
        </td>
        <td id="row10_tgc8">
            <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                <i class="fa fa-plus text-success"></i>
            </button>
        </td>
        <td id="row10_tgc9">
            <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                <i class="fa fa-plus text-success"></i>
            </button>
        </td>
        <td id="row10_tgc10">
            <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                <i class="fa fa-plus text-success"></i>
            </button>
        </td>
            <td class="count-by-module">4</td>
        </tr>
        <tr>
            <td>12</td>
            <td></td>
            <td class="text-left">ABC123-Module Name5</td>
            <td></td>
            <td>MPU</td>
            <td></td>
            <td>1</td>
            <td></td>
            <td id="row11_tgc1">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row11_tgc2">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row11_tgc3">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row11_tgc4">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row11_tgc5">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row11_tgc6">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row11_tgc7">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row11_tgc8">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row11_tgc9">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row11_tgc10">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td class="count-by-module">4</td>
        </tr>
        <tr>
            <td>13</td>
            <td></td>
            <td class="text-left">ABC123-Module Name6</td>
            <td></td>
            <td>MPU</td>
            <td></td>
            <td>1</td>
            <td></td>
          <td id="row12_tgc1">
            <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                <i class="fa fa-plus text-success"></i>
            </button>
        </td>
        <td id="row12_tgc2">
            <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                <i class="fa fa-plus text-success"></i>
            </button>
        </td>
        <td id="row12_tgc3">
            <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                <i class="fa fa-plus text-success"></i>
            </button>
        </td>
        <td id="row12_tgc4">
            <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                <i class="fa fa-plus text-success"></i>
            </button>
        </td>
        <td id="row12_tgc5">
            <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                <i class="fa fa-plus text-success"></i>
            </button>
        </td>
        <td id="row12_tgc6">
            <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                <i class="fa fa-plus text-success"></i>
            </button>
        </td>
        <td id="row12_tgc7">
            <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                <i class="fa fa-plus text-success"></i>
            </button>
        </td>
        <td id="row12_tgc8">
            <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                <i class="fa fa-plus text-success"></i>
            </button>
        </td>
        <td id="row12_tgc9">
            <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                <i class="fa fa-plus text-success"></i>
            </button>
        </td>
        <td id="row12_tgc10">
            <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                <i class="fa fa-plus text-success"></i>
            </button>
        </td>
            <td class="count-by-module">4</td>
        </tr>
        <tr>
            <td>14</td>
            <td></td>
            <td class="text-left">ABC123-Module Name7</td>
            <td></td>
            <td>MPU</td>
            <td></td>
            <td>1</td>
            <td></td>
           <td id="row13_tgc1">
            <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                <i class="fa fa-plus text-success"></i>
            </button>
        </td>
        <td id="row13_tgc2">
            <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                <i class="fa fa-plus text-success"></i>
            </button>
        </td>
        <td id="row13_tgc3">
            <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                <i class="fa fa-plus text-success"></i>
            </button>
        </td>
        <td id="row13_tgc4">
            <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                <i class="fa fa-plus text-success"></i>
            </button>
        </td>
        <td id="row13_tgc5">
            <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                <i class="fa fa-plus text-success"></i>
            </button>
        </td>
        <td id="row13_tgc6">
            <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                <i class="fa fa-plus text-success"></i>
            </button>
        </td>
        <td id="row13_tgc7">
            <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                <i class="fa fa-plus text-success"></i>
            </button>
        </td>
        <td id="row13_tgc8">
            <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                <i class="fa fa-plus text-success"></i>
            </button>
        </td>
        <td id="row13_tgc9">
            <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                <i class="fa fa-plus text-success"></i>
            </button>
        </td>
        <td id="row13_tgc10">
            <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                <i class="fa fa-plus text-success"></i>
            </button>
        </td>
            <td class="count-by-module">5</td>
        </tr>
        <tr>
            <td>15</td>
            <td></td>
            <td class="text-left">ABC123-Module Name8</td>
            <td></td>
            <td>MPU</td>
            <td></td>
            <td>1</td>
            <td></td>
           <td id="row14_tgc1">
            <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                <i class="fa fa-plus text-success"></i>
            </button>
        </td>
        <td id="row14_tgc2">
            <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                <i class="fa fa-plus text-success"></i>
            </button>
        </td>
        <td id="row14_tgc3">
            <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                <i class="fa fa-plus text-success"></i>
            </button>
        </td>
        <td id="row14_tgc4">
            <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                <i class="fa fa-plus text-success"></i>
            </button>
        </td>
        <td id="row14_tgc5">
            <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                <i class="fa fa-plus text-success"></i>
            </button>
        </td>
        <td id="row14_tgc6">
            <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                <i class="fa fa-plus text-success"></i>
            </button>
        </td>
        <td id="row14_tgc7">
            <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                <i class="fa fa-plus text-success"></i>
            </button>
        </td>
        <td id="row14_tgc8">
            <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                <i class="fa fa-plus text-success"></i>
            </button>
        </td>
        <td id="row14_tgc9">
            <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                <i class="fa fa-plus text-success"></i>
            </button>
        </td>
        <td id="row14_tgc10">
            <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                <i class="fa fa-plus text-success"></i>
            </button>
        </td>
            <td class="count-by-module">5</td>
        </tr>
        <tr>
            <td>16</td>
            <td></td>
            <td class="text-left">ABC123-Module Name9</td>
            <td></td>
            <td>MPU</td>
            <td></td>
            <td>1</td>
            <td></td>
           <td id="row15_tgc1">
            <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                <i class="fa fa-plus text-success"></i>
            </button>
        </td>
        <td id="row15_tgc2">
            <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                <i class="fa fa-plus text-success"></i>
            </button>
        </td>
        <td id="row15_tgc3">
            <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                <i class="fa fa-plus text-success"></i>
            </button>
        </td>
        <td id="row15_tgc4">
            <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                <i class="fa fa-plus text-success"></i>
            </button>
        </td>
        <td id="row15_tgc5">
            <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                <i class="fa fa-plus text-success"></i>
            </button>
        </td>
        <td id="row15_tgc6">
            <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                <i class="fa fa-plus text-success"></i>
            </button>
        </td>
        <td id="row15_tgc7">
            <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                <i class="fa fa-plus text-success"></i>
            </button>
        </td>
        <td id="row15_tgc8">
            <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                <i class="fa fa-plus text-success"></i>
            </button>
        </td>
        <td id="row15_tgc9">
            <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                <i class="fa fa-plus text-success"></i>
            </button>
        </td>
        <td id="row15_tgc10">
            <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                <i class="fa fa-plus text-success"></i>
            </button>
        </td>
            <td class="count-by-module">5</td>
        </tr>
        <tr>
            <td>17</td>
            <td></td>
            <td class="text-left">ABC123-Module Name10</td>
            <td></td>
            <td>Core</td>
            <td></td>
            <td>1</td>
            <td></td>
            <td id="row16_tgc1">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row16_tgc2">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row16_tgc3">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row16_tgc4">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row16_tgc5">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row16_tgc6">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row16_tgc7">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row16_tgc8">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row16_tgc9">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row16_tgc10">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td class="count-by-module">5</td>
        </tr>
        <tr>
            <td>18</td>
            <td></td>
            <td class="text-left">ABC123-Module Name11</td>
            <td></td>
            <td>Core</td>
            <td></td>
            <td>1</td>
            <td></td>
            <td id="row17_tgc1">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row17_tgc2">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row17_tgc3">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row17_tgc4">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row17_tgc5">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row17_tgc6">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row17_tgc7">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row17_tgc8">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row17_tgc9">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td id="row17_tgc10">
                <button class="ploaddbtn btn btn-light btn-xs" data-toggle="modal" data-target="#addplo">
                    <i class="fa fa-plus text-success"></i>
                </button>
            </td>
            <td class="count-by-module">2</td>
        </tr>
        <tr class="bold count-by-plo">
            <td colspan="8" class="text-right ">Count by PLO</td>
            <td>3</td>
            <td>4</td>
            <td>1</td>
            <td>2</td>
            <td>5</td>
            <td>6</td>
            <td>2</td>
            <td>2</td>
            <td>2</td>
            <td>2</td>
            <td></td>
        </tr>
    </table>

<div class="card-footer text-center">
    <a class="btn btn-purple btn-sm btn-space" href="{{ URL::route('programme-new') }}"> <i
            class="fa fa-arrow-left mr-3"></i>back to form</a>
</div>
    
</div>

{{-- m o d a l  --}}
<div class="modal fade " id="addplo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Programme Learning Outcomes</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label class="sr-only" for="inlineFormInput">Select PLO below</label>
                    <select class="form-control select2" id="ploNo">
                        <option value="1">PLO 1</option>
                        <option value="3">PLO 2</option>
                        <option value="2">PLO 3</option>
                        <option value="4">PLO 4</option>
                        <option value="5">PLO 5</option>
                    </select>
                </div>
            </div>
            <div class="modal-footer text-center">
                <button class="btn btn-primary btn-sm" data-dismiss="modal" id="okplo">OK</button>
            </div>
        </div>
    </div>
</div>

@endsection
@push('extraScripts')
<script src={{ asset('js/mapping-demo.js') }}></script>
@endpush