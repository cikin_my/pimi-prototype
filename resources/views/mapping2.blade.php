@extends('layouts.main.master')
@section('content')
<div class="card bg-light">
    <header class="card-header">
        <i class="fa fa-list mr-3"></i> MLO -PLO Mapping
    </header>
        <div class="card-body">
            <table id="MLOTable" class="table table-bordered text-center table-small-text" style="">
                <thead>
                    <tr>
                        <th rowspan="2" width="5px" class="mol-header">No</th>
                        <th rowspan="2" width="30%" class="mol-header">Module Learning Outcomes <br>(MLO)</th>
                        <th rowspan="2" width="7%" class="mol-header">MLO %</th>
                        <th rowspan="2" width="25%" class="mol-header">Programme Learning Outcomes <br>(PLO)</th>
                        <th colspan="2" width="15%" class="mol-header">TGC Attributes</th>
                        <th width="20%" class="mol-header">TGC Sub-Attributes</th>
                    </tr>
                </thead>
                <tr>
                    <td colspan="4"></td>
                    <td colspan="3">
                        <a href="{{ URL::route('tgc') }}" class="btn btn-primary btn-block btn-sm" target="_blank">
                            <i class="fa fa-info-circle mr-2"></i> INFO
                        </a>
                    </td>
                </tr>
                <tr class="one">
                    <td class="a">1</td>
                    <td class="a text-left">Explore architectural compositions from conception, translations creation
                        based
                        on basic design elements</td>
                    <td class="a">50%</td>
                    <td class="a" id="plo1" style="width:20%">
                        <button class="btn btn-purple btn-block btn-sm plotext" id="plotext1">PLO1</button>
                        <div class="dropdown" id="dropplo1">
                            <button class="btn btn-outline-purple btn-block btn-sm addplo" type="button"
                                data-toggle="modal" data-target="#dropdown">
                                Dropdown Example
                                <i class="fa fa-caret-down pull-right" aria-hidden="true"></i>
                            </button>
                    </td>
                    <td colspan="3" style="padding: 0;vertical-align:bottom">
                        <table id="table1" class="table table-bordered text-center mb-0">

                            <tr class="one1">
                                <td colspan="3" class="text-left" style="border-top:1px solid transparent;">
                                    <button type="button" id="add2" value="" class="btn btn-light btn-xs addbtn"
                                        data-toggle="modal" data-target="#addtgc">
                                        <i class="fa fa-plus text-success"></i>
                                    </button>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr class="two">
                    <td class="a">2</td>
                    <td class="a text-left">Lorem ipsum dolor sit amet, consectetur adipiscing elit....</td>
                    <td class="a"></td>
                    <td class="a" id="plo2" style="width:20%">
                        <button class="btn btn-purple btn-block btn-sm plotext" id="plotext2">PLO2</button>
                        <div class="dropdown" id="dropplo2">
                            <button class="btn btn-outline-purple btn-block btn-sm addplo" type="button"
                                data-toggle="modal" data-target="#dropdown">
                                Dropdown Example
                                <i class="fa fa-caret-down pull-right" aria-hidden="true"></i>
                            </button>
                    </td>
                    <td colspan="3" style="padding: 0;vertical-align:bottom">
                        <table id="table2" class="table table-bordered text-center mb-0">

                            <tr class="two2">
                                <td colspan="3" class="text-left" style="border-top:1px solid transparent;">
                                    <button type="button" id="add2" value="" class="btn btn-light btn-xs addbtn"
                                        data-toggle="modal" data-target="#addtgc">
                                        <i class="fa fa-plus text-success"></i>
                                    </button>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr class="three">
                    <td class="a">3</td>
                    <td class="a text-left">Lorem ipsum dolor sit amet, consectetur adipiscing elit....</td>
                    <td class="a"></td>
                    <td class="a" id="plo3" style="width:20%">
                        <button class="btn btn-purple btn-block btn-sm plotext" id="plotext3">PLO3</button>
                        <div class="dropdown" id="dropplo3">
                            <button class="btn btn-outline-purple btn-block btn-sm addplo" type="button"
                                data-toggle="modal" data-target="#dropdown">
                                Dropdown Example
                                <i class="fa fa-caret-down pull-right" aria-hidden="true"></i>
                            </button>
                    </td>
                    <td colspan="3" style="padding: 0;vertical-align:bottom">
                        <table id="table3" class="table table-border text-center mb-0">

                            <tr class="three3">
                                <td colspan="3" class="text-left" style="border-top:1px solid transparent;">
                                    <button type="button" id="add2" value="" class="btn btn-light btn-xs addbtn"
                                        data-toggle="modal" data-target="#addtgc">
                                        <i class="fa fa-plus text-success"></i>
                                    </button>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>

            <div class="col-md-12  text-right mr-0 pr-0">
                <button type="button" class="btn btn-primary btn-sm"><i class="fa fa-pencil mr-1"></i> Edit</button>
                <button type="button" class="btn btn-success btn-sm"><i class="fa fa-floppy-o mr-1"></i> Save </button>
                <button type="button" class="btn btn-info btn-sm "><i class="fa fa-print mr-1"></i> Print</button>
                <button type="button" class="btn btn-danger btn-sm "><i class="fa fa-sign-out mr-1"></i> Exit</button>
            </div>
        </div>
</div>


<!-- m o  d a l -->
<div class="modal fade " id="addtgc" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">TGC Attributes</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-border table-hover" style="font-size:12px">
                    <tbody>
                        <tr>
                            <td>TGC 1</td>
                            <td class="hidden-phone">Lorem ipsum…</td>
                            <td class="text-right">
                                <a class="btn btn-primary btn-sm add-row" href="" id="tgc1-select" data-dismiss="modal">
                                    Select <i class="fa fa-ellipsis-v ml-2" aria-hidden="true"></i></a>
                            </td>
                        </tr>
                        <tr>
                            <td>TGC 2</td>
                            <td class="hidden-phone">Lorem ipsum…</td>
                            <td class="text-right">
                                <a class="btn btn-primary btn-sm add-row" href="" id="tgc2-select" data-dismiss="modal">
                                    Select <i class="fa fa-ellipsis-v ml-2" aria-hidden="true"></i></a>
                            </td>
                        </tr>
                        <tr>
                            <td>TGC 3</td>
                            <td class="hidden-phone">Lorem ipsum…</td>
                            <td class="text-right">
                                <a class="btn btn-primary btn-sm add-row" href="" id="tgc3-select" data-dismiss="modal">
                                    Select <i class="fa fa-ellipsis-v ml-2" aria-hidden="true"></i></a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="modal fade " id="desc" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Description</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                Lorem ipsum dolo.. sit amet, consectetur adipiscing elit. Integer id luctus risus. Pellentesque id elit
                vel est feugiat
                eleifend. Maecenas scelerisque maximus lacus in lobortis. Vestibulum tempus lorem sed sem mattis,
            </div>
        </div>
    </div>
</div>
<div class="modal fade " id="selection" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Select</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-border table-hover" style="font-size:12px">
                    <tbody>
                        <tr>
                            <td width="15%">1a.1 </td>
                            <td class="hidden-phone">Lorem ipsum…</td>
                            <td class="text-center" width="5%">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="customCheck" name="example">
                                    <label class="custom-control-label" for="customCheck"></label>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>2a.1</td>
                            <td class="hidden-phone">Lorem ipsum…</td>
                            <td class="text-center" width="5%">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="customCheck1"
                                        name="example">
                                    <label class="custom-control-label" for="customCheck1"></label>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>2a.2</td>
                            <td class="hidden-phone">Lorem ipsum…</td>
                            <td class="text-center" width="5%">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="customCheck2"
                                        name="example">
                                    <label class="custom-control-label" for="customCheck2"></label>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="addselection">ok</button>

            </div>
        </div>
    </div>
</div>

{{-- dropdown  --}}
<div class="modal fade " id="dropdown" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Programme Learning Outcomes
                    (PLO)</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-border table-hover" style="font-size:12px">
                    <tbody>
                        <tr>
                            <td>PLO 1</td>
                            <td class="hidden-phone">Lorem ipsum…</td>
                            <td class="text-right">
                                <a class="btn btn-success btn-sm ploitem" href="" id="plo1-select" data-dismiss="modal">
                                    Select <i class="fa fa-table ml-2" aria-hidden="true"></i></a>

                            </td>
                        </tr>
                        <tr>
                            <td>PLO 2</td>
                            <td class="hidden-phone">Lorem ipsum…</td>
                            <td class="text-right">
                                <a class="btn btn-success btn-sm ploitem" href="" id="plo2-select" data-dismiss="modal">
                                    Select <i class="fa fa-table ml-2" aria-hidden="true"></i></a>
                            </td>
                        </tr>
                        <tr>
                            <td>PLO 3</td>
                            <td class="hidden-phone">Lorem ipsum…</td>
                            <td class="text-right">
                                <a class="btn btn-success btn-sm ploitem" href="" id="plo3-select" data-dismiss="modal">
                                    Select <i class="fa fa-table ml-2" aria-hidden="true"></i></a>
                            </td>
                        </tr>
                        <tr>
                            <td>PLO 4</td>
                            <td class="hidden-phone">Lorem ipsum…</td>
                            <td class="text-right">
                                <a class="btn btn-success btn-sm ploitem" href="#" id="plo4-select"
                                    data-dismiss="modal">
                                    Select <i class="fa fa-table ml-2" aria-hidden="true"></i></a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer text-center">
                {{-- <button id="" type="button" class="btn btn-dark btn-sm reset-plo" data-dismiss="modal">
                    <i class="fa fa-undo mr-1"></i>
                    Reset</button>  --}}

            </div>
        </div>

    </div>
</div>

{{-- add-tgc  --}}
<div class="modal fade " id="addtgc" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="modal-body">
                <table class="table table-border table-hover" style="font-size:12px">
                    <tbody>
                        <tr>
                            <td>TGC 1</td>
                            <td class="hidden-phone">Lorem ipsum…</td>
                            <td class="text-right">
                                <a class="btn btn-primary btn-sm add-row" href="" id="tgc1-select" data-dismiss="modal">
                                    Select <i class="fa fa-ellipsis-v ml-2" aria-hidden="true"></i></a>

                            </td>
                        </tr>
                        <tr>
                            <td>TGC 2</td>
                            <td class="hidden-phone">Lorem ipsum…</td>
                            <td class="text-right">
                                <a class="btn btn-primary btn-sm add-row" href="" id="tgc2-select" data-dismiss="modal">
                                    Select <i class="fa fa-ellipsis-v ml-2" aria-hidden="true"></i></a>
                            </td>
                        </tr>
                        <tr>
                            <td>TGC 3</td>
                            <td class="hidden-phone">Lorem ipsum…</td>
                            <td class="text-right">
                                <a class="btn btn-primary btn-sm add-row" href="" id="tgc3-select" data-dismiss="modal">
                                    Select <i class="fa fa-ellipsis-v ml-2" aria-hidden="true"></i></a>
                            </td>
                        </tr>

                    </tbody>
                </table>
            </div>


        </div>
    </div>
</div>
@endsection

@push('extraScripts')
<script src={{ asset('js/mapping-demo.js') }}></script>
@endpush