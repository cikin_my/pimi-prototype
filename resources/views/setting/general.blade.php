@extends('layouts.main.master')
@section('content')


<section class="card">
    <header class="card-header">
        Listings Management
        <span class="tools pull-right">
            <a class="fa fa-chevron-down" href="javascript:;"></a>
            <a class="fa fa-times" href="javascript:;"></a>
        </span>
    </header>
    <div class="card-body">
        <div class="form-group">
            <label for="exampleInputPassword1"></label>
            <div class="pull-right">
                <a href="#" id="toggleAccordionShow">open all</a> |
                <a href="#" id="toggleAccordionHide">close all</a>

            </div>


            <div class="accordion" id="accordionitem">
                <div class="card">
                    <div class="card-header bg-light border-bottom-0 accord1" id="headingOne">

                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne"
                            aria-expanded="true" aria-controls="collapseOne">
                            <i class="mr-3 fa" aria-hidden="true"></i>
                            Faculty Management
                        </button>

                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne"
                        data-parent="#accordionitem">
                        <div class="card-body tasks-widget">

                            <div class="task-content">
                                <div class="pull-right">
                                    <button class="btn btn-info btn-sm pull-right mb-1" data-toggle="modal"
                                        data-target="#add-faculty"> <i class="fa fa-plus"></i> Add New</button>
                                </div>
                                <table class="table table-bordered text-center">
                                    <thead>
                                        <tr>
                                            <td width="10%">ID/Code</td>
                                            <td>Name</td>
                                            <td width=" 20%">access</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>001</td>
                                            <td>Faculty name</td>
                                            <td>
                                                <div class="make-switch switch-mini align-center" data-on-label="Enable"
                                                    data-off-label="Disable">
                                                    <input type="checkbox">
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header bg-light border-bottom-0 accord1" id="headingTwo">

                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                            data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                            <i class="mr-3 fa" aria-hidden="true"></i>
                            School Management
                        </button>

                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionitem">
                        <div class="card-body">
                            <div class="task-content">
                                <div class="pull-right">
                                    <button class="btn btn-info btn-sm pull-right mb-1" data-toggle="modal"
                                        data-target="#add-school"> <i class="fa fa-plus"></i> Add New</button>
                                </div>
                                <table class="table table-bordered text-center">
                                    <thead>
                                        <tr>
                                            <td>ID/Code</td>
                                            <td>Name</td>
                                            <td>access</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>school of sample </td>
                                            <td>
                                                <div class="make-switch switch-mini align-center" data-on-label="Enable"
                                                    data-off-label="Disable">
                                                    <input type="checkbox">
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header bg-light border-bottom-0 accord1" id="headingTwo">

                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                            data-target="#studymode" aria-expanded="false" aria-controls="studymode">
                            <i class="mr-3 fa" aria-hidden="true"></i>
                            Study Mode Management
                        </button>

                    </div>
                    <div id="studymode" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionitem">
                        <div class="card-body">
                            <div class="task-content">
                                <div class="pull-right">
                                    <button class="btn btn-info btn-sm pull-right mb-1" data-toggle="modal"
                                        data-target="#add-studymode">
                                        <i class="fa fa-plus"></i> Add New</button>
                                </div>
                                <table class="table table-bordered text-center">
                                    <thead>
                                        <tr>
                                            <td>ID/Code</td>
                                            <td>Name</td>
                                            <td>access</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>school of sample </td>
                                            <td>
                                                <div class="make-switch switch-mini align-center" data-on-label="Enable"
                                                    data-off-label="Disable">
                                                    <input type="checkbox">
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header bg-light border-bottom-0 accord1" id="headingTwo">

                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#award"
                            aria-expanded="false" aria-controls="award">
                            <i class="mr-3 fa" aria-hidden="true"></i>
                            Awards Programme Management
                        </button>

                    </div>
                    <div id="award" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionitem">
                        <div class="card-body">
                            <div class="task-content">
                                <div class="pull-right">
                                    <button class="btn btn-info btn-sm pull-right mb-1" data-toggle="modal"
                                        data-target="#add-award">
                                        <i class="fa fa-plus"></i> Add New</button>
                                </div>
                                <table class="table table-bordered text-center">
                                    <thead>
                                        <tr>
                                            <td>ID/Code</td>
                                            <td>Name</td>
                                            <td>access</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>school of sample </td>
                                            <td>
                                                <div class="make-switch switch-mini align-center" data-on-label="Enable"
                                                    data-off-label="Disable">
                                                    <input type="checkbox">
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header bg-light border-bottom-0 accord1" id="headingTwo">

                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                            data-target="#programme" aria-expanded="false" aria-controls="programme">
                            <i class="mr-3 fa" aria-hidden="true"></i>
                            Programme Type Management
                        </button>

                    </div>
                    <div id="programme" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionitem">
                        <div class="card-body">
                            <div class="task-content">
                                <div class="pull-right">
                                    <button class="btn btn-info btn-sm pull-right mb-1" data-toggle="modal"
                                        data-target="#add-programme">
                                        <i class="fa fa-plus"></i> Add New</button>
                                </div>
                                <table class="table table-bordered text-center">
                                    <thead>
                                        <tr>
                                            <td>ID/Code</td>
                                            <td>Name</td>
                                            <td>access</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>school of sample </td>
                                            <td>
                                                <div class="make-switch switch-mini align-center" data-on-label="Enable"
                                                    data-off-label="Disable">
                                                    <input type="checkbox">
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header bg-light border-bottom-0 accord1" id="headingTwo">

                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                            data-target="#learning" aria-expanded="false" aria-controls="learning">
                            <i class="mr-3 fa" aria-hidden="true"></i>
                            Method of Learning and Teaching
                        </button>

                    </div>
                    <div id="learning" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionitem">
                        <div class="card-body">
                            <div class="task-content">
                                <div class="pull-right">
                                    <button class="btn btn-info btn-sm pull-right mb-1" data-toggle="modal"
                                        data-target="#add-learning">
                                        <i class="fa fa-plus"></i> Add New</button>
                                </div>
                                <table class="table table-bordered text-center">
                                    <thead>
                                        <tr>
                                            <td>ID/Code</td>
                                            <td>Name</td>
                                            <td>access</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>school of sample </td>
                                            <td>
                                                <div class="make-switch switch-mini align-center" data-on-label="Enable"
                                                    data-off-label="Disable">
                                                    <input type="checkbox">
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header bg-light border-bottom-0 accord1" id="headingTwo">

                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#asses"
                            aria-expanded="false" aria-controls="asses">
                            <i class="mr-3 fa" aria-hidden="true"></i>
                            Assessment Modes Management
                        </button>

                    </div>
                    <div id="asses" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionitem">
                        <div class="card-body">
                            <div class="task-content">
                                <div class="pull-right">
                                    <button class="btn btn-info btn-sm pull-right mb-1" data-toggle="modal"
                                        data-target="#add-asses">
                                        <i class="fa fa-plus"></i> Add New</button>
                                </div>
                                <table class="table table-bordered text-center ">
                                    <thead>
                                        <tr>
                                            <td>ID/Code</td>
                                            <td>Name</td>
                                            <td>access</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>school of sample </td>
                                            <td>
                                                <div class="make-switch switch-mini align-center" data-on-label="Enable"
                                                    data-off-label="Disable">
                                                    <input type="checkbox">
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="card">
    <header class="card-header">php artisan
        Campus Central Sync
        <span class="tools pull-right">
            <a class="fa fa-chevron-down" href="javascript:;"></a>
            <a class="fa fa-times" href="javascript:;"></a>
        </span>
    </header>
    <div class="card-body">
        <div class="form-group">
            <label for="exampleInputPassword1"></label>
            <div class="pull-right">
                <a href="#" id="toggleAccordionShow2">open all</a> |
                <a href="#" id="toggleAccordionHide2">close all</a>

            </div>


            <div class="accordion" id="accordionitem2">
                <div class="card">
                    <div class="card-header bg-light border-bottom-0 accord1" id="headingOne">
                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne"
                            aria-expanded="true" aria-controls="collapseOne">
                            <i class="mr-3 fa" aria-hidden="true"></i>
                            Programme Information
                        </button>
                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne"
                        data-parent="#accordionitem">
                        <div class="card-body tasks-widget">

                            <div class="task-content">
                                <div class="pull-right">
                                    <button class="btn btn-info btn-sm pull-right mb-1" data-toggle="modal"
                                        data-target="#save-programinfo">  Save</button>
                                </div>
                                <table class="table table-small-text table-hover">
                                    <tr>
                                        <th>Item</th>
                                        <th>Action</th>
                                    </tr>
                                    <tr>
                                        <td>Campus Code</td>
                                        <td>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1"
                                                    name="example1">
                                                <label class="custom-control-label" for="customCheck1"></label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>TCF Flag</td>
                                        <td>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1"
                                                    name="example1">
                                                <label class="custom-control-label" for="customCheck1"></label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Name of the Programme (as in the Scroll to be Awarded)</td>
                                        <td>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1"
                                                    name="example1">
                                                <label class="custom-control-label" for="customCheck1"></label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>School Owning</td>
                                        <td>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1"
                                                    name="example1">
                                                <label class="custom-control-label" for="customCheck1"></label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Field of Study and National Education Code (NEC)</td>
                                        <td>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1"
                                                    name="example1">
                                                <label class="custom-control-label" for="customCheck1"></label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>TU Programme Code</td>
                                        <td>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1"
                                                    name="example1">
                                                <label class="custom-control-label" for="customCheck1"></label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>MOE Programme Code</td>
                                        <td>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1"
                                                    name="example1">
                                                <label class="custom-control-label" for="customCheck1"></label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>MQA Programme Code</td>
                                        <td>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1"
                                                    name="example1">
                                                <label class="custom-control-label" for="customCheck1"></label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>MOE Expiry Date</td>
                                        <td>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1"
                                                    name="example1">
                                                <label class="custom-control-label" for="customCheck1"></label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>MQA Expiry Date</td>
                                        <td>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1"
                                                    name="example1">
                                                <label class="custom-control-label" for="customCheck1"></label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>MQF Level</td>
                                        <td>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1"
                                                    name="example1">
                                                <label class="custom-control-label" for="customCheck1"></label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Graduating Credit</td>
                                        <td>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1"
                                                    name="example1">
                                                <label class="custom-control-label" for="customCheck1"></label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Mode of Study</td>
                                        <td>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1"
                                                    name="example1">
                                                <label class="custom-control-label" for="customCheck1"></label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Duration of Study</td>
                                        <td>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1"
                                                    name="example1">
                                                <label class="custom-control-label" for="customCheck1"></label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Type of Award</td>
                                        <td>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1"
                                                    name="example1">
                                                <label class="custom-control-label" for="customCheck1"></label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Awarding Body</td>
                                        <td>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1"
                                                    name="example1">
                                                <label class="custom-control-label" for="customCheck1"></label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Type of Programme</td>
                                        <td>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1"
                                                    name="example1">
                                                <label class="custom-control-label" for="customCheck1"></label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Accreditation Body</td>
                                        <td>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1"
                                                    name="example1">
                                                <label class="custom-control-label" for="customCheck1"></label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Address of location outside Taylor’s Lakeside Campus</td>
                                        <td>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1"
                                                    name="example1">
                                                <label class="custom-control-label" for="customCheck1"></label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Method of Learning and Teaching</td>
                                        <td>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1"
                                                    name="example1">
                                                <label class="custom-control-label" for="customCheck1"></label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Mode of Delivery</td>
                                        <td>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1"
                                                    name="example1">
                                                <label class="custom-control-label" for="customCheck1"></label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Language of Instruction</td>
                                        <td>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1"
                                                    name="example1">
                                                <label class="custom-control-label" for="customCheck1"></label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Entry Requirements</td>
                                        <td>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1"
                                                    name="example1">
                                                <label class="custom-control-label" for="customCheck1"></label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Programme Educational Objectives (PEO)</td>
                                        <td>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1"
                                                    name="example1">
                                                <label class="custom-control-label" for="customCheck1"></label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Programme Learning Outcomes (PLO)</td>
                                        <td>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1"
                                                    name="example1">
                                                <label class="custom-control-label" for="customCheck1"></label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Programme Standards (PS)</td>
                                        <td>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1"
                                                    name="example1">
                                                <label class="custom-control-label" for="customCheck1"></label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Professional Body Requirements (PBR)</td>
                                        <td>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1"
                                                    name="example1">
                                                <label class="custom-control-label" for="customCheck1"></label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Mapping of PLO to TGC</td>
                                        <td>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1"
                                                    name="example1">
                                                <label class="custom-control-label" for="customCheck1"></label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Mapping of PLOs against the Programme Standards (PS)</td>
                                        <td>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1"
                                                    name="example1">
                                                <label class="custom-control-label" for="customCheck1"></label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Mapping of PLOs against Profession Body Requirements (PBR)</td>
                                        <td>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1"
                                                    name="example1">
                                                <label class="custom-control-label" for="customCheck1"></label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Mapping of PLO to PEO</td>
                                        <td>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1"
                                                    name="example1">
                                                <label class="custom-control-label" for="customCheck1"></label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Name of Specialization</td>
                                        <td>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1"
                                                    name="example1">
                                                <label class="custom-control-label" for="customCheck1"></label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Specialization Code</td>
                                        <td>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1"
                                                    name="example1">
                                                <label class="custom-control-label" for="customCheck1"></label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Specialization Synopsis</td>
                                        <td>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1"
                                                    name="example1">
                                                <label class="custom-control-label" for="customCheck1"></label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Minimum number of credits to be completed for the Specialization</td>
                                        <td>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1"
                                                    name="example1">
                                                <label class="custom-control-label" for="customCheck1"></label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Exclusion Criteria</td>
                                        <td>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1"
                                                    name="example1">
                                                <label class="custom-control-label" for="customCheck1"></label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Graduation Criteria</td>
                                        <td>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1"
                                                    name="example1">
                                                <label class="custom-control-label" for="customCheck1"></label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Internal Accreditation (IA)</td>
                                        <td>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1"
                                                    name="example1">
                                                <label class="custom-control-label" for="customCheck1"></label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Mapping of PLO to Modules</td>
                                        <td>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1"
                                                    name="example1">
                                                <label class="custom-control-label" for="customCheck1"></label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Proposal for Programme Changes</td>
                                        <td>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1"
                                                    name="example1">
                                                <label class="custom-control-label" for="customCheck1"></label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Effective Date</td>
                                        <td>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1"
                                                    name="example1">
                                                <label class="custom-control-label" for="customCheck1"></label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Revision Number</td>
                                        <td>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1"
                                                    name="example1">
                                                <label class="custom-control-label" for="customCheck1"></label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Approval Byby and Approval Date</td>
                                        <td>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1"
                                                    name="example1">
                                                <label class="custom-control-label" for="customCheck1"></label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Effective Intake</td>
                                        <td>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1"
                                                    name="example1">
                                                <label class="custom-control-label" for="customCheck1"></label>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <div class="pull-right">
                                    <button class="btn btn-info btn-sm pull-right mb-1" data-toggle="modal" data-target="#save-programinfo">
                                        Save</button>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header bg-light border-bottom-0 accord1" id="headingTwo">

                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                            data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                            <i class="mr-3 fa" aria-hidden="true"></i>
                           Module Information
                        </button>

                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionitem">
                        <div class="card-body">
                            <div class="task-content">
                                <div class="pull-right">
                                    <button class="btn btn-info btn-sm pull-right mb-1" data-toggle="modal"
                                        data-target="#save-moduleinfo">Save</button>
                                </div>
                                <table class="table table-hover table-small-text">
                                    <tr>
                                        <th>Item</th>
                                        <th>Action</th>
                                    </tr>
                                    <tr>
                                        <td>Module Name</td>
                                        <td> <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1"
                                                    name="example1">
                                                <label class="custom-control-label" for="customCheck1"></label>
                                            </div>
                                    </tr>
                                    <tr>
                                        <td>Module Code</td>
                                        <td> <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1"
                                                    name="example1">
                                                <label class="custom-control-label" for="customCheck1"></label>
                                            </div></td>
                                    </tr>
                                    <tr>
                                        <td>Synopsis</td>
                                        <td> <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1"
                                                    name="example1">
                                                <label class="custom-control-label" for="customCheck1"></label>
                                            </div></td>
                                    </tr>
                                    <tr>
                                        <td>Name(s) of Academic Staff teaching the Module</td>
                                        <td> <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1"
                                                    name="example1">
                                                <label class="custom-control-label" for="customCheck1"></label>
                                            </div></td>
                                    </tr>
                                    <tr>
                                        <td>Year Level (Module)</td>
                                        <td> <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1"
                                                    name="example1">
                                                <label class="custom-control-label" for="customCheck1"></label>
                                            </div></td>
                                    </tr>
                                    <tr>
                                        <td>Year Level (Programme)</td>
                                        <td> <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1"
                                                    name="example1">
                                                <label class="custom-control-label" for="customCheck1"></label>
                                            </div></td>
                                    </tr>
                                    <tr>
                                        <td>Semester Offered</td>
                                        <td> <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1"
                                                    name="example1">
                                                <label class="custom-control-label" for="customCheck1"></label>
                                            </div></td>
                                    </tr>
                                    <tr>
                                        <td>Credit Value</td>
                                        <td> <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1"
                                                    name="example1">
                                                <label class="custom-control-label" for="customCheck1"></label>
                                            </div></td>
                                    </tr>
                                    <tr>
                                        <td>Pre-requisite</td>
                                        <td> <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1"
                                                    name="example1">
                                                <label class="custom-control-label" for="customCheck1"></label>
                                            </div></td>
                                    </tr>
                                    <tr>
                                        <td>Co-Requisite</td>
                                        <td> <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1"
                                                    name="example1">
                                                <label class="custom-control-label" for="customCheck1"></label>
                                            </div></td>
                                    </tr>
                                    <tr>
                                        <td>Anti-Requisite</td>
                                        <td> <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1"
                                                    name="example1">
                                                <label class="custom-control-label" for="customCheck1"></label>
                                            </div></td>
                                    </tr>
                                    <tr>
                                        <td>Module Owner</td>
                                        <td> <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1"
                                                    name="example1">
                                                <label class="custom-control-label" for="customCheck1"></label>
                                            </div></td>
                                    </tr>
                                    <tr>
                                        <td>Module offered as</td>
                                        <td> <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1"
                                                    name="example1">
                                                <label class="custom-control-label" for="customCheck1"></label>
                                            </div></td>
                                    </tr>
                                    <tr>
                                        <td>Domain Name (for free electives only)</td>
                                        <td> <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1"
                                                    name="example1">
                                                <label class="custom-control-label" for="customCheck1"></label>
                                            </div></td>
                                    </tr>
                                    <tr>
                                        <td>Module Learning Outcomes</td>
                                        <td> <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1"
                                                    name="example1">
                                                <label class="custom-control-label" for="customCheck1"></label>
                                            </div></td>
                                    </tr>
                                    <tr>
                                        <td>Mapping of MLO-PLO</td>
                                        <td> <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1"
                                                    name="example1">
                                                <label class="custom-control-label" for="customCheck1"></label>
                                            </div></td>
                                    </tr>
                                    <tr>
                                        <td>Transferable skills (if applicable)</td>
                                        <td> <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1"
                                                    name="example1">
                                                <label class="custom-control-label" for="customCheck1"></label>
                                            </div></td>
                                    </tr>
                                    <tr>
                                        <td>Description of the Assessment Components</td>
                                        <td> <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1"
                                                    name="example1">
                                                <label class="custom-control-label" for="customCheck1"></label>
                                            </div></td>
                                    </tr>
                                    <tr>
                                        <td>Resit Assessment Components</td>
                                        <td> <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1"
                                                    name="example1">
                                                <label class="custom-control-label" for="customCheck1"></label>
                                            </div></td>
                                    </tr>
                                    <tr>
                                        <td>Rubrics for each Assessment Task</td>
                                        <td> <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1"
                                                    name="example1">
                                                <label class="custom-control-label" for="customCheck1"></label>
                                            </div></td>
                                    </tr>
                                    <tr>
                                        <td>Hurdle Assessment Guideline for the Module</td>
                                        <td> <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1"
                                                    name="example1">
                                                <label class="custom-control-label" for="customCheck1"></label>
                                            </div></td>
                                    </tr>
                                    <tr>
                                        <td>Distribution of Student Learning Time (SLT)</td>
                                        <td> <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1"
                                                    name="example1">
                                                <label class="custom-control-label" for="customCheck1"></label>
                                            </div></td>
                                    </tr>
                                    <tr>
                                        <td>Main References supporting the Module</td>
                                        <td> <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1"
                                                    name="example1">
                                                <label class="custom-control-label" for="customCheck1"></label>
                                            </div></td>
                                    </tr>
                                    <tr>
                                        <td>Other Additional Information</td>
                                        <td> <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1"
                                                    name="example1">
                                                <label class="custom-control-label" for="customCheck1"></label>
                                            </div></td>
                                    </tr>
                                    <tr>
                                        <td>Special Requirements to deliver the Module</td>
                                        <td> <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1"
                                                    name="example1">
                                                <label class="custom-control-label" for="customCheck1"></label>
                                            </div></td>
                                    </tr>
                                    <tr>
                                        <td>Latihan Industri/ Clinical Placement/ Practicum</td>
                                        <td> <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1"
                                                    name="example1">
                                                <label class="custom-control-label" for="customCheck1"></label>
                                            </div></td>
                                    </tr>
                                    <tr>
                                        <td>Effective Study Intake/ Semester</td>
                                        <td> <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1"
                                                    name="example1">
                                                <label class="custom-control-label" for="customCheck1"></label>
                                            </div></td>
                                    </tr>
                                    <tr>
                                        <td>Revision Number</td>
                                        <td> <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1"
                                                    name="example1">
                                                <label class="custom-control-label" for="customCheck1"></label>
                                            </div></td>
                                    </tr>
                                    <tr>
                                        <td>Approved By</td>
                                        <td> <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1"
                                                    name="example1">
                                                <label class="custom-control-label" for="customCheck1"></label>
                                            </div></td>
                                    </tr>
                                    <tr>
                                        <td>Approval Date</td>
                                        <td> <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1"
                                                    name="example1">
                                                <label class="custom-control-label" for="customCheck1"></label>
                                            </div></td>
                                    </tr>
                                    <tr>
                                        <td>Discipline Code</td>
                                        <td> <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1"
                                                    name="example1">
                                                <label class="custom-control-label" for="customCheck1"></label>
                                            </div></td>
                                    </tr>
                                    <tr>
                                        <td>Stream</td>
                                        <td> <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1"
                                                    name="example1">
                                                <label class="custom-control-label" for="customCheck1"></label>
                                            </div></td>
                                    </tr>
                                </table>
                                <div class="pull-right">
                                    <button class="btn btn-info btn-sm pull-right mb-1" data-toggle="modal" data-target="#save-moduleinfo">Save</button>
                                </div>
                             
                            </div>
                        </div>
                    </div>
                </div>
              
            </div>
        </div>
    </div>
</section>

{{------------------ modals  --}}
<div class="modal fade" id="add-faculty" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Add Faculty</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Faculty Code </label>
                        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                            placeholder="Enter Faculty Code">

                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Faculty Name</label>
                        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                            placeholder="Enter Faculty Name">

                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn-sm">Add</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="add-school" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Add School</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="exampleInputEmail1">School Code </label>
                        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                            placeholder="Enter School Code">

                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">School Name</label>
                        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                            placeholder="Enter School Name">

                    </div>
                    <div class="form-group">
                        <label class="">Faculty </label>
                        <select class="form-control select2">
                            <option value="AL">Faculty1</option>
                            <option value="WY">Faculty2</option>
                            <option value="AL">Faculty3</option>
                        </select>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn-sm">Add</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="add-studymode" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Add Study Mode</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="exampleInputEmail1">ID </label>
                        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                            value="001">

                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Mode Name</label>
                        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                            placeholder="Enter Mode Name">

                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn-sm">Add</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="add-award" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Add Awards Programme</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="exampleInputEmail1">ID </label>
                        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                            value="001">

                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Awards Programme Name</label>
                        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                            placeholder="Enter Awards Programme Name">

                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn-sm">Add</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="add-programme" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Add Programme Type</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="exampleInputEmail1">ID </label>
                        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                            value="001">

                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Programme Type Name</label>
                        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                            placeholder="Enter Programme Type Name">

                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn-sm">Add</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="add-learning" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Add Method of Learning and Teaching</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="exampleInputEmail1">ID </label>
                        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                            value="001">

                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Method Name</label>
                        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                            placeholder="Enter Method Name">

                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn-sm">Add</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="add-asses" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Add Method of Learning and Teaching</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="exampleInputEmail1">ID </label>
                        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                            value="001">

                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Assessment Mode Name</label>
                        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                            placeholder="Enter Assessment Mode Name ">

                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn-sm">Add</button>
            </div>
        </div>
    </div>



</div>

<div class="modal fade" id="save-programinfo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

            </div>
            <div class="modal-body text-center mt-4">
                <div class="text-center">
                    <div class="i-circle text-success">
                        <i class="icon fa fa-check"></i>
                    </div>
                    <h4>Saved</h4>
                    <p>Campus Central Sync settings for PI have been saved successfully</p>
                </div>




            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="save-moduleinfo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center mt-4">
                <div class="text-center">
                    <div class="i-circle text-success">
                        <i class="icon fa fa-check"></i>
                    </div>
                    <h4>Saved</h4>
                    <p>Campus Central Sync settings for MI have been saved successfully</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection