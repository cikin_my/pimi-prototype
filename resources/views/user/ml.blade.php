@extends('layouts.main.master')
@section('content')
<div class="row">
    <div class="col-sm-12">
        <section class="card">
            <header class="card-header">
                Module Leader List
                <span class="tools pull-right">
                    <button class="btn btn-sm btn-primary" data-toggle="modal" data-target="#add">
                        <i class="fa fa-user-plus mr-1 ml-1" aria-hidden="true"></i> Add New ML</button>
                    <a href="javascript:;" class="fa fa-chevron-down"></a>
                    <a href="javascript:;" class="fa fa-times"></a>
                </span>
            </header>
            <div class="card-body">
                <div class="adv-table">
                    <table class="display table table-bordered table-striped text-center" id="dynamic-table">
                        <thead>
                            <tr>
                                <th class="hidden-phone" width="5px">#</th>
                                <th width="10%">UserId</th>
                                <th>User Name</th>
                                <th class="hidden-phone">User Email</th>
                               
                                <th>Status</th>
                                <th class="action-col" width="20%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="">
                                <td>1</td>
                                <td>1234-19</td>
                                <td>Winnie</td>
                                <td class="center hidden-phone">user@email.com</td>
                              
                                <td>
                                    <div class="make-switch switch-mini" data-on-label="Active" data-off-label="">
                                        <input type="checkbox">
                                    </div>
                                </td>
                                <td class="action-col">
                                    <button class="btn btn-success btn-sm"><i class="fa fa-eye"></i></button>
                                    <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#edit"><i
                                            class="fa fa-pencil"></i></button>

                                </td>
                            </tr>
                            <tr class="">
                                <td>2</td>
                                <td>0919-11</td>
                                <td>Norisha</td>
                                <td class="center hidden-phone">5email@smaple.email.com</td>
                              
                                <td>
                                    <div class="make-switch switch-mini" data-on-label="Active" data-off-label="">
                                        <input type="checkbox">
                                    </div>
                                </td>
                                <td class="action-col">
                                    <button class="btn btn-success btn-sm"><i class="fa fa-eye"></i></button>
                                    <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#edit"><i
                                            class="fa fa-pencil"></i></button>

                                </td>
                            </tr>
                            <tr class="">
                                <td>3</td>
                                <td>0919-11</td>
                                <td>Norish</td>
                                <td class="center hidden-phone">Aemail@smaple.email.com</td>
                             
                                <td>
                                    <div class="make-switch switch-mini" data-on-label="Active" data-off-label="">
                                        <input type="checkbox">
                                    </div>
                                </td>
                                <td class="action-col">
                                    <button class="btn btn-success btn-sm"><i class="fa fa-eye"></i></button>
                                    <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#edit"><i
                                            class="fa fa-pencil"></i></button>

                                </td>
                            </tr>
                            <tr class="">
                                <td>4</td>
                                <td>0919-11</td>
                                <td>Allen Corey</td>
                                <td class="center hidden-phone">6email@smaple.email.com</td>
                                
                                <td>
                                    <div class="make-switch switch-mini" data-on-label="Active" data-off-label="">
                                        <input type="checkbox" checked>
                                    </div>
                                </td>
                                <td class="action-col">
                                    <button class="btn btn-success btn-sm"><i class="fa fa-eye"></i></button>
                                    <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#edit"><i
                                            class="fa fa-pencil"></i></button>

                                </td>
                            </tr>
                            
                        </tbody>
                        
                    </table>
                </div>
            </div>
        </section>
    </div>
</div>

<!-- 1. add Modal -->
<div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add new user</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label class="sr-only" for="inlineFormInput">Name</label>
                        <select class="js-example-form-control select2">
                            <option value="0">Search staff name or id</option>
                            <option value="1">Chimerra</option>
                            <option value="3">Hannah</option>
                            <option value="2">Wyoming</option>
                            <option value="4">Haroon</option>
                            <option value="5">Noelia Mowers</option>
                            <option value="">Delinda Gurr</option>
                            <option value="">Harry Kitzmiller</option>
                            <option value="">Yen Uribe</option>
                            <option value="">Borrero</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="inlineFormInput">Role</label>
                        <select class="js-example-form-control select2" disabled>                            
                            <option value="1" selected>Module Leader</option>
                        </select>
                    </div>
                    <div class="form-group pull-right">
                        <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal">Add</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
{{-- 2. edit modal  --}}
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="edit" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit user</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <form role="form">
                    <div class="form-group">
                        <label for="exampleInputEmail1">UserID</label>
                        <input type="email" class="form-control" id="exampleInputEmail3" placeholder="123456" disabled>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Username</label>
                        <input type="email" class="form-control" id="exampleInputEmail3" placeholder="JohnDoe">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Email address</label>
                        <input type="email" class="form-control" id="exampleInputEmail3" placeholder="John@email.com">
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="inlineFormInput">Role</label>
                        <select class="js-example-form-control select2" disabled>
                            <option value="0">Select Role</option>
                            <option value="1" selected>Module Leader</option>
                        </select>
                    </div>
                    <div class="pull-right">
                        <button class="btn btn-default btn-sm btn-primary" data-dismiss="modal">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
    
@endsection