@extends('layouts.main.master')
@section('content')
<div class="row">
    <div class="col-sm-12">
        <section class="card">
            <header class="card-header">
                Assigned HOS List
                <span class="tools pull-right">
                    <button class="btn btn-sm btn-primary" data-toggle="modal" data-target="#add">
                        <i class="fa fa-user-plus mr-1 ml-1" aria-hidden="true"></i> Assign HOS</button>
                    <a href="javascript:;" class="fa fa-chevron-down"></a>
                    <a href="javascript:;" class="fa fa-times"></a>
                </span>
            </header>
            <div class="card-body">
                <div class="adv-table">
                    <table class="display table table-bordered table-striped " id="dynamic-table">
                        <thead>
                            <tr>
                                <th class="hidden-phone" width="5px">No.</th>
                                <th width="25%">Name</th>
                                <th width="30%">School</th>
                                <th width="20%" class="hidden-phone">Date Assigned</th>
                                <th class="action-col" width="15%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="">
                                <td>1</td>
                                <td>Mr. Lin </td>
                                <td>School of sample</td>
                                <td class=" hidden-phone">20 July 2019</td>
                                <td class="action-col">
                                    <button class="btn btn-success btn-sm"><i class="fa fa-eye"></i></button>
                                    <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#edit"><i
                                            class="fa fa-pencil"></i></button>
                                </td>
                            </tr>
                            <tr class="">
                                <td>2</td>
                                <td>Mei limg </td>
                                <td>School of sample</td>
                                <td class=" hidden-phone">20 July 2017</td>

                                <td class="action-col">
                                    <button class="btn btn-success btn-sm"><i class="fa fa-eye"></i></button>
                                    <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#edit"><i
                                            class="fa fa-pencil"></i></button>

                                </td>
                            </tr>
                            <tr class="">
                                <td>3</td>
                                <td>Mr. Kimmie </td>
                                <td>School of sample</td>
                                <td class=" hidden-phone">20 July 2015</td>

                                <td class="action-col">
                                    <button class="btn btn-success btn-sm"><i class="fa fa-eye"></i></button>
                                    <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#edit"><i
                                            class="fa fa-pencil"></i></button>

                                </td>
                            </tr>


                        </tbody>

                    </table>
                </div>
            </div>
        </section>
    </div>
</div>

<!-- 1. add Modal -->
<div class="modal fade form-container" id="add" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Assign HOS </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label class="sr-only" for="inlineFormInput">Name</label>
                        <select class="js-example-form-control select2">
                            <option value="0">Search staff name or id</option>
                            <option value="1">Chimerra</option>
                            <option value="3">Hannah</option>
                            <option value="2">Wyoming</option>
                            <option value="4">Haroon</option>
                            <option value="5">Noelia Mowers</option>
                            <option value="">Delinda Gurr</option>
                            <option value="">Harry Kitzmiller</option>
                            <option value="">Yen Uribe</option>
                            <option value="">Borrero</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="inlineFormInput">School</label>
                        <select class="js-example-form-control select2">
                            <option value="0">Select School</option>
                            <option value="1">School of sample1</option>
                            <option value="3">School of sample2</option>
                            <option value="2">School of sample3</option>
                           
                        </select>
                    </div>
                 
                    <div class="form-group pull-right">
                        <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal">Add</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
{{-- 2. edit modal  --}}
<div class="modal fade " id="edit" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit School</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body ">
               <form>
                    
                    <div class="form-group">
                        <label class="sr-only" for="inlineFormInput">School</label>
                        <select class="form-control select2">
                            <option value="0">Select School</option>
                            <option value="1">School of sample1</option>
                            <option value="3">School of sample2</option>
                            <option value="2">School of sample3</option>                
                        </select>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal">Save</button>
            
            </div>
        </div>
    </div>
</div>
@endsection