@extends('layouts.main.master')

@section('content')
<div class="row">
    <div class="col-sm-12">
        <section class="card">
            <header class="card-header">
                <i class="fa fa-user mr-3"></i>User List
                <span class="tools pull-right">
                    <button class="btn btn-sm btn-success" data-toggle="modal" data-target="#add-role">
                        <i class="fa fa-low-vision mr-1 ml-1" aria-hidden="true"></i> Add New Role</button>
                    <a href="javascript:;" class="fa fa-chevron-down"></a>
                    <a href="javascript:;" class="fa fa-times"></a>
                </span>
            </header>
            <div class="card-body">
                <div class="adv-table">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th width="5px">#</th>
                                <th width="25%">Role Name</th>
                                <th width="35%">Description</th>
                                <th class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>Module Leader </td>
                                <td>Cillum ad ut irure tempor velit nostrud m Lorem sint.</td>
                                <td class="text-right">
                                    <button type="button" class="btn btn-info btn-sm mb-2" data-toggle="modal"
                                        data-target="#"><i class="fa fa-pencil"></i></button>
                                    <button type="button" class="btn btn-warning btn-sm mb-2" data-toggle="modal"
                                        data-target="#pi">PI view</button>
                                    <button type="button" class="btn btn-danger btn-sm mb-2" data-toggle="modal"
                                        data-target="#mi">Mi view</button>
                                    <button type="button" class="btn btn-default btn-sm mb-2 " disabled><i
                                            class="fa fa-times"></i></button>
                                </td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>Programme Director</td>
                                <td>Cillum ad ut irure tempor velit nostrim Lorem sint.</td>
                                <td class="text-right">
                                    <button type="button" class="btn btn-info btn-sm mb-2" data-toggle="modal"
                                        data-target="#"><i class="fa fa-pencil"></i></button>
                                    <button type="button" class="btn btn-warning btn-sm mb-2" data-toggle="modal"
                                        data-target="#pi">PI view</button>
                                    <button type="button" class="btn btn-danger btn-sm mb-2" data-toggle="modal"
                                        data-target="#mi">Mi view</button>
                                    <button type="button" class="btn btn-default btn-sm mb-2 " disabled><i
                                            class="fa fa-times"></i></button>
                                </td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td>RAG</td>
                                <td>tCillum ad ut irure tempor velit nostrud occaec Lorem sint. </td>
                                <td class="text-right">
                                    <button type="button" class="btn btn-info btn-sm mb-2" data-toggle="modal"
                                        data-target="#"><i class="fa fa-pencil"></i></button>
                                    <button type="button" class="btn btn-warning btn-sm mb-2" data-toggle="modal"
                                        data-target="#pi">PI view</button>
                                    <button type="button" class="btn btn-danger btn-sm mb-2" data-toggle="modal"
                                        data-target="#mi">Mi view</button>
                                    <button type="button" class="btn btn-default btn-sm mb-2 " disabled><i
                                            class="fa fa-times"></i></button>
                                </td>
                            </tr>
                            <tr>
                                <td>4</td>
                                <td>Head of School</td>
                                <td>tCillum ad ut irure tempor velit nostrud sint. </td>
                                <td class="text-right">
                                    <button type="button" class="btn btn-info btn-sm mb-2" data-toggle="modal"
                                        data-target="#"><i class="fa fa-pencil"></i></button>
                                    <button type="button" class="btn btn-warning btn-sm mb-2" data-toggle="modal"
                                        data-target="#pi">PI view</button>
                                    <button type="button" class="btn btn-danger btn-sm mb-2" data-toggle="modal"
                                        data-target="#mi">Mi view</button>
                                    <button type="button" class="btn btn-default btn-sm mb-2 " disabled><i
                                            class="fa fa-times"></i></button>
                                </td>
                            </tr>
                            <tr>
                                <td>5</td>
                                <td>ED</td>
                                <td>tCillum ad ut irure tempor velit nostrud occaecat </td>
                                <td class="text-right">
                                    <button type="button" class="btn btn-info btn-sm mb-2" data-toggle="modal"
                                        data-target="#"><i class="fa fa-pencil"></i></button>
                                    <button type="button" class="btn btn-warning btn-sm mb-2" data-toggle="modal"
                                        data-target="#pi">PI view</button>
                                    <button type="button" class="btn btn-danger btn-sm mb-2" data-toggle="modal"
                                        data-target="#mi">Mi view</button>
                                    <button type="button" class="btn btn-default btn-sm mb-2 " disabled><i
                                            class="fa fa-times"></i></button>
                                </td>
                            </tr>
                            <tr>
                                <td>6</td>
                                <td>Admin</td>
                                <td>System admin account</td>
                                <td class="text-right">
                                    <button type="button" class="btn btn-info btn-sm mb-2" data-toggle="modal"
                                        data-target="#edit"><i class="fa fa-pencil"></i></button>
                                    <button type="button" class="btn btn-warning btn-sm mb-2" data-toggle="modal"
                                        data-target="#pi">PI view</button>
                                    <button type="button" class="btn btn-danger btn-sm mb-2" data-toggle="modal"
                                        data-target="#mi">Mi view</button>
                                    <button type="button" class="btn btn-default btn-sm mb-2 " data-toggle="modal"
                                        data-target="#delete"><i class="fa fa-times"></i></button>
                                </td>
                            </tr>
                            <tr>
                                <td>7</td>
                                <td>Staff</td>
                                <td>Staff account </td>
                                <td class="text-right">
                                    <button type="button" class="btn btn-info btn-sm mb-2" data-toggle="modal"
                                        data-target="#edit"><i class="fa fa-pencil"></i></button>
                                    <button type="button" class="btn btn-warning btn-sm mb-2" data-toggle="modal"
                                        data-target="#pi">PI view</button>
                                    <button type="button" class="btn btn-danger btn-sm mb-2" data-toggle="modal"
                                        data-target="#mi">Mi view</button>
                                    <button type="button" class="btn btn-default btn-sm mb-2 " data-toggle="modal"
                                        data-target="#delete"><i class="fa fa-times"></i></button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
    </div>
</div>

{{-- MI  --}}
<div class="modal fade" id="mi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">MI View</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <section class="card" id="pi-item">
                    <header class="card-header">

                        <input class="form-check-input" type="checkbox" id="checkAll">
                        <label class="form-check-label" for="checkAll">
                            Check All
                        </label>
                    </header>
                    <div class="card-body">
                        <form action="#" method="get" accept-charset="utf-8">
                            <div class="checkboxes">


                                <input class="form-check-input" type="checkbox" id="gridCheck1">
                                <label class="form-check-label" for="gridCheck1">
                                   Module Name
                                </label>



                                <input class="form-check-input" type="checkbox" id="gridCheck2">
                                <label class="form-check-label" for="gridCheck2">
                                    Module Code
                                </label>
                                <input class="form-check-input" type="checkbox" id="gridCheck3">
                                <label class="form-check-label" for="gridCheck3">
                                   Synopsis
                                </label>
                                <input class="form-check-input" type="checkbox" id="gridCheck4">
                                <label class="form-check-label" for="gridCheck4">
                                   Name(s) of Academic Staff teaching the Modul
                                </label>
                                <input class="form-check-input" type="checkbox" id="gridCheck15">
                                <label class="form-check-label" for="gridCheck15">
                                   Year Level (Module)
                                </label>

                                <input class="form-check-input" type="checkbox" id="gridCheck16">
                                <label class="form-check-label" for="gridCheck16">
                                   Year Level (Programme)
                                </label>
                                <input class="form-check-input" type="checkbox" id="gridCheck17">
                                <label class="form-check-label" for="gridCheck17">
                                    MOE Programme Code
                                </label>
                                <input class="form-check-input" type="checkbox" id="gridCheck18">
                                <label class="form-check-label" for="gridCheck18">
                                    Semester Offered
                                </label>
                                <input class="form-check-input" type="checkbox" id="gridCheck19">
                                <label class="form-check-label" for="gridCheck19">
                                    Credit Value
                                </label>
                                <input class="form-check-input" type="checkbox" id="gridCheck10">
                                <label class="form-check-label" for="gridCheck10">
                                   Pre-requisite
                                </label>
                                <input class="form-check-input" type="checkbox" id="gridCheck11">
                                <label class="form-check-label" for="gridCheck11">
                                   Co-Requisite
                                </label>
                                <input class="form-check-input" type="checkbox" id="gridCheck12">
                                <label class="form-check-label" for="gridCheck12">
                                    Anti-Requisite
                                </label>
                                <input class="form-check-input" type="checkbox" id="gridCheck134">
                                <label class="form-check-label" for="gridCheck134">
                                   Module Owner
                                </label>
                                <input class="form-check-input" type="checkbox" id="gridCheck14">
                                <label class="form-check-label" for="gridCheck14">
                                    Module offered as
                                </label>
                                <input class="form-check-input" type="checkbox" id="gridCheck15">
                                <label class="form-check-label" for="gridCheck15">
                                   Domain Name (for free electives only)
                                </label>
                                <input class="form-check-input" type="checkbox" id="gridCheck16">
                                <label class="form-check-label" for="gridCheck16">
                                    Module Learning Outcomes
                                </label>
                                <input class="form-check-input" type="checkbox" id="gridCheck1">
                                <label class="form-check-label" for="gridCheck1">
                                    Mapping of MLO-PLO
                                </label>
                                <input class="form-check-input" type="checkbox" id="gridCheck1">
                                <label class="form-check-label" for="gridCheck1"> 
                            
                                   Transferable skills (if applicable)
                                </label>
                                <input class="form-check-input" type="checkbox" id="gridCheck1">
                                <label class="form-check-label" for="gridCheck1"> 
                                    Description of the Assessment Components
                                </label>
                                <input class="form-check-input" type="checkbox" id="gridCheck1">
                                <label class="form-check-label" for="gridCheck1">
                                    Resit Assessment Components
                                </label>
                                <input class="form-check-input" type="checkbox" id="gridCheck1">
                                <label class="form-check-label" for="gridCheck1">
                                   Rubrics for each Assessment Task
                                </label>
                                <input class="form-check-input" type="checkbox" id="gridCheck1">
                                <label class="form-check-label" for="gridCheck1">
                                   Hurdle Assessment Guideline for the Module
                                </label>
                                <input class="form-check-input" type="checkbox" id="gridCheck1">
                                <label class="form-check-label" for="gridCheck1">
                                  Distribution of Student Learning Time (SLT)
                                </label>
                                <input class="form-check-input" type="checkbox" id="gridCheck1">
                                <label class="form-check-label" for="gridCheck1">
                                   Main References supporting the Module
                                </label>
                                <input class="form-check-input" type="checkbox" id="gridCheck1">
                                <label class="form-check-label" for="gridCheck1">
                                   Other Additional Information
                                </label>
                                <input class="form-check-input" type="checkbox" id="gridCheck1">
                                <label class="form-check-label" for="gridCheck1">
                                  Special Requirements to deliver the Module
                                </label>
                                <input class="form-check-input" type="checkbox" id="gridCheck1">
                                <label class="form-check-label" for="gridCheck1">
                                  Latihan Industri/ Clinical Placement/ Practicum
                                </label>
                                <input class="form-check-input" type="checkbox" id="gridCheck1">
                                <label class="form-check-label" for="gridCheck1">
                                  Effective Study Intake/ Semester
                                </label>
                                <input class="form-check-input" type="checkbox" id="gridCheck1">
                                <label class="form-check-label" for="gridCheck1">
                                 Revision Number
                                </label>
                                <input class="form-check-input" type="checkbox" id="gridCheck1">
                                <label class="form-check-label" for="gridCheck1">
                                   Approved By
                                </label>
                                <input class="form-check-input" type="checkbox" id="gridCheck1">
                                <label class="form-check-label" for="gridCheck1">
                                   Approval Dat
                                </label>
                                <input class="form-check-input" type="checkbox" id="gridCheck1">
                                <label class="form-check-label" for="gridCheck1">
                                   Discipline Cod
                                </label>
                                <input class="form-check-input" type="checkbox" id="gridCheck1">
                                <label class="form-check-label" for="gridCheck1">
                                   Stream
                                </label>
                                <input class="form-check-input" type="checkbox" id="gridCheck1">
                                <label class="form-check-label" for="gridCheck1">
                                   Print MI
                                </label>
                              

                            </div>

                        </form>
                    </div>

                </section>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="save-pi">Save changes</button>
            </div>
        </div>
    </div>
</div>
{{-- PI  --}}
<div class="modal fade" id="pi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">PI View</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <section class="card" id="pi-item">
                    <header class="card-header">
                    
                   <input class="form-check-input" type="checkbox" id="checkAll">
                <label class="form-check-label" for="checkAll">
                    Campus Code
                </label>
                    </header>
                    <div class="card-body">
                        <form action="#" method="get" accept-charset="utf-8">
                            <div class="checkboxes">


                                <input class="form-check-input" type="checkbox" id="gridCheck1">
                                <label class="form-check-label" for="gridCheck1">
                                    Campus Code
                                </label>



                                <input class="form-check-input" type="checkbox" id="gridCheck2">
                                <label class="form-check-label" for="gridCheck2">
                                    TCF Flag
                                </label>
                                <input class="form-check-input" type="checkbox" id="gridCheck3">
                                <label class="form-check-label" for="gridCheck3">
                                    Name of the Programme (as in the Scroll to be Awarded)
                                </label>
                                <input class="form-check-input" type="checkbox" id="gridCheck4">
                                <label class="form-check-label" for="gridCheck4">
                                    School Owning
                                </label>
                                <input class="form-check-input" type="checkbox" id="gridCheck15">
                                <label class="form-check-label" for="gridCheck15">
                                    Field of Study and National Education Code (NEC)
                                </label>

                                <input class="form-check-input" type="checkbox" id="gridCheck16">
                                <label class="form-check-label" for="gridCheck16">
                                    TU Programme Code
                                </label>
                                <input class="form-check-input" type="checkbox" id="gridCheck17">
                                <label class="form-check-label" for="gridCheck17">
                                    MOE Programme Code
                                </label>
                                <input class="form-check-input" type="checkbox" id="gridCheck18">
                                <label class="form-check-label" for="gridCheck18">
                                    MQA Programme Code
                                </label>
                                <input class="form-check-input" type="checkbox" id="gridCheck19">
                                <label class="form-check-label" for="gridCheck19">
                                    MOE Expiry Date
                                </label>
                                <input class="form-check-input" type="checkbox" id="gridCheck10">
                                <label class="form-check-label" for="gridCheck10">
                                    MQA Expiry Date
                                </label>
                                <input class="form-check-input" type="checkbox" id="gridCheck11">
                                <label class="form-check-label" for="gridCheck11">
                                    MQF Level
                                </label>
                                <input class="form-check-input" type="checkbox" id="gridCheck12">
                                <label class="form-check-label" for="gridCheck12">
                                    Graduating Credit
                                </label>
                                <input class="form-check-input" type="checkbox" id="gridCheck134">
                                <label class="form-check-label" for="gridCheck134">
                                    Mode of Study
                                </label>
                                <input class="form-check-input" type="checkbox" id="gridCheck14">
                                <label class="form-check-label" for="gridCheck14">
                                    Duration of Study
                                </label>
                                <input class="form-check-input" type="checkbox" id="gridCheck15">
                                <label class="form-check-label" for="gridCheck15">
                                    Awarding Body
                                </label>
                                <input class="form-check-input" type="checkbox" id="gridCheck16">
                                <label class="form-check-label" for="gridCheck16"> Type of Programme
                                </label>
                                <input class="form-check-input" type="checkbox" id="gridCheck1">
                                <label class="form-check-label" for="gridCheck1">
                                    Accreditation Body
                                </label>
                                <input class="form-check-input" type="checkbox" id="gridCheck1">
                                <label class="form-check-label" for="gridCheck1"> Address of location outside
                                    Taylor’s Lakeside Campus
                                </label>
                                <input class="form-check-input" type="checkbox" id="gridCheck1">
                                <label class="form-check-label" for="gridCheck1"> Method of Learning and Teaching
                                </label>
                                <input class="form-check-input" type="checkbox" id="gridCheck1">
                                <label class="form-check-label" for="gridCheck1">
                                    Mode of Delivery
                                </label>
                                <input class="form-check-input" type="checkbox" id="gridCheck1">
                                <label class="form-check-label" for="gridCheck1">
                                    Language of Instruction
                                </label>
                                <input class="form-check-input" type="checkbox" id="gridCheck1">
                                <label class="form-check-label" for="gridCheck1">
                                    Entry Requirements
                                </label>
                                <input class="form-check-input" type="checkbox" id="gridCheck1">
                                <label class="form-check-label" for="gridCheck1">
                                    Programme Educational Objectives (PEO)
                                </label>
                                <input class="form-check-input" type="checkbox" id="gridCheck1">
                                <label class="form-check-label" for="gridCheck1">
                                    Programme Learning Outcomes (PLO)
                                </label>
                                <input class="form-check-input" type="checkbox" id="gridCheck1">
                                <label class="form-check-label" for="gridCheck1">
                                    Programme Standards (PS)
                                </label>
                                <input class="form-check-input" type="checkbox" id="gridCheck1">
                                <label class="form-check-label" for="gridCheck1">
                                    Professional Body Requirements (PBR)
                                </label>
                                <input class="form-check-input" type="checkbox" id="gridCheck1">
                                <label class="form-check-label" for="gridCheck1">
                                    Professional Body Requirements (PBR)
                                </label>
                                <input class="form-check-input" type="checkbox" id="gridCheck1">
                                <label class="form-check-label" for="gridCheck1">
                                    Mapping of PLO to TGC
                                </label>
                                <input class="form-check-input" type="checkbox" id="gridCheck1">
                                <label class="form-check-label" for="gridCheck1">
                                    Mapping of PLOs against the Programme Standards (PS)
                                </label>
                                <input class="form-check-input" type="checkbox" id="gridCheck1">
                                <label class="form-check-label" for="gridCheck1">
                                    Mapping of PLOs against Profession Body Requirements (PBR)
                                </label>
                                <input class="form-check-input" type="checkbox" id="gridCheck1">
                                <label class="form-check-label" for="gridCheck1">
                                    Mapping of PLO to PEO
                                </label>
                                <input class="form-check-input" type="checkbox" id="gridCheck1">
                                <label class="form-check-label" for="gridCheck1">
                                    Name of Specialization
                                </label>
                                <input class="form-check-input" type="checkbox" id="gridCheck1">
                                <label class="form-check-label" for="gridCheck1">
                                    Specialization Code
                                </label>
                                <input class="form-check-input" type="checkbox" id="gridCheck1">
                                <label class="form-check-label" for="gridCheck1">
                                    Specialization Synopsis
                                </label>
                                <input class="form-check-input" type="checkbox" id="gridCheck1">
                                <label class="form-check-label" for="gridCheck1">
                                    Minimum number of credits to be completed for the Specialization
                                </label>
                                <input class="form-check-input" type="checkbox" id="gridCheck1">
                                <label class="form-check-label" for="gridCheck1">
                                    Exclusion Criteria
                                </label>
                                <input class="form-check-input" type="checkbox" id="gridCheck1">
                                <label class="form-check-label" for="gridCheck1">
                                    Graduation Criteria
                                </label>
                                <input class="form-check-input" type="checkbox" id="gridCheck1">
                                <label class="form-check-label" for="gridCheck1"> Internal Accreditation (IA)
                                </label>
                                <input class="form-check-input" type="checkbox" id="gridCheck1">
                                <label class="form-check-label" for="gridCheck1"> Mapping of PLO to Modules
                                </label>
                                <input class="form-check-input" type="checkbox" id="gridCheck1">
                                <label class="form-check-label" for="gridCheck1"> Proposal for Programme Changes
                                </label>
                                <input class="form-check-input" type="checkbox" id="gridCheck1">
                                <label class="form-check-label" for="gridCheck1">
                                    Effective Date
                                </label>
                                <input class="form-check-input" type="checkbox" id="gridCheck1">
                                <label class="form-check-label" for="gridCheck1">
                                    Revision Number
                                </label>
                                <input class="form-check-input" type="checkbox" id="gridCheck1">
                                <label class="form-check-label" for="gridCheck1">
                                    Approval Byby and Approval Date
                                </label>
                                <input class="form-check-input" type="checkbox" id="gridCheck1">
                                <label class="form-check-label" for="gridCheck1">
                                    Effective Intake
                                </label>
                                <input class="form-check-input" type="checkbox" id="gridCheck1">
                                <label class="form-check-label" for="gridCheck1">
                                    Print PI
                                </label>

                            </div>

                        </form>
                    </div>

                </section>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="save-pi">Save changes</button>
            </div>
        </div>
    </div>
</div>
{{-- delete alert --}}
<div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="modal-body text-center mt-4">
                <h2>
                    <i class="fa fa-exclamation-triangle fa-lg text-danger" aria-hidden="true"></i>
                </h2>

                <h4>Are you sure?</h4>
                <p>This action will permanently delete the role.</p>

            </div>
            <div class="modal-footer">
                <button class="btn btn-danger btn-sm" type="button" data-dismiss="modal"> OK</button>
            </div>
        </div>
    </div>
</div>
<!-- add-role-->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="add-role" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add New Role</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <form role="form">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Role Name</label>
                        <input type="email" class="form-control" id="exampleInputEmail3" placeholder="Enter role name">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Description</label>
                        <textarea name="" id="" cols="30" rows="5" class="form-control"></textarea>
                    </div>

                    <div class="form-group">
                        <label for="exampleInputPassword1">Role Access</label>
                        <div class="pull-right">
                            <a href="#" id="toggleAccordionShow">open all</a> |
                            <a href="#" id="toggleAccordionHide">close all</a>

                        </div>


                        <div class="accordion" id="accordionitem">
                            <div class="card">
                                <div class="card-header bg-light border-bottom-0 accord1" id="headingOne">

                                    <button class="btn btn-link" type="button" data-toggle="collapse"
                                        data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        <i class="mr-3 fa" aria-hidden="true"></i>
                                        Dashboard
                                    </button>

                                </div>

                                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne"
                                    data-parent="#accordionitem">
                                    <div class="card-body">
                                        <table class="table table-borderless">
                                            <tr>
                                                <td width="">
                                                    <label>Access Dashboard</label>
                                                </td>
                                                <td width="10%" class="text-right">
                                                    <div class="make-switch switch-mini" data-on-label="YES"
                                                        data-off-label="No">
                                                        <input type="checkbox">
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header bg-light border-bottom-0 accord1" id="headingTwo">

                                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                        data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        <i class="mr-3 fa" aria-hidden="true"></i>
                                        Users & Roles Management
                                    </button>


                                </div>
                                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo"
                                    data-parent="#accordionitem">
                                    <div class="card-body">
                                        <table class="table table-borderless">
                                            <tr>
                                                <td width="">
                                                    <label>User Management</label>
                                                </td>
                                                <td width="10%" class="text-right">
                                                    <div class="make-switch switch-mini" data-on-label="YES"
                                                        data-off-label="NO">
                                                        <input type="checkbox">
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="">
                                                    <label>PD Management</label>
                                                </td>
                                                <td width="10%" class="text-right">
                                                    <div class="make-switch switch-mini" data-on-label="YES"
                                                        data-off-label="NO">
                                                        <input type="checkbox">
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="">
                                                    <label>ML Management</label>
                                                </td>
                                                <td width="10%" class="text-right">
                                                    <div class="make-switch switch-mini" data-on-label="YES"
                                                        data-off-label="NO">
                                                        <input type="checkbox">
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="">
                                                    <label>HOS Management</label>
                                                </td>
                                                <td width="10%" class="text-right">
                                                    <div class="make-switch switch-mini" data-on-label="YES"
                                                        data-off-label="NO">
                                                        <input type="checkbox">
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header bg-light border-bottom-0 accord1" id="headingThree">

                                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                        data-target="#module4" aria-expanded="false" aria-controls="module4">
                                        <i class="mr-3 fa" aria-hidden="true"></i>
                                        Programme Management
                                    </button>

                                </div>
                                <div id="module4" class="collapse" aria-labelledby="headingThree"
                                    data-parent="#accordionitem">
                                    <div class="card-body">
                                        <table class="table table-borderless">
                                            <tr>
                                                <td width="">
                                                    <label>Programme Info Management</label>
                                                </td>
                                                <td width="10%" class="text-right">
                                                    <div class="make-switch switch-mini" data-on-label="YES"
                                                        data-off-label="NO">
                                                        <input type="checkbox">
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="">
                                                    <label>Module Info Management</label>
                                                </td>
                                                <td width="10%" class="text-right">
                                                    <div class="make-switch switch-mini" data-on-label="YES"
                                                        data-off-label="NO">
                                                        <input type="checkbox">
                                                    </div>
                                                </td>
                                            </tr>

                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header bg-light border-bottom-0 accord1" id="headingThree">

                                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                        data-target="#module5" aria-expanded="false" aria-controls="module5">
                                        <i class="mr-3 fa" aria-hidden="true"></i>
                                        View Programme Information
                                    </button>

                                </div>
                                <div id="module5" class="collapse" aria-labelledby="headingThree"
                                    data-parent="#accordionitem">
                                    <div class="card-body">
                                        <table class="table table-borderless">
                                            <tr>
                                                <td width="">
                                                    <label>Programme Search</label>
                                                </td>
                                                <td width="10%" class="text-right">
                                                    <div class="make-switch switch-mini" data-on-label="YES"
                                                        data-off-label="NO">
                                                        <input type="checkbox">
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="">
                                                    <label>Module Search</label>
                                                </td>
                                                <td width="10%" class="text-right">
                                                    <div class="make-switch switch-mini" data-on-label="YES"
                                                        data-off-label="NO">
                                                        <input type="checkbox">
                                                    </div>
                                                </td>
                                            </tr>

                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header bg-light border-bottom-0 accord1" id="headingThree">

                                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                        data-target="#module6" aria-expanded="false" aria-controls="module6">
                                        <i class="mr-3 fa" aria-hidden="true"></i>
                                        Approvals & Review
                                    </button>

                                </div>
                                <div id="module6" class="collapse" aria-labelledby="headingThree"
                                    data-parent="#accordionitem">
                                    <div class="card-body">
                                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry
                                        richardson ad squid.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header bg-light border-bottom-0 accord1" id="headingThree">

                                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                        data-target="#module7" aria-expanded="false" aria-controls="module7">
                                        <i class="mr-3 fa" aria-hidden="true"></i>
                                        Tracking Programme Changes
                                    </button>

                                </div>
                                <div id="module7" class="collapse" aria-labelledby="headingThree"
                                    data-parent="#accordionitem">
                                    <div class="card-body">
                                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry
                                        richardson ad squid.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header bg-light border-bottom-0 accord1" id="headingThree">

                                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                        data-target="#module8" aria-expanded="false" aria-controls="module8">
                                        <i class="mr-3 fa" aria-hidden="true"></i>
                                        Reporting
                                    </button>

                                </div>
                                <div id="module8" class="collapse" aria-labelledby="headingThree"
                                    data-parent="#accordionitem">
                                    <div class="card-body">
                                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry
                                        richardson ad squid.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header bg-light border-bottom-0 accord1" id="headingThree">

                                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                        data-target="#module9" aria-expanded="false" aria-controls="module9">
                                        <i class="mr-3 fa" aria-hidden="true"></i>
                                        Settings
                                    </button>

                                </div>
                                <div id="module9" class="collapse" aria-labelledby="headingThree"
                                    data-parent="#accordionitem">
                                    <div class="card-body">
                                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry
                                        richardson ad squid.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header bg-light border-bottom-0 accord1" id="headingThree">

                                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                        data-target="#module10" aria-expanded="false" aria-controls="module10">
                                        <i class="mr-3 fa" aria-hidden="true"></i>
                                        Audit Trail
                                    </button>

                                </div>
                                <div id="module10" class="collapse" aria-labelledby="headingThree"
                                    data-parent="#accordionitem">
                                    <div class="card-body">
                                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry
                                        richardson ad squid.
                                    </div>
                                </div>

                            </div>
                            <div class="card">
                                <div class="card-header bg-light border-bottom-0 accord1" id="headingThree">

                                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                        data-target="#module11" aria-expanded="false" aria-controls="module11">
                                        <i class="mr-3 fa" aria-hidden="true"></i>
                                        Standalone Module Creation
                                    </button>

                                </div>
                                <div id="module11" class="collapse" aria-labelledby="headingThree"
                                    data-parent="#accordionitem">
                                    <div class="card-body">
                                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry
                                        richardson ad squid.
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="pull-right">
                        <button type="submit" class="btn btn-primary btn-sm">Add</button>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
<!-- edit-role-->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="edit" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Role</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <form role="form">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Role Name</label>
                        <input type="email" class="form-control" id="exampleInputEmail3" placeholder="Admin">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Description</label>
                        <textarea name="" id="" cols="30" rows="5" class="form-control">
                            system admin account
                        </textarea>
                    </div>

                    <div class="form-group">
                        <label for="exampleInputPassword1">Role Access</label>
                        <div class="pull-right">
                            <a href="#" id="toggleAccordionShow">open all</a> |
                            <a href="#" id="toggleAccordionHide">close all</a>

                        </div>


                        <div class="accordion" id="accordionitem">
                            <div class="card">
                                <div class="card-header bg-light border-bottom-0 accord1" id="headingOne">

                                    <button class="btn btn-link" type="button" data-toggle="collapse"
                                        data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        <i class="mr-3 fa" aria-hidden="true"></i>
                                        Dashboard
                                    </button>

                                </div>

                                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne"
                                    data-parent="#accordionitem">
                                    <div class="card-body">
                                        <table class="table table-borderless">
                                            <tr>
                                                <td width="">
                                                    <label>Access Dashboard</label>
                                                </td>
                                                <td width="10%" class="text-right">
                                                    <div class="make-switch switch-mini" data-on-label="YES"
                                                        data-off-label="NO">
                                                        <input type="checkbox" checked>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header bg-light border-bottom-0 accord1" id="headingTwo">

                                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                        data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        <i class="mr-3 fa" aria-hidden="true"></i>
                                        Users & Roles Management
                                    </button>


                                </div>
                                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo"
                                    data-parent="#accordionitem">
                                    <div class="card-body">
                                        <table class="table table-borderless">
                                            <tr>
                                                <td width="">
                                                    <label>User Management</label>
                                                </td>
                                                <td width="10%" class="text-right">
                                                    <div class="make-switch switch-mini" data-on-label="YES"
                                                        data-off-label="NO">
                                                        <input type="checkbox" checked>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="">
                                                    <label>PD Management</label>
                                                </td>
                                                <td width="10%" class="text-right">
                                                    <div class="make-switch switch-mini" data-on-label="YES"
                                                        data-off-label="NO">
                                                        <input type="checkbox" checked>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="">
                                                    <label>ML Management</label>
                                                </td>
                                                <td width="10%" class="text-right">
                                                    <div class="make-switch switch-mini" data-on-label="YES"
                                                        data-off-label="NO">
                                                        <input type="checkbox" checked>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="">
                                                    <label>HOS Management</label>
                                                </td>
                                                <td width="10%" class="text-right">
                                                    <div class="make-switch switch-mini" data-on-label="YES"
                                                        data-off-label="NO">
                                                        <input type="checkbox" checked>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header bg-light border-bottom-0 accord1" id="headingThree">

                                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                        data-target="#module4" aria-expanded="false" aria-controls="module4">
                                        <i class="mr-3 fa" aria-hidden="true"></i>
                                        Programme Management
                                    </button>

                                </div>
                                <div id="module4" class="collapse" aria-labelledby="headingThree"
                                    data-parent="#accordionitem">
                                    <div class="card-body">
                                        <table class="table table-borderless">
                                            <tr>
                                                <td width="">
                                                    <label>Programme Info Management</label>
                                                </td>
                                                <td width="10%" class="text-right">
                                                    <div class="make-switch switch-mini" data-on-label="YES"
                                                        data-off-label="NO">
                                                        <input type="checkbox" checked>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="">
                                                    <label>Module Info Management</label>
                                                </td>
                                                <td width="10%" class="text-right">
                                                    <div class="make-switch switch-mini" data-on-label="YES"
                                                        data-off-label="NO">
                                                        <input type="checkbox" checked>
                                                    </div>
                                                </td>
                                            </tr>

                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header bg-light border-bottom-0 accord1" id="headingThree">

                                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                        data-target="#module5" aria-expanded="false" aria-controls="module5">
                                        <i class="mr-3 fa" aria-hidden="true"></i>
                                        View Programme Information
                                    </button>

                                </div>
                                <div id="module5" class="collapse" aria-labelledby="headingThree"
                                    data-parent="#accordionitem">
                                    <div class="card-body">
                                        <table class="table table-borderless">
                                            <tr>
                                                <td width="">
                                                    <label>Programme Search</label>
                                                </td>
                                                <td width="10%" class="text-right">
                                                    <div class="make-switch switch-mini" data-on-label="YES"
                                                        data-off-label="NO">
                                                        <input type="checkbox" checked>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="">
                                                    <label>Module Search</label>
                                                </td>
                                                <td width="10%" class="text-right">
                                                    <div class="make-switch switch-mini" data-on-label="YES"
                                                        data-off-label="NO">
                                                        <input type="checkbox" checked>
                                                    </div>
                                                </td>
                                            </tr>

                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header bg-light border-bottom-0 accord1" id="headingThree">

                                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                        data-target="#module6" aria-expanded="false" aria-controls="module6">
                                        <i class="mr-3 fa" aria-hidden="true"></i>
                                        Approvals & Review
                                    </button>

                                </div>
                                <div id="module6" class="collapse" aria-labelledby="headingThree"
                                    data-parent="#accordionitem">
                                    <div class="card-body">
                                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry
                                        richardson ad squid.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header bg-light border-bottom-0 accord1" id="headingThree">

                                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                        data-target="#module7" aria-expanded="false" aria-controls="module7">
                                        <i class="mr-3 fa" aria-hidden="true"></i>
                                        Tracking Programme Changes
                                    </button>

                                </div>
                                <div id="module7" class="collapse" aria-labelledby="headingThree"
                                    data-parent="#accordionitem">
                                    <div class="card-body">
                                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry
                                        richardson ad squid.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header bg-light border-bottom-0 accord1" id="headingThree">

                                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                        data-target="#module8" aria-expanded="false" aria-controls="module8">
                                        <i class="mr-3 fa" aria-hidden="true"></i>
                                        Reporting
                                    </button>

                                </div>
                                <div id="module8" class="collapse" aria-labelledby="headingThree"
                                    data-parent="#accordionitem">
                                    <div class="card-body">
                                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry
                                        richardson ad squid.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header bg-light border-bottom-0 accord1" id="headingThree">

                                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                        data-target="#module9" aria-expanded="false" aria-controls="module9">
                                        <i class="mr-3 fa" aria-hidden="true"></i>
                                        Settings
                                    </button>

                                </div>
                                <div id="module9" class="collapse" aria-labelledby="headingThree"
                                    data-parent="#accordionitem">
                                    <div class="card-body">
                                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry
                                        richardson ad squid.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header bg-light border-bottom-0 accord1" id="headingThree">

                                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                        data-target="#module10" aria-expanded="false" aria-controls="module10">
                                        <i class="mr-3 fa" aria-hidden="true"></i>
                                        Audit Trail
                                    </button>

                                </div>
                                <div id="module10" class="collapse" aria-labelledby="headingThree"
                                    data-parent="#accordionitem">
                                    <div class="card-body">
                                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry
                                        richardson ad squid.
                                    </div>
                                </div>

                            </div>
                            <div class="card">
                                <div class="card-header bg-light border-bottom-0 accord1" id="headingThree">

                                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                        data-target="#module11" aria-expanded="false" aria-controls="module11">
                                        <i class="mr-3 fa" aria-hidden="true"></i>
                                        Standalone Module Creation
                                    </button>

                                </div>
                                <div id="module11" class="collapse" aria-labelledby="headingThree"
                                    data-parent="#accordionitem">
                                    <div class="card-body">
                                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry
                                        richardson ad squid.
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="pull-right">
                        <button type="submit" class="btn btn-primary btn-sm">Add</button>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>


@endsection