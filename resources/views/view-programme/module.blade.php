@extends('layouts.main.master')
@section('content')
<div class="card bg-light">
    <header class="card-header">
        <i class="fa fa-file-text mr-3"></i> Module Search
    </header>
    <div class="card-body">
        <!-- filter -->
        <section class="card ">
            <header class="card-header">
                <i class="fa fa-filter mr-3"></i> Filter Options
            </header>
            <div class="card-body">
               <form>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>By Programme</label>
                                <select class="form-control select2">
                                    <option value="0">Select Programme</option>
                                    <option value="1">Programme 1</option>
                                    <option value="3">Programme 2 </option>
                                    <option value="2">Programme 3</option>
                
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>By Module</label>
                                <select class="form-control select2">
                                    <option value="0">Select Module</option>
                                    <option value="1">Module 1</option>
                                    <option value="3">Module 2 </option>
                                    <option value="2">Module 3</option>
                        
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label> By School</label>
                                <select class="form-control select2">
                                    <option value="0">Select School</option>
                                    <option value="1">School A</option>
                                    <option value="3">School B </option>
                                    <option value="2">School C</option>
                                    <option value="4">School D</option>
                                    <option value="5">School E</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>By Faculty</label>
                                <select class="form-control select2">
                                    <option value="0">Select Faculty name</option>
                                    <option value="1">Faculty a</option>
                                    <option value="3">Faculty b</option>
                                    <option value="2">Faculty c</option>
                
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4 mt-4 "></div>
                        <div class="col-md-4 mt-4 ">
                            <button class="btn btn-space btn-primary btn-sm btn-search" id="">
                                <i class="fa fa-search"></i>
                                Search</button>
                            <button class="btn btn-space btn-secondary btn-sm btn-reset" value="reset" id="">
                                <i class="fa fa-refresh"></i> Reset</button>
                        </div>
                        <div class="col-md-4 mt-4 "></div>
                    </div>
                </form>
            </div>
        </section>
        
        {{-- intro --}}
        <div class="card intro-card" id="">
            <div class="card-body text-center">
                <span class="text-muted" style="letter-spacing: 1.5px">
                    <i class="fa fa-info-circle"></i> Start your search/filter to see the list
                </span>
            </div>
        </div>
        
        <!-- table -->
        <div class="card table-result" id="" style="display: none">
            <header class="card-header">
                <i class="fa fa-list mr-3"></i> Module Search Result
            </header>
            <div class="card-body">
                <table class="table table-condensed table-borderless text-center table-hover bg-1">
                    <thead>
                        <tr>
                            <th width="5%" class="text-center">ID</th>
                            <th width="15%" class="text-center">Last Updated (Date)</th>
                            <th width="12%" class="text-center">Module Code</th>
                            <th width="" class="text-center">Module Name</th>
                            <th width="10%" class="text-center">Status</th>
                            <th width="10%" class="text-center">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>20 Mei 2019</td>
                            <td class="text-center">220</td>
                            <td>Module 1</td>
                            <td><span class="badge badge-light">Draft </span></td>
                            <td class="text-center">
        
                                <!-- view -->
                                <a class="btn btn-info btn-xs" href="#"><i class="fa fa-eye"
                                        aria-hidden="true"></i></a>
                            </td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>2 Mei 2019</td>
                            <td class="text-center">220</td>
                            <td>Module 1</td>
                            <td><span class="badge badge-info">Review </span></td>
                            <td class="text-center">
        
                                <!-- view -->
                                <a class="btn btn-info btn-xs" href="#"><i class="fa fa-eye"
                                        aria-hidden="true"></i></a>
                            </td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>2 Mei 2019</td>
                            <td class="text-center">220</td>
                            <td>Module 1</td>
                            <td><span class="badge badge-success">Approved </span> </td>
                            <td class="text-center">
        
                                <!-- view -->
                                <a class="btn btn-info btn-xs" href="#"><i class="fa fa-eye"
                                        aria-hidden="true"></i></a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection