@extends('layouts.main.master')
@section('content')
<div class="card bg-light">
    <header class="card-header">
        <i class="fa fa-th mr-3"></i> TGC-PLO MAPPING
        <span id="spnText"></span>
    </header>
    <div class="card-body">


        <table class="table table-bordered table-small-text" id="mapping-btn">
            <tbody>
                <tr>
                    <th colspan="2" rowspan="2" class="text-center align-middle main-cell">Programme Learning Outcomes
                        (PLOs)</th>
                    <th class="tgc-cell">TGC 01</th>
                    <th class="tgc-cell">TGC 02</th>
                    <th class="tgc-cell">TGC 03</th>
                    <th class="tgc-cell">TGC 04</th>
                    <th class="tgc-cell">TGC 05</th>
                    <th rowspan="2" class="plo-count-cell align-middle"> Count By PLO</th>
                </tr>Pscr
                <tr>
                    <td class="tgc-cell-2">Descipline Specific Knowledge</td>
                    <td class="tgc-cell-2">Problem Solving </td>
                    <td class="tgc-cell-2">Critical Thinking</td>
                    <td class="tgc-cell-2">Life-Long Learning</td>
                    <td class="tgc-cell-2">Global Perspective</td>
                </tr>
                <tr>
                    <th class="plo-no-cell" width="7%">PLO 01</th>
                    <td width="16%"><button class="btn btn-sm btn-outline-warning text-muted pl-3 pr-3 plo" data-toggle="modal" data-target="#desc">PLO
                            Description 1</button></td>
                    <td>
                        <div class="row">
                            <div class="col-md-12">
                                <button class="btn btn-success btn-block btn-xs PL01" id="pl01-1">PLO 01</button>
                                <button class="btn btn-light  btn-xs add-to-table pull-left mb-0 pb-0" id="add-td1"><i
                                        class="fa fa-plus"></i></button>
                                <button class="btn btn-light  btn-xs sub-from-table pull-left mb-0 pb-0" id="sub-td1"><i
                                        class="fa fa-minus"></i></button>
                            </div>
                        </div>
                    <td>
                        <div class="row">
                            <div class="col-md-12">
                                <button class="btn btn-success btn-block btn-xs PL01" id="pl01-2">PLO 01</button>
                                <button class="btn btn-light  btn-xs add-to-table pull-left mb-0 pb-0" id="add-td2"><i
                                        class="fa fa-plus"></i></button>
                                <button class="btn btn-light  btn-xs sub-from-table pull-left mb-0 pb-0" id="sub-td2"><i
                                        class="fa fa-minus"></i></button>
                            </div>
                        </div>


                    </td>
                    <td>
                        <div class="row">
                            <div class="col-md-12">
                                <button class="btn btn-success btn-block btn-xs PL01" id="pl01-3">PLO 01</button>
                                <button class="btn btn-light  btn-xs add-to-table pull-left mb-0 pb-0" id="add-td3"><i
                                        class="fa fa-plus"></i></button>
                                <button class="btn btn-light  btn-xs sub-from-table pull-left mb-0 pb-0" id="sub-td3"><i
                                        class="fa fa-minus"></i></button>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="row">
                            <div class="col-md-12">
                                <button class="btn btn-success btn-block btn-xs PL01" id="pl01-4">PLO 01</button>
                                <button class="btn btn-light  btn-xs add-to-table pull-left mb-0 pb-0" id="add-td4"><i
                                        class="fa fa-plus"></i></button>
                                <button class="btn btn-light  btn-xs sub-from-table pull-left mb-0 pb-0" id="sub-td4"><i
                                        class="fa fa-minus"></i></button>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="row">
                            <div class="col-md-12">
                                <button class="btn btn-success btn-block btn-xs PL01" id="pl01-5">PLO 01</button>
                                <button class="btn btn-light  btn-xs add-to-table pull-left mb-0 pb-0" id="add-td5"><i
                                        class="fa fa-plus"></i></button>
                                <button class="btn btn-light  btn-xs sub-from-table pull-left mb-0 pb-0" id="sub-td5"><i
                                        class="fa fa-minus"></i></button>
                            </div>
                        </div>
                    </td>
                    <td class="count-cell">1</td>
                </tr>
                <tr>
                    <th class="plo-no-cell" width="5%">PLO 02</th>
                    <td><button class="btn btn-sm btn-outline-warning text-muted pl-3 pr-3 plo" data-toggle="modal" data-target="#desc">PLO Description
                            2</button></td>
                    <td>
                        <div class="row">
                            <div class="col-md-12">
                                <button class="btn btn-success btn-block btn-xs PL02" id="pl02-1">PLO 02</button>
                                <button class="btn btn-light  btn-xs add-to-table pull-left mb-0 pb-0" id="2add-td1"><i
                                        class="fa fa-plus"></i></button>
                                <button class="btn btn-light  btn-xs sub-from-table pull-left mb-0 pb-0"
                                    id="2sub-td1"><i class="fa fa-minus"></i></button>
                            </div>
                        </div>
                    <td>
                        <div class="row">
                            <div class="col-md-12">
                                <button class="btn btn-success btn-block btn-xs PL02" id="pl02-2">PLO 02</button>
                                <button class="btn btn-light  btn-xs add-to-table pull-left mb-0 pb-0" id="2add-td2"><i
                                        class="fa fa-plus"></i></button>
                                <button class="btn btn-light  btn-xs sub-from-table pull-left mb-0 pb-0"
                                    id="2sub-td2"><i class="fa fa-minus"></i></button>
                            </div>
                        </div>


                    </td>
                    <td>
                        <div class="row">
                            <div class="col-md-12">
                                <button class="btn btn-success btn-block btn-xs PL02" id="pl02-3">PLO 02</button>
                                <button class="btn btn-light  btn-xs add-to-table pull-left mb-0 pb-0" id="2add-td3"><i
                                        class="fa fa-plus"></i></button>
                                <button class="btn btn-light  btn-xs sub-from-table pull-left mb-0 pb-0"
                                    id="2sub-td3"><i class="fa fa-minus"></i></button>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="row">
                            <div class="col-md-12">
                                <button class="btn btn-success btn-block btn-xs PL02" id="pl02-4">PLO 02</button>
                                <button class="btn btn-light  btn-xs add-to-table pull-left mb-0 pb-0" id="2add-td4"><i
                                        class="fa fa-plus"></i></button>
                                <button class="btn btn-light  btn-xs sub-from-table pull-left mb-0 pb-0"
                                    id="2sub-td4"><i class="fa fa-minus"></i></button>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="row">
                            <div class="col-md-12">
                                <button class="btn btn-success btn-block btn-xs PL01" id="pl02-5">PLO 02</button>
                                <button class="btn btn-light  btn-xs add-to-table pull-left mb-0 pb-0" id="2add-td5"><i
                                        class="fa fa-plus"></i></button>
                                <button class="btn btn-light  btn-xs sub-from-table pull-left mb-0 pb-0"
                                    id="2sub-td5"><i class="fa fa-minus"></i></button>
                            </div>
                        </div>
                    </td>
                    <td class="count-cell">1</td>
                </tr>
                <tr>
                    <th class="plo-no-cell" width="5%">PLO 03</th>
                    <td><button class="btn btn-sm btn-outline-warning text-muted pl-3 pr-3 plo" data-toggle="modal" data-target="#desc">PLO Description
                            3</button></td>
                    <td>
                        <div class="row">
                            <div class="col-md-12">
                                <button class="btn btn-success btn-block btn-xs PL01" id="pl03-1">PLO 03</button>
                                <button class="btn btn-light  btn-xs add-to-table pull-left mb-0 pb-0" id="3add-td1"><i
                                        class="fa fa-plus"></i></button>
                                <button class="btn btn-light  btn-xs sub-from-table pull-left mb-0 pb-0"
                                    id="3sub-td1"><i class="fa fa-minus"></i></button>
                            </div>
                        </div>
                    <td>
                        <div class="row">
                            <div class="col-md-12">
                                <button class="btn btn-success btn-block btn-xs PL01" id="pl03-2">PLO 03</button>
                                <button class="btn btn-light  btn-xs add-to-table pull-left mb-0 pb-0" id="3add-td2"><i
                                        class="fa fa-plus"></i></button>
                                <button class="btn btn-light  btn-xs sub-from-table pull-left mb-0 pb-0"
                                    id="3sub-td2"><i class="fa fa-minus"></i></button>
                            </div>
                        </div>


                    </td>
                    <td>
                        <div class="row">
                            <div class="col-md-12">
                                <button class="btn btn-success btn-block btn-xs PL01" id="pl03-3">PLO 03</button>
                                <button class="btn btn-light  btn-xs add-to-table pull-left mb-0 pb-0" id="3add-td3"><i
                                        class="fa fa-plus"></i></button>
                                <button class="btn btn-light  btn-xs sub-from-table pull-left mb-0 pb-0"
                                    id="3sub-td3"><i class="fa fa-minus"></i></button>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="row">
                            <div class="col-md-12">
                                <button class="btn btn-success btn-block btn-xs PL01" id="pl03-4">PLO 03</button>
                                <button class="btn btn-light  btn-xs add-to-table pull-left mb-0 pb-0" id="3add-td4"><i
                                        class="fa fa-plus"></i></button>
                                <button class="btn btn-light  btn-xs sub-from-table pull-left mb-0 pb-0"
                                    id="3sub-td4"><i class="fa fa-minus"></i></button>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="row">
                            <div class="col-md-12">
                                <button class="btn btn-success btn-block btn-xs PL01" id="pl03-5">PLO 03</button>
                                <button class="btn btn-light  btn-xs add-to-table pull-left mb-0 pb-0" id="3add-td5"><i
                                        class="fa fa-plus"></i></button>
                                <button class="btn btn-light  btn-xs sub-from-table pull-left mb-0 pb-0"
                                    id="3sub-td5"><i class="fa fa-minus"></i></button>
                            </div>
                        </div>
                    </td>
                    <td class="count-cell">1</td>

                </tr>
                <tr>
                    <th class="plo-no-cell" width="5%">PLO 04</th>
                    <td><button class="btn btn-sm btn-outline-warning text-muted pl-3 pr-3 plo" data-toggle="modal" data-target="#desc" >PLO Description
                            4</button></td>
                    <td>
                        <div class="row">
                            <div class="col-md-12">
                                <button class="btn btn-success btn-block btn-xs PL01" id="pl04-1">PLO 04</button>
                                <button class="btn btn-light  btn-xs add-to-table pull-left mb-0 pb-0" id="4add-td1"><i
                                        class="fa fa-plus"></i></button>
                                <button class="btn btn-light  btn-xs sub-from-table pull-left mb-0 pb-0"
                                    id="4sub-td1"><i class="fa fa-minus"></i></button>
                            </div>
                        </div>
                    <td>
                        <div class="row">
                            <div class="col-md-12">
                                <button class="btn btn-success btn-block btn-xs PL01" id="pl04-2">PLO 04</button>
                                <button class="btn btn-light  btn-xs add-to-table pull-left mb-0 pb-0" id="4add-td2"><i
                                        class="fa fa-plus"></i></button>
                                <button class="btn btn-light  btn-xs sub-from-table pull-left mb-0 pb-0"
                                    id="4sub-td2"><i class="fa fa-minus"></i></button>
                            </div>
                        </div>


                    </td>
                    <td>
                        <div class="row">
                            <div class="col-md-12">
                                <button class="btn btn-success btn-block btn-xs PL01" id="pl04-3">PLO 04</button>
                                <button class="btn btn-light  btn-xs add-to-table pull-left mb-0 pb-0" id="4add-td3"><i
                                        class="fa fa-plus"></i></button>
                                <button class="btn btn-light  btn-xs sub-from-table pull-left mb-0 pb-0"
                                    id="4sub-td3"><i class="fa fa-minus"></i></button>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="row">
                            <div class="col-md-12">
                                <button class="btn btn-success btn-block btn-xs PL01" id="pl04-4">PLO 04</button>
                                <button class="btn btn-light  btn-xs add-to-table pull-left mb-0 pb-0" id="4add-td4"><i
                                        class="fa fa-plus"></i></button>
                                <button class="btn btn-light  btn-xs sub-from-table pull-left mb-0 pb-0"
                                    id="4sub-td4"><i class="fa fa-minus"></i></button>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="row">
                            <div class="col-md-12">
                                <button class="btn btn-success btn-block btn-xs PL01" id="pl04-5">PLO 04</button>
                                <button class="btn btn-light  btn-xs add-to-table pull-left mb-0 pb-0" id="4add-td5"><i
                                        class="fa fa-plus"></i></button>
                                <button class="btn btn-light  btn-xs sub-from-table pull-left mb-0 pb-0"
                                    id="4sub-td5"><i class="fa fa-minus"></i></button>
                            </div>
                        </div>
                    </td>

                    <td class="count-cell">1</td>
                </tr>
                <tr>
                    <th class="plo-no-cell" width="5%">PLO 05</th>
                    <td><button class="btn btn-sm btn-outline-warning text-muted pl-3 pr-3 plo" data-toggle="modal" data-target="#desc">PLO Description
                            5</button></td>
                    <td>
                        <div class="row">
                            <div class="col-md-12">
                                <button class="btn btn-success btn-block btn-xs PL01" id="pl05-1">PLO 05</button>
                                <button class="btn btn-light  btn-xs add-to-table pull-left mb-0 pb-0"
                                    id="5add-td1"><i class="fa fa-plus"></i></button>
                                <button class="btn btn-light  btn-xs sub-from-table pull-left mb-0 pb-0"
                                    id="5sub-td1"><i class="fa fa-minus"></i></button>
                            </div>
                        </div>
                    <td>
                        <div class="row">
                            <div class="col-md-12">
                                <button class="btn btn-success btn-block btn-xs PL01" id="pl05-2">PLO 05</button>
                                <button class="btn btn-light  btn-xs add-to-table pull-left mb-0 pb-0"
                                    id="5add-td2"><i class="fa fa-plus"></i></button>
                                <button class="btn btn-light  btn-xs sub-from-table pull-left mb-0 pb-0"
                                    id="5sub-td2"><i class="fa fa-minus"></i></button>
                            </div>
                        </div>


                    </td>
                    <td>
                        <div class="row">
                            <div class="col-md-12">
                                <button class="btn btn-success btn-block btn-xs PL01" id="pl05-3">PLO 05</button>
                                <button class="btn btn-light  btn-xs add-to-table pull-left mb-0 pb-0"
                                    id="5add-td3"><i class="fa fa-plus"></i></button>
                                <button class="btn btn-light  btn-xs sub-from-table pull-left mb-0 pb-0"
                                    id="5sub-td3"><i class="fa fa-minus"></i></button>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="row">
                            <div class="col-md-12">
                                <button class="btn btn-success btn-block btn-xs PL01" id="pl05-4">PLO 05</button>
                                <button class="btn btn-light  btn-xs add-to-table pull-left mb-0 pb-0"
                                    id="5add-td4"><i class="fa fa-plus"></i></button>
                                <button class="btn btn-light  btn-xs sub-from-table pull-left mb-0 pb-0"
                                    id="5sub-td4"><i class="fa fa-minus"></i></button>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="row">
                            <div class="col-md-12">
                                <button class="btn btn-success btn-block btn-xs PL01" id="pl05-5">PLO 05</button>
                                <button class="btn btn-light  btn-xs add-to-table pull-left mb-0 pb-0"
                                    id="5add-td5"><i class="fa fa-plus"></i></button>
                                <button class="btn btn-light  btn-xs sub-from-table pull-left mb-0 pb-0"
                                    id="5sub-td5"><i class="fa fa-minus"></i></button>
                            </div>
                        </div>
                    </td>
                    <td class="count-cell">1</td>


                </tr>
                <tr>
                    <th colspan="2" class="tgc-count-cell">Count By TGC</th>
                    <td class="count-cell">1</td>
                    <td class="count-cell">1</td>
                    <td class="count-cell">1</td>
                    <td class="count-cell">1</td>
                    <td class="count-cell">1</td>
                    <td class="empty-cell"></td>
                </tr>
            </tbody>
        </table>
           
            <div class="col-md-12  text-right mr-0 pr-0">
                <button type="button" class="btn btn-primary btn-sm"><i class="fa fa-pencil mr-1"></i> Edit</button>
                <button type="button" class="btn btn-success btn-sm"><i class="fa fa-floppy-o mr-1"></i> Save </button>
                <button type="button" class="btn btn-info btn-sm "><i class="fa fa-print mr-1"></i> Print</button>
                <button type="button" class="btn btn-danger btn-sm "><i class="fa fa-sign-out mr-1"></i> Exit</button>                
            </div>


            <div class="card-footer text-center">
                <a class="btn btn-purple btn-sm btn-space" href="{{ URL::route('programme-new') }}"> <i
                        class="fa fa-arrow-left mr-3"></i>back to form</a>
            </div>
       
</div>


<!-- Modal -->
<div class="modal fade" id="desc" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myModal2"><i class="fa fa-paper"></i> <i class="fa fa-pagelines mr-2" aria-hidden="true"></i> Description</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla semper odio vel risus volutpat interdum. Ut non dignissim
                elit, et egestas tortor. Praesent ut justo nec diam consectetur accumsan aliquet quis neque. Pellentesque fringilla
                ligula id mauris posuere posuere. Mauris ultricies tortor erat, vitae consequat lacus bibendum ac.
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal"> <i class="fa fa-smile-o mr-2" aria-hidden="true"></i> ok</button>
               
            </div>
        </div>
    </div>
</div>
@endsection

@push('extraScripts')
<script src={{ asset('js/mapping-demo.js') }}></script>
@endpush