@extends('layouts.main.master')
@section('content')
<div class="card bg-light">
    <header class="card-header">
        <i class="fa fa-list mr-3"></i>Student Learning Time (SLT)
    </header>
    <div class="card-body">
        <table id="step10-table" class="table table-bordered table-small-text ">
            <tr class="text-center">
                <th rowspan="2">Week</th>
                <th class="add1" id="week1" data-toggle="modal" data-target="#additem">
                    +
                </th>
                <th class="add1" id="week2" data-toggle="modal" data-target="#additem">
                    +
                </th>
                <th class="add1" id="week3" data-toggle="modal" data-target="#additem">
                    +
                </th>
                <th class="add1" id="week4" data-toggle="modal" data-target="#additem">
                    +
                </th>
                <th class="add1" id="week5" data-toggle="modal" data-target="#additem">
                    +
                </th>
                <th class="add1" id="week6" data-toggle="modal" data-target="#additem">
                    +
                </th>
                <th width="20%">Student Learning Time(SLT)</th>
            </tr>
            <tr class="text-center">
                <td colspan="6" class="hour-cell">HOURS</td>
                <td>Weekly Hours</td>
            </tr>
            {{-- 1.=========== --}}
            <tr class="week-num">
                <td>week1</td>
                <td class="num-child"><input type="number" class="form-control borderless-input" id="inlineFormInput"
                        placeholder="123"></td>
                <td class="num-child"><input type="number" class="form-control borderless-input" id="inlineFormInput"
                        placeholder="123"></td>
                <td class="num-child"><input type="number" class="form-control borderless-input" id="inlineFormInput"
                        placeholder="123"></td>
                <td class="num-child"><input type="number" class="form-control borderless-input" id="inlineFormInput"
                        placeholder="123"></td>
                <td class="num-child"><input type="number" class="form-control borderless-input" id="inlineFormInput"
                        placeholder="123"></td>
                <td class="num-child"><input type="number" class="form-control borderless-input" id="inlineFormInput"
                        placeholder="123"></td>
                <td class="addtotal"></td>
            </tr>
            <tr>
                <td width="10%"><input type="text" class="form-control borderless-input" id="inlineFormInput"
                        placeholder="Content Online"></td>
                <td rowspan="2"><input type="text" class="form-control borderless-input" id="inlineFormInput"
                        placeholder="sample text"></td>
                <td rowspan="2"><input type="text" class="form-control borderless-input" id="inlineFormInput"
                        placeholder="sample text"></td>
                <td rowspan="2"><input type="text" class="form-control borderless-input" id="inlineFormInput"
                        placeholder="sample text"></td>
                <td rowspan="2"><input type="text" class="form-control borderless-input" id="inlineFormInput"
                        placeholder="sample text"></td>
                <td rowspan="2"><input type="text" class="form-control borderless-input" id="inlineFormInput"
                        placeholder="sample text"></td>
                <td rowspan="2"><input type="text" class="form-control borderless-input" id="inlineFormInput"
                        placeholder="sample text"></td>
                <td></td>
            </tr>
            <tr class="text-center MLO">
                <td id="mlo1" class="">
                    <span id="mlotext0">MLO 1 </span>
                    <button id="dropmlo1" class="addmlo btn btn-purple btn-xs btn-block" data-toggle="modal"
                        data-target="#dropdownMLO">Add
                        MLO
                    </button>
                </td>
                <td></td>
            </tr>
            {{-- 2.=========== --}}
            <tr class="week-num">
                <td>week2</td>
                <td class="num-child"><input type="number" class="form-control borderless-input" id="inlineforminput"
                        placeholder="123"></td>
                <td class="num-child"><input type="number" class="form-control borderless-input" id="inlineforminput"
                        placeholder="123"></td>
                <td class="num-child"><input type="number" class="form-control borderless-input" id="inlineforminput"
                        placeholder="123"></td>
                <td class="num-child"><input type="number" class="form-control borderless-input" id="inlineforminput"
                        placeholder="123"></td>
                <td class="num-child"><input type="number" class="form-control borderless-input" id="inlineforminput"
                        placeholder="123"></td>
                <td class="num-child"><input type="number" class="form-control borderless-input" id="inlineforminput"
                        placeholder="123"></td>
               <td class="addtotal"></td>
            </tr>
            <tr>
                <td width="10%"><input type="text" class="form-control borderless-input" id="inlineFormInput"
                        placeholder="Content Online"></td>
                <td rowspan="2"><input type="text" class="form-control borderless-input" id="inlineforminput"
                        placeholder="sample text"></td>
                <td rowspan="2"><input type="text" class="form-control borderless-input" id="inlineforminput"
                        placeholder="sample text"></td>
                <td rowspan="2"><input type="text" class="form-control borderless-input" id="inlineforminput"
                        placeholder="sample text"></td>
                <td rowspan="2"><input type="text" class="form-control borderless-input" id="inlineforminput"
                        placeholder="sample text"></td>
                <td rowspan="2"><input type="text" class="form-control borderless-input" id="inlineforminput"
                        placeholder="sample text"></td>
                <td rowspan="2"><input type="text" class="form-control borderless-input" id="inlineforminput"
                        placeholder="sample text"></td>
                <td></td>
            </tr>
            <tr class="text-center MLO">
                <td class="" id="mlo2">
                    <span id="mlotext2" class="mlotext">MLO 2 </span>
                    <div class="dropdown" id="dropmlo2">
                        <button id="dropmlo2" class="addmlo btn btn-purple btn-xs btn-block" data-toggle="modal"
                            data-target="#dropdownMLO">Add MLO
                        </button>
                    </div>
                </td>
                <td></td>
            </tr>
            {{-- 3.=========== --}}
            <tr class="week-num">
                <td>week3</td>
                <td class="num-child"><input type="number" class="form-control borderless-input" id="inlineforminput"
                        placeholder="123"></td>
                <td class="num-child"><input type="number" class="form-control borderless-input" id="inlineforminput"
                        placeholder="123"></td>
                <td class="num-child"><input type="number" class="form-control borderless-input" id="inlineforminput"
                        placeholder="123"></td>
                <td class="num-child"><input type="number" class="form-control borderless-input" id="inlineforminput"
                        placeholder="123"></td>
                <td class="num-child"><input type="number" class="form-control borderless-input" id="inlineforminput"
                        placeholder="123"></td>
                <td class="num-child"><input type="number" class="form-control borderless-input" id="inlineforminput"
                        placeholder="123"></td>
               <td class="addtotal"></td>
            </tr>
            <tr>
                <td width="10%"><input type="text" class="form-control borderless-input" id="inlineFormInput"
                        placeholder="Content Online"></td>
                <td rowspan="2"><input type="text" class="form-control borderless-input" id="inlineforminput"
                        placeholder="sample text"></td>
                <td rowspan="2"><input type="text" class="form-control borderless-input" id="inlineforminput"
                        placeholder="sample text"></td>
                <td rowspan="2"><input type="text" class="form-control borderless-input" id="inlineforminput"
                        placeholder="sample text"></td>
                <td rowspan="2"><input type="text" class="form-control borderless-input" id="inlineforminput"
                        placeholder="sample text"></td>
                <td rowspan="2"><input type="text" class="form-control borderless-input" id="inlineforminput"
                        placeholder="sample text"></td>
                <td rowspan="2"><input type="text" class="form-control borderless-input" id="inlineforminput"
                        placeholder="sample text"></td>
                <td></td>
            </tr>
            <tr class="text-center MLO">
                <td class="" id="mlo3">
                    <span id="mlotext3" class="mlotext">MLO 3 </span>
                    <div class="dropdown" id="dropmlo3">
                        <button id="dropmlo3" class="addmlo btn btn-purple btn-xs btn-block" data-toggle="modal"
                            data-target="#dropdownMLO">Add MLO
                        </button>
                    </div>
                </td>
                <td></td>
            </tr>
            {{-- 4.=========== --}}
            <tr class="week-num">
                <td>week4</td>
                <td class="num-child"><input type="number" class="form-control borderless-input" id="inlineforminput"
                        placeholder="123"></td>
                <td class="num-child"><input type="number" class="form-control borderless-input" id="inlineforminput"
                        placeholder="123"></td>
                <td class="num-child"><input type="number" class="form-control borderless-input" id="inlineforminput"
                        placeholder="123"></td>
                <td class="num-child"><input type="number" class="form-control borderless-input" id="inlineforminput"
                        placeholder="123"></td>
                <td class="num-child"><input type="number" class="form-control borderless-input" id="inlineforminput"
                        placeholder="123"></td>
                <td class="num-child"><input type="number" class="form-control borderless-input" id="inlineforminput"
                        placeholder="123"></td>
               
                <td class="addtotal"></td>
            </tr>
            <tr>
                <td width="10%"><input type="text" class="form-control borderless-input" id="inlineFormInput"
                        placeholder="Content Online"></td>
                <td rowspan="2"><input type="text" class="form-control borderless-input" id="inlineforminput"
                        placeholder="sample text"></td>
                <td rowspan="2"><input type="text" class="form-control borderless-input" id="inlineforminput"
                        placeholder="sample text"></td>
                <td rowspan="2"><input type="text" class="form-control borderless-input" id="inlineforminput"
                        placeholder="sample text"></td>
                <td rowspan="2"><input type="text" class="form-control borderless-input" id="inlineforminput"
                        placeholder="sample text"></td>
                <td rowspan="2"><input type="text" class="form-control borderless-input" id="inlineforminput"
                        placeholder="sample text"></td>
                <td rowspan="2"><input type="text" class="form-control borderless-input" id="inlineforminput"
                        placeholder="sample text"></td>
                        
                <td></td>
            </tr>
            <tr class="text-center MLO">
                <td class="" id="mlo4">
                    <span id="mlotext4" class="mlotext">MLO 3 </span>
                    <div class="dropdown" id="dropmlo4">
                        <button id="dropmlo4" class="addmlo btn btn-purple btn-xs btn-block" data-toggle="modal"
                            data-target="#dropdownMLO">Add MLO
                        </button>
                    </div>
                </td>
                <td></td>
            </tr>
            {{-- 5.=========== --}}
            <tr class="week-num">
                <td>week5</td>
                <td class="num-child"><input type="number" class="form-control borderless-input" id="inlineforminput"
                        placeholder="123">
                </td>
                <td class="num-child"><input type="number" class="form-control borderless-input" id="inlineforminput"
                        placeholder="123">
                </td>
                <td class="num-child"><input type="number" class="form-control borderless-input" id="inlineforminput"
                        placeholder="123">
                </td>
                <td class="num-child"><input type="number" class="form-control borderless-input" id="inlineforminput"
                        placeholder="123">
                </td>
                <td class="num-child"><input type="number" class="form-control borderless-input" id="inlineforminput"
                        placeholder="123">
                </td>
                <td class="num-child"><input type="number" class="form-control borderless-input" id="inlineforminput" placeholder="123">
                </td>
                <td class="addtotal"></td>
            </tr>
            <tr>
                <td width="10%"><input type="text" class="form-control borderless-input" id="inlineFormInput"
                        placeholder="Content Online"></td>
                <td rowspan="2"><input type="text" class="form-control borderless-input" id="inlineforminput"
                        placeholder="sample text"></td>
                <td rowspan="2"><input type="text" class="form-control borderless-input" id="inlineforminput"
                        placeholder="sample text"></td>
                <td rowspan="2"><input type="text" class="form-control borderless-input" id="inlineforminput"
                        placeholder="sample text"></td>
                <td rowspan="2"><input type="text" class="form-control borderless-input" id="inlineforminput"
                        placeholder="sample text"></td>
                <td rowspan="2"><input type="text" class="form-control borderless-input" id="inlineforminput"
                        placeholder="sample text"></td>
                <td rowspan="2"><input type="text" class="form-control borderless-input" id="inlineforminput"
                        placeholder="sample text"></td>
                <td></td>
            </tr>
            <tr class="text-center MLO">
                <td class="" id="mlo5">
                    <span id="mlotext5" class="mlotext">MLO 2 </span>
                    <div class="dropdown" id="dropmlo5">
                        <button id="dropmlo5" class="addmlo btn btn-purple btn-xs btn-block" data-toggle="modal"
                            data-target="#dropdownMLO">Add MLO
                        </button>
                    </div>
                </td>
                <td></td>
            </tr>

            <tr class="step10total-cell">
                <td class="text-right">Total</td>
                <td colspan="7" class="totalall"> </td>
            </tr>
        </table>


        <div class="col-md-12  text-right mr-0 pr-0">

            <button type="button" class="btn btn-success btn-sm" id="savetable10"> <i class="fa fa-floppy-o mr-1"></i> Save </button>
            <button type="button" class="btn btn-info btn-sm " data-toggle="modal" data-target="#modal-ok"><i
                    class="fa fa-print mr-1"></i> OK</button>

        </div>
    </div>
</div>


<!-- m o  d a l -->
<div class="modal fade " id="additem" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Please Select</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="custom-control custom-checkbox mb-6">
                    <input type="checkbox" class="custom-control-input" id="checkbox1">
                    <label class="custom-control-label" for="checkbox1">Guided Learning F2F (Lecture)</label>
                </div>
                <div class="custom-control custom-checkbox mb-6">
                    <input type="checkbox" class="custom-control-input" id="checkbox2">
                    <label class="custom-control-label" for="checkbox2">Guided Learning F2F (Tutorial)</label>
                </div>
                <div class="custom-control custom-checkbox mb-6">
                    <input type="checkbox" class="custom-control-input" id="checkbox3">
                    <label class="custom-control-label" for="checkbox3">Guided Learning F2F (Practical)</label>
                </div>
                <div class="custom-control custom-checkbox mb-6">
                    <input type="checkbox" class="custom-control-input" id="checkbox4">
                    <label class="custom-control-label" for="checkbox4">Guided Learning F2F (Other)</label>
                </div>
                <div class="custom-control custom-checkbox mb-6">
                    <input type="checkbox" class="custom-control-input" id="checkbox5">
                    <label class="custom-control-label" for="checkbox5">Guided Learning NF2F/Online</label>
                </div>
                <div class="custom-control custom-checkbox mb-6">
                    <input type="checkbox" class="custom-control-input" id="checkbox6">
                    <label class="custom-control-label" for="checkbox6">Independent Student Learning Time</label>
                </div>
                <div class="custom-control custom-checkbox mb-6">
                    <input type="checkbox" class="custom-control-input" id="checkbox7">
                    <label class="custom-control-label" for="checkbox7">Assessment Task (F2F)</label>
                </div>
                <div class="custom-control custom-checkbox mb-6">
                    <input type="checkbox" class="custom-control-input" id="checkbox8">
                    <label class="custom-control-label" for="checkbox8">Assessment Task (Online)</label>
                </div>
                <div class="custom-control custom-checkbox mb-6">
                    <input type="checkbox" class="custom-control-input" id="checkbox9">
                    <label class="custom-control-label" for="checkbox9">Assessment Task (Independent SLT)</label>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="okadditem">ok</button>
            </div>
        </div>
    </div>
</div>


{{-- dropdown  --}}
<div class="modal fade " id="dropdownMLO" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Module Learning Outcomes
                    (MLO)</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-border table-hover" style="font-size:12px">
                    <tbody>
                        <tr>
                            <td>MLO 1</td>
                            <td class="hidden-phone">Lorem ipsum…</td>
                            <td class="text-right">
                                <a class="btn btn-success btn-sm mloitem" href="" id="plo1-select" data-dismiss="modal">
                                    Select <i class="fa fa-table ml-2" aria-hidden="true"></i></a>

                            </td>
                        </tr>
                        <tr>
                            <td>MLO 2</td>
                            <td class="hidden-phone">Lorem ipsum…</td>
                            <td class="text-right">
                                <a class="btn btn-success btn-sm mloitem" href="" id="plo2-select" data-dismiss="modal">
                                    Select <i class="fa fa-table ml-2" aria-hidden="true"></i></a>
                            </td>
                        </tr>
                        <tr>
                            <td>MLO 3</td>
                            <td class="hidden-phone">Lorem ipsum…</td>
                            <td class="text-right">
                                <a class="btn btn-success btn-sm mloitem" href="" id="plo3-select" data-dismiss="modal">
                                    Select <i class="fa fa-table ml-2" aria-hidden="true"></i></a>
                            </td>
                        </tr>
                        <tr>
                            <td>MLO 4</td>
                            <td class="hidden-phone">Lorem ipsum…</td>
                            <td class="text-right">
                                <a class="btn btn-success btn-sm mloitem" href="#" id="plo4-select"
                                    data-dismiss="modal">
                                    Select <i class="fa fa-table ml-2" aria-hidden="true"></i></a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer text-center">


            </div>
        </div>

    </div>
</div>
 {{-- ok --}}
<div class="modal fade " id="modal-ok" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Submit <i class="fa fa-paper-plane"></i></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body form hovering ld-over-inverse" id="test">

                <div id="message">
                    <div class="text-center">
                        <div class="i-circle text-warning"><i class="icon fa fa-exclamation-triangle"
                                aria-hidden="true"></i></div>
                        <h4>Are you sure ?</h4>
                        <p> You want to submit this Updated for Student Learning Time (SLT)? </p>
                    </div>
                    <div class="form-group text-center">
                        <div class="btn btn-primary btn-sm btn-rounded btn-space hovering ld-over-inverse"
                            id="importData">
                            Yes
                            {{-- <div class="ld ld-ball ld-flip"></div> --}}
                        </div>
                    </div>
                </div>
                <div id="import-msg">
                    <div class="text-center">
                        <div class="i-circle text-success">
                            <i class="icon fa fa-check"></i>
                        </div>
                        <h4>Sent!</h4>
                        <p> Programme submitted</p>
                        <div class="form-group text-center">
                            <a class="btn btn-primary btn-sm btn-rounded btn-space"
                                href="{{ URL::route('module-info.edit') }}">
                                close
                            </a>
                        </div>
                    </div>
                </div>
                <div class="ld ld-ball ld-flip"></div>




            </div>
        </div>
    </div>
</div>

{{-- save  --}}


@endsection

@push('extraScripts')
<script src={{ asset('js/mapping-demo.js') }}></script>
@endpush