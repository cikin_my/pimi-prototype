<ul class="sidebar-menu" id="nav-accordion">
    <li>
        <a class="active" href="{{ URL::route('dashboard') }}">
            <i class="fa fa-dashboard"></i>
            <span>Dashboard</span>
        </a>
    </li>

    <li class="sub-menu parent ">
        <a href="javascript:;">
            <i class="fa fa-user"></i>
            <span>User & Role <br> Management</span>
        </a>
        <ul class="sub">  
            <li class="{{ Request::is('role') ? 'active' : '' }}"><a href="{{ URL::route('role') }}">Role Management </a></li>   
            <div class="divider"></div>    
            <li class="{{ Request::is('user') ? 'active' : '' }}"><a href="{{ URL::route('user') }}">General User Management </a></li>                  
            <li class="{{ Request::is('pd') ? 'active' : '' }}"><a href="{{ URL::route('pd') }}">PD Management</a></li>  
            <li class="{{ Request::is('ml') ? 'active' : '' }}"><a href="{{ URL::route('ml') }}">ML Management</a></li>
            <li class="{{ Request::is('hos') ? 'active' : '' }}"><a href="{{ URL::route('hos') }}">HOS Management</a></li>
            <li class="{{ Request::is('ed') ? 'active' : '' }}"><a href="{{ URL::route('ed') }}">ED Management</a></li> 
        </ul>
    </li>
    <li class="sub-menu parent">
        <a href="javascript:;">
            <i class="fa fa-wrench"></i>
            <span>System Setup</span>
        </a>
        <ul class="sub">           
            <li><a href="{{ URL::route('tgc') }}"><span>TGC Attributes & Sub-Attributes <span>  </a>
            </li>
            <li><a href="{{ URL::route('mqf') }}"> MQF Learning Outcome Domains</a></li>
            <li><a href="{{ URL::route('mapping') }}"> TGC-MQF Mapping</a></li>
            <li><a href="{{ URL::route('nec') }}"> NEC Codes</a></li>
            <li><a href="{{ URL::route('discipline') }}"> Discipline Codes</a></li>
        </ul>
    </li>

    <li class="sub-menu parent">
        <a href="javascript:;">
            <i class="fa fa-bookmark"></i>
            <span>Programme <br>Management</span>
        </a>
        <ul class="sub">
            <li><a href="{{ URL :: route('programme-info') }}">Programme Info Management</a></li>
            <li><a href="{{ URL :: route('module-info') }}">Module Info Management</a></li>          
        </ul>
    </li>

    <li class="sub-menu parent">
        <a href="javascript:;">
            <i class="fa fa-list-alt"></i>
            <span>view Programme<br> Information</span>
        </a>
        <ul class="sub">
            <li><a href="{{ URL :: route('search-programme') }}">Programme Search</a></li>
            <li><a href="{{ URL :: route('search-module') }}">Module Search</a></li>         
        </ul>
    </li>
   <li>
        <a href={{ URL :: route('review') }}>
            <i class="fa fa-check-square-o"></i>
            <span>Approvals & Review</span>
        </a>
    </li>
    <li>
        <a href="{{ URL::route('tracking') }}">
            <i class="fa fa-tasks"></i>
            <span>Tracking Programme <br> Change</span>
        </a>
    </li>    
    <li>
        <a href="">
            <i class="fa fa-flag"></i>
            <span>Reportings</span>
        </a>
    </li>

    <li>
        <a href="{{ URL :: route('setting') }}">
            <i class="fa fa-cogs"></i>
            <span>Settings</span>
        </a>
    </li>

    <li>
        <a href="login.html">
            <i class="fa fa-clock-o"></i>
            <span>Audit Trail</span>
        </a>
    </li>

    <!--multi level menu end-->

</ul>