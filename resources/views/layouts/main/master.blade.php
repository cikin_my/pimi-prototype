<!DOCTYPE html>
<html lang="en">
@include('partials.head')

<body class="light-sidebar-nav">

    <section id="container">

        @include('layouts.main.header')
        @include('layouts.main.sidebar')

        <!--main content start-->
        <section id="main-content">
            <section class="wrapper">
            @yield('content')
            </section>
        </section>
        <!--main content end-->

        @include('layouts.main.rightsidebar')
        @include('layouts.main.footer')
    </section>
    @include('partials.script')

</body>
</html>
