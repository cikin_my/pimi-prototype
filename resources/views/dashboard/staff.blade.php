@extends('layouts.main.master')

@section('content')
<div class="container">
    <section class="error-wrapper">
        <i class="icon-404"></i>
        <h1>Coming Soon</h1>
        <h2>The page you are looking for are still under construction.</h2>
        <p class="text-muted">Would you like to go <a class="text-danger" href="/">home?</a></p>
    </section>
</div>
@endsection

@push('extraScripts')

@endpush