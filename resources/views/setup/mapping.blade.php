@extends('layouts.main.master')
@section('content')
<section class="card">
    <header class="card-header">
       TGC-MQF Mapping
    </header>
    <div class="card-body">
        <table class="table table-bordered table-condensed  text-center">
            <thead>
                <tr>
                    <td>ID</td>
                    <td>Version No.</td>
                    <td>Date</td>
                    <td>View</td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>1</td>
                    <td>1.0</td>
                    <td>10 June 2019</td>
                    <td>
                        <a class="btn btn-primary btn-sm" href="{{ URL::route('mapping-table') }}">
                            View Mapping <i class="fa fa-eye ml-2" aria-hidden="true"></i></a>
                    </td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>1.2</td>
                    <td>10 June 2019</td>
                    <td>
                        <a class="btn btn-primary btn-sm" href="{{ URL::route('mapping-table') }}">
                            View Mapping <i class="fa fa-eye ml-2" aria-hidden="true"></i></a>
                    </td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>3.0</td>
                    <td>15 July 2019</td>
                    <td>
                        <a class="btn btn-primary btn-sm" href="{{ URL::route('mapping-table') }}">
                            View Mapping <i class="fa fa-eye ml-2" aria-hidden="true"></i></a>
                    </td>
                </tr>
                <tr>
                    <td>4</td>
                    <td>4.0</td>
                    <td>24 July 2019</td>
                    <td>
                        <a class="btn btn-primary btn-sm" href="{{ URL::route('mapping-table') }}">
                            View Mapping <i class="fa fa-eye ml-2" aria-hidden="true"></i></a>
                    </td>
                </tr>
                <tr>
                    <td>5</td>
                    <td>5.0</td>
                    <td>30 July 2019</td>
                    <td>
                        <a class="btn btn-primary btn-sm" href="{{ URL::route('mapping-table') }}">
                            View Mapping <i class="fa fa-eye ml-2" aria-hidden="true"></i></a>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    {{-- <div class="card-footer text-muted text-center">       
    </div> --}}
</section>

@endsection