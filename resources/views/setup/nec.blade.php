@extends('layouts.main.master')
@section('content')
<section class="card">
    <header class="card-header">
        NEC Codes
    </header>
    <div class="card-body">
        <table class="table table-bordered table-condensed table-hover tgc">
            <thead>
                <tr>
                    <th width="12%" class="text-center">NEC CODE</th>
                    <th>DESCRIPTION</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="col-item">221</td>
                    <td>Religion</td>
                </tr>
                <tr>
                    <td class="col-item">222</td>
                    <td>Languages = 223+224,</td>
                </tr>
                <tr>
                    <td class="col-item">223</td>
                    <td>National Language,</td>
                </tr>
                <tr>
                    <td class="col-item">224</td>
                    <td>Other languages,</td>
                </tr>
                <tr>
                    <td class="col-item">225</td>
                    <td>History and archaeology,</td>
                </tr>
                <tr>
                    <td class="col-item">226</td>
                    <td>Philosophy and ethics,</td>
                </tr>
                <tr>
                    <td class="col-item">227</td>
                    <td>History, philosophy and related subjects = 225+226,</td>
                </tr>
                <tr>
                    <td class="col-item">80</td>
                    <td>Literacy and numeracy,</td>
                </tr>
                <tr>
                    <td class="col-item">90</td>
                    <td>Personal skills,</td>
                </tr>
                <tr>
                    <td class="col-item">141</td>
                    <td>Teaching and training = 143 + 144 + 145 + 146,</td>
                </tr>
                <tr>
                    <td class="col-item">142</td>
                    <td>Education science</td>
                </tr>
                <tr>
                    <td class="col-item">143</td>
                    <td>Training for preschool teachers</td>
                </tr>
                <tr>
                    <td class="col-item">144</td>
                    <td>Training for teachers at basic levels</td>
                </tr>
                <tr>
                    <td class="col-item">145</td>
                    <td>Training for teachers with subject specialisation</td>
                </tr>
                <tr>
                    <td class="col-item">146</td>
                    <td>Training for teachers of vocational subjects</td>
                </tr>
                <tr>
                    <td class="col-item">211</td>
                    <td>Fine arts</td>
                </tr>
                <tr>
                    <td class="col-item">212</td>
                    <td>Music and performing arts</td>
                </tr>
                <tr>
                    <td class="col-item">213</td>
                    <td>Audio-visual techniques and media production</td>
                </tr>
                <tr>
                    <td class="col-item">214</td>
                    <td>Design</td>
                </tr>
                <tr>
                    <td class="col-item">215</td>
                    <td>Craft skills</td>
                </tr>
            </tbody>
        </table>

       
    </div>
  
</section>

@endsection