<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
               <div class="modal-header">
                <h5 class="modal-title" id="myModal">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-border table-hover" style="font-size:12px">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Version No.</th>
                            <th>Date</th>
                            <th class="text-center" width="15%">MQF Level</th>
                            <th width="30%" class="text-center">View</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1.</td>
                            <td class="hidden-phone">1.0</td>
                            <td class="text-left">20 August 2019 </td>
                            <td class="text-center">1</td>
                            <td class="text-right">
                                <a class="btn btn-success btn-sm" href="{{ URL::route('tgc-table') }}">
                                    click to view TGC2 table <i class="fa fa-table ml-2" aria-hidden="true"></i></a>
            
                            </td>
                        </tr>
                        <tr>
                            <td>2.</td>
                            <td class="hidden-phone">1.5</td>
                            <td class="text-left">2 September 2019 </td>
                            <td class="text-center">1</td>
                            <td class="text-right">
                                <a class="btn btn-success btn-sm" href="{{ URL::route('tgc-table') }}">
                                    click to view TGC2 table <i class="fa fa-table ml-2" aria-hidden="true"></i></a>
                            </td>
                        </tr>
                        <tr>
                            <td>3.</td>
                            <td class="hidden-phone">2.0</td>
                            <td class="text-left">22 September 2019 </td>
                            <td class="text-center">3</td>
                            <td class="text-right">
                                <a class="btn btn-success btn-sm" href="{{ URL::route('tgc-table') }}">
                                    click to view TGC2 table <i class="fa fa-table ml-2" aria-hidden="true"></i></a>
                            </td>
                        </tr>
                        <tr>
                            <td>4.</td>
                            <td class="hidden-phone">1.0</td>
                            <td class="text-left">10 October 2019 </td>
                            <td class="text-center">1</td>
                            <td class="text-right">
                                <a class="btn btn-success btn-sm" href="{{ URL::route('tgc-table') }}">
                                    click to view TGC2 table <i class="fa fa-table ml-2" aria-hidden="true"></i></a>
                            </td>
                        </tr>
                        <tr>
                            <td>5.</td>
                            <td class="hidden-phone">3.0</td>
                            <td class="text-left">20 August 2019 </td>
                            <td class="text-center">4</td>
                            <td class="text-right">
                                <a class="btn btn-success btn-sm" href="{{ URL::route('tgc-table') }}">
                                    click to view TGC2 table <i class="fa fa-table ml-2" aria-hidden="true"></i></a>
                            </td>
                        </tr>
            
            
            
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>