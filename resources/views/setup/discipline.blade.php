@extends('layouts.main.master')
@section('content')
<section class="card">
    <header class="card-header">
        Discipline Codes
    </header>
    <div class="card-body">
        <table class="table table-hover table-condensed table-bordered tgc">
            <thead>
                <tr>
                    <th width="12%">DISCIPLINE CODE</th>
                    <th>DISCIPLINE DESCRIPTION</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th class="col-item">AUD</th>
                    <th>Audio-visual techniques and media production</th>
                </tr>
                <tr>
                    <td class="col-item">CRA</td>
                    <td>Craft skills</td>
                </tr>
                <tr>
                    <td class="col-item">DES</td>
                    <td>Design</td>
                </tr>
                <tr>
                    <td class="col-item">EDU</td>
                    <td>Education science</td>
                </tr>
                <tr>
                    <td class="col-item">ART</td>
                    <td>Fine arts</td>
                </tr>
                <tr>
                    <td class="col-item">HIS</td>
                    <td>History and archaeology</td>
                </tr>
                <tr>
                    <td class="col-item">HIS</td>
                    <td>History, philosophy and related subjects = 225+226</td>
                </tr>
                <tr>
                    <td class="col-item">LAN</td>
                    <td>Languages = 223+224</td>
                </tr>
                <tr>
                    <td class="col-item">LAN</td>
                    <td>Literacy and numeracy</td>
                </tr>
                <tr>
                    <td class="col-item">ART</td>
                    <td>Music and performing arts</td>
                </tr>
                <tr>
                    <td class="col-item">LAN</td>
                    <td>National Language</td>
                </tr>
                <tr>
                    <td class="col-item">LAN</td>
                    <td>Other languages</td>
                </tr>
                <tr>
                    <td class="col-item">PER</td>
                    <td>Personal skills</td>
                </tr>
                <tr>
                    <td class="col-item">PHI</td>
                    <td>Philosophy and ethics</td>
                </tr>
                <tr>
                    <td class="col-item">REL</td>
                    <td>Religion</td>
                </tr>
                <tr>
                    <td class="col-item">TRA</td>
                    <td>Teaching and training = 143 + 144 + 145 + 146</td>
                </tr>
                <tr>
                    <td class="col-item">TRA</td>
                    <td>Training for preschool teachers</td>
                </tr>
                <tr>
                    <td class="col-item">TRA</td>
                    <td>Training for teachers at basic levels</td>
                </tr>
                <tr>
                    <td class="col-item">TRA</td>
                    <td>Training for teachers of vocational subjects</td>
                </tr>
                <tr>
                    <td class="col-item">TRA</td>
                    <td>Training for teachers with subject specialisation</td>
                </tr>
            </tbody>
        </table>
    </div>

</section>

@endsection