@extends('layouts.main.master')
@section('content')
<section class="card">
    <header class="card-header">
       TGC Attributes & Sub-Attributes
    </header>
    <div class="card-body">
        <table class="table table-border table-condensed">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Version No.</th>
                    <th>Date</th>
                    <th class="text-center" width="10%">MQF Level</th>
                    <th width="20%" class="text-center">View</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>1.</td>
                    <td class="hidden-phone">1.0</td>
                    <td class="text-left">20 August 2019 </td>
                    <td class="text-center">1</td>
                    <td class="text-right">
                        <a class="btn btn-success btn-sm" href="{{ URL::route('tgc-table') }}">
                            click to view TGC2 table <i class="fa fa-table ml-2" aria-hidden="true"></i></a>
        
                    </td>
                </tr>
                <tr>
                    <td>2.</td>
                    <td class="hidden-phone">1.5</td>
                    <td class="text-left">2 September 2019 </td>
                    <td class="text-center">1</td>
                    <td class="text-right">
                       <a class="btn btn-success btn-sm" href="{{ URL::route('tgc-table') }}">
                            click to view TGC2 table <i class="fa fa-table ml-2" aria-hidden="true"></i></a>
                    </td>
                </tr>
                <tr>
                    <td>3.</td>
                    <td class="hidden-phone">2.0</td>
                    <td class="text-left">22 September 2019 </td>
                    <td class="text-center">3</td>
                    <td class="text-right">
                       <a class="btn btn-success btn-sm" href="{{ URL::route('tgc-table') }}">
                            click to view TGC2 table <i class="fa fa-table ml-2" aria-hidden="true"></i></a>
                    </td>
                </tr>
                <tr>
                    <td>4.</td>
                    <td class="hidden-phone">1.0</td>
                    <td class="text-left">10 October 2019 </td>
                    <td class="text-center">1</td>
                    <td class="text-right">
                        <a class="btn btn-success btn-sm" href="{{ URL::route('tgc-table') }}">
                            click to view TGC2 table <i class="fa fa-table ml-2" aria-hidden="true"></i></a>
                    </td>
                </tr>
                <tr>
                    <td>5.</td>
                    <td class="hidden-phone">3.0</td>
                    <td class="text-left">20 August 2019 </td>
                    <td class="text-center">4</td>
                    <td class="text-right">
                     <a class="btn btn-success btn-sm" href="{{ URL::route('tgc-table') }}">
                        click to view TGC2 table <i class="fa fa-table ml-2" aria-hidden="true"></i></a>
                    </td>
                </tr>
        
        
        
            </tbody>
        </table>
    </div>
</section>


    
@endsection