@extends('layouts.main.master')
@section('content')

<section class="card">
    <header class="card-header">
        MQF LOs View
    </header>
    <div class="card-body">
        <table class="table table-condensed table-bordered tgc" style="font-size:.85em;" id="tgc">  
            <tbody>
                <tr>
                    <td>MQF1</td>
                    <td colspan="2">Knowledge and Understanding</td>
                </tr>
                <tr>
                    <td>MQF2</td>
                    <td colspan="2">Cognitive Skills</td>
                </tr>
                <tr>
                    <td rowspan="4">MQF3</td>
                    <td colspan="2">Functional Work Skills</td>
                </tr>
                <tr>
                    <td class="text-right sub-b" width="5%">3a</td>
                    <td class="sub-b">Practical skills</td>
                </tr>
                <tr>
                    <td class="text-right sub-b" width="5%">3b</td>
                    <td class="sub-b">Interpersonal skills</td>
                </tr>
                <tr>
                    <td class="text-right sub-b" width="5%">3c</td>
                    <td class="sub-b">Digital skills</td>
                </tr>
                <tr>
                    <td>Etc.</td>
                    <td colspan="2"></td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="card-footer text-muted text-center">
        <a href="{{ URL::route('mqf') }}" class="btn btn-primary btn-sm pr-2"><i class="fa fa-long-arrow-left mr-2 ml-2"
                aria-hidden="true"></i> Back</a>
    </div>



</section>

@endsection