@extends('layouts.main.master')
@section('content')
<section class="card">
    <header class="card-header">
       Mapping View
    </header>
    <div class="card-body">
        <table class="table table-bordered table-condensed table-hover tgc " >
            <thead>
                <tr>                   
                    <th colspan="3" width="25%" class="text-center">MQF Learning Outcome Domain</th>
                    <th colspan="2" class="text-center">TGC-Focused curriculum learning outcomes</th>
                    
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>1</td>
                    <td colspan="2">Knowledge and understanding</td>
                    <td colspan="2">
                        <span class="text-danger"><i>(TGC Attribute Title)</i></span> Dicipline Specific Knowledge<br>
                        <span class="text-danger"><i>(TGC Attribute Description)</i></span> The ability to demonstrate professional competence, 
                            comprehend and adapt dicipline specific knowledge and be able to intergrate knowledge across diverse perspectives
                    </td>
                </tr>
                <tr>
                    <td>2</td>
                    <td colspan="2">Cognitive skills</td>
                    <td colspan="2">
                        <span class="badge badge-primary">Problem solving, critical and creativce thinking skills</span><br>
                        The ability to rationally, critically and creatively analyze,synthesize and evaluate evidences to arrive at solutions or conclution.
                    </td>
                </tr>
                <tr>
                    <td>3</td>
                    <td colspan="5">Functional work skills with focus on:</td>
                </tr>
                <tr>
                    <td></td>
                    <td class="sub-c">a</td>
                    <td class="sub-c2 text-info" width="20%">Practical Skills</td>
                    <td class="sub-c2">
                        <span class="text-danger"><i>(TGC Attribute Title - TGC2, not 2a or 2b)</i></span><br>
                        <span class="badge badge-info">Problem solving, critical and creativce thinking skills</span><br>
                        <span class="text-danger"><i>(TGC Attribute Description)</i></span><br>
                        The ability to rationally, critically and creatively analyze,synthesize and evaluate evidences
                        to arrive at solutions or
                        conclution.
                        <br>
                    </td>

                </tr>
                <tr>
                    <td></td>
                    <td class="sub-c">b</td>
                    <td class="sub-c2 text-info">Interpersonal Skills</td>
                    <td class="sub-c2">
                        <span class="badge badge-info">Social Competencies</span><br>
                        The ability to empathize with others, interact positively with them and foster a stable and harmonious relationships.
                        <br>
                       
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td class="sub-c">c</td>
                    <td class="sub-c2 text-info">Digital Skills</td>
                    <td class="sub-c2">
                        <span class="text-danger"><i>(Mapped to 2 TGC Attribute)</i></span><br>
                        <span class="badge badge-info">Problem solving, critical and creativce thinking skills</span><br>
                        The ability to rationally, critically and creatively analyze,synthesize and evaluate evidences
                        to arrive at solutions or
                        conclution.<br><br>
                        <span class="badge badge-info">Lifelong Learning</span><br> The ability to develop self disposition and skills
                        with the aim of improving professional competence throught self-directed learning,
                        self-inquiry, self-assessment, self-reflection, undetaken on an ongoing basis.
                        <br>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td class="sub-c">d</td>
                    <td class="sub-c2 text-info">Numeracy Skills</td>
                    <td class="sub-c2">
                        <span class="badge badge-info">Problem solving, critical and creativce thinking skills.</span><br>
                        The ability to rationally, critically and creatively analyze,synthesize and evaluate evidences
                        to arrive at solutions or
                        conclution.
                        <br>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td class="sub-c">e</td>
                    <td class="sub-c2 text-info">Leadership, autonomy and responsibility</td>
                    <td class="sub-c2">
                        <span class="badge badge-info">Social Competencies</span><br>
                        The ability to empathize with others, interact positively with them and foster a stable and harmonious relationships.
                        <br>
                    </td>
                </tr>
                <tr>
                    <td>4</td>
                    <td colspan="2">Personal and Enterpreneurial skills</td>
                    <td colspan="2">
                        <span class="badge badge-warning text-white">Personal Competencies</span><br>
                        The ability be self-aware and to self-regulate emotions through skillful management of one's personal goals, intentions, responses and behaviour
                        <br><br>
                        <span class="badge badge-warning text-white">Enterpreneurialism </span><br>
                        The ability to influance change bu being proactive , resourceful and prudent in assuming risk.
                    </td>
                   
                </tr>
            </tbody>
        </table>
    </div>
    <div class="card-footer text-muted text-center">
        <a href="{{ URL::route('mapping') }}" class="btn btn-danger btn-sm pr-2">
            <i class="fa fa-long-arrow-left mr-2 ml-2" aria-hidden="true"></i>
            Back
        </a>
    </div>
</section>

@endsection