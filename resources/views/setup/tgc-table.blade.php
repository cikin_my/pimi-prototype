@extends('layouts.main.master')
@section('content')

<section class="card">
    <header class="card-header">
        Transferable Skills
    </header>
    <div class="card-body">
        <table class="table table-condensed table-bordered tgc" style="font-size:.85em;" id="tgc">
            <thead>
                <tr>
                    <th width="10%" class="left-align">TGC</th>
                    <th width="15%" class="left-align">TGC Attribute</th>
                    <th width="30%" class="text-center">Description of TGC Attributes</th>
                    <th width="10%" class="text-center">Sub-Attribute</th>
                    <th width="" class="text-center">Description of sub-attributes</th>
                </tr>
            </thead>

            <tbody>
                <tr class="divider1">
                    <td rowspan="4">TGC1</td>
                    <td rowspan="4">Diciplines-Specific knowlegde</td>
                    <td rowspan="4">Diciplines-Specific knowlegde refer to ability to
                        dmeonstrate professional competence, comprehend and adapt dicipline
                        specific knowledge,
                        and be bale to intergrate knowledge across diverse perspective</td>
                    <td class="sub-a">1.1</td>
                    <td>Knowledge: Demonstrate a broad and coherent theoretical and
                        technical knowledge to communicate and understanding
                        relating to the disdpline-specific content</td>
                </tr>
                <tr class="">
                    <td class="sub-a">1.2</td>
                    <td>
                        Comprehension: Demonstrate comprehension of disciplinary concepts
                    </td>
                </tr>
                <tr class="">
                    <td class="sub-a">1.3</td>
                    <td>
                        Transfer: Adapt and apply skills, abilities, theories or
                        methodologies gained in one situation to new situations

                    </td>
                </tr>
                <tr class="">
                    <td class="sub-a">1.4</td>
                    <td>
                        Connect: Analyse, synthesize and integrate knowledge from more than
                        one field of study or perspective
                    </td>
                </tr>
                <!-- ========================================== -->
                <tr class="divider1">
                    <td>TGC2</td>
                    <td>Problem Solving,
                        Critical and Creative
                        Thinking Skills
                    </td>
                    <td>The ability to rationally, critically and
                        creatively analyze, synthesize and evaluate evidences to arrive at
                        solution or conclusion.
                    </td>
                    <td colspan="2" class="empty-td"></td>
                </tr>
                <!-- ----------------------------------- -->
                <tr class="divider1">
                    <td rowspan="4">TGC2a</td>
                    <td rowspan="4">Problem Solving Skills</td>
                    <td rowspan="4">
                        Problem Solving, Critical and Creative
                        Thinking Skills refer to the ability to rationally, critically and
                        creatively analyze, synthesize and evaluate evidence
                        to arrive at a solution or conclusion.

                    </td>
                    <td class="sub-a">2a.1
                    </td>
                    <td>
                        identify the problem
                    </td>
                </tr>
                <tr class="a-2">
                    <td class="sub-a">2a.2</td>
                    <td>
                        Propose solutions to existing and emerging problems
                    </td>
                </tr>
                <tr class="a-2">
                    <td class="sub-a"> 2a.3</td>
                    <td>
                        Implement a solution
                    </td>
                </tr>
                <tr class="a-2">
                    <td class="sub-a"> 2a.4</td>
                    <td>
                        Evaluate process and outcomes
                    </td>
                </tr>
                <!-- -------------------------------- -->
                <tr class="divider1">
                    <td rowspan="5">TGC2b</td>
                    <td rowspan="5">
                        Critical and Creative Thinking Skills
                    </td>
                    <td rowspan="5">
                        Critical and Creative Thinking Skills refer to the ability to examine
                        an issue through flexible and divergent thinking,
                        and critically and creatively analyse, synthesize
                        and evaluate evidence to justify a solution or conclusion.
                    </td>
                    <td class="sub-a">
                        2b.1
                    </td>
                    <td>
                        Flexibility and divergent thinking


                    </td>
                </tr>
                <tr class="a-3">
                    <td class="sub-a"> 2b.2</td>
                    <td>
                        Creative thinking

                    </td>
                </tr>
                <tr class="a-3">
                    <td class="sub-a"> 2b.3</td>
                    <td>

                        Risk-taking/Pushing the boundaries

                    </td>
                </tr>
                <tr class="a-3">
                    <td class="sub-a"> 2b.4</td>
                    <td>

                        Analyze and synthesize the evidence

                    </td>
                </tr>
                <tr class="a-3">
                    <td class="sub-a"> 2b.5</td>
                    <td>

                        Justify and theorize your position
                        (perspective/thesis/hypothesis)
                    </td>
                </tr>
                {{--  <tr>
                    <td>TGC3</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>TGC4</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>TGC5</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>TGC6</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>TGC7</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>TGC8</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>  --}}
            </tbody>
        </table>
    </div>
    <div class="card-footer text-muted text-center">
       <a href="{{ URL::route('tgc') }}" class="btn btn-warning btn-sm pr-2"><i class="fa fa-long-arrow-left mr-2 ml-2"
                aria-hidden="true"></i> Back</a>
    </div>



</section>

@endsection
