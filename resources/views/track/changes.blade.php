@extends('layouts.main.master')
@section('content')
<div class="card bg-light">
    <header class="card-header">
        <i class="fa fa-list mr-3"></i> Percentage of Change in Primary Major
    </header>
    <div class="card-body">

        <div class="row">
            <div class="col-md-12">
               <div class="card bg-1">
                
                    <div class="card-body">
                        <table class="table table-hover table-borderless table-small-text summary-tb">
                            <tr>
                                <td class="field" width="55%">ICT2386</td>
                                <td class="text-field">: Bachelor in Computer Science</td>
                            </tr>
                            <tr>
                                <td class="field">Specialization</td>
                                <td class="text-field">: Networking</td>
                            </tr>
                            <tr>
                                <td class="field">Version </td>
                                <td class="text-field">: 1.02</td>
                            </tr>
                            <tr>
                                <td class="field">Status </td>
                                <td class="text-field">: Approved</td>
                            </tr>
                            <tr>
                                <td class="field">Graduating Credits
                                </td>
                                <td class="text-field">: 120 credits</td>
                            </tr>
                          
                
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <table class="table table-border  bg-2 text-center table-small-text table-hover">
            <thead>
                <tr class="text-center">
                    <th>No.</th>
                    <th width="25%">Module code and name</th>
                    <th>Credits</th>
                    <th width="15%">Type of Change</th>
                    <th>Cumulative Changes by Credit</th>
                    <th>% of Changes</th>
                    <th width="25%">Changed by (Staff Name)</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>1</td>
                    <td class="text-left">ACC6304 -Financial Reporting</td>
                    <td>4</td>
                    <td>Remove</td>
                    <td>4</td>
                    <td>4%</td>
                    <td></td>
                </tr>
               <tr>
                    <td>2</td>
                    <td class="text-left">ABC1234 -Course</td>
                    <td>4</td>
                    <td>New</td>
                    <td>8</td>
                    <td>8%</td>
                    <td></td>
                </tr>
                <tr>
                    <td>-</td>
                    <td class="text-left">-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                </tr>
                <tr>
                    <td>-</td>
                    <td class="text-left">-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                </tr>
                <tr>
                    <td>-</td>
                    <td class="text-left">-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td class="bg-1">30%</td>
                    <td>-</td>
                </tr>
            </tbody>
        </table>

        <div class="card-footer text-right">
            <a href="{{ URL::route('tracking') }}" class="btn btn-primary btn-save btn-sm">Save <i class="fa fa-save ml-2"></i></a>
        </div>
    </div>
   
</div>

@endsection