@extends('layouts.main.master')
@section('content')
<div class="card bg-light">
    <header class="card-header">
        <i class="fa fa-list mr-3"></i> Tracking Programme Changes
    </header>
    <div class="card-body">
      <table class="table table-border tgc bg-2">
          <thead>
            <tr class="text-center">
                <th>ID</th>
                <th width="15%">Last Updated (Date) </th>
                <th width="15%">Programme Code </th>
                <th>Programme Name</th>
                <th>Status</th>
                <th width="25%">Actions</th>
            </tr>
        </thead>
        <tbody>
                <tr>
                    <td class="text-center">1</td>
                    <td>12 Oct 2019</td>
                    <td>812/4/0065</td>
                    <td>Programme A</td>
                    <td class="text-center"><span class="badge badge-success">Review</span></td>
                    <td class="text-center">
                        <a class="btn btn-xs btn-primary " href="">
                          View Programme Info</a>
                        <a class="btn btn-xs btn-dark" href="{{ URL::route('changes') }}">
                         Track Changes</a>
                
                    </td>
                </tr>
                <tr>
                    <td class="text-center">2</td>
                    <td>12 Oct 2019</td>
                    <td>812/4/0065</td>
                    <td>Programme B</td>
                    <td class="text-center"><span class="badge badge-secondary">Draft</span></td>
                   <td class="text-center">
                        <a class="btn btn-xs btn-primary " href="">
                           View Programme Info</a>
                        <a class="btn btn-xs btn-dark" href="{{ URL::route('changes') }}">
                            Track Changes</a>
                    
                    </td>
                </tr>
                <tr>
                    <td class="text-center">3</td>
                    <td>12 Oct 2019</td>
                    <td>812/4/0065</td>
                    <td>Programme C</td>
                    <td class="text-center"><span class="badge badge-primary">Corrections</span></td>
                   <td class="text-center">
                        <a class="btn btn-xs btn-primary " href="">
                         View Programme Info</a>
                        <a class="btn btn-xs btn-dark" href="{{ URL::route('changes') }}">
                          Track Changes</a>
                    
                    </td>
                </tr>
            
            </tbody>

      </table>
    </div>
</div>

@endsection