@extends('layouts.main.master')
@section('content')
    <div class="row">
        <div class="col-sm-12">
            <section class="card">
                <header class="card-header">
                    Add Modules
                </header>

                <div class="card-body">
                    <div class="col-sm-12">
                        <section class="card tasks-widget">
                            <div class="card-body col-sm-12">
                                <ul class="task-list">
                                    <li>Select a category for which you wish to add/select modules:</li>
                            </div>
                            <div class="card-body">
                                <ul class="task-list">
                                    <li>
                                        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#core">
                                            Core<i class="fa fa-plus pull-right"></i>
                                        </button>
                                    </li>
                                    <li>
                                        <button type="button" class="btn btn-success" data-toggle="modal"
                                            data-target="#mpuucm">
                                            MPU/UCM<i class="fa fa-plus pull-right"></i>
                                        </button>
                                    </li>
                                    <li>
                                        <button type="button" class="btn btn-warning" data-toggle="modal"
                                            data-target="#specialization">
                                            Specialization<i class="fa fa-plus pull-right"></i>
                                        </button>
                                    </li>
                                </ul>
                            </div>
                        </section>
                    </div>
                </div>
            </section>
        </div>
    </div>

    <!-- 1. core Modal -->
    <div class="modal fade form-container" id="core" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Core Module</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group col-md-12">
                            <button type="button" class="btn btn-secondary btn-sm" data-toggle="modal" data-dismiss="modal"
                                data-target="#core-new">Create New Module</button>
                            &ensp;|&ensp;
                            <button type="button" class="btn btn-secondary btn-sm" data-toggle="modal" data-dismiss="modal"
                                data-target="#core-existing">Choose an Existing Module</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

    <!-- submit Modal !! please note that this is a public submit modal, used for all SUBMIT button in prototype !!-->
    <div class="modal fade form-container" id="submit" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body alert alert-success" role="alert">New module has been added!
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <button type="button" class="btn btn-success" href="{{ route('list') }}">Complete module
                        information</button>
                </div>

            </div>
        </div>
    </div>

    <!-- 1.1 core new module Modal -->
    <div class="modal fade form-container" id="core-new" role="dialog" aria-labelledby="exampleModalLabel1"
        aria-hidden="true">

        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">New Core Module</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <input type="name" class="form-control" id="core-new" placeholder="Module name">
                        </div>
                        <div class="form-group">
                            <label class="sr-only" for="core-new">Assign module leader</label>
                            <select class="form-control select2">
                                <option value="0" disable>Search module leader</option>
                                <option value="1">Module leader 1</option>
                                <option value="2">Module leader 2</option>
                                <option value="3">Module leader 3</option>
                                <option value="4">Module leader 4</option>
                                <option value="5">Module leader 5</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="sr-only" for="core-new">Year level (programme)</label>
                            <select class="form-control select2">
                                <option value="0" disable>Select year level (programme)</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>

                            </select>
                        </div>
                        <div class="form-group">
                            <label class="sr-only" for="inlineFormInput">School owning the programme</label>
                            <select class="form-control select2">
                                <option value="0" disable>Select school owning the programme</option>
                                <option value="1">School of Computing & IT</option>
                                <option value="2">School of Biosciences</option>
                                <option value="3">Taylor's Business School</option>
                                <option value="4">School of Education</option>
                                <option value="5">School of Engineering</option>
                                <option value="6">School of Medicine</option>
                                <option value="7">Taylor's Law School</option>

                            </select>
                        </div>

                        <div class="form-group pull-right">
                            <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-dismiss="modal"
                                data-target="#submit">Submit</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

    <!-- 1.2 core existing module Modal -->
    <div class="modal fade form-container" id="core-existing" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">

        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Existing Core Module</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <label class="sr-only" for="core-existing">Module</label>
                            <select class="form-control select2">
                                <option value="0" disable>Search module</option>
                                <option value="1">Module 1</option>
                                <option value="2">Module 2</option>
                                <option value="3">Module 3</option>
                                <option value="4">Module 4</option>
                                <option value="5">Module 5</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="sr-only" for="core-existing">Year level (programme)</label>
                            <select class="form-control select2">
                                <option value="0" disable>Select year level (programme)</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>

                            </select>
                        </div>

                        <div class="form-group pull-right">
                            <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-dismiss="modal"
                                data-target="#submit">Submit</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>


    <!-- 2 mpu/ucm module Modal -->
    <div class="modal fade form-container" id="mpuucm" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">MPU/UCM Module</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <label class="sr-only" for="core-existing">MPU/UCM module</label>
                            <select class="form-control select2">
                                <option value="0" disable>Select MPU/UCM module</option>
                                <option value="6">Module 6</option>
                                <option value="7">Module 7</option>
                                <option value="8">Module 8</option>
                                <option value="9">Module 9</option>
                                <option value="10">Module 10</option>
                            </select>
                        </div>

                        <div class="form-group pull-right">
                            <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-dismiss="modal"
                                data-target="#submit">Submit</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

    <!-- 3. specialization Modal -->
    <div class="modal fade form-container" id="specialization" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">

        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Specialization Module</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="custom-control custom-checkbox mb-6">
                            <input type="checkbox" class="custom-control-input" id="checkbox1">
                            <label class="custom-control-label" for="checkbox1">Specialization 1</label>
                        </div>
                        <div class="custom-control custom-checkbox mb-6">
                            <input type="checkbox" class="custom-control-input" id="checkbox2">
                            <label class="custom-control-label" for="checkbox2">Specialization 2</label>
                        </div>
                        <div class="custom-control custom-checkbox mb-6">
                            <input type="checkbox" class="custom-control-input" id="checkbox3">
                            <label class="custom-control-label" for="checkbox3">Specialization 3</label>
                        </div>
                        <div class="custom-control custom-checkbox mb-6">
                            <input type="checkbox" class="custom-control-input" id="checkbox4">
                            <label class="custom-control-label" for="checkbox4">Specialization 4</label>
                        </div>
                        <div class="custom-control custom-checkbox mb-6">
                            <input type="checkbox" class="custom-control-input" id="checkbox5">
                            <label class="custom-control-label" for="checkbox5">Specialization 5</label>
                        </div><br />
                        <div class="form-group">
                            <button type="button" id="spec-category" class="btn btn-success">Add Specialization
                                Category</button>
                        </div>
                        <div class="form-group row" id="spec-text" style="display:none;">
                            <div class="col-md-12 col-sm-12">
                                <div class="input-group">
                                    <input type="text" class="form-control mr-1">
                                    <span class="btn btn-success" disabled><i class="fa fa-check f14"></i></span>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group pull-right">
                            <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-dismiss="modal"
                                data-target="#specialization-next">Next</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

    <!-- 3.1 specialization next Modal -->
    <div class="modal fade form-container" id="specialization-next" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">

        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Specialization Module</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group col-md-12">
                            <button type="button" class="btn btn-secondary btn-sm" data-toggle="modal" data-dismiss="modal"
                                data-target="#specialization-new">Create New Module</button>
                            &ensp;|&ensp;
                            <button type="button" class="btn btn-secondary btn-sm" data-toggle="modal" data-dismiss="modal"
                                data-target="#specialization-existing">Choose an Existing Module</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

    <!-- 3.2 specialization new module Modal -->
    <div class="modal fade form-container" id="specialization-new" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">

        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">New Core Module</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <input type="name" class="form-control" id="specialization-new" placeholder="Module name">
                        </div>
                        <div class="form-group">
                            <label class="sr-only" for="specialization-new">Assign module leader</label>
                            <select class="form-control select2">
                                <option value="0" disable>Search module leader</option>
                                <option value="1">Module 1</option>
                                <option value="2">Module 2</option>
                                <option value="3">Module 3</option>
                                <option value="4">Module 4</option>
                                <option value="5">Module 5</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="sr-only" for="specialization-new">Year level (programme)</label>
                            <select class="form-control select2">
                                <option value="0" disable>Select year level (programme)</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>

                            </select>
                        </div>
                        <div class="form-group">
                            <label class="sr-only" for="core-module-leader">School owning the programme</label>
                            <select class="form-control select2">
                                <option value="0" disable>Select school owning the programme</option>
                                <option value="1">School of Computing & IT</option>
                                <option value="2">School of Biosciences</option>
                                <option value="3">Taylor's Business School</option>
                                <option value="4">School of Education</option>
                                <option value="5">School of Engineering</option>
                                <option value="6">School of Medicine</option>
                                <option value="7">Taylor's Law School</option>

                            </select>
                        </div>

                        <div class="form-group pull-right">
                            <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-dismiss="modal"
                                data-target="#submit">Submit</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

    <!-- 3.3 specialization existing module Modal -->
    <div class="modal fade form-container" id="specialization-existing" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">

        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Existing Core Module</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <label class="sr-only" for="core-module-leader">Module</label>
                            <select class="form-control select2">
                                <option value="0" disable>Search module</option>
                                <option value="1">Module 1</option>
                                <option value="2">Module 2</option>
                                <option value="3">Module 3</option>
                                <option value="4">Module 4</option>
                                <option value="5">Module 5</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="sr-only" for="inlineFormInput">Year level (programme)</label>
                            <select class="form-control select2">
                                <option value="0" disable>Select year level (programme)</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>

                            </select>
                        </div>

                        <div class="form-group pull-right">
                            <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-dismiss="modal"
                                data-target="#submit">Submit</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

@endsection