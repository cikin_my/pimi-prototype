@extends('layouts.main.master')
@section('content')

<div class="row">
    <div class="col-sm-12">
        <section class="card">
            <header class="card-header">
                Module List
                <span class="tools pull-right">
                    <button class="btn btn-sm btn-primary" href="{{ route('add') }}">
                        <i class="fa fa-plus mr-1 ml-1" aria-hidden="true"></i> Add Modules</button>
                </span>
            </header>

{{-- filter option --}}
        <div class="card card-primary">
            <div class="card-body">
                <div class="row">

                    <div class="col-md-6">
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label text-md-right">Module Category</label>
                            <div class="col-sm-12 col-md-6">
                                <select class="form-control select2">
                                    <option value="">Please select</option>
                                    <option value="1">Core</option>
                                    <option value="2">MPU/UCM</option>
                                    <option value="3">Specialization</option>
                                </select>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-6 text-left">

                        <div class="form-group row">
                            <label class="col-md-3 col-form-label text-md-right">Status</label>
                            <div class="col-12 col-md-6">
                                <select class="form-control select2">
                                    <option value="">Please select</option>
                                    <option value="1">Draft</option>
                                    <option value="2">Review</option>
                                    <option value="3">Corrections</option>
                                    <option value="4">Approved</option>
                                    <option value="5">Published</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <hr>
                    <div class="col-md-12 mt-4">
                        <div class="text-center">
                            <button class="btn btn-space btn-primary btn-rounded" type="submit"><i
                                    class="fa fa-search"></i> Search</button>
                            <button class="btn btn-space btn-secondary btn-rounded" value="reset">
                                <i class="fa fa-refresh"></i> Reset</button>
                        </div>
                    </div>

                </div>

            </div>
        </div>
        </section>

    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <section class="card">
            <div class="card-body">
                <div class="adv-table">
                    <table class="display table table-bordered table-striped " id="dynamic-table">
                        <thead>
                            <tr>
                                <th class="hidden-phone" width="5px">No.</th>
                                <th width="10%">Module Code</th>
                                <th width="15%">Module Name</th>
                                <th width="15%" class="hidden-phone">Last Update</th>
                                <th width="15%">Module Leader</th>
                                <th width="15%">Module Category</th>
                                <th width="5%">Credits</th>
                                <th width="5%">Status</th>
                                <th class="action-col" width="15%">Action</th>
                            </tr>
                        </thead>

                        <tbody>
                            <tr class="">
                                <td>1</td>
                                <td>MD1 </td>
                                <td>Module 22</td>
                                <td class=" hidden-phone">1 September 2019</td>
                                <td>Module leader 1</td>
                                <td>Core</td>
                                <td>3.60</td>
                                <td>Approved</td>
                                <td class="action-col">
                                    <button class="btn btn-success btn-sm"><i class="fa fa-eye"></i></button>
                                    <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#edit"><i
                                            class="fa fa-pencil"></i></button>
                                </td>
                            </tr>
                            <tr class="">
                                <td>2</td>
                                <td>MD2 </td>
                                <td>Module 2</td>
                                <td class=" hidden-phone">13 August 2019</td>
                                <td>Module leader 2</td>
                                <td>MPU/UCM</td>
                                <td>2.56</td>
                                <td>Corrections</td>
                                <td class="action-col">
                                    <button class="btn btn-success btn-sm"><i class="fa fa-eye"></i></button>
                                    <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#edit"><i
                                            class="fa fa-pencil"></i></button>
                                </td>
                            </tr>
                            <tr class="">
                                <td>3</td>
                                <td>MD3 </td>
                                <td>Module 3</td>
                                <td class=" hidden-phone">21 July 2019</td>
                                <td>Module leader 3</td>
                                <td>Specialization</td>
                                <td>3.33</td>
                                <td>Published</td>
                                <td class="action-col">
                                    <button class="btn btn-success btn-sm"><i class="fa fa-eye"></i></button>
                                    <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#edit"><i
                                            class="fa fa-pencil"></i></button>
                                </td>
                            </tr>

                        </tbody>

                    </table>
                </div>
            </div>
        </section>
    </div>
</div>

<!-- 1. edit modal -->
<div class="modal fade" id="edit" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Module</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label>Module name</label>
                        <input type="name" class="form-control" placeholder="Module name" value="Module 22">
                    </div>
                    <div class="form-group">
                        <label>Year level (module)</label>
                        <select class="form-control select2">
                            <option value="0" disable>Please select</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                           
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Year level (programme)</label>
                        <select class="form-control select2">
                            <option value="0" disable>Please select</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                           
                        </select>
                    </div>

                    <!--tab nav start-->
                        <ul class="nav nav-tabs mb-2" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="pre-tab" data-toggle="tab" href="#pre" role="tab" aria-controls="pre" aria-selected="true">Pre-requisite</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="co-tab" data-toggle="tab" href="#co" role="tab" aria-controls="co" aria-selected="false">Co-requisite</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="anti-tab" data-toggle="tab" href="#anti" role="tab" aria-controls="anti" aria-selected="false">Anti-requisite</a>
                            </li>
                        </ul>
                        <br/>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="pre" role="tabpanel" aria-labelledby="pre-tab">
                                <div class="form-group">
                                    <label>General Pre-requisite</label>
                                    <input type="name" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Module Pre-requisite</label>
                                    <div class="custom-control custom-checkbox mb-6 col-md-6">
                                        <input type="checkbox" class="custom-control-input" id="checkbox1">
                                        <label class="custom-control-label" for="checkbox1">Module 2</label>
                                    </div>
                                    <div class="custom-control custom-checkbox mb-6 col-md-6">
                                        <input type="checkbox" class="custom-control-input" id="checkbox2">
                                        <label class="custom-control-label" for="checkbox2">Module 3</label>
                                    </div>
                                    <div class="custom-control custom-checkbox mb-6 col-md-6">
                                        <input type="checkbox" class="custom-control-input" id="checkbox3">
                                        <label class="custom-control-label" for="checkbox3">Module 4</label>
                                    </div>
                                    <div class="custom-control custom-checkbox mb-6 col-md-6">
                                        <input type="checkbox" class="custom-control-input" id="checkbox4">
                                        <label class="custom-control-label" for="checkbox4">Module 5</label>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="co" role="tabpanel" aria-labelledby="co-tab">
                                <div class="form-group">
                                    <label>General Co-requisite</label>
                                    <input type="name" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Module Co-requisite</label>
                                    <div class="custom-control custom-checkbox mb-6 col-md-6">
                                        <input type="checkbox" class="custom-control-input" id="checkbox1">
                                        <label class="custom-control-label" for="checkbox1">Module 2</label>
                                    </div>
                                    <div class="custom-control custom-checkbox mb-6 col-md-6">
                                        <input type="checkbox" class="custom-control-input" id="checkbox2">
                                        <label class="custom-control-label" for="checkbox2">Module 3</label>
                                    </div>
                                    <div class="custom-control custom-checkbox mb-6 col-md-6">
                                        <input type="checkbox" class="custom-control-input" id="checkbox3">
                                        <label class="custom-control-label" for="checkbox3">Module 4</label>
                                    </div>
                                    <div class="custom-control custom-checkbox mb-6 col-md-6">
                                        <input type="checkbox" class="custom-control-input" id="checkbox4">
                                        <label class="custom-control-label" for="checkbox4">Module 5</label>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="anti" role="tabpanel" aria-labelledby="anti-tab">
                            <div class="form-group">
                                <label>General Anti-requisite</label>
                                <input type="name" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Module Pre-requisite</label>
                                <div class="custom-control custom-checkbox mb-6 col-md-6">
                                    <input type="checkbox" class="custom-control-input" id="checkbox1">
                                    <label class="custom-control-label" for="checkbox1">Module 2</label>
                                </div>
                                <div class="custom-control custom-checkbox mb-6 col-md-6">
                                    <input type="checkbox" class="custom-control-input" id="checkbox2">
                                    <label class="custom-control-label" for="checkbox2">Module 3</label>
                                </div>
                                <div class="custom-control custom-checkbox mb-6 col-md-6">
                                    <input type="checkbox" class="custom-control-input" id="checkbox3">
                                    <label class="custom-control-label" for="checkbox3">Module 4</label>
                                </div>
                                <div class="custom-control custom-checkbox mb-6 col-md-6">
                                    <input type="checkbox" class="custom-control-input" id="checkbox4">
                                    <label class="custom-control-label" for="checkbox4">Module 5</label>
                                </div>
                            </div>
                            </div>
                        </div>
                    <hr>
                    <!--tab nav start-->

                    <div class="form-group">
                        <label>Module owner</label>
                        <select class="form-control select2">
                            <option value="0" disable>Please select</option>
                            <option value="1">School of Computing & IT</option>
                            <option value="2">School of Biosciences</option>
                            <option value="3">Taylor's Business School</option>
                            <option value="4">School of Education</option>
                            <option value="5">School of Engineering</option>
                            <option value="6">School of Medicine</option>
                            <option value="7">Taylor's Law School</option>
                           
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Module offered as</label>
                        <div class="custom-control custom-checkbox mb-6 col-md-6">
                            <input type="checkbox" class="custom-control-input" id="checkbox1">
                            <label class="custom-control-label" for="checkbox1">Primary Major – Core</label>
                        </div>
                        <div class="custom-control custom-checkbox mb-6 col-md-6">
                            <input type="checkbox" class="custom-control-input" id="checkbox2">
                            <label class="custom-control-label" for="checkbox2">Primary Major – Specialization</label>
                        </div>
                        <div class="custom-control custom-checkbox mb-6 col-md-6">
                            <input type="checkbox" class="custom-control-input" id="checkbox3">
                            <label class="custom-control-label" for="checkbox3">Minor</label>
                        </div>
                        <div class="custom-control custom-checkbox mb-6 col-md-6">
                            <input type="checkbox" class="custom-control-input" id="checkbox4">
                            <label class="custom-control-label" for="checkbox4">Free Elective</label>
                        </div>
                        <div class="custom-control custom-checkbox mb-6 col-md-6">
                            <input type="checkbox" class="custom-control-input" id="checkbox4">
                            <label class="custom-control-label" for="checkbox4">Extension</label>
                        </div>
                        <div class="custom-control custom-checkbox mb-6 col-md-6">
                            <input type="checkbox" class="custom-control-input" id="checkbox4">
                            <label class="custom-control-label" for="checkbox4">MPU</label>
                        </div>
                        <div class="custom-control custom-checkbox mb-6 col-md-6">
                            <input type="checkbox" class="custom-control-input" id="checkbox4">
                            <label class="custom-control-label" for="checkbox4">Second Major</label>
                        </div>
                    </div>
                 
                    <div class="form-group pull-right">
                        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-dismiss="modal" data-target="#submit">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- submit Modal !!-->
<div class="modal fade form-container" id="submit" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body alert alert-success" role="alert">Module Information Saved Successfully
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group pull-right">
                        <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-dismiss="modal">OK</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection