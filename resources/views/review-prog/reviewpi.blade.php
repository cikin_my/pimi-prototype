@extends('layouts.main.master')
@section('content')
<div class="card bg-light">
    <header class="card-header">
        <i class="fa fa-edit mr-3"></i> Review PI
    </header>
    <div class="card-body">

        <section class="card">
            <header class="card-header">
                Review Pi
            </header>
            <div class="card-body">
                <button class="btn btn-sm btn-primary">Review Pi <i class="fa fa-eye ml-3"></i></button>
            </div>
        </section>

        <section class="card">
            <header class="card-header">
                Approval
            </header>
            <div class="card-body">
                <table class="table table-bordered table-condensed table-hover tgc text-center">
                    <thead>
                        <tr>
                            <th>Approval 1 by RAG</th>
                            <th>Approval 2 by RAG</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="customCheck1"
                                        name="example1">
                                    <label class="custom-control-label" for="customCheck1"></label>
                                </div>
                            </td>
                            <td>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="customCheck1"
                                        name="example1">
                                    <label class="custom-control-label" for="customCheck1"></label>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </section>

        <section class="card">
            <header class="card-header">
                Approval Notes
            </header>
            <div class="card-body">

                <form role="form">
                
                    <div class="form-group">                        
                        <textarea name="" id="" cols="30" rows="5" class="form-control" placeholder="Please add approval note here..."></textarea>
                    </div>
                    <div class="form-group text-center">
                        <button class="btn btn-primary btn-small btn-space"> Save <i class="fa fa-save ml-2"></i></button>
                    </div>
                </form>

                <table class="table table-bordered table-condensed table-hover tgc text-center">
                    <thead>
                        <tr>
                            <th width="10px">#</th>
                            <th>Approval Notes</th>
                            <th width="25%">Time/Date</th>
                        </tr>
                    </thead>
                    <tbody class="text-left">
                        <tr>
                            <td>1</td>
                            <td>
                               Approval Note_001
                            </td>
                            <td>
                               12 Sept 2019 : 1040am <button class="btn btn-xs btn-primary pull-right"> View <i class="fa fa-expand ml-1" aria-hidden="true"></i> </button>
                            </td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>
                                Approval Note_002
                            </td>
                            <td>
                                20 Sept 2019 : 1040am <button class="btn btn-xs btn-primary pull-right"> View <i class="fa fa-expand ml-1"
                                        aria-hidden="true"></i> </button>
                            </td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>
                                Approval Note_003
                            </td>
                            <td>
                                25 Sept 2019 : 1040am <button class="btn btn-xs btn-primary pull-right"> View <i class="fa fa-expand ml-1"
                                        aria-hidden="true"></i> </button>
                            </td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>
                                Approval Note_004
                            </td>
                            <td>
                                30 Sept 2019 : 1040am <button class="btn btn-xs btn-primary pull-right"> View <i class="fa fa-expand ml-1"
                                        aria-hidden="true"></i> </button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </section>
        <section class="card">
            <header class="card-header">
                RAG Internal Notes
            </header>
            <div class="card-body">
        
                <form role="form">
        
                    <div class="form-group">
                        <textarea name="" id="" cols="30" rows="5" class="form-control"
                            placeholder="Please add approval note here..."></textarea>
                    </div>
                    <div class="form-group text-center">
                        <button class="btn btn-success btn-small btn-space"> Save <i class="fa fa-save ml-2"></i></button>
                    </div>
                </form>
        
                <table class="table table-bordered table-condensed table-hover tgc text-center bg-3">
                    <thead>
                        <tr>
                            <th width="10px">#</th>
                            <th>Approval Notes</th>
                            <th width="25%">Time/Date</th>
                        </tr>
                    </thead>
                    <tbody class="text-left">
                        <tr>
                            <td>1</td>
                            <td>
                                Approval Note_001
                            </td>
                            <td>
                                12 Sept 2019 : 1040am <button class="btn btn-xs btn-success pull-right"> View <i
                                        class="fa fa-expand ml-1" aria-hidden="true"></i> </button>
                            </td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>
                                Approval Note_002
                            </td>
                            <td>
                                20 Sept 2019 : 1040am <button class="btn btn-xs btn-success pull-right"> View <i
                                        class="fa fa-expand ml-1" aria-hidden="true"></i> </button>
                            </td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>
                                Approval Note_003
                            </td>
                            <td>
                                25 Sept 2019 : 1040am <button class="btn btn-xs btn-success pull-right"> View <i
                                        class="fa fa-expand ml-1" aria-hidden="true"></i> </button>
                            </td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>
                                Approval Note_004
                            </td>
                            <td>
                                30 Sept 2019 : 1040am <button class="btn btn-xs btn-success pull-right"> View <i
                                        class="fa fa-expand ml-1" aria-hidden="true"></i> </button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </section>

        <div class="card-footer text-center">
            <a class="btn btn-danger btn-sm btn-space" href="{{ URL::route('review') }}"> <i class="fa fa-arrow-left"></i>back</a>
        </div>

    </div>
   
</div>

@endsection
