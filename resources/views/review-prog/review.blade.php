@extends('layouts.main.master')
@section('content')
<div class="card bg-light">
    <header class="card-header">
        <i class="fa fa-list mr-3"></i> Programme Review
    </header>
    <div class="card-body">
        <table class="table table-bordered tgc">
            <thead>
                <tr class="text-center">
                    <th>ID</th>
                    <th width="15%">Last Updated (Date) </th>
                    <th width="15%">Programme Code </th>
                    <th>Programme Name</th>
                    <th>Status</th>
                    <th width="18%">Actions</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="text-center">1</td>
                    <td >12 Oct 2019</td>
                    <td>812/4/0065</td>
                    <td>Programme A</td>
                    <td class="text-center"><span class="badge badge-success">Review</span></td>
                    <td class="text-center">
                        <a class="btn btn-xs btn-warning " href="{{ URL::route('programme-view') }}">
                            <span data-content="View PI" data-placement="top" data-trigger="hover" class="popovers"><i
                                    class="fa fa-eye"></i><span></a>
                        <a class="btn btn-xs btn-primary" href="{{ URL::route('review-pi') }}">
                            <span data-content="Review PI" data-placement="top" data-trigger="hover" class="popovers"><i
                                    class="fa fa-edit"></i> </span></a>
                        <a class="btn btn-xs btn-success" href="{{ URL::route('structure') }}">
                            <span data-content="Review Structure " data-placement="top" data-trigger="hover"
                                class="popovers"><i class="fa fa-sitemap"></i> </span></a>
                        <a class="btn btn-xs btn-dark" href="#download" data-toggle="modal" data-target="#download">
                            <span data-content="Download " data-placement="top" data-trigger="hover" class="popovers"><i
                                    class="fa fa-cloud-download"></i></span> </a>
                        <a class="btn btn-xs btn-info" href="" data-toggle="modal" data-target="#publish">
                            <span data-content="Publish " data-placement="top" data-trigger="hover" class="popovers"><i
                                    class="fa fa-upload"></i> </span></a>
                    </td>
                </tr>
                <tr>
                    <td class="text-center">2</td>
                    <td >12 Oct 2019</td>
                    <td>812/4/0065</td>
                    <td>Programme B</td>
                    <td class="text-center"><span class="badge badge-secondary">Draft</span></td>
                    <td class="text-center">
                        <a class="btn btn-xs btn-warning " href="{{ URL::route('programme-new') }}">
                            <span data-content="View PI" data-placement="top" data-trigger="hover" class="popovers"><i
                                    class="fa fa-eye"></i><span></a>
                        <a class="btn btn-xs btn-primary" href="{{ URL::route('programme-new') }}">
                            <span data-content="Review PI" data-placement="top" data-trigger="hover" class="popovers"><i
                                    class="fa fa-edit"></i> </span></a>
                        <a class="btn btn-xs btn-success" href="{{ URL::route('structure') }}">
                            <span data-content="Review Structure " data-placement="top" data-trigger="hover"
                                class="popovers"><i class="fa fa-sitemap"></i> </span></a>
                        <a class="btn btn-xs btn-dark" href="#download" data-toggle="modal" data-target="#download">
                            <span data-content="Download " data-placement="top" data-trigger="hover" class="popovers"><i
                                    class="fa fa-cloud-download"></i></span> </a>
                        <a class="btn btn-xs btn-info" href="" data-toggle="modal" data-target="#publish">
                            <span data-content="Publish " data-placement="top" data-trigger="hover" class="popovers"><i
                                    class="fa fa-upload"></i> </span></a>
                    </td>
                </tr>
                <tr>
                    <td class="text-center">3</td>
                    <td >12 Oct 2019</td>
                    <td>812/4/0065</td>
                    <td>Programme C</td>
                    <td class="text-center"><span class="badge badge-primary">Corrections</span></td>
                    <td class="text-center">
                        <a class="btn btn-xs btn-warning " href="{{ URL::route('programme-new') }}">
                            <span data-content="View PI" data-placement="top" data-trigger="hover" class="popovers"><i
                                    class="fa fa-eye"></i><span></a>
                        <a class="btn btn-xs btn-primary" href="{{ URL::route('programme-new') }}">
                            <span data-content="Review PI" data-placement="top" data-trigger="hover" class="popovers"><i
                                    class="fa fa-edit"></i> </span></a>
                        <a class="btn btn-xs btn-success" href="{{ URL::route('structure') }}">
                            <span data-content="Review Structure " data-placement="top" data-trigger="hover"
                                class="popovers"><i class="fa fa-sitemap"></i> </span></a>
                        <a class="btn btn-xs btn-dark" href="#download" data-toggle="modal" data-target="#download">
                            <span data-content="Download " data-placement="top" data-trigger="hover" class="popovers"><i
                                    class="fa fa-cloud-download"></i></span> </a>
                        <a class="btn btn-xs btn-info" href="" data-toggle="modal" data-target="#publish">
                            <span data-content="Publish " data-placement="top" data-trigger="hover" class="popovers"><i
                                    class="fa fa-upload"></i> </span></a>
                    </td>
                </tr>

            </tbody>
        </table>


        
    </div>
</div>

{{-- m o d a l --}}
<div class="modal fade form-container" id="download" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">

    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Download</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group col-md-12 text-center">
                        <button type="button" class="btn btn-light btn-sm" data-toggle="modal" data-dismiss="modal"
                            data-target="#core-new">MQA01 Format</button>
                        &ensp;|&ensp;
                        <button type="button" class="btn btn-light btn-sm" data-toggle="modal" data-dismiss="modal"
                            data-target="#core-existing">Short Version</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>

<div class="modal fade " id="publish" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Publish Programme <i class="fa fa-upload mr-2"></i></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body form hovering ld-over-inverse" id="test">

                <div id="message">
                    <div class="text-center">
                        <div class="i-circle text-warning"><i class="icon fa fa-exclamation-triangle"
                                aria-hidden="true"></i></div>
                        <h4>Are you sure ? </h4>
                        <p>you want to Publish this Programme and Module Info?<br>
                            Publishing will lock any and all edits on the PI and
                            related MI</p>
                    </div>
                    <div class="form-group text-center">
                        <div class="btn btn-primary btn-sm btn-rounded btn-space hovering ld-over-inverse"
                            id="importData">
                            Yes-Publish
                        </div>
                        <a class="btn btn-dark btn-sm btn-rounded btn-space" id="importData"
                            href="{{ URL::route('review') }}">
                            No-close
                        </a>

                    </div>
                </div>
                <div id="import-msg">
                    <div class="text-center">
                        <div class="i-circle text-success">
                            <i class="icon fa fa-check"></i>
                        </div>
                        <h4>Done!</h4>
                        <p>The Programme <span class="badge badge-primary">ACC1001</span> been successfully published
                        </p>
                    </div>
                </div>
                <div class="ld ld-ball ld-flip"></div>



            </div>
        </div>
    </div>
</div>

@endsection