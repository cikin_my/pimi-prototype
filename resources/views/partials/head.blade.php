<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <link rel="shortcut icon" href="{{ asset('img/favicon.png') }}">

    <title>PIMI</title>

    <!-- Bootstrap core CSS -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap-reset.css') }}" rel="stylesheet">
    <!--external css-->

    {{-- <link href="{{ asset('assets/font-awesome/css/font-awesome.css') }}" rel="stylesheet" /> --}}
    <link rel="stylesheet" href="{{ asset('assets/font-awesome-4.7.0/css/font-awesome.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css') }}" type="text/css"   media="screen" />
    <link rel="stylesheet" href="{{ asset('css/owl.carousel.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('css/tasks.css') }}" >
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/bootstrap-datepicker/css/datepicker.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/bootstrap-timepicker/compiled/timepicker.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/bootstrap-daterangepicker/daterangepicker-bs3.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/bootstrap-datetimepicker/css/datetimepicker.css') }}" />
    <!--loading button-->
    <link rel="stylesheet" href="{{ asset('assets/loading-io/loading.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/loading-io/loading-btn.css') }}">    
    <!--dynamic table-->
    <link href="{{ asset('assets/advanced-datatable/media/css/demo_page.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/advanced-datatable/media/css/demo_table.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="{{ asset('assets/data-tables/DT_bootstrap.css') }}" />
    <!--right slidebar-->
    <link href="{{ asset('css/slidebars.css') }}" rel="stylesheet">
    <!--select 2-->
    {{-- <link rel="stylesheet" type="text/css" href="{{ 'assets/select2/css/select2.min.css' }}" /> --}}
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet" />
    
    <link href="{{ asset('assets/summernote/summernote-bs4.css') }}" rel="stylesheet">
    {{-- gritter --}}
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/gritter/css/jquery.gritter.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/nestable/jquery.nestable.css') }}" />
    <!--bootstrap switcher-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/bootstrap-switch/static/stylesheets/bootstrap-switch.css') }}" />
    
    <!--Form Wizard-->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.steps.css') }}" />


    <!-- Custom styles for this template -->
    <link href="{{asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style-responsive.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="{{asset('css/app.css') }}">
</head>
