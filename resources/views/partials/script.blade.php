<script src="{{ asset('js/jquery.js') }}"></script>
<script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
<script class="include" type="text/javascript" src="{{ asset('js/jquery.dcjqaccordion.2.7.js') }}"></script>
<script src="{{ asset('js/jquery.scrollTo.min.js') }}"></script>
<script src="{{ asset('js/jquery.nicescroll.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/jquery.sparkline.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js') }}"></script>
<script src="{{ asset('js/owl.carousel.js') }}"></script>
<script src="{{ asset('js/jquery.customSelect.min.js') }}"></script>
<script src="{{ asset('js/respond.min.js') }}"></script>

<script src="{{ asset('js/slidebars.min.js') }}"></script>
<script src="{{ asset('js/bootstrap-validator.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/jquery.steps.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/jquery.validate.min.js') }}" type="text/javascript"></script>

<script type="text/javascript" src="{{ asset('assets/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js') }}">
</script>
<script type="text/javascript" src="{{ asset('assets/bootstrap-daterangepicker/moment.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/bootstrap-daterangepicker/daterangepicker.js') }}"></script>


{{-- data table  --}}
<script type="text/javascript" language="javascript"
    src="{{ asset('assets/advanced-datatable/media/js/jquery.dataTables.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/data-tables/DT_bootstrap.js') }}"></script>

<!--dynamic table initialization -->
<script src="{{ asset('js/dynamic_table_init.js') }}"></script>
<!--select2-->
<script type="text/javascript" src="{{ asset('assets/select2/js/select2.min.js') }}"></script>
{{-- gritter  --}}
<script type="text/javascript" src="{{ asset('assets/gritter/js/jquery.gritter.js') }}"></script>
<!--bootstrap-switch-->
<script src="{{asset ('assets/bootstrap-switch/static/js/bootstrap-switch.js') }}"></script>
<!-- Bootstrap Date Picker -->
{{-- <script type="text/javascript" src="{{ asset('assets/bootstrap-datepicker/js/bootstrap-datepicker.js') }}">
</script> --}}
{{-- <script type="text/javascript" src="{{ asset('assets/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js') }}">
--}}
{{-- </script> --}}
{{-- <script type="text/javascript" src="{{ asset('assets/bootstrap-daterangepicker/moment.min.js') }}"></script> --}}
{{-- <script type="text/javascript" src="{{ asset('assets/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
--}}
<script type="text/javascript" src="{{ asset('assets/bootstrap-timepicker/js/bootstrap-timepicker.js') }}"></script>


<!--pickers script-->
<script src="{{ asset('js/pickers/init-date-picker.js') }}"></script>
<script src="{{ asset('js/pickers/init-datetime-picker.js') }}"></script>

<!--custom checkbox & radio-->
<script type="text/javascript" src="{{ asset('js/ga.js') }}"></script>
<!--summernote-->
<script src="{{ asset('assets/summernote/summernote-bs4.min.js') }}"></script>



<!--script for this page DASHBOARS-->
<script src="{{ asset('js/sparkline-chart.js') }}"></script>
<script src="{{ asset('js/easy-pie-chart.js') }}"></script>
<script src="{{ asset('js/count.js') }}"></script>
<script src="{{ asset('js/gritter.js') }}" type="text/javascript"></script>

<script src="{{ asset('js/jquery.stepy.js') }}"></script>


<!--common script for all pages-->
<script src={{ asset('js/custom.js') }}></script>
<script src="{{ asset('js/common-scripts.js') }}"></script>

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>

<script>
    //owl carousel
    $(document).ready(function () {
        $("#owl-demo").owlCarousel({
            navigation: true,
            slideSpeed: 300,
            paginationSpeed: 400,
            singleItem: true,
            autoPlay: true

        });
    });
    $(function () {
        $('select.styled').customSelect();
    });

    
    //step wizard
    $(function () {
        $('#default').stepy({
            backLabel: 'Previous',
            block: true,
            nextLabel: 'Next',
            titleClick: true,
            titleTarget: '.stepy-tab'
        });
    });

    $(document).ready(function () {
        var form = $("#wizard-validation-form");
        form.validate({
            errorPlacement: function errorPlacement(error, element) {
                element.after(error);
            }
        });
        form.children("div").steps({
            headerTag: "h3",
            bodyTag: "section",
            transitionEffect: "slideLeft",
            onStepChanging: function (event, currentIndex, newIndex) {
                form.validate().settings.ignore = ":disabled,:hidden";
                return form.valid();
            },
            onFinishing: function (event, currentIndex) {
                form.validate().settings.ignore = ":disabled";
                return form.valid();
            },
            onFinished: function (event, currentIndex) {
                alert("Submitted!");
            }
        }).validate({
            errorPlacement: function errorPlacement(error, element) {
                element.after(error);
            },
            rules: {
                confirm: {
                    equalTo: "#password"
                }
            }
        });
    });
</script>

@stack('extraScripts')