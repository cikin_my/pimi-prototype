<!DOCTYPE HTML>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>PIMI @ Taylors</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Tylor's PIMI" />
	<meta name="keywords" content="pimi" />
	<meta name="author" content="grtech sdn bhd" />


	<link rel="shortcut icon" href="{{ asset('landing/images/favicon//favicon.ico') }}" type="image/x-icon">
	<link rel="icon" href="{{ asset('images/favicon//favicon.ico') }}" type="image/x-icon">

	<link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,700" rel="stylesheet">


	<!-- Animate.css -->
	<link rel="stylesheet" href="{{ asset('landing/css/animate.css') }}">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="{{ asset('landing/css/icomoon.css') }}">
	<!-- Bootstrap  -->
	<!-- <link rel="stylesheet" href="css/bootstrap.css"> -->
	<link rel="stylesheet" href="{{ url('https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css') }}"
		integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

	<!-- fontawesome -->
    <link href="{{ asset('landing/fonts/fontawesome-free-5.11.2-web/css/fontawesome.css') }}" rel="stylesheet">
	<link href="{{ asset('landing/fonts/fontawesome-free-5.11.2-web/css/brands.css') }}" rel="stylesheet">
	<link href="{{ asset('landing/fonts/fontawesome-free-5.11.2-web/css/solid.css') }}" rel="stylesheet">

	<!-- Magnific Popup -->
	<link rel="stylesheet" href="{{ asset('landing/css/magnific-popup.css') }}">

	<!-- Flexslider  -->
	<link rel="stylesheet" href="{{ asset('landing/css/flexslider.css') }}">

	<!-- Owl Carousel -->
	<link rel="stylesheet" href="{{ asset('landing/css/owl.carousel.min.css') }}">
	<link rel="stylesheet" href="{{ asset('landing/css/owl.theme.default.min.css') }}">

	<!-- Flaticons  -->
	<link rel="stylesheet" href="{{ asset('landing/fonts/flaticon/font/flaticon.css') }}">

	<!-- Theme style  -->
	<link rel="stylesheet" href="{{ asset('landing/css/style.css') }}">
	<link rel="stylesheet" href="{{ asset('landing/css/main.css') }}">


	<!-- Modernizr JS -->
	<script src="{{ asset('landing/js/modernizr-2.6.2.min.js') }}"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->
	</head>

<body>

	<div class="colorlib-loader"></div>

	<div id="page">
		<nav class="colorlib-nav" role="navigation">
			<div class="top-menu">
				<div class="container">
					<div class="row">
						<div class="col-2">
							<a href="#"><img src="{{ asset('landing/images/logo/colorless-1.png') }}" alt="" style="width:100%"></a>
						</div>
						<div class="col-10 text-right menu-1">
							<ul>
								<li class="active"><a href="index.html"><span class="icon"><i class="flaticon-house mr-2"></i></span>Home</a>
								</li>
								<li><a href="">About</a>
								</li>
								<li class="has-dropdown"> <a href="{{ URL::route('dashboard') }}">login</a>
									<ul class="dropdown">
										<li><a>Login to start application</a>
										</li>
									</ul>
								</li>
								<li><a href="">Contact</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</nav>
		<aside id="colorlib-hero">
			<div class="flexslider">
				<ul class="slides">
					<li style="background-image: url(landing/images/img_bg_4.jpg);">
						<div class="overlay"></div>
						<div class="container-fluid">
							<div class="row">
								<div class="col-lg-8 col-md-12 offset-md-2 slider-text">
									<div class="slider-text-inner text-center">
										<h1>Programme and Module Development	</h1>
										<h2>Automation of process | Guidelines</h2>
									</div>
								</div>
							</div>
						</div>
					</li>
					<li style="background-image: url(landing/images/img_bg_2.jpg);">
						<div class="overlay"></div>
						<div class="container-fluid">
							<div class="row">
								<div class="col-lg-8 col-md-12 offset-md-2 slider-text">
									<div class="slider-text-inner text-center">
										<h1>Tracking Programme Changes</h1>
										<h2>Track changes of existing programme</h2>
									</div>
								</div>
							</div>
						</div>
					</li>
					<li style="background-image: url(landing/images/img_bg_3.jpg);">
						<div class="overlay"></div>
						<div class="container-fluids">
							<div class="row">
								<div class="col-lg-8 col-md-12 offset-md-2 slider-text">
									<div class="slider-text-inner text-center">
										<h1>Programme & Module Search</h1>
										<h2>Designed for easily filter and search programme and modules</h2>
									</div>
								</div>
							</div>
						</div>
					</li>
					<li style="background-image: url(landing/images/img_bg_1.jpg);">
						<div class="overlay"></div>
						<div class="container-fluids">
							<div class="row">
								<div class="col-lg-8 col-md-12 offset-md-2 slider-text">
									<div class="slider-text-inner text-center">
										<h1>We Build Your Futures</h1>
										<h2>We Design All Kinds of Buildings</h2>
									</div>
								</div>
							</div>
						</div>
					</li>
				</ul>
			</div>
		</aside>

		

		<div id="colorlib-intro">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 tabulation animate-box">
						<ul class="nav nav-tabs">
							<li class="active nav-item"><a data-toggle="tab" href="#plan" class="nav-link"><i class="fas fa-user mr-2 text-danger"></i> User Login</a>
							</li>
							<div class="row">
								<div class="col md-12 m-5">
									<h4 class="text-white text-center ml-5">  Login to start Application <i class="fas fa-tachometer-alt ml-2"></i></h4>
								</div>
							</div>
						
								
						
							
							
						</ul>
						<div class="tab-content">
							<div id="plan" class="tab-pane active">
								<div class="row">
									<div class="col-lg-6">
										<div class="services-img" style="background-image: url(landing/images/001.jpg);">
										</div>
									</div>
									<div class="col-lg-6 bg-grey">
										<div class="services-desc mt-4">
												<form action="#">
													<div class="row form-group">
														<div class="col-md-12">
															<!-- <label for="fname">First Name</label> -->
															<input type="text" id="username" class="form-control" placeholder="Your Username">
														</div>														
													</div>
												
													<div class="row form-group">
														<div class="col-md-12">
															<!-- <label for="lname">Last Name</label> -->
															<input type="password" id="pass" class="form-control" placeholder="Your Password">
															<small style="letter-spacing: mormal"><a href="" class="btn btn-light btn-xs text-danger mt-2">Forgot your password?</a> </small>
														</div>
													
													</div>
												
													<div class="row form-group">
														
													</div>
												
													
													<div class="form-group text-center row">
														<div class="col-md-6">
															<a href="{{ URL::route('dashboard') }}" type="submit" value="Login" class="btn btn-danger btn-sm btn-block"> <i class="fas fa-sign-in-alt mr-2"></i>
																Login</a >
														</div>
														<div class="col-md-6">
															<button data-toggle="modal" data-target="#createacc" value="Login" class="btn btn-secondary btn-sm btn-block"> <i class="fas fa-lock mr-2"></i>
																Create New Account</button>
														</div>
														

													</div>
												</form>
										</div>
									</div>
								</div>
							</div>
							<div id="general" class="tab-pane fade">
								<div class="row">
									<div class="col-lg-6">
										<div class="services-img" style="background-image: url(landing/images/building-2.jpg);">
										</div>
									</div>
									<div class="col-lg-6">
										<div class="services-desc">
											<p>A small river named Duden flows by their place and supplies it with the
												necessary regelialia. It is a paradisematic country, in which roasted
												parts
												of sentences fly into your mouth. Far far away, behind the word
												mountains,
												far from the countries Vokalia and Consonantia, there live the blind
												texts.</p>
											<ul>
												<li>It is a paradisematic country in which</li>
												<li>Even the all-powerful Pointing has no control</li>
												<li>When she reached the first hills of the Italic Mountains</li>
												<li>Alphabet Village and the subline of her own road</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
							<div id="manage" class="tab-pane fade">
								<div class="row">
									<div class="col-lg-6">
										<div class="services-img" style="background-image: url(landing/images/building-3.jpg);">
										</div>
									</div>
									<div class="col-lg-6">
										<div class="services-desc">
											<p>A small river named Duden flows by their place and supplies it with the
												necessary regelialia. It is a paradisematic country, in which roasted
												parts
												of sentences fly into your mouth. Far far away, behind the word
												mountains,
												far from the countries Vokalia and Consonantia, there live the blind
												texts.</p>
											<ul>
												<li>It is a paradisematic country in which</li>
												<li>Even the all-powerful Pointing has no control</li>
												<li>When she reached the first hills of the Italic Mountains</li>
												<li>Alphabet Village and the subline of her own road</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
							<div id="modeling" class="tab-pane fade">
								<div class="row">
									<div class="col-lg-6">
										<div class="services-img" style="background-image: url(landing/images/building-4.jpg);">
										</div>
									</div>
									<div class="col-lg-6">
										<div class="services-desc">
											<p>A small river named Duden flows by their place and supplies it with the
												necessary regelialia. It is a paradisematic country, in which roasted
												parts
												of sentences fly into your mouth. Far far away, behind the word
												mountains,
												far from the countries Vokalia and Consonantia, there live the blind
												texts.</p>
											<ul>
												<li>It is a paradisematic country in which</li>
												<li>Even the all-powerful Pointing has no control</li>
												<li>When she reached the first hills of the Italic Mountains</li>
												<li>Alphabet Village and the subline of her own road</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="about-wrap">
			<div id="colorlib-about" class="colorlib-about-img" style="background-image: url(landing/images/cover_img_2.jpg);"
				data-stellar-background-ratio="0.5">
				<div class="overlay"></div>
				<div class="container">
					<div class="row">
						<div class="col-lg-8 offset-md-2 text-center colorlib-heading colorlib-heading2 animate-box">
							<h2 style="letter-spacing: 2px;"> PIMI </h2>
							<p>programme information and module information is a platform where user can 
								create programmes and modules, getting an approval, Improving communication through specific channel 
							to enhance productivity and efficiency of process</p>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12 animate-box">
							<div class="about-flex">
								<div class="col-half"> <a href="#" class="staff-img"
										style="background-image: url(landing/images/about/1.jpg);">
		
										<div class="desc-staff">		
											<span>TAYLOR’S BUSINESS SCHOOL</span>
										</div>
		
									</a>
									<a href="" class="staff-img" style="background-image: url(landing/images/about/2.jpg);">
		
										<div class="desc-staff">		
											<span>SCHOOL OF HOSPITALITY, TOURISM & EVENTS</span>		
										</div>
		
									</a>
									<a href="" class="staff-img" style="background-image: url(landing/images/about/3.jpg);">
		
										<div class="desc-staff">
											<span>SCHOOL OF ARCHITECTURE, BUILDING AND DESIGN</span>		
										</div>
		
									</a>
									<a href="#" class="staff-img" style="background-image: url(landing/images/about/4.jpg);">
		
										<div class="desc-staff">
											<span>SCHOOL OF MEDIA AND COMMUNICATION</span>		
										</div>
		
									</a>
								</div>
								<div class="col-half">
									<div class="desc">
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna
										aliqua.</p>
										<div class="fancy-collapse-panel">
											<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
												<div class="card">
													<div class="card-header" role="tab" id="headingOne">
														<h5 class="card-title">
		
															<a data-toggle="collapse" data-parent="#accordion"
																href="#collapseOne" aria-expanded="true"
																aria-controls="collapseOne">Programme Development & Approval
															</a>
		
														</h4>
													</div>
													<div id="collapseOne" class="panel-collapse collapse show" role="tabpanel"
														aria-labelledby="headingOne">
														<div class="card-body">
															<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna
															aliqua.</p>
														</div>
													</div>
												</div>
												<div class="card">
													<div class="card-header" role="tab" id="headingTwo">
														<h5 class="card-title">
		
															<a class="collapsed" data-toggle="collapse" data-parent="#accordion"
																href="#collapseTwo" aria-expanded="false"
																aria-controls="collapseTwo">Module Development & Approval
		
															</a>
		
														</h4>
													</div>
													<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel"
														aria-labelledby="headingTwo">
														<div class="card-body">
															<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna
															aliqua.</p>
														</div>
													</div>
												</div>
												<div class="card">
													<div class="card-header" role="tab" id="headingThree">
														<h5 class="card-title">
		
															<a class="collapsed" data-toggle="collapse" data-parent="#accordion"
																href="#collapseThree" aria-expanded="false"
																aria-controls="collapseThree">Programme & Module Handbook
		
															</a>
		
														</h4>
													</div>
													<div id="collapseThree" class="panel-collapse collapse" role="tabpanel"
														aria-labelledby="headingThree">
														<div class="card-body">
															<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna
															aliqua.</p>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div id="colorlib-services">
			<div class="container">
				<div class="row">
					<div class="col-lg-4 text-center animate-box">
						<div class="services"> <span class="icon">

								<i class="flaticon-value"></i>

							</span>
							<div class="desc">
								<h3>General User management</h3>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam</p>
							</div>
						</div>
					</div>
					<div class="col-lg-4 text-center animate-box">
						<div class="services"> <span class="icon">

								<i class="flaticon-new-file"></i>

							</span>
							<div class="desc">
								<h3>Create Pogrammes & Modules</h3>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam</p>
							</div>
						</div>
					</div>
					<div class="col-lg-4 text-center animate-box">
						<div class="services"> <span class="icon">

								<i class="flaticon-approve-invoice"></i>

							</span>
							<div class="desc">
								<h3>Programmes approval system</h3>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam</p>
							</div>
						</div>
					</div>
					<div class="col-lg-4 text-center animate-box">
						<div class="services"> <span class="icon">

								<i class="flaticon-reminder"></i>

							</span>
							<div class="desc">
								<h3>Notification & reminder</h3>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam</p>
							</div>
						</div>
					</div>
					<div class="col-lg-4 text-center animate-box">
						<div class="services"> <span class="icon">

								<i class="flaticon-checklist"></i>

							</span>
							<div class="desc">
								<h3>Handle complex form submission</h3>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam</p>
							</div>
						</div>
					</div>
					<div class="col-lg-4 text-center animate-box">
						<div class="services"> <span class="icon">

								<i class="flaticon-implement"></i>

							</span>
							<div class="desc">
								<h3>Programmes Management</h3>
								<p>Separated they live in Bookmarksgrove right at the coast of the Semantics</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div id="colorlib-counter" class="colorlib-counters" style="background-image: url(landing/images/cover_img_1.jpg);"
			data-stellar-background-ratio="0.5">
			<div class="overlay"></div>
			<div class="container">
				<div class="row no-gutter">
					<div class="col-lg-3 col-md-6 aside-stretch text-center animate-box">
						<div class="counter-entry"> <span class="icon icon-white"><i
									class="flaticon-mortarboard"></i></span>
							<span class="colorlib-counter js-counter" data-from="0" data-to="555" data-speed="5000"
								data-refresh-interval="50"></span> <span class="colorlib-counter-label">Programme & Module</span>
						</div>
					</div>
					<div class="col-lg-3 col-md-6 text-center animate-box">
						<div class="counter-entry"> <span class="icon"><i class="flaticon-education"></i></span>
							<span class="colorlib-counter js-counter" data-from="0" data-to="231" data-speed="5000"
								data-refresh-interval="50"></span>
							<span class="colorlib-counter-label">Programmes</span>
						</div>
					</div>
					<div class="col-lg-3 col-md-6 text-center animate-box">
						<div class="counter-entry"> <span class="icon"><i class="flaticon-chemistry"></i></span>
							<span class="colorlib-counter js-counter" data-from="0" data-to="324" data-speed="5000"
								data-refresh-interval="50"></span> <span
								class="colorlib-counter-label">Modules</span>
						</div>
					</div>
					<div class="col-lg-3 col-md-6 text-center animate-box">
						<div class="counter-entry"> <span class="icon"><i class="flaticon-test"></i></span>
							<span class="colorlib-counter js-counter" data-from="0" data-to="30" data-speed="5000"
								data-refresh-interval="50"></span>
							<span class="colorlib-counter-label">Programme Change</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	
		<footer id="colorlib-footer">
			<div class="contact-information">
				<div class="container">
					<div class="row">
						<div class="col-lg-8 offset-md-2">
							<div class="row">
								<div class="col-lg-4 text-center">
									<div class="info-wrap"> <span class="icon"><i class="icon-location4"></i></span>
										<p>No. 1 Jalan Taylor's 47500, 
											<br>Subang Jaya,Selangor Darul Ehsan,Malaysia</p>
									</div>
								</div>
								<div class="col-lg-4 text-center">
									<div class="info-wrap"> <span class="icon"><i class="icon-world"></i></span>
										<p><a href="#">applications@taylors.edu.my</a>
											<br> <a href="https://university.taylors.edu.my/">https://university.taylors.edu.my/</a>
										</p>
									</div>
								</div>
								<div class="col-lg-4 text-center">
									<div class="info-wrap"> <span class="icon"><i class="icon-phone2"></i></span>
										<p><a href="#"><i class="fas fa-phone mr-2"></i> 603-5629 5000</a>
											<br> <a href="#"><i class="fas fa-fax mr-2"></i>603-5629 5001</a>
										</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="container">
				<div class="row mb-2 mt-0 pt-0">
					<div class="col-lg-12 colorlib-widget text-center">						
							<ul class="colorlib-social-icons">
								<li><a href="#"><i class="icon-twitter2 mr-4"></i></a>
								</li>
								<li><a href="#"><i class="icon-facebook2 mr-4"></i></a>
								</li>
								<li><a href="#"><i class="icon-instagram mr-4"></i></a>
								</li>
								<li><a href="#"><i class="icon-youtube2 mr-4"></i></a>
								</li>
							</ul>
						</p>
					</div>					
				</div>
			</div>
			<div class="copy">
				<div class="container">
					<div class="row">
						<div class="col-lg-12 text-center">
							<p> <small class="block">
									Copyright 2019 &#xA9; Taylors University 
									<a href="https://colorlib.com" target="_blank">.</a>
								</small>
							</p>
						</div>
					</div>
				</div>
			</div>
		</footer>
	</div>

	<div class="gototop js-top">
		<a href="#" class="js-gotop"><i class="icon-arrow-up2"></i></a>
	</div>



	<div class="modal fade " id="createacc" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
		aria-hidden="true">
		<div class="modal-dialog " role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Create New Account</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body bg-grey">
					<form>
					<div class="row form-group">
							<div class="col-md-12">
								<!-- <label for="fname">First Name</label> -->
								<input type="text" id="username" class="form-control" placeholder="Your ID">
							</div>
						</div>
						<div class="row form-group">
								<div class="col-md-12">
									<!-- <label for="fname">First Name</label> -->
									<input type="text" id="username" class="form-control" placeholder="Your Email Address">
								</div>
							</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-danger" data-dismiss="modal">Submit</button>
				</div>
			</div>
		</div>
	</div>

	<script src="{{ asset('landing/js/jquery-3.4.1.min.js') }}"></script>

	<script src="https://code.jquery.com/jquery-migrate-3.1.0.min.js"
		integrity="sha256-ycJeXbll9m7dHKeaPbXBkZH8BuP99SmPm/8q5O+SbBc=" crossorigin="anonymous"></script>


	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
		integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous">
	</script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
		integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous">
	</script>
	<!-- jQuery -->
	<!-- <script src="js/jquery.min.js"></script> -->
	<!-- jQuery Easing -->
	<script src="{{ asset('landing/js/jquery.easing.1.3.js') }}"></script>
	<!-- Bootstrap -->
	<!-- <script src="js/bootstrap.min.js"></script> -->


	<!-- Waypoints -->
	<script src="{{ asset('landing/js/jquery.waypoints.min.js') }}"></script>
	<!-- Stellar Parallax -->
	<script src="{{ asset('landing/js/jquery.stellar.min.js') }}"></script>
	<!-- Flexslider -->
	<script src="{{ asset('landing/js/jquery.flexslider-min.js') }}"></script>
	<!-- Owl carousel -->
	<script src="{{ asset('landing/js/owl.carousel.min.js') }}"></script>
	<!-- Magnific Popup -->
	<script src="{{ asset('landing/js/jquery.magnific-popup.min.js') }}"></script>
	<script src="{{ asset('landing/js/magnific-popup-options.js') }}"></script>
	<!-- Counters -->
	<script src="{{ asset('landing/js/jquery.countTo.js') }}"></script>
	<!-- Main -->
	<script src="{{ asset('landing/js/main.js') }}"></script>

</body>

</html>
