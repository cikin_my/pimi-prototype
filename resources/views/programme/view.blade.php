@extends('layouts.main.master')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <section class="card">
            <header class="card-header">
                 New Programme Information Summary 
            </header>
            <div class="card-body">

                <form class="form-horizontal" id="">
                    <fieldset title="Step1" class="step" id="default-step-1">

                        <legend> </legend>

                        <div class="card bg-light">
                            <header class="card-header">
                                Section 1
                            </header>
                            <div class="card-body">
                            </div>

                            <div class="row pl-4 pr-4">
                                <div class="col-md-6">
                                    <div class="card bg-1">

                                        <div class="card-body">
                                            <table
                                                class="table table-hover table-borderless table-small-text summary-tb">
                                                <tr>
                                                    <td class="field" width="55%">Campus Code</td>
                                                    <td class="text-field">Taylor's Lakeside Campus</td>
                                                </tr>
                                                <tr>
                                                    <td class="field">TCF Flagx</td>
                                                    <td class="text-field">YES</td>
                                                </tr>
                                                <tr>
                                                    <td class="field">Programme Name</td>
                                                    <td class="text-field">Programme A</td>
                                                </tr>
                                                <tr>
                                                    <td class="field">School Owning</td>
                                                    <td class="text-field">School of Education</td>
                                                </tr>
                                                <tr>
                                                    <td class="field">Field of Study and National Education Code (NEC)
                                                    </td>
                                                    <td class="text-field">Language</td>
                                                </tr>
                                                <tr>
                                                    <td class="field">TU Programme Code</td>
                                                    <td class="text-field">92475</td>
                                                </tr>

                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="card bg-1">

                                        <div class="card-body">
                                            <table
                                                class="table table-hover table-borderless table-small-text summary-tb">
                                                <tr>
                                                    <td class="field">MOE Programme Code</td>
                                                    <td class="text-field">To be keyed in by RAG upon approval</td>
                                                </tr>
                                                <tr>
                                                    <td class="field">MQA Programme Code</td>
                                                    <td class="text-field">To be keyed in by RAG upon approval</td>
                                                </tr>
                                                <tr>
                                                    <td class="field">MOE Expiry Date</td>
                                                    <td class="text-field">To be keyed in by RAG upon approval</td>
                                                </tr>
                                                <tr>
                                                    <td class="field">MQF Level</td>
                                                    <td class="text-field">Diploma</td>
                                                </tr>
                                                <tr>
                                                    <td class="field">Graduating Credit</td>
                                                    <td class="text-field"></td>
                                                </tr>
                                                <tr>
                                                    <td class="field">Mode of Study</td>
                                                    <td class="text-field">Part Time</td>
                                                </tr>

                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>



                    </fieldset>

                    <fieldset title="Step 2" class="step" id="default-step-2">
                        <legend> </legend>
                        <div class="card bg-light">
                            <header class="card-header">
                                Section 2
                            </header>
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="">Duration of Study</label>
                                    <div class="col-lg-12">
                                        <table class="table table-borderless text-center">
                                            <tr class="bg-info1">
                                                <td rowspan="2" class="light-column"></td>
                                                <td class="text-center fulltime" colspan="2">Full Time</td>
                                                <td class="text-center partime" colspan="2">Part Time Time</td>
                                            </tr>
                                            <tr class="bg-info2">
                                                <td class="fulltime"><span class="badge badge-warning text-white">Long
                                                        Semester</span></td>
                                                <td class="fulltime"><span class="badge badge-info">Short
                                                        Semester</span></td>
                                                <td class="partime"><span class="badge badge-warning text-white">Long
                                                        Semester</span></td>
                                                <td class="partime"><span class="badge badge-info">Short Semester</span>
                                                </td>

                                            </tr>
                                            <tr class="bg-info3">
                                                <td class="light-column">No. of Weeks*</td>
                                                <td class="fulltime">16</td>
                                                <td class="fulltime">12</td>
                                                <td class="partime">14</td>
                                                <td class="partime">10</td>

                                            </tr>
                                            <tr class="bg-info3">
                                                <td class="light-column">No. of Semester</td>
                                                <td class="fulltime">
                                                    4
                                                </td>
                                                <td class="fulltime">
                                                    1
                                                </td>
                                                <td class="partime">
                                                    4
                                                </td>
                                                <td class="partime">
                                                    2
                                                </td>
                                            </tr>
                                            <tr class="bg-info3">
                                                <td class="light-column">No. of Years</td>
                                                <td class="fulltime">
                                                    3
                                                </td>
                                                <td class="fulltime">
                                                    4
                                                </td>
                                                <td class="partime">
                                                    4
                                                </td>
                                                <td class="partime">
                                                    5
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-12 text-right">
                                        <button type="button" class="btn btn-primary btn-sm">Download PDF <i
                                                class="ml-2 fa fa-file-pdf-o" aria-hidden="true"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </fieldset>


                    <fieldset title="Step 3" class="step" id="default-step-3">
                        <legend> </legend>
                        <div class="card bg-light">
                            <header class="card-header">
                                Section 3
                            </header>

                            <div class="row p-4">

                                <div class="col-md-12">
                                    <div class="card bg-1">
                                        <div class="card-body">
                                            <table
                                                class="table table-hover table-borderless table-small-text summary-tb">
                                                <tr>
                                                    <td class="field" width="55%">Type of Award</td>
                                                    <td class="text-field"> Award Programme | Single Award (1 testamur )
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="field">Awarding Body </td>
                                                    <td class="text-field">Own</td>
                                                </tr>
                                                <tr>
                                                    <td class="field"> Type of Programme</td>
                                                    <td class="text-field">Mixed Mode</td>
                                                </tr>
                                                <tr>
                                                    <td class="field">Mode of Study </td>
                                                    <td class="text-field">Double Specialization</td>
                                                </tr>
                                                <tr>
                                                    <td class="field">Accreditation Body</td>
                                                    <td class="text-field">Malaysian Qualifications Agency (MQA)</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>





                    </fieldset>


                    <fieldset title="Step 4" class="step" id="default-step-3">
                        <legend></legend>
                        <div class="card bg-light">
                            <header class="card-header">
                                Section 4
                            </header>
                            <div class="card-body">


                                <div class="row p-2">

                                    <div class="col-md-12">
                                        <div class="card bg-1">
                                            <div class="card-body">
                                                <table
                                                    class="table table-hover table-borderless table-small-text summary-tb">
                                                    <tr>
                                                        <td class="field" width="55%">Taylor's Lakeside Campus Address
                                                        </td>
                                                        <td class="text-field"> No. 1 Jalan Taylor's 47500 Subang Jaya,
                                                            Selangor Darul Ehsan,Malaysia</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="field">Method of Learning </td>
                                                        <td class="text-field">Field Work</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="field"> Mode of Delivery </td>
                                                        <td class="text-field">Conventional (traditional, online and
                                                            blended learning </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="field"> </td>
                                                        <td class="text-field">Open and Distance Learning (ODL) </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="field">Language of Instruction </td>
                                                        <td class="text-field">English </td>
                                                    </tr>

                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </fieldset>

                    <fieldset title="Step 5" class="step" id="default-step-4">
                        <legend></legend>
                        <div class="card bg-light">
                            <header class="card-header">
                                Section 5
                            </header>
                            <div class="card-body">
                                <h5>Programme Educational Objectives</h5>
                                <div id="table">

                                    <table class="table table-hover table-borderless table-small-text summary-tb bg-2">
                                        <thead>
                                            <tr>
                                                <th width="10%" class="text-center">PEO ID</th>
                                                <th>PEO Description</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="text-center">PEO 1</td>
                                                <td contenteditable="true">
                                                    Knowledge and Understanding:
                                                    At the end of the programme, students should be able to
                                                    demonstrate knowledge and understanding of the
                                                    terminology and the theoretical basis of performance
                                                </td>

                                            </tr>
                                            <tr>
                                                <td class="text-center">PEO 2</td>
                                                <td contenteditable="true">
                                                    Application:
                                                    At the end of the programme, students should be able to
                                                    use art confidently as a form of expression and
                                                    communication while demonstrating a range of technical
                                                    skills in formal or informal performance and exhibition.
                                                </td>

                                            </tr>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>


                    </fieldset>

                    <fieldset title="Step 6" class="step" id="default-step-5">
                        <legend></legend>

                        <div class="card bg-light">
                            <header class="card-header">
                                Section 6
                            </header>
                            <div class="card-body">
                                <h5>Programme Educational Objectives</h5>
                                <div id="table2">

                                <table class="table table-hover table-borderless summary-tb bg-2 ">
                                        <thead>
                                            <tr class="text-center">
                                                <th width="10%" class="text-left">PLO No</th>
                                                <th>PLO Description</th>
                                                <th>Set KPI/Target</th>
                                                <th>T & L Strategies</th>
                                                <th width="13%">Assessment Mode</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>PLO 1</td>
                                                <td contenteditable="true" class="text-left">
                                                    Demonstrate understanding of key theories of the field of theatre
                                                    permormance in analysis, critique, performance and production of
                                                    works.
                                                </td>
                                                <td>50%</td>
                                                <td class="text-left">
                                                    The facilitator will demonstrate the exercises and student will
                                                    learn
                                                    throught practical engament in these exercises
                                                </td>
                                                <td>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="custom-control custom-checkbox">

                                                                Journal Writing

                                                            </div>
                                                        </div>

                                                    </div>
                                                </td>

                                            </tr>
                                            <tr>
                                                <td>PLO 2</td>
                                                <td contenteditable="true" class="text-left">
                                                    Lorem ipsum, dolor sit amet consectetur adipisicing elit. Earum
                                                    accusamus
                                                    tempora cupiditate ipsam molestiae quas quos, asperiores ex nemo
                                                    sint,
                                                    libero sit quisquam maxime dicta dolorem. Quas minus deserunt odit.
                                                </td>
                                                <td>50%</td>
                                                <td class="text-left">Lorem ipsum dolor sit amet consectetur adipisicing
                                                    elit. Dicta
                                                    facere rerum
                                                    in qui accusamus, vitae totam dignissimos ullam a necessitatibus?
                                                    Dolor
                                                    tempora, dolore hic omnis veniam earum deserunt molestias est.</td>
                                                <td>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="custom-control custom-checkbox">
                                                                Presentation
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="custom-control custom-checkbox">
                                                                Journal Writing
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>

                                            </tr>
                                            <tr>
                                                <td>PLO 3</td>
                                                <td contenteditable="true" class="text-left">
                                                    Lorem ipsum, dolor sit amet consectetur adipisicing elit. Earum
                                                    accusamus
                                                    tempora cupiditate ipsam molestiae quas quos, asperiores ex nemo
                                                    sint,
                                                    libero sit quisquam maxime dicta dolorem. Quas minus deserunt odit.
                                                </td>
                                                <td>50%</td>
                                                <td class="text-left">Lorem ipsum dolor sit amet consectetur adipisicing
                                                    elit. Dicta
                                                    facere rerum
                                                    in qui accusamus, vitae totam dignissimos ullam a necessitatibus?
                                                    Dolor
                                                    tempora, dolore hic omnis veniam earum deserunt molestias est.</td>
                                                <td>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="custom-control custom-checkbox">
                                                                Journal Writing
                                                            </div>

                                                        </div>

                                                    </div>
                                                </td>

                                            </tr>
                                            <tr>
                                                <td>PLO 4</td>
                                                <td contenteditable="true" class="text-left">
                                                    Lorem ipsum, dolor sit amet consectetur adipisicing elit. Earum
                                                    accusamus
                                                    tempora cupiditate ipsam molestiae quas quos, asperiores ex nemo
                                                    sint,
                                                    libero sit quisquam maxime dicta dolorem. Quas minus deserunt odit.
                                                </td>
                                                <td>50%</td>
                                                <td class="text-left">Lorem ipsum dolor sit amet consectetur adipisicing
                                                    elit. Dicta
                                                    facere rerum
                                                    in qui accusamus, vitae totam dignissimos ullam a necessitatibus?
                                                    Dolor
                                                    tempora, dolore hic omnis veniam earum deserunt molestias est.</td>
                                                <td>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="custom-control custom-checkbox">
                                                                Presentation

                                                            </div>
                                                        </div>

                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>


                    </fieldset>

                    <fieldset title="Step 7" class="step" id="default-step-6">
                        <legend></legend>
                        <div class="card bg-light">
                            <header class="card-header">
                                Section 7
                            </header>
                            <div class="card-body">
                                <h4>Programme Standards</h4>
                             
                                   
                                 <table class="table table-borderless table-hover summary-tb bg-2">
                                        <thead>
                                            <tr class="text-center">
                                                <th width="10%">PS ID</th>
                                                <th>PS Description</th>                                               
                                            </tr>
                                            <tr>
                                                <td class="col-item text-center">PS 1</td>
                                                <td>
                                                    Lorem ipsum dolor sit amet consectetur, adipisicing elit. Impedit
                                                    consectetur deleniti quia ut dolores! Laboriosam, quaerat
                                                    repellendus
                                                    placeat reiciendis possimus odio aliquid magni dolore vel quia at
                                                    dignissimos. Illo, voluptatum?
                                                </td>
                                               
                                            </tr>
                                            <tr>
                                                <td class="col-item text-center">PS 2</td>
                                                <td>Lorem ipsum dolor sit amet consectetur adipisicing elit. Harum ea
                                                    accusantium accusamus quisquam velit! Eligendi placeat corporis ex
                                                    quibusdam
                                                    suscipit unde minima magni, quasi nam quis saepe odio dolores eius.
                                                </td>
                                               
                                            </tr>
                                           
                                        </thead>
                                    </table>
                               
                            </div>
                        </div>
                    </fieldset>

                    <fieldset title="Step 8" class="step" id="default-step-7">
                        <legend></legend>

                        <div class="card bg-light">
                            <header class="card-header">
                                Section 8
                            </header>
                            <div class="card-body">
                                <h5>Professional Body Requirements</h5>
                                <div id="table4">                                  
                                  <table class="table table-hover table-borderless  table-tb bg-2">
                                        <thead>
                                            <tr>
                                                <th width="5%">No.</th>
                                                <th width="25%">Professional Body Main Requirements</th>
                                                <th width="" colspan="2">Professional Body Sub Requirements
                                                    (Optional)</th>
                                               
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                  LAM 1
                                                </td>
                                                <td>
                                                   
                                                </td>
                                                <td>LAM 1.1</td>
                                                <td class="text-left">
                                                   Cillum ad ut irure tempor velit nostrud occaecat ullamco aliqua anim Lorem sint. Veniam sint duis incididunt do esse
                                                magna mollit excepteur laborum qui. Id id reprehenderit sit est eu aliqua
                                                </td>
                                               
                                            </tr>
                                            <tr>
                                                <td>                                                   
                                                </td>
                                                <td contenteditable="true">                                                 
                                                </td>
                                                <td>LAM 1.2
                                                </td>
                                                <td class="text-left">
                                                   Cillum ad ut irure tempor velit nostrud occaecat ullamco aliqua anim Lorem sint. Veniam sint duis incididunt do esse
                                                magna mollit excepteur laborum qui. Id id reprehenderit sit est eu aliqua
                                                </td>
                                              
                                            </tr>
                                            <tr>
                                                <td>LAM 2</td>
                                                <td contenteditable="true">                                               
                                                </td>
                                                <td>LAM 2.1</td>
                                                <td class="text-left">
                                                   Cillum ad ut irure tempor velit nostrud occaecat ullamco aliqua anim Lorem sint. Veniam sint duis incididunt do esse
                                                magna mollit excepteur laborum qui. Id id reprehenderit sit est eu aliqua
                                                </td>                                               
                                            </tr>
                                            <tr>
                                                <td>                                                  
                                                </td>
                                                <td contenteditable="true">                                                   
                                                </td>
                                                <td>LAM 2.2</td>
                                                <td class="text-left">
                                                    Cillum ad ut irure tempor velit nostrud occaecat ullamco aliqua anim Lorem sint. Veniam sint duis incididunt do esse
                                                    magna mollit excepteur laborum qui. Id id reprehenderit sit est eu aliqua
                                                </td> 
                                            </tr>
                                            <tr class="hide">
                                                <td>
                                                   
                                                </td>
                                                <td contenteditable="true">                                                   
                                                </td>
                                                <td>LAM 2.2</td>
                                                <td class="text-left">
                                                  Cillum ad ut irure tempor velit nostrud occaecat ullamco aliqua anim Lorem sint. Veniam sint duis incididunt do esse
                                                magna mollit excepteur laborum qui. Id id reprehenderit sit est eu aliqua
                                                </td>
                                               
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>


                    </fieldset>

                    <fieldset title="Step 9" class="step" id="default-step-8">
                        <legend></legend>
                        <div class="card bg-light">
                            <header class="card-header">
                                Section 9
                            </header>
                            <div class="card-body">
                                <a class="btn btn-space btn-xs btn-primary" href="{{ URL::route('mapping1') }}">Click here to proceed to TGC-PLO Mapping
                                    <i class="fa fa-chevron-right"></i></a>
                            </div>
                        </div>

                    </fieldset>

                    <fieldset title="Step 10" class="step" id="default-step-9">
                        <legend></legend>
                        <div class="card bg-light">
                            <header class="card-header">
                                Section 10
                            </header>
                            <div class="card-body">
                        
                            </div>
                        </div>

                    </fieldset>
                    <fieldset title="Step 11" class="step" id="default-step-10">
                        <legend></legend>
                        <div class="card bg-light">
                            <header class="card-header">
                                Section 11
                            </header>
                            <div class="card-body">
                        
                            </div>
                        </div>

                    </fieldset>
                    <fieldset title="Step 12" class="step" id="default-step-11">
                        <legend></legend>
                        <div class="card bg-light">
                            <header class="card-header">
                                Section 12
                            </header>
                            <div class="card-body">
                        
                            </div>
                        </div>

                    </fieldset>
                    <fieldset title="Step 13" class="step" id="default-step-12">
                        <legend></legend>
                        <div class="card bg-light">
                            <header class="card-header">
                                Section 13
                            </header>
                            <div class="card-body">

                                <div class="row p-2">
                                
                                    <div class="col-md-12">
                                        <div class="card bg-1">
                                            <div class="card-body">
                                                <table class="table table-hover table-borderless table-small-text summary-tb">
                                                    <tr>
                                                        <td class="field" width="55%">Name of Specialization
                                                        </td>
                                                        <td class="text-field">Specialization of abc </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="field">Specializtion Code </td>
                                                        <td class="text-field"><button class="btn btn-xs btn-primary mt-2"><i class="fa fa-info-circle mr-2" aria-hidden="true"></i>To be keyed in by
                                                            RAG</button></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="field">Specialization Synopsis </td>
                                                        <td class="text-field">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia
                                                        aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="field">Minimum number of credits to be completed for specialization </td>
                                                        <td class="text-field">30</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="field">Exclusion Criteria</td>
                                                        <td class="text-field">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia
                                                        aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3. </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="field">Graduation Criteria</td>
                                                        <td class="text-field">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad
                                                            squid. 3 wolf moon officia
                                                            aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3. </td>
                                                    </tr>
                                
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                
                                </div>

                                
                            </div>
                        </div>

                    </fieldset>

                    <fieldset title="Step 14" class="step" id="default-step-13">
                        <legend></legend>
                        <div class="card bg-light">
                            <header class="card-header">
                                Section 14
                            </header>
                            <div class="card-body">
                                <div id="table5">
                                   
                                  <table class="table table-borderless table-hover summary-tb bg-2">
                                        <thead>
                                            <tr>
                                                <th width="10%">ID</th>
                                                <th width="40%">Description</th>
                                             
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>IA1
                                                </td>
                                                <td class="text-left">
                                                   Lorem ipsum, dolor sit amet consectetur adipisicing elit. 
                                                   Vitae neque reiciendis voluptatibus illum! Impedit vitae ipsa a beatae, 
                                                   reprehenderittempora voluptates hic quos explicabo doloremque fugit eaque 
                                                   laudantium quibusdam exercitationem!">

                                                </td>
                                              
                                            </tr>
                                            <tr>
                                                <td>IA2
                                                </td>
                                                <td class="text-left">
                                                   Lorem ipsum, dolor sit amet consectetur adipisicing elit.
                                                        Vitae neque reiciendis voluptatibus illum! Impedit vitae ipsa a beatae,
                                                        reprehenderittempora voluptates hic quos explicabo doloremque fugit eaque
                                                        laudantium quibusdam exercitationem!">
                                                </td>
                                               
                                            </tr>
                                            <tr class="hide">
                                                <td>IA3
                                                </td>
                                                <td class="text-left">
                                                   Lorem ipsum, dolor sit amet consectetur adipisicing elit.
                                                        Vitae neque reiciendis voluptatibus illum! Impedit vitae ipsa a beatae,
                                                        reprehenderittempora voluptates hic quos explicabo doloremque fugit eaque
                                                        laudantium quibusdam exercitationem!">
                                                </td>
                                               
                                            </tr>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>


                    </fieldset>

                    <fieldset title="Step 15" class="step" id="default-step-14">
                        <legend></legend>
                        <div class="card">
                            <header class="card-header">
                                Section 15
                            </header>
                            <div class="card-body">
                              <a class="btn btn-space btn-xs btn-primary" href="{{ URL::route('mapping3') }}"> Click here to proceed to PLO-Module
                                Mapping <i class="fa fa-chevron-right"></i></a>
                            </div>
                        </div>
                    </fieldset>

                    <fieldset title="Step 16" class="step" id="default-step-15">
                        <legend></legend>
                        <div class="card bg-light">
                            <header class="card-header">
                                Section 16
                            </header>
                            <div class="card-body">
                                <div class="row p-2">
                                
                                    <div class="col-md-12">
                                        <div class="card bg-1">
                                            <div class="card-body">
                                                <table class="table table-hover table-borderless table-small-text summary-tb">
                                                    <tr>
                                                        <td class="field" width="">Effective Date
                                                        </td>
                                                        <td class="text-field">10 Oct 2019</td>
                                                        <td class="field">Revision Number</td>
                                                        <td class="text-field">1234567</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="field">Approval Date</td>
                                                        <td class="text-field">22 Sept 2019</td>
                                                        <td class="field">Approval Byy</td>
                                                        <td class="text-field">Sr Ahmah Hamidi b Roslan</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="field">Approval Date</td>
                                                        <td class="text-field">22 Sept 2019</td>
                                                        <td class="field">Approval Byy</td>
                                                        <td class="text-field">Prof adnan robert</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="field">Approval Date</td>
                                                        <td class="text-field">22 Sept 2019</td>
                                                        <td class="field">Approval Byy</td>
                                                        <td class="text-field">Mdm Jenny</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="field"> Effective Intake Month </td>
                                                        <td class="text-field">September </td>
                                                        <td class="field"> Effective Intake Year </td>
                                                        <td class="text-field">2020 </td>
                                                    </tr>
                                                    
                                                </table>
                                            </div>
                                        </div>
                                    </div>                                  
                                
                                </div>
                               
                            </div>
                        </div>



                    </fieldset>

                </form>


                <div class="card-footer text-center">
                    <a class="btn btn-danger btn-sm btn-space" href="{{ URL::route('review') }}"> <i
                            class="fa fa-arrow-left"></i>back</a>
                </div>

            </div>
        </section>
    </div>
</div>


@endsection