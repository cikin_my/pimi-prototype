@extends('layouts.main.master')
@section('content')
{{-- filter  --}}
<section class="card">
    <header class="card-header text-center">
        <i class="fa fa-filter mr-3"></i> Filter Options
        <span class="tools pull-right">
            <a href="javascript:;" class="fa fa-chevron-down"></a>
        </span>
    </header>
    <div class="card-body">
        <form>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                    <label>By Last Updated</label>
                    <div class="input-group" data-date="12/07/2017" data-date-format="mm/dd/yyyy">
                        <input type="text" class="form-control rounded dpd1" name="from">
                        <span class="px-3 py-2">To</span>
                        <input type="text" class="form-control rounded dpd2" name="to">
                    </div>
                </div>
            </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label> By Status</label>
                        <select class="form-control select2">
                            <option value="0">Select status</option>
                            <option value="1">Draft</option>
                            <option value="3">Review </option>
                            <option value="2">Corrections</option>
                            <option value="4">Approved</option>
                            <option value="5">Published </option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>By Module Name</label>
                        <select class="form-control select2">
                            <option value="0">Select Module name</option>
                            <option value="1">Chimerra</option>
                            <option value="3">Hannah</option>
                            <option value="2">Wyoming</option>
                            <option value="4">Haroon</option>
                            <option value="5">Noelia Mowers</option>
                            <option value="">Delinda Gurr</option>
                            <option value="">Harry Kitzmiller</option>
                            <option value="">Yen Uribe</option>
                            <option value="">Borrero</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4 mt-4 "></div>
                <div class="col-md-4 mt-4 ">
                    <button class="btn btn-space btn-primary btn-sm" type="submit"><i class="fa fa-search"></i>
                        Search</button>
                    <button class="btn btn-space btn-secondary btn-sm" value="reset">
                        <i class="fa fa-refresh"></i> Reset</button>
                </div>
                <div class="col-md-4 mt-4 "></div>
            </div>




        </form>


    </div>
</section>

{{-- table  --}}
<section class="card">

    <header class="card-header">
        <i class="fa fa-thumb-tack mr-3"> </i>Module Info Management
    </header>
    <div class="card-body">
        <table class="table table-condensed table-bordered text-center table-hover">
            <thead>
                <tr>
                    <th width="" class="text-center">ID</th>
                    <th width="" class="text-center">Last Updated (Date)</th>
                    <th width="12%" class="text-center">Module Code</th>
                    <th width="" class="text-center">Module Name</th>
                    <th width="" class="text-center">Status</th>
                    <th width="20%" class="text-center">Actions</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>1</td>
                    <td>20 Mei 2019</td>
                    <td class="text-center">220</td>
                    <td>Module 1</td>
                    <td><span class="badge badge-light">Draft </span></td>
                    <td class="text-center">
                        <a href="" class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o"
                                aria-hidden="true"></i></a>
                        <a href="" class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></a>                     
                        <a href="" class="btn btn-warning btn-sm"><i class="fa fa-share" aria-hidden="true"></i></a>
                      
                    </td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>2 Mei 2019</td>
                    <td class="text-center">220</td>
                    <td>Module 1</td>
                    <td><span class="badge badge-info">Review </span></td>
                    <td class="text-center">
                        <a href="" class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o"
                                aria-hidden="true"></i></a>
                        <a href="" class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></a>                     
                        <a href="" class="btn btn-warning btn-sm"><i class="fa fa-share" aria-hidden="true"></i></a>
                      
                    </td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>2 Mei 2019</td>
                    <td class="text-center">220</td>
                    <td>Module 1</td>
                    <td><span class="badge badge-success">Approved </span> </td>
                    <td class="text-center">
                        <a href="" class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o"
                                aria-hidden="true"></i></a>
                        <a href="" class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></a>                     
                        <a href="" class="btn btn-warning btn-sm"><i class="fa fa-share" aria-hidden="true"></i></a>
                      
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</section>
@endsection