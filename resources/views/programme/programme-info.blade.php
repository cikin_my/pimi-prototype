@extends('layouts.main.master')
@section('content')
{{-- filter  --}}
<section class="card">
    <header class="card-header">
        <i class="fa fa-filter mr-3"></i> Filter Options
        <span class="tools pull-right">
            <a href="javascript:;" class="fa fa-chevron-down"></a>
        </span>
    </header>
    <div class="card-body">
        <form>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>By Last Updated</label>
                        <div class="input-group" data-date="12/07/2017" data-date-format="mm/dd/yyyy">
                            <input type="text" class="form-control rounded dpd1" name="from">
                            <span class="px-3 py-2">To</span>
                            <input type="text" class="form-control rounded dpd2" name="to">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label> By Status</label>
                        <select class="form-control select2">
                            <option value="0">Select status</option>
                            <option value="1">Draft</option>
                            <option value="3">Review </option>
                            <option value="2">Corrections</option>
                            <option value="4">Approved</option>
                            <option value="5">Published </option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>By Programme Name</label>
                        <select class="form-control select2">
                            <option value="0">Select programme name</option>
                            <option value="1">Chimerra</option>
                            <option value="3">Hannah</option>
                            <option value="2">Wyoming</option>
                            <option value="4">Haroon</option>
                            <option value="5">Noelia Mowers</option>
                            <option value="">Delinda Gurr</option>
                            <option value="">Harry Kitzmiller</option>
                            <option value="">Yen Uribe</option>
                            <option value="">Borrero</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4 mt-4 "></div>
                <div class="col-md-4 mt-4 ">
                    <button class="btn btn-space btn-primary btn-sm" type="submit"><i class="fa fa-search"></i>
                        Search</button>
                    <button class="btn btn-space btn-secondary btn-sm" value="reset">
                        <i class="fa fa-refresh"></i> Reset</button>
                </div>
                <div class="col-md-4 mt-4 "></div>
            </div>




        </form>


    </div>
</section>

{{-- table  --}}
<section class="card">

    <header class="card-header">
        <i class="fa fa-thumb-tack mr-3"> </i>Programme Info Management
        <div class="pull-right">
            <a href="{{ URL::route('programme-new') }}" class="btn btn-primary btn-sm mr-2"><i
                    class="fa fa-plus mr-2 ml-2"></i> Add New Programme Information</a>
        </div>
    </header>
    <div class="card-body">
        <table class="table table-condensed table-bordered text-center table-hover">
            <thead>
                <tr>
                    <th width="" class="text-center">ID</th>
                    <th width="" class="text-center">Last Updated (Date)</th>
                    <th width="12%" class="text-center">Programme Code</th>
                    <th width="" class="text-center">Programme Name</th>
                    <th width="" class="text-center">Status</th>
                    <th width="20%" class="text-center">Actions</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>1</td>
                    <td>20 Mei 2019</td>
                    <td class="text-center">220</td>
                    <td>programme 1</td>
                    <td><span class="badge badge-light">Draft </span></td>
                    <td class="text-center">
                        <!-- edit -->
                        <a class="btn btn-primary btn-xs" href="{{ URL::route('programme-new') }}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                        <!-- view -->
                        <a class="btn btn-info btn-xs" href="{{ URL::route('programme-new') }}"><i class="fa fa-eye" aria-hidden="true"></i></a>
                        <!-- structure -->
                        <a class="btn btn-success btn-xs" href="{{ URL::route('structure') }}"><i class="fa fa-sitemap" aria-hidden="true"></i></a>
                        <!-- submit -->
                        <a class="btn btn-warning btn-xs" data-toggle="modal" href="#modal-submit"><i class="fa fa-paper-plane" aria-hidden="true"></i></a>
                        <!-- download -->
                        <a class="btn btn-danger btn-xs" data-toggle="modal" href="#modal-download"><i class="fa fa-cloud-download" aria-hidden="true"></i></a>
                    </td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>2 Mei 2019</td>
                    <td class="text-center">220</td>
                    <td>programme 1</td>
                    <td><span class="badge badge-info">Review </span></td>
                    <td class="text-center">
                        <!-- edit -->
                        <a class="btn btn-primary btn-xs" href="{{ URL::route('programme-new') }}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                        <!-- view -->
                        <a class="btn btn-info btn-xs" href="{{ URL::route('programme-new') }}"><i class="fa fa-eye" aria-hidden="true"></i></a>
                        <!-- structure -->
                        <a class="btn btn-success btn-xs" href="{{ URL::route('structure') }}"><i class="fa fa-sitemap" aria-hidden="true"></i></a>
                        <!-- submit -->
                        <a class="btn btn-warning btn-xs" data-toggle="modal" href="#modal-submit"><i class="fa fa-paper-plane" aria-hidden="true"></i></a>
                        <!-- download -->
                        <a class="btn btn-danger btn-xs" data-toggle="modal" href="#modal-download"><i class="fa fa-cloud-download" aria-hidden="true"></i></a>
                    </td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>2 Mei 2019</td>
                    <td class="text-center">220</td>
                    <td>programme 1</td>
                    <td><span class="badge badge-success">Approved </span> </td>
                    <td class="text-center">
                        <!-- edit -->
                        <a class="btn btn-primary btn-xs" href="{{ URL::route('programme-new') }}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                        <!-- view -->
                        <a class="btn btn-info btn-xs" href="{{ URL::route('programme-new') }}"><i class="fa fa-eye" aria-hidden="true"></i></a>
                        <!-- structure -->
                        <a class="btn btn-success btn-xs" href="{{ URL::route('structure') }}"><i class="fa fa-sitemap" aria-hidden="true"></i></a>
                        <!-- submit -->
                        <a class="btn btn-warning btn-xs" data-toggle="modal" href="#modal-submit"><i class="fa fa-paper-plane" aria-hidden="true"></i></a>
                        <!-- download -->
                        <a class="btn btn-danger btn-xs" data-toggle="modal" href="#modal-download"><i class="fa fa-cloud-download" aria-hidden="true"></i></a>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</section>

{{-- modal --}}
<div class="modal fade " id="modal-submit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Submit Programme <i class="fa fa-paper-plane"></i></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body form hovering ld-over-inverse" id="test">

                <div id="message">
                    <div class="text-center">
                        <div class="i-circle text-warning"><i class="icon fa fa-exclamation-triangle"
                                aria-hidden="true"></i></div>
                        <h4>Are you sure?</h4>
                        <p> are you sure you want to submit this Programme Info for Review?</p>
                    </div>
                    <div class="form-group text-center">
                        <div class="btn btn-primary btn-sm btn-rounded btn-space hovering ld-over-inverse"
                            id="importData">
                            Yes
                            {{-- <div class="ld ld-ball ld-flip"></div> --}}
                        </div>
                    </div>
                </div>
                <div id="import-msg">
                    <div class="text-center">
                        <div class="i-circle text-success">
                            <i class="icon fa fa-check"></i>
                        </div>
                        <h4>Sent!</h4>
                        <p> Programme submitted for Review</p>
                    </div>
                </div>
                <div class="ld ld-ball ld-flip"></div>



            </div>
        </div>
    </div>
</div>
<div class="modal fade form-container" id="modal-download" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Download <i class="fa fa-cloud-download"></i></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group col-md-12 text-center">
                        <button type="button" class="btn btn-light btn-sm" data-toggle="modal" data-dismiss="modal"
                            data-target="#core-new"> <i class="fa fa-download mr-2"></i> MQA01 Format</button>
                        &ensp;|&ensp;
                        <button type="button" class="btn btn-light btn-sm" data-toggle="modal" data-dismiss="modal"
                            data-target="#core-existing"> <i class="fa fa-download mr-2"></i> Short Version</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>



@endsection