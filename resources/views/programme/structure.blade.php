@extends('layouts.main.master')
@section('content')

<section class="card">
    <header class="card-header">
        Structure
    </header>
    <div class="card-body">
        <ul class="nav nav-tabs mb-2"  role="tablist">
            <li class="nav-item" id="structure-add">
                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#add" role="tab" aria-controls="add"
                    aria-selected="true"> <i class="fa fa-plus pr-2"></i>Add Modules</a>
            </li>
            <li class="nav-item">
                <a class="nav-link " id="profile-tab" data-toggle="tab" href="#list" role="tab" aria-controls="profile"
                    aria-selected="false"><i class="fa fa-list pr-2"></i>Module Listing</a>
            </li>

        </ul>
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="add" role="tabpanel" aria-labelledby="home-tab">

                <section class="card tasks-widget">
                    <div class="card-body col-sm-12">
                        <ul class="task-list">
                            <li>Select a category for which you wish to add/select modules:</li>
                    </div>
                    <div class="card-body">
                        <ul class="task-list">
                            <li>

                                <button type="button" class="btn btn-info" data-toggle="modal" data-target="#core">
                                    <i class="fa fa-plus mr-3"></i> <span style="letter-spacing:1px">Core </span>
                                </button>
                            </li>
                            <li>
                                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#mpuucm">
                                    <i class="fa fa-plus mr-3"></i><span style="letter-spacing:1px">MPU/UCM</span>
                                </button>
                            </li>
                            <li>
                                <button type="button" class="btn btn-warning" data-toggle="modal"
                                    data-target="#specialization">
                                    <i class="fa fa-plus mr-3"></i><span
                                        style="letter-spacing:1px">Specialization</span>
                                </button>
                            </li>
                            <li>
                                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#Complementary">
                                    <i class="fa fa-plus mr-3"></i><span style="letter-spacing:1px">Complementary Studies</span>
                                </button>
                            </li>
                        </ul>
                    </div>
                </section>
            </div>
            <div class="tab-pane fade" id="list" role="tabpanel" aria-labelledby="profile-tab">
                <div class="row">
                    <div class="col-sm-12">
                        <section class="card mb-0">
                            <header class="card-header text-center"><i class="fa fa-filter"></i> Filter Option </header>

                            {{-- filter option --}}
                            <div class="card bg-light">
                                <div class="card-body mb-0">
                                    <div class="row">

                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="col-md-12 col-form-label "> By Module
                                                    Category</label>
                                                <div class="col-sm-12 col-md-12">
                                                    <select class="form-control select2">
                                                        <option value="">Please select</option>
                                                        <option value="1">Core</option>
                                                        <option value="2">MPU/UCM</option>
                                                        <option value="3">Specialization</option>
                                                    </select>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-md-6 text-left">
                                            <div class="form-group row">
                                                <label class="col-md-12 col-form-label"> By Status</label>
                                                <div class="col-12 col-md-12">
                                                    <select class="form-control select2">
                                                        <option value="">Please select</option>
                                                        <option value="1">Draft</option>
                                                        <option value="2">Review</option>
                                                        <option value="3">Corrections</option>
                                                        <option value="4">Approved</option>
                                                        <option value="5">Published</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4 mt-4 "></div>
                                        <div class="col-md-4 mt-4 ">
                                            <button class="btn btn-space btn-primary btn-sm" type="submit"><i
                                                    class="fa fa-search"></i> Search</button>
                                            <button class="btn btn-space btn-secondary btn-sm" value="reset">
                                                <i class="fa fa-refresh"></i> Reset</button>
                                        </div>
                                        <div class="col-md-4 mt-4 "></div>

                                    </div>

                                </div>
                            </div>
                        </section>

                    </div>
                </div>


                <div class="row">
                    <div class="col-sm-12">
                        <section class="card bg-light">
                            <div class="card-body">
                                {{-- <div class="tools pull-right">
                                    <button class="btn btn-sm btn-primary" href="{{ route('add') }}">
                                <i class="fa fa-plus mr-1 ml-1" aria-hidden="true"></i> Add Modules</button>
                            </div> --}}
                            <div class="adv-table">
                                <table class="display table table-bordered table-striped table-hover" id="dynamic-table">
                                    <thead>
                                        <tr>
                                            <th class="hidden-phone" width="5px">No.</th>
                                            <th width="10%">Module Code</th>
                                            <th width="15%">Module Name</th>
                                            <th width="15%" class="hidden-phone">Last Update</th>
                                            <th width="15%">Module Leader</th>
                                            <th width="15%">Module Category</th>
                                            <th width="5%">Credits</th>
                                            <th width="5%">Status</th>
                                            <th class="action-col" width="15%">Action</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <tr class="">
                                            <td>1</td>
                                            <td>MD1 </td>
                                            <td>Module 22</td>
                                            <td class=" hidden-phone">1 September 2019</td>
                                            <td>Module leader 1</td>
                                            <td>Core</td>
                                            <td>3.60</td>
                                            <td>Approved</td>
                                            <td class="action-col">
                                                <button class="btn btn-success btn-sm"><i
                                                        class="fa fa-eye"></i></button>
                                                <button class="btn btn-primary btn-sm" data-toggle="modal"
                                                    data-target="#edit"><i class="fa fa-pencil"></i></button>
                                            </td>
                                        </tr>
                                        <tr class="">
                                            <td>2</td>
                                            <td>MD2 </td>
                                            <td>Module 2</td>
                                            <td class=" hidden-phone">13 August 2019</td>
                                            <td>Module leader 2</td>
                                            <td>MPU/UCM</td>
                                            <td>2.56</td>
                                            <td>Corrections</td>
                                            <td class="action-col">
                                                <button class="btn btn-success btn-sm"><i
                                                        class="fa fa-eye"></i></button>
                                                <button class="btn btn-primary btn-sm" data-toggle="modal"
                                                    data-target="#edit"><i class="fa fa-pencil"></i></button>
                                            </td>
                                        </tr>
                                        <tr class="">
                                            <td>3</td>
                                            <td>MD3 </td>
                                            <td>Module 3</td>
                                            <td class=" hidden-phone">21 July 2019</td>
                                            <td>Module leader 3</td>
                                            <td>Specialization</td>
                                            <td>3.33</td>
                                            <td>Published</td>
                                            <td class="action-col">
                                                <button class="btn btn-success btn-sm"><i
                                                        class="fa fa-eye"></i></button>
                                                <button class="btn btn-primary btn-sm" data-toggle="modal"
                                                    data-target="#edit"><i class="fa fa-pencil"></i></button>
                                            </td>
                                        </tr>

                                    </tbody>

                                </table>
                            </div>
                    </div>
</section>
</div>
</div>
</div>

</div>
</div>
<div class="card-footer text-muted text-center">
    <a href="{{ URL::route('programme-info') }}" class="btn btn-space btn-primary btn-sm"><i
            class="fa fa-long-arrow-left mr-2 ml-2" aria-hidden="true"></i> Back</a>


</div>



</section>

{{-- ====================
add module
==================== --}}

<!-- 1. core Modal -->
<div class="modal fade form-container" id="core" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Core Module</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group col-md-12 text-center">
                        <button type="button" class="btn btn-light btn-sm" data-toggle="modal" data-dismiss="modal"
                            data-target="#core-new">Create New Module</button>
                        &ensp;|&ensp;
                        <button type="button" class="btn btn-light btn-sm" data-toggle="modal" data-dismiss="modal"
                            data-target="#core-existing">Choose an Existing Module</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>

<!-- submit Modal !! please note that this is a public submit modal, used for all SUBMIT button in prototype !!-->
<div class="modal fade form-container" id="submit" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">


    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="modal-body text-center mt-4">
                <div class="text-center">
                    <div class="i-circle text-success">
                        <i class="icon fa fa-check"></i>
                    </div>
                    <h4>Done!</h4>
                    <p>New module has been added!</p>
                </div>


                <div class="divider"></div>
                <div class="text-center">
                    <a type="button" class="btn btn-success btn-sm mt-3" href="{{ URL::route('structure', array('', '#list')) }}" id="complete-module" >Complete module
                        information</a>
                </div>
            </div>

        </div>
    </div>
</div>

<!-- 1.1 core new module Modal -->
<div class="modal fade form-container" id="core-new" role="dialog" aria-labelledby="exampleModalLabel1"
    aria-hidden="true">

    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">New Core Module</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <input type="name" class="form-control" id="core-new" placeholder="Module name">
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="core-new">Assign module leader</label>
                        <select class="form-control select2">
                            <option value="0" disable>Search module leader</option>
                            <option value="1">Module leader 1</option>
                            <option value="2">Module leader 2</option>
                            <option value="3">Module leader 3</option>
                            <option value="4">Module leader 4</option>
                            <option value="5">Module leader 5</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="core-new">Year level (programme)</label>
                        <select class="form-control select2">
                            <option value="0" disable>Select year level (programme)</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>

                        </select>
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="inlineFormInput">School owning the programme</label>
                        <select class="form-control select2">
                            <option value="0" disable>Select school owning the programme</option>
                            <option value="1">School of Computing & IT</option>
                            <option value="2">School of Biosciences</option>
                            <option value="3">Taylor's Business School</option>
                            <option value="4">School of Education</option>
                            <option value="5">School of Engineering</option>
                            <option value="6">School of Medicine</option>
                            <option value="7">Taylor's Law School</option>

                        </select>
                    </div>

                    <div class="form-group pull-right">
                        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-dismiss="modal"
                            data-target="#submit">Submit</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>

<!-- 1.2 core existing module Modal -->
<div class="modal fade form-container" id="core-existing" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">

    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Existing Core Module</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label class="sr-only" for="core-existing">Module</label>
                        <select class="form-control select2">
                            <option value="0" disable>Search module</option>
                            <option value="1">Module 1</option>
                            <option value="2">Module 2</option>
                            <option value="3">Module 3</option>
                            <option value="4">Module 4</option>
                            <option value="5">Module 5</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="core-existing">Year level (programme)</label>
                        <select class="form-control select2">
                            <option value="0" disable>Select year level (programme)</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>

                        </select>
                    </div>

                    <div class="form-group pull-right">
                        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-dismiss="modal"
                            data-target="#submit">Submit</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>

<!-- 2 mpu/ucm module Modal -->
<div class="modal fade form-container" id="mpuucm" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">MPU/UCM Module</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label class="sr-only" for="core-existing">MPU/UCM module</label>
                        <select class="form-control select2">
                            <option value="0" disable>Select MPU/UCM module</option>
                            <option value="6">Module 6</option>
                            <option value="7">Module 7</option>
                            <option value="8">Module 8</option>
                            <option value="9">Module 9</option>
                            <option value="10">Module 10</option>
                        </select>
                    </div>

                    <div class="form-group pull-right">
                        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-dismiss="modal"
                            data-target="#submit">Submit</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>

<!-- 3. specialization Modal -->
<div class="modal fade form-container" id="specialization" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">

    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Specialization Module</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card">
                    <header class="card-header">
                        Please select your specialization:
                    </header>
                    <div class="card-body">
                        <form class="text-center">
                            <div class="custom-control custom-checkbox mb-6">
                                <input type="checkbox" class="custom-control-input" id="checkbox1">
                                <label class="custom-control-label" for="checkbox1">Specialization 1</label>
                            </div>
                            <div class="custom-control custom-checkbox mb-6">
                                <input type="checkbox" class="custom-control-input" id="checkbox2">
                                <label class="custom-control-label" for="checkbox2">Specialization 2</label>
                            </div>
                            <div class="custom-control custom-checkbox mb-6">
                                <input type="checkbox" class="custom-control-input" id="checkbox3">
                                <label class="custom-control-label" for="checkbox3">Specialization 3</label>
                            </div>
                            <div class="custom-control custom-checkbox mb-6">
                                <input type="checkbox" class="custom-control-input" id="checkbox4">
                                <label class="custom-control-label" for="checkbox4">Specialization 4</label>
                            </div>
                            <div class="custom-control custom-checkbox mb-6">
                                <input type="checkbox" class="custom-control-input" id="checkbox5">
                                <label class="custom-control-label" for="checkbox5">Specialization 5</label>
                            </div><br />
                            <div class="form-group">
                                <button type="button" id="spec-category" class="btn btn-success btn-sm"> <i
                                        class="fa fa-check"></i> Add Specialization
                                    Category</button>
                            </div>
                            <div class="form-group row" id="spec-text" style="display:none;">
                                <div class="col-md-12 col-sm-12">
                                    <div class="input-group">
                                        <input type="text" class="form-control mr-1">
                                        <span class="btn btn-success" disabled><i class="fa fa-check f14"></i></span>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group pull-right">
                        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-dismiss="modal"
                            data-target="#specialization-next">Next</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>

<!-- 3.1 specialization next Modal -->
<div class="modal fade form-container" id="specialization-next" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">

    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Specialization Module</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group col-md-12">
                        <button type="button" class="btn btn-light btn-sm" data-toggle="modal" data-dismiss="modal"
                            data-target="#specialization-new">Create New Module</button>
                        &ensp;|&ensp;
                        <button type="button" class="btn btn-light btn-sm" data-toggle="modal" data-dismiss="modal"
                            data-target="#specialization-existing">Choose an Existing Module</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>

<!-- 3.2 specialization new module Modal -->
<div class="modal fade form-container" id="specialization-new" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">

    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">New Core Module</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <input type="name" class="form-control" id="specialization-new" placeholder="Module name">
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="specialization-new">Assign module leader</label>
                        <select class="form-control select2">
                            <option value="0" disable>Search module leader</option>
                            <option value="1">Module 1</option>
                            <option value="2">Module 2</option>
                            <option value="3">Module 3</option>
                            <option value="4">Module 4</option>
                            <option value="5">Module 5</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="specialization-new">Year level (programme)</label>
                        <select class="form-control select2">
                            <option value="0" disable>Select year level (programme)</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>

                        </select>
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="core-module-leader">School owning the programme</label>
                        <select class="form-control select2">
                            <option value="0" disable>Select school owning the programme</option>
                            <option value="1">School of Computing & IT</option>
                            <option value="2">School of Biosciences</option>
                            <option value="3">Taylor's Business School</option>
                            <option value="4">School of Education</option>
                            <option value="5">School of Engineering</option>
                            <option value="6">School of Medicine</option>
                            <option value="7">Taylor's Law School</option>

                        </select>
                    </div>

                    <div class="form-group pull-right">
                        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-dismiss="modal"
                            data-target="#submit">Submit</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>

<!-- 3.3 specialization existing module Modal -->
<div class="modal fade form-container" id="specialization-existing" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">

    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Existing Core Module</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label class="sr-only" for="core-module-leader">Module</label>
                        <select class="form-control select2">
                            <option value="0" disable>Search module</option>
                            <option value="1">Module 1</option>
                            <option value="2">Module 2</option>
                            <option value="3">Module 3</option>
                            <option value="4">Module 4</option>
                            <option value="5">Module 5</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="inlineFormInput">Year level (programme)</label>
                        <select class="form-control select2">
                            <option value="0" disable>Select year level (programme)</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>

                        </select>
                    </div>

                    <div class="form-group pull-right">
                        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-dismiss="modal"
                            data-target="#submit">Submit</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>


<!-- 4. Complementary Studies Modal -->
<div class="modal fade form-container" id="Complementary" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">

    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Complementary Studies</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card">
                    <header class="card-header">
                        Please select the Category:
                    </header>
                    <div class="card-body">
                        <form class="text-center">
                            <div class="custom-control custom-checkbox mb-6">
                                <input type="checkbox" class="custom-control-input" id="checkbox1">
                                <label class="custom-control-label" for="checkbox1">Category 1</label>
                            </div>
                            <div class="custom-control custom-checkbox mb-6">
                                <input type="checkbox" class="custom-control-input" id="checkbox2">
                                <label class="custom-control-label" for="checkbox2">Category 2</label>
                            </div>
                            <div class="custom-control custom-checkbox mb-6">
                                <input type="checkbox" class="custom-control-input" id="checkbox3">
                                <label class="custom-control-label" for="checkbox3">Category 3</label>
                            </div>
                            <div class="custom-control custom-checkbox mb-6">
                                <input type="checkbox" class="custom-control-input" id="checkbox4">
                                <label class="custom-control-label" for="checkbox4">Category 4</label>
                            </div>
                            <div class="custom-control custom-checkbox mb-6">
                                <input type="checkbox" class="custom-control-input" id="checkbox5">
                                <label class="custom-control-label" for="checkbox5">Category 5</label>
                            </div><br />
                            <div class="form-group">
                                <button type="button" id="spec-category" class="btn btn-success btn-sm"> <i
                                        class="fa fa-check"></i> Add Category
                                    </button>
                            </div>
                            <div class="form-group row" id="spec-text" style="display:none;">
                                <div class="col-md-12 col-sm-12">
                                    <div class="input-group">
                                        <input type="text" class="form-control mr-1">
                                        <span class="btn btn-success" disabled><i class="fa fa-check f14"></i></span>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group pull-right">
                        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-dismiss="modal"
                            data-target="#specialization-next">Next</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>


{{-- =====================
module listing modal
====================== --}}
<!-- 1. edit modal -->
<div class="modal fade" id="edit" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Module</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label>Module name</label>
                        <input type="name" class="form-control" placeholder="Module name" value="Module 22">
                    </div>
                    <div class="form-group">
                        <label>Year level (module)</label>
                        <select class="form-control select2">
                            <option value="0" disable>Please select</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>

                        </select>
                    </div>
                    <div class="form-group">
                        <label>Year level (programme)</label>
                        <select class="form-control select2">
                            <option value="0" disable>Please select</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>

                        </select>
                    </div>

                    <!--tab nav start-->
                    <ul class="nav nav-tabs mb-2" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="pre-tab" data-toggle="tab" href="#pre" role="tab"
                                aria-controls="pre" aria-selected="true">Pre-requisite</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="co-tab" data-toggle="tab" href="#co" role="tab" aria-controls="co"
                                aria-selected="false">Co-requisite</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="anti-tab" data-toggle="tab" href="#anti" role="tab"
                                aria-controls="anti" aria-selected="false">Anti-requisite</a>
                        </li>
                    </ul>
                    <br />
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="pre" role="tabpanel" aria-labelledby="pre-tab">
                            <div class="form-group">
                                <label>General Pre-requisite</label>
                                <input type="name" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Module Pre-requisite</label>
                                <div class="custom-control custom-checkbox mb-6 col-md-6">
                                    <input type="checkbox" class="custom-control-input" id="checkbox1">
                                    <label class="custom-control-label" for="checkbox1">Module 2</label>
                                </div>
                                <div class="custom-control custom-checkbox mb-6 col-md-6">
                                    <input type="checkbox" class="custom-control-input" id="checkbox2">
                                    <label class="custom-control-label" for="checkbox2">Module 3</label>
                                </div>
                                <div class="custom-control custom-checkbox mb-6 col-md-6">
                                    <input type="checkbox" class="custom-control-input" id="checkbox3">
                                    <label class="custom-control-label" for="checkbox3">Module 4</label>
                                </div>
                                <div class="custom-control custom-checkbox mb-6 col-md-6">
                                    <input type="checkbox" class="custom-control-input" id="checkbox4">
                                    <label class="custom-control-label" for="checkbox4">Module 5</label>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="co" role="tabpanel" aria-labelledby="co-tab">
                            <div class="form-group">
                                <label>General Co-requisite</label>
                                <input type="name" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Module Co-requisite</label>
                                <div class="custom-control custom-checkbox mb-6 col-md-6">
                                    <input type="checkbox" class="custom-control-input" id="checkbox1">
                                    <label class="custom-control-label" for="checkbox1">Module 2</label>
                                </div>
                                <div class="custom-control custom-checkbox mb-6 col-md-6">
                                    <input type="checkbox" class="custom-control-input" id="checkbox2">
                                    <label class="custom-control-label" for="checkbox2">Module 3</label>
                                </div>
                                <div class="custom-control custom-checkbox mb-6 col-md-6">
                                    <input type="checkbox" class="custom-control-input" id="checkbox3">
                                    <label class="custom-control-label" for="checkbox3">Module 4</label>
                                </div>
                                <div class="custom-control custom-checkbox mb-6 col-md-6">
                                    <input type="checkbox" class="custom-control-input" id="checkbox4">
                                    <label class="custom-control-label" for="checkbox4">Module 5</label>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="anti" role="tabpanel" aria-labelledby="anti-tab">
                            <div class="form-group">
                                <label>General Anti-requisite</label>
                                <input type="name" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Module Pre-requisite</label>
                                <div class="custom-control custom-checkbox mb-6 col-md-6">
                                    <input type="checkbox" class="custom-control-input" id="checkbox1">
                                    <label class="custom-control-label" for="checkbox1">Module 2</label>
                                </div>
                                <div class="custom-control custom-checkbox mb-6 col-md-6">
                                    <input type="checkbox" class="custom-control-input" id="checkbox2">
                                    <label class="custom-control-label" for="checkbox2">Module 3</label>
                                </div>
                                <div class="custom-control custom-checkbox mb-6 col-md-6">
                                    <input type="checkbox" class="custom-control-input" id="checkbox3">
                                    <label class="custom-control-label" for="checkbox3">Module 4</label>
                                </div>
                                <div class="custom-control custom-checkbox mb-6 col-md-6">
                                    <input type="checkbox" class="custom-control-input" id="checkbox4">
                                    <label class="custom-control-label" for="checkbox4">Module 5</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <!--tab nav start-->

                    <div class="form-group">
                        <label>Module owner</label>
                        <select class="form-control select2">
                            <option value="0" disable>Please select</option>
                            <option value="1">School of Computing & IT</option>
                            <option value="2">School of Biosciences</option>
                            <option value="3">Taylor's Business School</option>
                            <option value="4">School of Education</option>
                            <option value="5">School of Engineering</option>
                            <option value="6">School of Medicine</option>
                            <option value="7">Taylor's Law School</option>

                        </select>
                    </div>
                    <div class="form-group">
                        <label>Module offered as</label>
                        <div class="custom-control custom-checkbox mb-6 col-md-6">
                            <input type="checkbox" class="custom-control-input" id="checkbox1">
                            <label class="custom-control-label" for="checkbox1">Primary Major – Core</label>
                        </div>
                        <div class="custom-control custom-checkbox mb-6 col-md-6">
                            <input type="checkbox" class="custom-control-input" id="checkbox2">
                            <label class="custom-control-label" for="checkbox2">Primary Major – Specialization</label>
                        </div>
                        <div class="custom-control custom-checkbox mb-6 col-md-6">
                            <input type="checkbox" class="custom-control-input" id="checkbox3">
                            <label class="custom-control-label" for="checkbox3">Minor</label>
                        </div>
                        <div class="custom-control custom-checkbox mb-6 col-md-6">
                            <input type="checkbox" class="custom-control-input" id="checkbox4">
                            <label class="custom-control-label" for="checkbox4">Free Elective</label>
                        </div>
                        <div class="custom-control custom-checkbox mb-6 col-md-6">
                            <input type="checkbox" class="custom-control-input" id="checkbox4">
                            <label class="custom-control-label" for="checkbox4">Extension</label>
                        </div>
                        <div class="custom-control custom-checkbox mb-6 col-md-6">
                            <input type="checkbox" class="custom-control-input" id="checkbox4">
                            <label class="custom-control-label" for="checkbox4">MPU</label>
                        </div>
                        <div class="custom-control custom-checkbox mb-6 col-md-6">
                            <input type="checkbox" class="custom-control-input" id="checkbox4">
                            <label class="custom-control-label" for="checkbox4">Second Major</label>
                        </div>
                    </div>

                    <div class="form-group pull-right">
                        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-dismiss="modal"
                            data-target="#submit"> <i class="fa fa-save mr-2"></i>Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- submit Modal !!-->
<div class="modal fade form-container" id="submit" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body alert alert-success" role="alert">Module Information Saved Successfully
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group pull-right">
                        <button type="button" class="btn btn-success btn-sm" data-toggle="modal"
                            data-dismiss="modal">OK</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection