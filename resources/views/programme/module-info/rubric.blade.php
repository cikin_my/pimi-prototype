@extends('layouts.main.master')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <section class="card">
            <header class="card-header">
               Assessment Task 2: Group Assessment
            </header>
            <div class="card-body">
                <div class="stepy-tab">
                    <ul id="default-titles" class="stepy-titles clearfix">
                        <li id="default-title-0" class="current-step">
                            <div>Step 1</div>
                        </li>
                        <li id="default-title-1" class="">
                            <div>Step 2</div>
                        </li>

                    </ul>
                </div>
                <form class="form-horizontal" id="default">
                    <fieldset title="Step1" class="step" id="default-step-0">

                        <legend> </legend>
                        <div class="card">
                            <header class="card-header">
                                Section 1
                            </header>
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="my-textarea">Criteria</label>
                                    <textarea id="my-textarea" class="form-control" name="" rows="3"></textarea>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="">Weightage</label>
                                        <div class="input-group">
                                            <input class="form-control" type="text">
                                            <div class="input-group-append">
                                                <span class="input-group-text" id="my-addon">%</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="">MLO Assessed</label>
                                        <input type="text" name="" id="" class="form-control" readonly value="MLO2">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </fieldset>

                    <fieldset title="Step 2" class="step" id="default-step-1">
                        <legend> </legend>
                        <div class="card">
                            <header class="card-header">
                                Section 2
                            </header>
                            <div class="card-body">
                               <div class="form-group">
                                   <label >Out Standing (9-10)</label>
                                   <textarea class="form-control" name="" rows="3">Lorem ipsum dolor sit amet consectetur adipisicing elit. In provident est recusandae illo quam quidem, accusantium saepe dolorum eligendi minus doloremque quasi ipsam numquam! Sed molestiae numquam maxime cumque praesentium.</textarea>
                               </div>
                               <div class="form-group">
                                   <label for="">Mastering (7-8)</label>
                                   <textarea id="" class="form-control" name="" rows="3">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Perferendis ex quidem repudiandae dolorem rem cum totam possimus ratione magnam nobis voluptatum quo asperiores incidunt dignissimos ab, eos est quisquam necessitatibus.</textarea>
                               </div>
                               <div class="form-group">
                                   <label for="">Developing (5-6)</label>
                                   <textarea id="" class="form-control" name="" rows="3">Lorem ipsum dolor sit amet consectetur adipisicing elit. Tenetur aut dolores facere laudantium quidem molestiae provident suscipit? Doloribus velit nesciunt similique aliquid perspiciatis placeat voluptatum iste. Illo error blanditiis nisi!</textarea>
                               </div>
                               <div class="form-group">
                                   <label for="">Beginning (0-4) </label>
                                   <textarea id="" class="form-control" name="" rows="3">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Hic iure debitis quas fugiat aliquam dignissimos voluptatem quisquam, facilis nulla sit labore nemo nihil. Dolorum possimus fugit iusto. Nesciunt, iste eius!</textarea>
                               </div>
                               <div class="form-group">
                                   <a href="{{ route('module-info.edit') }}" class="btn btn-warning btn-sm" >Cancel</a>
                                   <button type="button" onclick="save()" class="btn btn-success btn-sm">Save</button>
                               </div>
                            </div>
                        </div>

                    </fieldset>




                    <div  class="finish">
                        <a href="{{ URL::route('programme-info') }}" class="btn btn-success" >
                            <i class="fa fa-check"></i>
                            Finish !
                        </a>
                    </div>


                </form>

            </div>
        </section>
    </div>
</div>


{{--  message modal  --}}
<div class="modal fade " id="saved-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myModal">Assessment Rubrics Saved</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p class="text-center text-primary">
                   <i class="fa fa-check-circle-o fa-5x" aria-hidden="true"></i>
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

@endsection
@push('extraScripts')
    <script>
        function save(){
            $('#saved-modal').modal('show')
        }
    </script>
@endpush
