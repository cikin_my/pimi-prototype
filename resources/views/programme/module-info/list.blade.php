@extends('layouts.main.master')

@section('content')
<div class="col-sm-12">
    <section class="card">
        <header class="card-header">
            <i class="fa fa-filter mr-3"></i> Filter Options
            <span class="tools pull-right">
                <a href="javascript:;" class="fa fa-chevron-down"></a>
            </span>
        </header>
        <div class="card-body">
            <div class="row">              
                   <div class="col-md-12">
                        <div class="form-group">
                            <label>By Last Updated</label>
                            <div class="input-group" data-date="12/07/2017" data-date-format="mm/dd/yyyy">
                                <input type="text" class="form-control rounded dpd1" name="from">
                                <span class="px-3 py-2">To</span>
                                <input type="text" class="form-control rounded dpd2" name="to">
                            </div>
                        </div>
                    </div>  
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-form-label  col-lg-12 col-sm-12 ">
                            Module Name
                        </label>
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="form-group select2">
                                <label class="sr-only">Name</label>
                                <select class="form-control select2">
                                    <option value="0">Search module name</option>
                                    <option value="1">Module Name 1</option>
                                    <option value="3">Module Name 2</option>
                                    <option value="2">Module Name 3</option>
                                    <option value="4">Module Name 4</option>
                                    <option value="5">Module Name 5</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-form-label col-lg-12 col-sm-12 ">
                            Status
                        </label>
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="form-group select2">
                                <label class="sr-only">Name</label>
                                <select class="form-control select2">
                                    <option value="0">Search status</option>
                                    <option value="Draft">Draft</option>
                                    <option value="Review">Review</option>
                                    <option value="Corrections">Corrections</option>
                                    <option value="Approved">Approved</option>
                                    <option value="Published">Published</option>
                                </select>
                            </div>
                        </div>
                    </div>

                </div>
              <div class="col-md-4 mt-4 "></div>
            <div class="col-md-4 mt-4 ">
                <button class="btn btn-space btn-primary btn-sm" type="submit"><i class="fa fa-search"></i>
                    Search</button>
                <button class="btn btn-space btn-secondary btn-sm" value="reset">
                    <i class="fa fa-refresh"></i> Reset</button>
            </div>
            <div class="col-md-4 mt-4 "></div>
            </div>
        </div>
    </section>

    <section class="card">
        <header class="card-header">
            Module Info Management
            <span class="tools pull-right">
                <a href="{{ route('module-info.edit') }}" class="btn btn-sm btn-primary btn-space text-white p-2"><i class="fa fa-user-plus mr-1 ml-1"
                        aria-hidden="true"></i> Add New Module</a>
              
            </span>
        </header>
        <div class="card-body">
            <div class="adv-table">
                <table class="display table table-bordered table-striped text-center" id="dynamic-table">
                    <thead>
                        <tr>
                            <th class="hidden-phone" width="5px">#</th>
                            <th width="10%">ID</th>
                            <th>Last Updated</th>
                            <th class="hidden-phone">Module Code</th>
                            <th>Module Name</th>
                            <th>Status</th>
                            <th class="action-col" width="20%">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>1</td>
                            <td>Friday, November 11 2019</td>
                            <td class="center hidden-phone">ART35208</td>
                            <td class="center hidden-phone">Module 22</td>
                            <td>Review</td>
                            <td class="action-col">
                                <button class="btn btn-success btn-sm" title="Show"><i class="fa fa-eye"></i></button>
                                <a href="{{ route('module-info.edit') }}" class="btn btn-primary btn-sm" title="Edit">
                                    <i class="fa fa-pencil"></i></a>

                                <button class="btn btn-secondary btn-sm" title="Submit">
                                    <i class="fa fa-paper-plane" aria-hidden="true"></i>
                                </button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</div>
@endsection

@push('extraScripts')
<script>
    $(document).ready(function () {
        $(".form_date-component").datepicker({
            format: "dd MM yyyy",
            autoclose: true,
            todayBtn: true,
        });
    });
</script>
@endpush
