@extends('layouts.main.master')

@section('content')

<section class="card">
    <header class="card-header">
        Add New Module Information
    </header>
    <div class="card-body">
        <div class="stepy-tab">
            <ul id="default-titles" class="stepy-titles clearfix">
                <li id="default-title-0" class="current-step">
                    <div>Step 1</div>
                </li>
                <li id="default-title-1" class="">
                    <div>Step 2</div>
                </li>
                <li id="default-title-2" class="">
                    <div>Step 3</div>
                </li>
                <li id="default-title-3" class="">
                    <div>Step 4</div>
                </li>

                <li id="default-title-4" class="">
                    <div>Step 5</div>
                </li>
                <li id="default-title-5" class="">
                    <div>Step 6</div>
                </li>
                <li id="default-title-6" class="">
                    <div>Step 7</div>
                </li>
                <li id="default-title-7" class="">
                    <div>Step 8</div>
                </li>
                <li id="default-title-8" class="">
                    <div>Step 9</div>
                </li>
                <li id="default-title-9" class="">
                    <div>Step 10</div>
                </li>
                <li id="default-title-10" class="">
                    <div>Step 11</div>
                </li>
                <li id="default-title-11" class="">
                    <div>Step 12</div>
                </li>
                <li id="default-title-12" class="">
                    <div>Step 13</div>
                </li>

            </ul>
        </div>
        <form class="form-horizontal" id="default">
            <fieldset title="Step1" class="step" id="default-step-0">
                <legend> </legend>
                <div class="card bg-light">
                    <header class="card-header">

                    </header>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row pb-0 pb-0">
                                            <div class="form-group col-md-6 mb-1">
                                                <label>Module Name</label>
                                                <input type="text" class="form-control" placeholder="Module Name"
                                                    value="Module 22" disabled>
                                            </div>
                                            <div class="form-group col-md-6 mb-1">
                                                <label>Module Code</label>
                                                <input type="text" class="form-control" placeholder="Module Code"
                                                    value="ART35208">
                                            </div>
                                        </div>
                                        <div class="form-group pt-2 mb-0">
                                            <label>Synopsis</label>
                                            <textarea class="form-control" cols="60" rows="5"
                                                placeholder="Synopsis"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row pb-0 pb-0">
                                            <div class="form-group col-md-6 mb-1">
                                                <label>Year Level (Module)</label>
                                                <input type="text" class="form-control"
                                                    placeholder="Year Level (Module)" value="4" disabled>
                                            </div>
                                            <div class="form-group col-md-6 mb-1">
                                                <label>Year Level (Programme)</label>
                                                <input type="text" class="form-control"
                                                    placeholder="Year Level (Programme)" value="3" disabled>
                                            </div>
                                        </div>
                                        <div class="form-group pt-2 mb-0">
                                            <label>Semester(s) Offered</label>
                                            <select class="form-control select2-multiple" multiple>
                                                <option value="0">Search Semester(s) Offered</option>
                                                <option value="1">January</option>
                                                <option value="3">March</option>
                                                <option value="2">April</option>
                                                <option value="4">May</option>
                                                <option value="5">June</option>
                                                <option value="5">July</option>
                                                <option value="5">August</option>
                                                <option value="5">September</option>
                                                <option value="5">October</option>
                                                <option value="5">November</option>
                                                <option value="5">December</option>
                                            </select>
                                        </div>
                                        <div class="form-group pt-2 mb-0">
                                            <label>Credit Value</label>
                                            <input type="number" class="form-control" step="1"
                                                placeholder="Credit Value"
                                                onchange="if(parseInt(this.value,10)<10)this.value='0'+this.value;">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </fieldset>
            <fieldset title="Step2" class="step" id="default-step-1">
                <legend> </legend>
                <div class="card bg-light">
                    <header class="card-header"></header>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="card">
                                    <div class="card-body">

                                        <!-- Pre-Requisite -->
                                        <div class="row pb-0 pb-0">
                                            <div class="form-group col-md-6 ">
                                                <label>Pre-Requisite</label>
                                                <input type="text" class="form-control"
                                                    placeholder="General Pre-requisite">
                                            </div>
                                            <div class="form-group col-md-6 ">
                                                <label>Module Pre-requisite</label>
                                                <select class="form-control select2-multiple" multiple>
                                                    <option value="0">Search Module Pre-requisite</option>
                                                    <option value="2">Module 2</option>
                                                    <option value="3">Module 3</option>
                                                    <option value="4">Module 4</option>
                                                    <option value="5">Module 5</option>
                                                </select>
                                            </div>
                                        </div>

                                        <!-- Co-Requisite -->
                                        <div class="row pb-0 pb-0">
                                            <div class="form-group col-md-6">
                                                <label>General Co-requisite</label>
                                                <input type="text" class="form-control"
                                                    placeholder="General Co-requisite">
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Module Co-requisite</label>
                                                <select class="form-control select2-multiple" multiple>
                                                    <option value="0">Search Module Co-requisite</option>
                                                    <option value="2">Module 2</option>
                                                    <option value="3">Module 3</option>
                                                    <option value="4">Module 4</option>
                                                    <option value="5">Module 5</option>
                                                </select>
                                            </div>
                                        </div>

                                        <!-- Anti-Requisite -->
                                        <div class="row pb-0 pb-0">
                                            <div class="form-group col-md-6">
                                                <label>General Anti-requisite</label>
                                                <input type="text" class="form-control"
                                                    placeholder="General Anti-requisite">
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Module Anti-requisite</label>
                                                <select class="form-control select2-multiple" multiple>
                                                    <option value="0">Search Module Anti-requisite</option>
                                                    <option value="2">Module 2</option>
                                                    <option value="3">Module 3</option>
                                                    <option value="4">Module 4</option>
                                                    <option value="5">Module 5</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="card">
                                    <div class="card-body">

                                        <!-- Module Owner -->
                                        <div class="form-group mb-0">
                                            <label>Module Owner</label>
                                            <input type="text" class="form-control" placeholder="Module Owner"
                                                value="School of Computing & IT" disabled>
                                        </div>

                                        <!-- Module offered as -->
                                        <div class="form-group pt-2 mb-0">
                                            <label>Module offered as</label>
                                            <input type="text" class="form-control" value="Primary Major –Core" disabled
                                                placeholder="Module offered as">
                                        </div>

                                        <!-- Domain Name (for free electives only) -->
                                        <div class="form-group pt-2 mb-0">
                                            <label>Domain Name (for free electives only)</label>
                                            <select class="select2 form-control">
                                                <option value="0">Search Domain Name</option>
                                                <option value="AHSS">Arts, Humanities and Social Sciences</option>
                                                <option value="BSM">Business, Services and Management</option>
                                                <option value="STS">Science, Technology and Society</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>


            </fieldset>
            <fieldset title="Step3" class="step" id="default-step-2">
                <legend> </legend>
                <div class="card bg-light">
                    <header class="card-header"></header>
                    <div class="card-body">
                        <div class="col-md-12">
                            <div class="card-body">
                                <div class="form-group">
                                    <label>Module Learning Outcomes</label>
                                    <a class="btn btn-outline-danger btn-sm" data-toggle="modal" href="#guideline">
                                        <i class="fa fa-book mr-2"></i>
                                        Guidelines</a>
                                    <table class="table table-sm mt-3  table-hover table-responsive" id="subjectTable">
                                        <thead class="text-dark text-uppercase font-weight-bolder">
                                            <tr>
                                                <th width="35%" class="font-weight-bolder">No./ID</th>
                                                <th width="45%" class="text-center font-weight-bolder">
                                                    Description
                                                </th>
                                                <th width="30%" class="text-center font-weight-bolder">MLO %
                                                </th>
                                                <th width="10%" class="text-right font-weight-bolder"></th>
                                            </tr>
                                        </thead>
                                        <tbody class="no-border-x subjectBody">
                                            <tr class="subjectRow" id="subjectRow0">
                                                <td>
                                                    <input type="text" class="form-control" placeholder="No./ID">
                                                </td>

                                                <td class="text-center">
                                                    <input type="text" class="form-control" placeholder="Description">
                                                </td>
                                                <td class="text-center">
                                                    <input type="number" min="0" max="100" class="form-control"
                                                        placeholder="MLO %">
                                                </td>
                                                <td class="text-inverse">
                                                    <button type="button"
                                                        class="btn btn-link btn-space btn-rounded btn-sm remove">
                                                        <i class="fa fa-times fa-lg"></i></button>
                                                </td>
                                            </tr>
                                        </tbody>
                                        <tfoot>
                                            <tr id="addButtonRow">
                                                <td colspan="2" class="text-left">
                                                    <button type="button"
                                                        class="btn btn-primary btn-space btn-rounded btn-sm add">
                                                        <i class="fa fa-plus"></i>
                                                        <span class="form-add-subject">Add more
                                                            subject</span>
                                                    </button>
                                                </td>
                                                <td>
                                                    <input type="text calculate" class="form-control" value="0"
                                                        disabled>
                                                </td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </fieldset>
            <fieldset title="Step4" class="step" id="default-step-1">
                <legend> </legend>
                <div class="card bg-light">
                    <header class="card-header">
                        Section 4
                    </header>
                    <div class="card-body">
                        <a class="btn btn-space btn-sm btn-primary" href="{{ URL::route('mapping2') }}">Click here to
                            proceed to MLO-PLO Mapping <i class="fa fa-chevron-right"></i></a>
                    </div>
                </div>

            </fieldset>
            <fieldset title="Step5" class="step" id="default-step-1">
                <legend> </legend>
                <div class="card bg-light">
                    <header class="card-header">
                        Transferrable Skills
                    </header>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">

                                <table class="table table-condensed table-bordered tgc" style="font-size:.85em;"
                                    id="tgc">
                                    <thead>
                                        <tr>
                                            <th width="10%" class="left-align">TGC</th>
                                            <th width="15%" class="left-align">TGC Attribute</th>
                                            <th width="30%" class="text-center">Description of TGC Attributes</th>
                                            <th width="10%" class="text-center">Sub-Attribute</th>
                                            <th width="" class="text-center">Description of sub-attributes</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <tr class="divider1">
                                            <td rowspan="4">TGC1</td>
                                            <td rowspan="4">Diciplines-Specific knowlegde</td>
                                            <td rowspan="4">Diciplines-Specific knowlegde refer to ability to
                                                dmeonstrate professional competence, comprehend and adapt dicipline
                                                specific knowledge,
                                                and be bale to intergrate knowledge across diverse perspective</td>
                                            <td class="sub-a">1.1</td>
                                            <td>Knowledge: Demonstrate a broad and coherent theoretical and
                                                technical knowledge to communicate and understanding
                                                relating to the disdpline-specific content</td>
                                        </tr>
                                        <tr class="">
                                            <td class="sub-a">1.2</td>
                                            <td>
                                                Comprehension: Demonstrate comprehension of disciplinary concepts
                                            </td>
                                        </tr>
                                        <tr class="">
                                            <td class="sub-a">1.3</td>
                                            <td>
                                                Transfer: Adapt and apply skills, abilities, theories or
                                                methodologies gained in one situation to new situations

                                            </td>
                                        </tr>
                                        <tr class="">
                                            <td class="sub-a">1.4</td>
                                            <td>
                                                Connect: Analyse, synthesize and integrate knowledge from more than
                                                one field of study or perspective
                                            </td>
                                        </tr>
                                        <!-- ========================================== -->
                                        <tr class="divider1">
                                            <td>TGC2</td>
                                            <td>Problem Solving,
                                                Critical and Creative
                                                Thinking Skills
                                            </td>
                                            <td>The ability to rationally, critically and
                                                creatively analyze, synthesize and evaluate evidences to arrive at
                                                solution or conclusion.
                                            </td>
                                            <td colspan="2" class="empty-td"></td>
                                        </tr>
                                        <!-- ----------------------------------- -->
                                        <tr class="divider1">
                                            <td rowspan="4">TGC2a</td>
                                            <td rowspan="4">Problem Solving Skills</td>
                                            <td rowspan="4">
                                                Problem Solving, Critical and Creative
                                                Thinking Skills refer to the ability to rationally, critically and
                                                creatively analyze, synthesize and evaluate evidence
                                                to arrive at a solution or conclusion.

                                            </td>
                                            <td class="sub-a">2a.1
                                            </td>
                                            <td>
                                                identify the problem
                                            </td>
                                        </tr>
                                        <tr class="a-2">
                                            <td class="sub-a">2a.2</td>
                                            <td>
                                                Propose solutions to existing and emerging problems
                                            </td>
                                        </tr>
                                        <tr class="a-2">
                                            <td class="sub-a"> 2a.3</td>
                                            <td>
                                                Implement a solution
                                            </td>
                                        </tr>
                                        <tr class="a-2">
                                            <td class="sub-a"> 2a.4</td>
                                            <td>
                                                Evaluate process and outcomes
                                            </td>
                                        </tr>
                                        <!-- -------------------------------- -->
                                        <tr class="divider1">
                                            <td rowspan="5">TGC2b</td>
                                            <td rowspan="5">
                                                Critical and Creative Thinking Skills
                                            </td>
                                            <td rowspan="5">
                                                Critical and Creative Thinking Skills refer to the ability to examine
                                                an issue through flexible and divergent thinking,
                                                and critically and creatively analyse, synthesize
                                                and evaluate evidence to justify a solution or conclusion.
                                            </td>
                                            <td class="sub-a">
                                                2b.1
                                            </td>
                                            <td>
                                                Flexibility and divergent thinking


                                            </td>
                                        </tr>
                                        <tr class="a-3">
                                            <td class="sub-a"> 2b.2</td>
                                            <td>
                                                Creative thinking

                                            </td>
                                        </tr>
                                        <tr class="a-3">
                                            <td class="sub-a"> 2b.3</td>
                                            <td>

                                                Risk-taking/Pushing the boundaries

                                            </td>
                                        </tr>
                                        <tr class="a-3">
                                            <td class="sub-a"> 2b.4</td>
                                            <td>

                                                Analyze and synthesize the evidence

                                            </td>
                                        </tr>
                                        <tr class="a-3">
                                            <td class="sub-a"> 2b.5</td>
                                            <td>

                                                Justify and theorize your position
                                                (perspective/thesis/hypothesis)
                                            </td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </fieldset>
            <fieldset title="Step6" class="step" id="default-step-1">
                <legend> </legend>
                <div class="card bg-light">
                    <header class="card-header">

                    </header>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        Description of Assessment Components
                                    </div>
                                    <div class="card-body">

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <div class="col-sm-12">
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input"
                                                                id="customCheck1">
                                                            <label class="custom-control-label" for="customCheck1">
                                                                TC-External Assessment</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <a href="javasrcipt:void(0)"
                                                    class="btn btn-space btn-sm btn-primary">Save</a>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Resit Assessment</label>
                                            <div class="col-lg-12">
                                                <div class="custom-control custom-radio custom-control-inline">
                                                    <input type="radio" id="awarding1" value="yes" name="awarding_body"
                                                        class="custom-control-input">
                                                    <label class="custom-control-label" for="awarding1">Yes</label>
                                                </div>
                                                <div class="custom-control custom-radio custom-control-inline">
                                                    <input type="radio" id="awarding2" value="other" checked
                                                        name="awarding_body" class="custom-control-input">
                                                    <label class="custom-control-label" for="awarding2">No</label>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Resit Opportunity -->
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Resit Opportunity</label>
                                            <div class="col-sm-12">
                                                <select class="form-control select2">
                                                    <option value="">Please select</option>
                                                    <option value="1">Dropdown 1</option>
                                                    <option value="2">Dropdown 2</option>
                                                    <option value="3">Dropdown 3</option>
                                                    <option value="4">Dropdown 4</option>
                                                    <option value="5">Dropdown 5</option>
                                                </select>
                                            </div>
                                        </div>

                                        <!-- Assessment Code -->
                                        <div class="form-group">
                                            <label class="col-lg-2">Assessment Code</label>
                                            <div class="col-sm-12">
                                                <input type="text" class="form-control" value="MPU3211210131102019"
                                                    placeholder="Assessment Code">
                                            </div>
                                        </div>

                                        <!-- Maximum Marks -->
                                        <div class="form-group">
                                            <label class="col-lg-2">Maximum Marks</label>
                                            <div class="col-sm-12">
                                                <input type="number" min="0" max="100" class="form-control"
                                                    placeholder="Maximum Marks">
                                            </div>
                                        </div>

                                        <!-- Assessment Tasks -->
                                        <div class="form-group">
                                            <div class="col-lg-12 ">
                                                <label>Assessment Tasks</label>
                                                <a class="btn btn-outline-danger btn-sm mb-3" data-toggle="modal"
                                                    href="#add-assessment">
                                                    Add New Assessment Task</a>

                                                <!-- Table -->
                                                <table class="table table-bordered tgc pt-3">
                                                    <thead>
                                                        <tr>
                                                            <th width="10%">No</th>
                                                            <th>Assessment Task</th>
                                                            <th width="10%">Weight</th>
                                                            <th width="10%">MLO Assessed</th>
                                                            <th width="10%">PLO Assessed</th>
                                                            <th width="10%">Due Date</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>AT001</td>
                                                            <td>
                                                                <a data-toggle="modal" href="#show-assessment">
                                                                    Assessment Task 1: Group Assessment - Journey of the
                                                                    Five Senses
                                                                </a>
                                                            </td>
                                                            <td class="text-center">10%</td>
                                                            <td>MLO 1</td>
                                                            <td>PLO 3</td>
                                                            <td>Week 3</td>
                                                        </tr>
                                                        <tr>
                                                            <td>AT002</td>
                                                            <td>
                                                                <a data-toggle="modal" href="#show-assessment">
                                                                    Assessment Task 2: Site Analysis
                                                                </a>
                                                            </td>
                                                            <td class="text-center">15%</td>
                                                            <td>MLO 2</td>
                                                            <td>PLO 1</td>
                                                            <td>Week 6</td>
                                                        </tr>
                                                        <tr>
                                                            <td></td>
                                                            <td>
                                                            </td>
                                                            <td class="text-center">25%</td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>



                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </fieldset>
            <fieldset title="Step7" class="step" id="default-step-1">
                <legend> </legend>
                <div class="card bg-light">
                    <header class="card-header">Resit Assessment</header>
                </div>
                <div class="card-body">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="">Resit Assessment</label>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1"
                                    value="1" checked>
                                <label class="form-check-label" for="inlineRadio1">Yes</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2"
                                    value="0">
                                <label class="form-check-label" for="inlineRadio2">No</label>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="">Resit Opportunity</label>
                            <select name="" class="form-control select2" id="">
                                <option value="">Please Select</option>
                                <option value="">Dropdown 1</option>
                                <option value="">Dropdown 2</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="">Assessment Code</label>
                        <input id="" class="form-control" readonly type="text" name="" value="MPU3211210131102019">
                    </div>
                    <div class="form-group">
                        <label for="">Maximum Marks</label>
                        <input id="" class="form-control" type="number" name="">
                    </div>
                    <div class="form-group">
                        <span class="float-right mb-3 mr-2"><button type="button" onclick="addAssessment()"
                                class="btn btn-success btn-sm"><i class="fa fa-plus" aria-hidden="true"></i> Add New
                                Assessment
                            </button></span>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Assessment Task</th>
                                    <th>Weight</th>
                                    <th>MLO Assessed</th>
                                    <th>PLO Assessed</th>
                                    <th>Due Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>AT001</td>
                                    <td>
                                        <a href="">Assessment Task 1: Group Assessment - Journey of the Five Senses</a>
                                    </td>
                                    <td>10 %</td>
                                    <td>MLO 1</td>
                                    <td>PLO 3</td>
                                    <td>Week 3</td>
                                </tr>
                                <tr>
                                    <td>AT002</td>
                                    <td>
                                        <a href="">Assessment Task 2: Site Analysis</a>
                                    </td>
                                    <td>15 %</td>
                                    <td>MLO 2</td>
                                    <td>PLO 1</td>
                                    <td>Week 6</td>
                                </tr>


                            </tbody>
                            <tfoot>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td>25%</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>

            </fieldset>
            <fieldset title="Step8" class="step" id="default-step-1">
                <legend> </legend>
                <div class="card bg-light">
                    <header class="card-header">
                        Rubrics for Assessment Tasks
                    </header>
                    <div class="card-body">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Assessment Task</th>
                                    <th>MLO Assessed</th>
                                    <th>Rubrics</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Assessment Task 1</td>
                                    <td><a href="#" onclick="openMLO(1)">MLO1</a></td>
                                    <td><button type="button" onclick="openRubric()"
                                            class="btn btn-sm btn-secondary">Rubrics</button>
                                    </td>
                                    <td>
                                        <a href="{{ route('add-rubrics') }}" class="btn btn-success btn-sm"
                                            title="Add Rubric"><i class="fa fa-plus" aria-hidden="true"></i></a>
                                        <button class="btn btn-warning btn-sm" title="Edit Rubric"><i
                                                class="fa fa-pencil" aria-hidden="true"></i></i></button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Assessment Task 2</td>
                                    <td><a href="#" onclick="openMLO(2)">MLO2</a></td>
                                    <td><button type="button" onclick="openRubric()"
                                            class="btn btn-sm btn-secondary">Rubrics</button>
                                    </td>
                                    <td>
                                        <a href="{{ route('add-rubrics') }}" class="btn btn-success btn-sm"
                                            title="Add Rubric"><i class="fa fa-plus" aria-hidden="true"></i></a>
                                        <button class="btn btn-warning btn-sm" title="Edit Rubric"><i
                                                class="fa fa-pencil" aria-hidden="true"></i></i></button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Assessment Task 3</td>
                                    <td><a href="#" onclick="openMLO(3)">MLO3</a></td>
                                    <td><button type="button" onclick="openRubric()"
                                            class="btn btn-sm btn-secondary">Rubrics</button>
                                    </td>
                                    <td>
                                        <a href="{{ route('add-rubrics') }}" class="btn btn-success btn-sm"
                                            title="Add Rubric"><i class="fa fa-plus" aria-hidden="true"></i></a>
                                        <button class="btn btn-warning btn-sm" title="Edit Rubric"><i
                                                class="fa fa-pencil" aria-hidden="true"></i></i></button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

            </fieldset>
            <fieldset title="Step9" class="step" id="default-step-1">
                <legend> </legend>
                <div class="card bg-light">
                    <header class="card-header"></header>
                    <div class="card-body">
                        <div class="row">

                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body">

                                        <div class="form-group mb-0">
                                            <label>Hurdle Assessment Guideline for the Module</label>
                                            <textarea name="" id="" cols="30" rows="10" class="form-control"></textarea>
                                        </div>


                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </fieldset>
            <fieldset title="Step10" class="step" id="default-step-1">
                <legend> </legend>
                <div class="card bg-light">
                    <header class="card-header">Student Learning Time (SLT)</header>
                    <div class="card-body">
                       <div class="card-body">
                            <ul class="task-list">
                                <li>
                        
                                    <a href="{{ URL::route('step10') }}" type="button" class="btn btn-info" >
                                        <i class="fa fa-plus mr-3"></i> <span style="letter-spacing:1px">Normal (Long) Semester </span>
                                    </a>
                                </li>
                               
                                <li class="mt-3">
                                    <a href="{{ URL::route('step10') }}" type="button" class="btn btn-warning" >
                                        <i class="fa fa-plus mr-3"></i><span style="letter-spacing:1px">Year-Long Module</span>
                                    </a>
                                </li>
                                <li class="mt-3">
                                    <a href="{{ URL::route('step10') }}" type="button" class="btn btn-success">
                                        <i class="fa fa-plus mr-3"></i><span style="letter-spacing:1px">Short Semester</span>
                                    </a>
                                </li>
                                <li class="mt-3">
                                    <a href="{{ URL::route('step10') }}" type="button" class="btn btn-danger">
                                        <i class="fa fa-plus mr-3"></i><span style="letter-spacing:1px">Internship</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
  
               
         

            </fieldset>
            <fieldset title="Step11" class="step" id="default-step-1">
                <legend> </legend>
                <div class="card bg-light">
                    <header class="card-header"></header>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="card">
                                    <div class="card-body">

                                        <!-- Main references supporting the module -->
                                        <div class="form-group mb-0">
                                            <label>Main references supporting the module</label>
                                            <a class="btn btn-sm btn-block btn-primary" data-toggle="modal"
                                                href="#main-references">
                                                Select References</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="card">
                                    <div class="card-body">

                                        <!-- Other Additional Information -->
                                        <div class="form-group mb-0">
                                            <label>Other Additional Information</label>
                                            <a class="btn btn-sm btn-block btn-primary" data-toggle="modal"
                                                href="#main-references">
                                                Select References</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset>
            <fieldset title="Step12" class="step" id="default-step-1">
                <legend> </legend>
                <div class="card bg-light">
                    <header class="card-header">

                    </header>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="card">
                                    <div class="card-body">
                                        <!-- Special Requirements to deliver the Module -->
                                        <div class="form-group pt-2 mb-0">
                                            <label>Special Requirements to deliver the Module</label>
                                            <textarea class="form-control" cols="60" rows="5"
                                                placeholder="Special Requirements to deliver the Module"></textarea>
                                        </div>

                                        <div class="form-group pt-2 mb-0 mt-3">
                                            <div class="custom-control custom-checkbox mb-2">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1">
                                                <label class="custom-control-label" for="customCheck1">Latihan
                                                    Industri</label>
                                            </div>
                                            <div class="custom-control custom-checkbox mb-2">
                                                <input type="checkbox" class="custom-control-input" id="customCheck2">
                                                <label class="custom-control-label" for="customCheck2">Clinical
                                                    Placement</label>
                                            </div>
                                            <div class="custom-control custom-checkbox mb-2">
                                                <input type="checkbox" class="custom-control-input" id="customCheck3">
                                                <label class="custom-control-label" for="customCheck3">Practicum</label>
                                            </div>
                                            <div class="custom-control custom-checkbox mb-2">
                                                <input type="checkbox" class="custom-control-input" id="customCheck4">
                                                <label class="custom-control-label" for="customCheck4">WBL using 2
                                                    weeks</label>
                                            </div>
                                        </div>

                                        <!-- Effective Study Intake/Semester -->
                                        <div class="form-group pt-2 mb-0">
                                            <label>Effective Study Intake/Semester</label>
                                            <div class="input-group" data-date="12/07/2017"
                                                data-date-format="mm/dd/yyyy">
                                                <input type="text" class="form-control rounded dpd1" name="from"
                                                    placeholder="Effective Study Intake/Semester">
                                            </div>
                                        </div>

                                        <!-- Revision Number -->
                                        <div class="form-group pt-2 mb-0">
                                            <label>Revision Number</label>
                                            <input type="text" class="form-control" value="2.01" disabled>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="card">
                                    <div class="card-body">

                                        <!-- Approved By -->
                                        <div class="form-group pt-2 mb-0">
                                            <label>Approved By</label>
                                            <select class="select2 form-control">
                                                <option value="0">Search Approved By</option>
                                                <option value="School of Computing & IT">School of Computing & IT
                                                </option>
                                                <option value="School of Biosciences">School of Biosciences</option>
                                                <option value="Taylor's Business School">Taylor's Business School
                                                </option>
                                                <option value="School of Education">School of Education</option>
                                                <option value="School of Engineering">School of Engineering</option>
                                                <option value="School of Medicine">School of Medicine</option>
                                                <option value="Taylor's Law School">Taylor's Law School</option>
                                            </select>
                                        </div>

                                        <!-- Approval Date -->
                                        <div class="form-group pt-2 mb-0">
                                            <label>Approval Date</label>
                                            <div class="input-group" data-date="12/07/2017"
                                                data-date-format="mm/dd/yyyy">
                                                <input type="text" class="form-control rounded dpd1" name="from"
                                                    placeholder="Approval Date">
                                            </div>
                                        </div>

                                        <!-- Discipline Code -->
                                        <div class="form-group pt-2 mb-0">
                                            <label>Discipline Code</label>
                                            <select class="select2 form-control">
                                                <option value="0">Search Discipline Code</option>
                                                <option value="AUD">
                                                    Audio-visual techniques and media production</option>
                                                <option value="CRA">Craft skills</option>
                                                <option value="DES">Design</option>
                                                <option value="EDU">Education Science</option>
                                                <option value="ART">Fine arts</option>
                                                <option value="HIS">History and archaeology</option>
                                                <option value="HIS">History, philosophy and related subjects = 225+226
                                                </option>
                                                <option value="LAN">Literacy and numeracy</option>
                                                <option value="ART">Music and performing arts</option>
                                                <option value="LAN">National Language</option>
                                                <option value="LAN">Other languages</option>
                                                <option value="PER">Personal skills</option>
                                                <option value="PHI">Philosophy and ethics</option>
                                                <option value="REL">Religion</option>
                                                <option value="TRA">Teaching and training = 143 + 144 + 145 + 146
                                                </option>
                                                <option value="TRA">Training for preschool teachers</option>
                                                <option value="TRA">Training for teachers at basic levels</option>
                                                <option value="TRA">Training for teachers of vocational subjects
                                                </option>
                                                <option value="TRA">Training for teachers with subject specialisation
                                                </option>
                                            </select>
                                        </div>

                                        <!-- Stream -->
                                        <div class="form-group pt-2 mb-0">
                                            <label>Stream</label>
                                            <textarea class="form-control" cols="60" rows="5"
                                                placeholder="Stream"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset>
            <fieldset title="Step13" class="step" id="default-step-1">

                <legend> </legend>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <header class="card-header">
                                    Tracking of Module Changes
                                </header>
                                <div class="card-body">
                                    <p class="text-muted mb-4" style="letter-spacing:1px">
                                        <i class="fa fa-chevron-right" aria-hidden="true"></i>
                                        Module Code & Name:
                                        <strong style="color:#A9D86E">ACC60304-Financial Reporting</strong></p>
                                    <table class="table table-bordered tgc">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Existing Module Learning Outcome</th>
                                                <th>Revise Module Learning Outcome</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>sed do eiusmod tempor incididunt ut labore et dolore magna
                                                    aliqua. </td>
                                                <td>nam rick grimes malum cerebro. De carne lumbering animata
                                                    corpora quaeritis.</td>
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td>sed do eiusmod tempor incididunt ut labore et dolore magna
                                                    aliqua. </td>
                                                <td>nam rick grimes malum cerebro. De carne lumbering animata
                                                    corpora quaeritis.</td>
                                            </tr>
                                            <tr>
                                                <td>5</td>
                                                <td>sed do eiusmod tempor incididunt ut labore et dolore magna
                                                    aliqua. d</td>
                                                <td>nam rick grimes malum cerebro. De carne lumbering animata
                                                    corpora quaeritis.</td>
                                            </tr>
                                        </tbody>
                                    </table>

                                    <div class="row ">
                                        <div class="col-md-8">

                                        </div>
                                        <div class="col-md-4">
                                            <div class="card bg-1">

                                                <div class="card-body">
                                                    <table
                                                        class="table table-hover table-borderless table-small-text summary-tb">
                                                        <tr>
                                                            <td class="field">Minor</td>
                                                            <td class="text-field"> -<30% changes to ML </td> </tr> <tr>
                                                            <td class="field">Major</td>
                                                            <td class="text-field">->30% changes to MLO</td>
                                                        </tr>

                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="card">
                                <header class="card-header">
                                    Select the type of Change
                                </header>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="col-md-5 col-form-label text-md-right">Module
                                                    Category</label>
                                                <div class="col-sm-12 col-md-6">
                                                    <select class="form-control select2">
                                                        <option value="">Please select</option>
                                                        <option value="1">New</option>
                                                        <option value="2">Remove</option>
                                                        <option value="3">Replace (Drop)</option>
                                                        <option value="2">Replace (Add)</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <button class="btn btn-space btn-sm btn-primary"> Save</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer text-center">
                                    <a class="btn btn-success btn-sm" data-toggle="modal" href="#submitme">
                                        <i class="fa fa-paper-plane mr-2"></i>
                                        Submit Module Information with Changes</a>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </fieldset>
            <div class="finish">
                <a href="{{ URL::route('module-info') }}" class="btn btn-success">
                    <i class="fa fa-check"></i>
                    Finish !
                </a>
            </div>
        </form>
    </div>
</section>


<div class="modal fade" id="submitme" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

            </div>
            <div class="modal-body text-center mt-4">
                <div class="text-center">
                    <div class="i-circle text-success">
                        <i class="icon fa fa-check"></i>
                    </div>
                    <h4>Submitted!</h4>
                    <p>Module Information Successfully submitted for Review</p>
                </div>




            </div>




        </div>
    </div>
</div>
</div>
<!-- Modal Guidelines -->
<div class="modal fade" id="guideline" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Guidelines</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <section class="card tasks-widget">

                    <div class="card-body">

                        <div class="task-content text-center">

                            <ul class="task-list">
                                <li>
                                    <div class="task-title">
                                        <span class="task-title-sp">2 credits: </span>
                                        <span class="badge badge-sm badge-danger">2-3 MLOs</span>

                                    </div>
                                </li>
                                <li>

                                    <div class="task-title">
                                        <span class="task-title-sp">4 credits: </span>
                                        <span class="badge badge-sm badge-warning text-white">3-4 MLOs</span>

                                    </div>
                                </li>
                                <li>

                                    <div class="task-title">
                                        <span class="task-title-sp">8 credits: </span>
                                        <span class="badge badge-sm badge-success">5-6 MLOs</span>

                                    </div>
                                </li>
                            </ul>
                        </div>


                    </div>
                </section>

            </div>
        </div>
    </div>
</div>

<!-- Modal Main references supporting the module -->
<div class="modal fade" id="main-references" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Guidelines</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="adv-table">
                    <table
                        class="display table table-bordered table-striped text-center table-condensed tgc table-small-text"
                        id="dynamic-table">
                        <thead>
                            <tr>
                                <th class="hidden-phone" width="5px">#</th>
                                <th width="5%">No.</th>
                                <th>Author</th>
                                <th class="hidden-phone">Year of Publication</th>
                                <th>Title</th>
                                <th>Edition</th>
                                <th>Publisher</th>
                                <th>ISBN</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <div class="custom-control custom-checkbox mb-3">
                                        <input type="checkbox" class="custom-control-input" id="customCheck1">
                                        <label class="custom-control-label" for="customCheck1"></label>
                                    </div>
                                </td>
                                <td>1</td>
                                <td class="text-left">Prof. Joseph Robert</td>
                                <td class="center hidden-phone">2005</td>
                                <td class="center hidden-phone text-left">Lorem Ipsum is simply dummy text</td>
                                <td>Edition 1</td>
                                <td>Publisher 1</td>
                                <td>111-222-333</td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="custom-control custom-checkbox mb-3">
                                        <input type="checkbox" class="custom-control-input" id="customCheck2">
                                        <label class="custom-control-label" for="customCheck2"></label>
                                    </div>
                                </td>
                                <td>2</td>
                                <td class="text-left">John Doe</td>
                                <td class="center hidden-phone">2009</td>
                                <td class="center hidden-phone text-left">Lorem Ipsum is simply dummy text</td>
                                <td>Edition 2</td>
                                <td>Publisher 2</td>
                                <td>111-222-333</td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="custom-control custom-checkbox mb-3">
                                        <input type="checkbox" class="custom-control-input" id="customCheck3">
                                        <label class="custom-control-label" for="customCheck3"></label>
                                    </div>
                                </td>
                                <td>3</td>
                                <td class="text-left">William Bill Turner</td>
                                <td class="center hidden-phone">2010</td>
                                <td class="center hidden-phone text-left">Lorem Ipsum is simply dummy text</td>
                                <td>Edition 3</td>
                                <td>Publisher 3</td>
                                <td>111-222-333</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Add New Assessment Task -->
<div class="modal fade" id="add-assessment" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add New Assessment Task</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="form-group">
                    <label class="col-lg-12">Assessment Task No</label>
                    <div class="col-lg-12">
                        <input type="text" class="form-control" value="AT003" placeholder="Assessment Task No">
                    </div>
                </div>

                <!-- Assessment Task Title -->
                <div class="form-group">
                    <label class="col-lg-12">Assessment Task Title</label>
                    <div class="col-lg-12">
                        <input type="text" class="form-control" placeholder="Assessment Task Title">
                    </div>
                </div>

                <!-- Weightage -->
                <div class="form-group">
                    <label class="col-lg-12">Weightage</label>
                    <div class="input-group col-lg-12">
                        <input type="number" class="form-control" min="0" max="100" placeholder="Weightage"
                            aria-label="Weightage" aria-describedby="btnGroupAddon">
                        <div class="input-group-prepend">
                            <div class="input-group-text" id="btnGroupAddon">%</div>
                        </div>
                    </div>
                </div>

                <!-- Due Date -->
                <div class="form-group">
                    <label class="col-lg-12">Due Date</label>
                    <div class="col-lg-12">
                        <input type="text" class="form-control" placeholder="Due Date">
                    </div>
                </div>

                <!-- Due Date (for short semester) -->
                <div class="form-group">
                    <label class="col-lg-12">Due Date (for short semester)</label>
                    <div class="col-lg-12">
                        <input type="text" class="form-control" placeholder="Due Date (for short semester)">
                    </div>
                </div>

                <!-- MLO Assessed -->
                <div class="form-group">
                    <label class="col-lg-12">MLO Assessed</label>
                    <div class="col-lg-12">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>List MLO</th>
                                    <th>Check</th>
                                    <th>Final Piece</th>
                                    <th>Weightage (%) </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Lorem ipsum, dolor sit amet consectetur </td>
                                    <td class="text-center">
                                        <input type="checkbox">
                                    </td>
                                    <td>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="exampleRadios"
                                                id="exampleRadios1" value="option1" checked>
                                            <label class="form-check-label" for="exampleRadios1">
                                                Yes
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="exampleRadios"
                                                id="exampleRadios2" value="option2">
                                            <label class="form-check-label" for="exampleRadios2">
                                                No
                                            </label>
                                        </div>
                                    </td>
                                    <td>
                                        <input type="number" min="0" max="100" class="form-control"
                                            placeholder="Weightage %" </td> </tr> <tr>
                                    <td>Lorem ipsum, dolor sit amet consectetur </td>
                                    <td class="text-center">
                                        <input type="checkbox">
                                    </td>
                                    <td>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="exampleRadios"
                                                id="exampleRadios1" value="option1" checked>
                                            <label class="form-check-label" for="exampleRadios1">
                                                Yes
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="exampleRadios"
                                                id="exampleRadios2" value="option2">
                                            <label class="form-check-label" for="exampleRadios2">
                                                No
                                            </label>
                                        </div>
                                    </td>
                                    <td>
                                        <input type="number" min="0" max="100" class="form-control"
                                            placeholder="Weightage %" </td> </tr> <tr>
                                    <td>Lorem ipsum, dolor sit amet consectetur </td>
                                    <td class="text-center">
                                        <input type="checkbox">
                                    </td>
                                    <td>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="exampleRadios"
                                                id="exampleRadios1" value="option1" checked>
                                            <label class="form-check-label" for="exampleRadios1">
                                                Yes
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="exampleRadios"
                                                id="exampleRadios2" value="option2">
                                            <label class="form-check-label" for="exampleRadios2">
                                                No
                                            </label>
                                        </div>
                                    </td>
                                    <td>
                                        <input type="number" min="0" max="100" class="form-control"
                                            placeholder="Weightage %">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <!-- Overview -->
                <div class="form-group">
                    <label class="col-lg-12">Overview </label>
                    <div class="col-lg-12">
                        <textarea class="form-control" cols="60" rows="5" placeholder="Overview"></textarea>
                    </div>
                </div>

                <!-- Assessment Activities  -->
                <div class="form-group">
                    <label class="col-lg-12">Assessment Activities </label>
                    <div class="col-lg-12">
                        <textarea class="form-control" cols="60" rows="5"
                            placeholder="Assessment Activities"></textarea>
                    </div>
                </div>

                <!-- Teaching & Learning Activities  -->
                <div class="form-group">
                    <label class="col-lg-12">Teaching & Learning Activities </label>
                    <div class="col-lg-12">
                        <textarea class="form-control" cols="60" rows="5"
                            placeholder="Teaching & Learning Activities"></textarea>
                    </div>
                </div>

                <!-- Details  -->
                <div class="form-group">
                    <label class="col-lg-12">Details </label>
                    <div class="col-lg-12">
                        <textarea class="form-control" cols="60" rows="5" placeholder="Details"></textarea>
                    </div>
                </div>

                <!-- Maximum Marks -->
                <div class="form-group">
                    <label class="col-lg-12">Maximum Marks</label>
                    <div class="col-lg-12">
                        <input type="number" min="0" max="100" class="form-control" placeholder="Maximum Marks">
                    </div>
                </div>

                <!-- Passing Marks -->
                <div class="form-group">
                    <label class="col-lg-12">Passing Marks</label>
                    <div class="col-lg-12">
                        <input type="number" min="0" max="100" class="form-control" placeholder="Passing Marks">
                    </div>
                </div>

                <!-- Alternative Passing Marks -->
                <div class="form-group">
                    <label class="col-lg-12">Alternative Passing Marks</label>
                    <div class="col-lg-12">
                        <input type="number" min="0" max="100" class="form-control"
                            placeholder="Alternative Passing Marks">
                    </div>
                </div>

                <!-- Pass Grade -->
                <div class="form-group">
                    <label class="col-lg-12">Pass Grade</label>
                    <div class="col-lg-12">
                        <input type="text" class="form-control" placeholder="Pass Grade">
                    </div>
                </div>

                <!-- Cal Method -->
                <div class="form-group">
                    <label class="col-lg-12">Cal Method</label>
                    <div class="col-lg-12">
                        <input type="text" class="form-control" placeholder="Cal Method">
                    </div>
                </div>

                <!-- Final Exam -->
                <div class="form-group">
                    <label class="col-lg-12">Final Exam</label>
                    <div class="col-lg-12">
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="finalexam1" value="yes" name="awarding_body"
                                class="custom-control-input">
                            <label class="custom-control-label" for="finalexam1">Yes</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="finalexam2" value="other" name="awarding_body"
                                class="custom-control-input">
                            <label class="custom-control-label" for="finalexam2">No</label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" id="submit-assessment">Submit</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Add New Assessment Task -->
<div class="modal fade" id="show-assessment" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Assessment Task</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="form-group">
                    <label class="col-lg-12">Assessment Task No</label>
                    <div class="col-lg-12">
                        <input type="text" class="form-control" value="AT001" placeholder="Assessment Task No" disabled>
                    </div>
                </div>

                <!-- Assessment Task Title -->
                <div class="form-group">
                    <label class="col-lg-12">Assessment Task Title</label>
                    <div class="col-lg-12">
                        <input type="text" class="form-control"
                            value="Assessment Task 1: Group Assessment - Journey of the Five Senses"
                            placeholder="Assessment Task Title" disabled>
                    </div>
                </div>

                <!-- Weightage -->
                <div class="form-group">
                    <label class="col-lg-12">Weightage</label>
                    <div class="input-group col-lg-12">
                        <input type="number" min="0" max="100" class="form-control" value="10" placeholder="Weightage"
                            aria-label="Weightage" aria-describedby="btnGroupAddon" disabled>
                        <div class="input-group-prepend">
                            <div class="input-group-text" id="btnGroupAddon">%</div>
                        </div>
                    </div>
                </div>

                <!-- Due Date -->
                <div class="form-group">
                    <label class="col-lg-12">Due Date</label>
                    <div class="col-lg-12">
                        <input type="text" value="Week 3" class="form-control" placeholder="Due Date" disabled>
                    </div>
                </div>

                <!-- Due Date (for short semester) -->
                <div class="form-group">
                    <label class="col-lg-12">Due Date (for short semester)</label>
                    <div class="col-lg-12">
                        <input type="text" value="Week 3" class="form-control"
                            placeholder="Due Date (for short semester)" disabled>
                    </div>
                </div>

                <!-- MLO Assessed -->
                <div class="form-group">
                    <label class="col-lg-12">MLO Assessed</label>
                    <div class="col-lg-12">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>List MLO</th>
                                    <th>Check</th>
                                    <th>Final Piece</th>
                                    <th>Weightage (%) </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Lorem ipsum, dolor sit amet consectetur </td>
                                    <td class="text-center">
                                        <input type="checkbox" disabled checked>
                                    </td>
                                    <td>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="exampleRadios"
                                                id="exampleRadios1" value="option1" checked disabled>
                                            <label class="form-check-label" for="exampleRadios1">
                                                Yes
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="exampleRadios"
                                                id="exampleRadios2" value="option2" disabled>
                                            <label class="form-check-label" for="exampleRadios2">
                                                No
                                            </label>
                                        </div>
                                    </td>
                                    <td>
                                        <input type="number" min="0" max="100" value="30" class="form-control"
                                            placeholder="Weightage %" disabled>
                                    </td>
                                </tr>

                                <tr>
                                    <td>Lorem ipsum, dolor sit amet consectetur </td>
                                    <td class="text-center">
                                        <input type="checkbox" disabled checked>
                                    </td>
                                    <td>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="exampleRadios"
                                                id="exampleRadios1" value="option1" disabled>
                                            <label class="form-check-label" for="exampleRadios1">
                                                Yes
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="exampleRadios"
                                                id="exampleRadios2" value="option2" disabled>
                                            <label class="form-check-label" for="exampleRadios2">
                                                No
                                            </label>
                                        </div>
                                    </td>
                                    <td>
                                        <input type="number" min="0" max="100" class="form-control"
                                            placeholder="Weightage %" value="10" disabled>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Lorem ipsum, dolor sit amet consectetur </td>
                                    <td class="text-center">
                                        <input type="checkbox" disabled checked>
                                    </td>
                                    <td>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="exampleRadios"
                                                id="exampleRadios1" value="option1" disabled>
                                            <label class="form-check-label" for="exampleRadios1">
                                                Yes
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="exampleRadios"
                                                id="exampleRadios2" value="option2" disabled>
                                            <label class="form-check-label" for="exampleRadios2">
                                                No
                                            </label>
                                        </div>
                                    </td>
                                    <td>
                                        <input type="number" min="0" max="100" class="form-control"
                                            placeholder="Weightage %" value="50" disabled>
                                    </td>
                                </tr>


                            </tbody>
                        </table>
                    </div>
                </div>

                <!-- Overview -->
                <div class="form-group">
                    <label class="col-lg-12">Overview</label>
                    <div class="col-lg-12">
                        <textarea class="form-control" cols="60" rows="5" disabled
                            placeholder="Overview">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</textarea>
                    </div>
                </div>

                <!-- Assessment Activities  -->
                <div class="form-group">
                    <label class="col-lg-12">Assessment Activities</label>
                    <div class="col-lg-12">
                        <textarea class="form-control" cols="60" rows="5" disabled
                            placeholder="Assessment Activities">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</textarea>
                    </div>
                </div>

                <!-- Teaching & Learning Activities  -->
                <div class="form-group">
                    <label class="col-lg-12">Teaching & Learning Activities </label>
                    <div class="col-lg-12">
                        <textarea class="form-control" cols="60" rows="5" disabled
                            placeholder="Teaching & Learning Activities">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</textarea>
                    </div>
                </div>

                <!-- Details  -->
                <div class="form-group">
                    <label class="col-lg-12">Details </label>
                    <div class="col-lg-12">
                        <textarea class="form-control" cols="60" rows="5" disabled
                            placeholder="Details">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</textarea>
                    </div>
                </div>

                <!-- Maximum Marks -->
                <div class="form-group">
                    <label class="col-lg-12">Maximum Marks</label>
                    <div class="col-lg-12">
                        <input type="number" class="form-control" placeholder="Maximum Marks" value="10" disabled>
                    </div>
                </div>

                <!-- Passing Marks -->
                <div class="form-group">
                    <label class="col-lg-12">Passing Marks</label>
                    <div class="col-lg-12">
                        <input type="number" class="form-control" placeholder="Passing Marks" value="5" disabled>
                    </div>
                </div>

                <!-- Alternative Passing Marks -->
                <div class="form-group">
                    <label class="col-lg-12">Alternative Passing Marks</label>
                    <div class="col-lg-12">
                        <input type="number" class="form-control" placeholder="Alternative Passing Marks" value="4"
                            disabled>
                    </div>
                </div>

                <!-- Pass Grade -->
                <div class="form-group">
                    <label class="col-lg-12">Pass Grade</label>
                    <div class="col-lg-12">
                        <input type="text" class="form-control" placeholder="Pass Grade"
                            value="Lorem Ipsum is simply dummy text of the printing and typesetting industry." disabled>
                    </div>
                </div>

                <!-- Cal Method -->
                <div class="form-group">
                    <label class="col-lg-12">Cal Method</label>
                    <div class="col-lg-12">
                        <input type="text" class="form-control" placeholder="Cal Method"
                            value="Lorem Ipsum is simply dummy text of the printing and typesetting industry." disabled>
                    </div>
                </div>

                <!-- Final Exam -->
                <div class="form-group">
                    <label class="col-lg-12">Final Exam</label>
                    <div class="col-lg-12">
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="finalexam1" value="yes" name="awarding_body"
                                class="custom-control-input" checked disabled>
                            <label class="custom-control-label" for="finalexam1">Yes</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="finalexam2" value="other" name="awarding_body"
                                class="custom-control-input" disabled>
                            <label class="custom-control-label" for="finalexam2">No</label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


{{--  MLO Modal View  --}}
<div class="modal fade " id="mlo-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myModal">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Explicabo, iure aliquid ex ad sed vero
                    incidunt consequatur autem blanditiis quidem nulla quae praesentium enim maxime sint natus numquam
                    est eaque!
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

{{--  Rubrics Modal  --}}
<div class="modal fade " id="rubric-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myModal">Rubrics</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="my-textarea">Criteria</label>
                    <textarea id="my-textarea" class="form-control" name="" rows="3"
                        readonly>Lorem ipsum dolor sit amet consectetur adipisicing elit. Suscipit sit distinctio accusamus a, perferendis quis mollitia cumque numquam architecto, laborum quisquam sequi vitae officiis autem. Non qui cumque magni libero.</textarea>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="">Weightage</label>
                        <div class="input-group">
                            <input class="form-control" type="text" readonly>
                            <div class="input-group-append">
                                <span class="input-group-text" id="my-addon">%</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="">MLO Assessed</label>
                        <input type="text" name="" id="" class="form-control" readonly value="MLO2">
                    </div>
                </div>
                <div class="form-group">
                    <label>Out Standing (9-10)</label>
                    <textarea class="form-control" name="" rows="3"
                        readonly>Lorem ipsum dolor sit amet consectetur adipisicing elit. In provident est recusandae illo quam quidem, accusantium saepe dolorum eligendi minus doloremque quasi ipsam numquam! Sed molestiae numquam maxime cumque praesentium.</textarea>
                </div>
                <div class="form-group">
                    <label for="">Mastering (7-8)</label>
                    <textarea id="" class="form-control" name="" rows="3"
                        readonly>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Perferendis ex quidem repudiandae dolorem rem cum totam possimus ratione magnam nobis voluptatum quo asperiores incidunt dignissimos ab, eos est quisquam necessitatibus.</textarea>
                </div>
                <div class="form-group">
                    <label for="">Developing (5-6)</label>
                    <textarea id="" class="form-control" name="" rows="3"
                        readonly>Lorem ipsum dolor sit amet consectetur adipisicing elit. Tenetur aut dolores facere laudantium quidem molestiae provident suscipit? Doloribus velit nesciunt similique aliquid perspiciatis placeat voluptatum iste. Illo error blanditiis nisi!</textarea>
                </div>
                <div class="form-group">
                    <label for="">Beginning (0-4) </label>
                    <textarea id="" class="form-control" name="" rows="3"
                        readonly>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Hic iure debitis quas fugiat aliquam dignissimos voluptatem quisquam, facilis nulla sit labore nemo nihil. Dolorum possimus fugit iusto. Nesciunt, iste eius!</textarea>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

{{--  New Assessment Modal   --}}
<div class="modal fade " id="assessment-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myModal">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="">Assessment Task No</label>
                    <input id="" class="form-control" type="text" name="" value="AT003">
                </div>
                <div class="form-group">
                    <label for="">Assessment Task Title</label>
                    <textarea name="" class="form-control" id="" cols="30" rows="10"></textarea>
                </div>
                <div class="form-group">
                    <label for="">Weightage</label>
                    <div class="input-group">
                        <input class="form-control" type="text">
                        <div class="input-group-append">
                            <span class="input-group-text" id="my-addon">%</span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="">MLO Assessed</label><br>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>List MLO</th>
                                <th>Check</th>
                                <th>Final Piece</th>
                                <th>Weightage (%) </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Lorem ipsum, dolor sit amet consectetur </td>
                                <td class="text-center">
                                    <input type="checkbox">
                                </td>
                                <td>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="exampleRadios"
                                            id="exampleRadios1" value="option1" checked>
                                        <label class="form-check-label" for="exampleRadios1">
                                            Yes
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="exampleRadios"
                                            id="exampleRadios2" value="option2">
                                        <label class="form-check-label" for="exampleRadios2">
                                            No
                                        </label>
                                    </div>
                                </td>
                                <td>
                                    <input type="number" min="0" max="100" class="form-control"
                                        placeholder="Weightage %" </td> </tr> <tr>
                                <td>Lorem ipsum, dolor sit amet consectetur </td>
                                <td class="text-center">
                                    <input type="checkbox">
                                </td>
                                <td>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="exampleRadios"
                                            id="exampleRadios1" value="option1" checked>
                                        <label class="form-check-label" for="exampleRadios1">
                                            Yes
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="exampleRadios"
                                            id="exampleRadios2" value="option2">
                                        <label class="form-check-label" for="exampleRadios2">
                                            No
                                        </label>
                                    </div>
                                </td>
                                <td>
                                    <input type="number" min="0" max="100" class="form-control"
                                        placeholder="Weightage %" </td> </tr> <tr>
                                <td>Lorem ipsum, dolor sit amet consectetur </td>
                                <td class="text-center">
                                    <input type="checkbox">
                                </td>
                                <td>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="exampleRadios"
                                            id="exampleRadios1" value="option1" checked>
                                        <label class="form-check-label" for="exampleRadios1">
                                            Yes
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="exampleRadios"
                                            id="exampleRadios2" value="option2">
                                        <label class="form-check-label" for="exampleRadios2">
                                            No
                                        </label>
                                    </div>
                                </td>
                                <td>
                                    <input type="number" min="0" max="100" class="form-control"
                                        placeholder="Weightage %" </td> </tr> </tbody> </table> </div> <div
                                        class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="">Maximum Marks</label>
                                        <input id="" class="form-control" type="number" name="">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="">Passing Marks</label>
                                        <input id="" class="form-control" type="number" name="">
                                    </div>
                </div>
                <div class="form-group">
                    <label for="">Alternative Passing Marks</label>
                    <input id="" class="form-control" type="number" name="">
                </div>
                <div class="form-group">
                    <label for="">Pass Grade</label>
                    <textarea name="" class="form-control" id="" cols="30" rows="10"></textarea>
                </div>
                <div class="form-group">
                    <label for="">Final Exam</label>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="final_exam" id="inlineRadio1" value="1"
                            checked>
                        <label class="form-check-label" for="inlineRadio1">Yes</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="final_exam" id="inlineRadio2" value="0">
                        <label class="form-check-label" for="inlineRadio2">No</label>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" onclick="saveAssessment()" class="btn btn-success">Submit</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade " id="saved-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myModal">Assessment Task Added Successfully</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p class="text-center text-primary">
                    <i class="fa fa-check-circle-o fa-5x" aria-hidden="true"></i>
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


@endsection

@push('extraScripts')
<!--script for this page-->
{{-- <script src="{{ asset('js/jquery.stepy.js') }}"></script> --}}

<script type="text/javascript">
    // let row = $(".subjectRow");
    // let id = 1;

    $(document).ready(function () {

        $(".form_date-component").datepicker({
            format: "dd MM yyyy",
            autoclose: true,
            todayBtn: true,
        });

        $(".remove").on('click', function () {
            if ($("#subjectTable tr").length === 3) {
                $(".remove").hide();
            } else if ($("#subjectTable tr").length - 1 == 3) {
                $(".remove").hide();
                removeRow($(this));
            } else {
                removeRow($(this));
            }
        });

        $(".add").on('click', function () {
            addRow();

            $(this).closest("tr").appendTo("#subjectTable");

            if ($("#subjectTable tr").length === 3) {
                $(".remove").hide();
            } else {
                $(".remove").show();
            }
        });

        $('#buttonSumbit').click(function (e) {
            e.preventDefault();

            Swal.fire({
                type: 'success',
                title: 'Success',
                text: 'Module Information Successfully submitted for Review'
            });
        });

        $('#submit-assessment').click(function (e) {
            e.preventDefault();

            Swal.fire(
                'Success',
                `Assessment Task Added Successfully`,
                'success'
            ).then(() => {
                $(`#add-assessment`).modal('hide');
            });
        });

    });

    function removeRow(button) {
        button.closest("tr").remove();
    }

    function addRow() {
        var clonedDiv = row.clone(true, true);
        clonedDiv.appendTo("#subjectTable");
    }

    function openMLO(id) {
        $('#mlo-modal').modal('show')
        $('.modal-title').text(`MLO ${id}`)
    }

    function openRubric() {
        $('#rubric-modal').modal('show')
    }

    function add() {
        $('#mlo-add').modal('show')
        $('.modal-title').text('Assessment Task 2: Group Assessment')
    }

    function addAssessment() {
        $('#assessment-modal').modal('show')
        $('.modal-title').text('New Assessment Task')
    }

    function saveAssessment() {
        $('#assessment-modal').modal('hide')
        $('#saved-modal').modal('show')
        $('.modal-title').text('Assessment Task Added Successfully')
    }
</script>

@endpush