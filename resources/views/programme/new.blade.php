@extends('layouts.main.master')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <section class="card">
            <header class="card-header">
                Add New Programme Information
            </header>
            <div class="card-body">
                <div class="stepy-tab">
                    <ul id="default-titles" class="stepy-titles clearfix">
                        <li id="default-title-0" class="current-step">
                            <div>Step 1</div>
                        </li>
                        <li id="default-title-1" class="">
                            <div>Step 2</div>
                        </li>
                        <li id="default-title-2" class="">
                            <div>Step 3</div>
                        </li>
                        <li id="default-title-3" class="">
                            <div>Step 4</div>
                        </li>

                        <li id="default-title-4" class="">
                            <div>Step 5</div>
                        </li>
                        <li id="default-title-5" class="">
                            <div>Step 6</div>
                        </li>
                        <li id="default-title-6" class="">
                            <div>Step 7</div>
                        </li>
                        <li id="default-title-7" class="">
                            <div>Step 8</div>
                        </li>
                        <li id="default-title-8" class="">
                            <div>Step 9</div>
                        </li>
                        <li id="default-title-9" class="">
                            <div>Step 10</div>
                        </li>
                        <li id="default-title-10" class="">
                            <div>Step 11</div>
                        </li>
                        <li id="default-title-11" class="">
                            <div>Step 12</div>
                        </li>
                        <li id="default-title-12" class="">
                            <div>Step 12</div>
                        </li>
                        <li id="default-title-13" class="">
                            <div>Step 14</div>
                        </li>
                        <li id="default-title-14" class="">
                            <div>Step 15</div>
                        </li>
                        <li id="default-title-15" class="">
                            <div>Step 16</div>
                        </li>
                    </ul>
                </div>
                <form class="form-horizontal" id="default">
                    <fieldset title="Step1" class="step" id="default-step-0">

                        <legend> </legend>
                        <div class="card">
                            <header class="card-header">
                                Section 1
                            </header>
                            <div class="card-body">
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Campus Code</label>
                                    <div class="col-lg-12">
                                        <select name="" id="campus_code" class="form-control">
                                            <option value="TBS">Taylor's Business School</option>
                                            <option value="TCHT">Taylor's Lakeside Campus</option>
                                            <option value="TCPJ">Taylor's College Petaling Jaya</option>
                                            <option value="TCSH">Taylor's College Sri Hartamas</option>
                                            <option value="TCSJ">Taylor's University College Main Campus</option>
                                            <option value="TUCLS">Taylor's Lakeside Campus</option>
                                            <option value="BUV">British University Vietnam</option>
                                            <option value="TCLS">Taylor's College</option>
                                            <option value="IIAS">IIAS School Management</option>
                                            <option value="IS">Institut Sinaran</option>
                                            <option value="FMSH">Fishtail Mountain, School of Hospitality, Tourism &
                                                Management
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label"></label>
                                    <div class="col-lg-12">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="customCheck"
                                                name="example1">
                                            <label class="custom-control-label" for="customCheck">TCF Flagx</label>
                                        </div>

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Programme Name</label>
                                    <div class="col-lg-12">
                                        <input type="text" class="form-control" placeholder="Programme Name">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">School Owning</label>
                                    <div class="col-lg-12">
                                        <select name="" id="campus_code" class="form-control select2">
                                            <option value="TBS">School of Computing & IT</option>
                                            <option value="TCHT">School of Bioscience</option>
                                            <option value="TCPJ">Taylor's Business School</option>
                                            <option value="TCSH">School of Education</option>
                                            <option value="TCSJ">School of Engineering</option>
                                            <option value="TCSJ">School of Medicine</option>
                                            <option value="TCSJ">Taylor's Law School</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Field of Study and National Education Code
                                        (NEC)</label>
                                    <div class="col-lg-12">
                                        <select name="" id="nec_code" class="form-control select2">
                                            <option value="221">Religion</option>
                                            <option value="222">Language</option>
                                            <option value="223">National League</option>
                                            <option value="224">Other Languages</option>
                                            <option value="225">History and Archaeology</option>
                                            <option value="226">Philosophy and ethics</option>
                                            <option value="227">History, philosophy and related subjects </option>
                                            <option value="80">Literacy and Numeracy</option>
                                            <option value="90">Personal skills</option>
                                            <option value="141">Teaching and training </option>
                                            <option value="142">Education science</option>
                                            <option value="143">Training for preschool teachers</option>
                                            <option value="144">Training for teachers at basic levels</option>
                                            <option value="145">Training for teachers with subject specialisation
                                            </option>
                                            <option value="146">Training for teachers of vocational subjects</option>
                                            <option value="211">Fine arts</option>
                                            <option value="212">Music and performing arts</option>
                                            <option value="213">Audio-visual techniques and media production</option>
                                            <option value="214">Design</option>
                                            <option value="215">Craft skills</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">TU Programme Code</label>
                                    <div class="col-lg-12">
                                        <input type="text" name="tu_programme_code" id="tu_programme_code" disabled
                                            class="form-control"placeholder="92475">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">MOE Programme Code</label>
                                    <div class="col-lg-12">
                                        <input type="text" class="form-control" readonly
                                            placeholder="To be keyed in by RAG upon approval">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">MQA Programme Code</label>
                                    <div class="col-lg-12">
                                        <input type="text" class="form-control" readonly
                                            placeholder="To be keyed in by RAG upon approval">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">MOE Expiry Date</label>
                                    <div class="col-lg-12">
                                        <div class="input-group date dpYears" data-date-viewmode="years"
                                            data-date-format="dd-mm-yyyy" data-date="">
                                            <input placeholder="To be keyed in by RAG upon approval" readonly
                                                type="text" class="form-control" placeholder="" aria-label="Right Icon"
                                                aria-describedby="dp-ig">
                                            <div class="input-group-append">
                                                <button id="dp-ig" class="btn btn-outline-secondary" type="button"><i
                                                        class="fa fa-calendar f14"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">MQA Expiry Date</label>
                                    <div class="col-lg-12">
                                        <div class="input-group date dpYears" data-date-viewmode="years"
                                            data-date-format="dd-mm-yyyy" data-date="">
                                            <input placeholder="To be keyed in by RAG upon approval" readonly
                                                type="text" class="form-control" placeholder="" aria-label="Right Icon"
                                                aria-describedby="dp-ig">
                                            <div class="input-group-append">
                                                <button id="dp-ig" class="btn btn-outline-secondary" type="button"><i
                                                        class="fa fa-calendar f14"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">MQF Level</label>
                                    <div class="col-lg-12">
                                        <select name="" id="mqf_level" class="form-control select2">
                                            <option value=""></option>
                                            <option value="1">1 (Certificate)</option>
                                            <option value="2">2 (Certificate)</option>
                                            <option value="3">3 (Certificate)</option>
                                            <option value="4">4 (Diploma)</option>
                                            <option value="5">5 (Advance Diploma)</option>
                                            <option value="6">6 (Graduate Certificate)</option>
                                            <option value="6">6 (Graduate Diploma) </option>
                                            <option value="6">6 (Bachelor's Degree)</option>
                                            <option value="7">7 (Postgraduate Certificate)</option>
                                            <option value="7">7 (Postgraduate Diploma)</option>
                                            <option value="7">7 (Master's by Mixed Mode & Coursework)</option>
                                            <option value="7">7 (Master's by Research)</option>
                                            <option value="8">8 (Doctoral Degree by Mixed Mode & Coursework)</option>
                                            <option value="8">8 (PhD by Research)</option>

                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Graduating Credit</label>
                                    <div class="col-lg-12">
                                        <input class="form-control" type="text" name="graduating_credit">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Mode of Study</label>
                                    <div class="col-lg-12">
                                        <select name="" id="study_mode" class="form-control select2">
                                            <option value="Part Time">Part Time</option>
                                            <option value="Full Time">Full Time</option>
                                            <option value="Research">Research</option>
                                            <option value="Mixed Mode">Mixed Mode</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </fieldset>

                    <fieldset title="Step 2" class="step" id="default-step-1">
                        <legend> </legend>
                        <div class="card">
                            <header class="card-header">
                                Section 2
                            </header>
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="">Duration of Study</label>
                                    <div class="col-lg-12">
                                        <table class="table table-borderless ">
                                            <tr class="bg-info1">
                                                <td rowspan="2"></td>
                                                <td class="text-center fulltime" colspan="2">Full Time</td>
                                                <td class="text-center partime" colspan="2">Part Time Time</td>
                                            </tr>
                                            <tr class="bg-info2">
                                                <td class="fulltime"><span class="badge badge-warning text-white">Long
                                                        Semester</span></td>
                                                <td class="fulltime"><span class="badge badge-info">Short
                                                        Semester</span></td>
                                                <td class="partime"><span class="badge badge-warning text-white">Long
                                                        Semester</span></td>
                                                <td class="partime"><span class="badge badge-info">Short Semester</span>
                                                </td>

                                            </tr>
                                            <tr class="bg-info3">
                                                <td>No. of Weeks*</td>
                                                <td class="fulltime"><input type="text"
                                                        class="form-control mb-2 borderless-input" id="inlineFormInput"
                                                        placeholder=""></td>
                                                <td class="fulltime"><input type="text"
                                                        class="form-control mb-2 borderless-input" id="inlineFormInput"
                                                        placeholder=""></td>
                                                <td class="partime"><input type="text"
                                                        class="form-control mb-2 borderless-input" id="inlineFormInput"
                                                        placeholder=""></td>
                                                <td class="partime"><input type="text"
                                                        class="form-control mb-2 borderless-input" id="inlineFormInput"
                                                        placeholder=""></td>

                                            </tr>
                                            <tr class="bg-info3">
                                                <td>No. of Semester</td>
                                                <td class="fulltime">
                                                    <input type="text" class="form-control mb-2 borderless-input"
                                                        id="inlineFormInput" placeholder="">
                                                </td>
                                                <td class="fulltime">
                                                    <input type="text" class="form-control mb-2 borderless-input"
                                                        id="inlineFormInput" placeholder="">
                                                </td>
                                                <td class="partime">
                                                    <input type="text" class="form-control mb-2 borderless-input"
                                                        id="inlineFormInput" placeholder="">
                                                </td>
                                                <td class="partime">
                                                    <input type="text" class="form-control mb-2 borderless-input"
                                                        id="inlineFormInput" placeholder="">
                                                </td>
                                            </tr>
                                            <tr class="bg-info3">
                                                <td>No. of Years</td>
                                                <td class="fulltime">
                                                    <input type="text" class="form-control mb-2 borderless-input"
                                                        id="inlineFormInput" placeholder="">
                                                </td>
                                                <td class="fulltime">
                                                    <input type="text" class="form-control mb-2 borderless-input"
                                                        id="inlineFormInput" placeholder="">
                                                </td>
                                                <td class="partime">
                                                    <input type="text" class="form-control mb-2 borderless-input"
                                                        id="inlineFormInput" placeholder="">
                                                </td>
                                                <td class="partime">
                                                    <input type="text" class="form-control mb-2 borderless-input"
                                                        id="inlineFormInput" placeholder="">
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-12 text-right">
                                        <button type="button" class="btn btn-primary btn-sm">Download PDF <i
                                                class="ml-2 fa fa-file-pdf-o" aria-hidden="true"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>






                    </fieldset>


                    <fieldset title="Step 3" class="step" id="default-step-2">
                        <legend> </legend>
                        <div class="card">
                            <header class="card-header">
                                Section 3
                            </header>
                            <div class="card-body">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label col-lg-2 mb-3">Type of Award </label>
                                    <div class="col-lg-9">
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="customRadioInline1" value="award" name="award"
                                                class="custom-control-input">
                                            <label class="custom-control-label" for="customRadioInline1">Award
                                                Programme</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="customRadioInline2" value="non_award" name="award"
                                                class="custom-control-input">
                                            <label class="custom-control-label" for="customRadioInline2">Non-Award
                                                Programme</label>
                                        </div>

                                    </div>
                                </div>
                                <div class="form-group award">
                                    <label class="col-lg-2 control-label"></label>
                                    <div class="col-lg-12">
                                        <select id="award_type" name="award_type" class="form-control select2">
                                            <option value="">Single Award (1 testamur )</option>
                                            <option value="">Joint Award (1 testamur )</option>
                                            <option value="">Dual Award (2 testamur )</option>
                                            <option value="">Multiple Award (1 testamur )</option>
                                            <option value="">Franchise Programme</option>
                                            <option value="">External Award (1 testamur )</option>
                                            <option value="other">Other </option>

                                        </select>
                                    </div>
                                </div>
                                <div class="form-group other-type">
                                    <label class="col-lg-2 control-label"></label>
                                    <div class="col-lg-12">
                                        <input type="text" name="" class="form-control" placeholder="Other">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Awarding Body</label>
                                    <div class="col-lg-12">
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="awarding1" value="own" name="awarding_body"
                                                class="custom-control-input">
                                            <label class="custom-control-label" for="awarding1">Own</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="awarding2" value="other" name="awarding_body"
                                                class="custom-control-input">
                                            <label class="custom-control-label" for="awarding2">Other</label>
                                        </div>

                                    </div>
                                </div>
                                <div class="form-group awarding-body">
                                    <label class="col-lg-2 control-label"></label>
                                    <div class="col-lg-12">
                                        <input type="text" name="" class="form-control" placeholder="Other">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Type of Programme</label>
                                    <div class="col-lg-12">
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="programme_type1" value="taught"
                                                name="programme_type" class="custom-control-input">
                                            <label class="custom-control-label" for="programme_type1">Taught</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="programme_type2" value="research"
                                                name="programme_type" class="custom-control-input">
                                            <label class="custom-control-label" for="programme_type2">Research</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="programme_type2" value="mixed" name="programme_type"
                                                class="custom-control-input">
                                            <label class="custom-control-label" for="programme_type3">Mixed Mode</label>
                                        </div>

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Mode of Study</label>
                                    <div class="col-lg-12">
                                        <select name="" id="programme_type" class="form-control select2">
                                            <option value="Part Time">Single major with complementary study</option>
                                            <option value="Full Time">Single major with specialization and complementary
                                                study
                                            </option>
                                            <option value="Research">Double Specialization</option>
                                            <option value="Mixed Mode">Doule Degree</option>
                                            <option value="Mixed Mode">Transfer</option>
                                            <option value="Mixed Mode">Programme</option>
                                            <option value="Mixed Mode">Short Programme</option>
                                            <option value="other">Other</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group other-programme-type">
                                    <label class="col-lg-2 control-label"></label>
                                    <div class="col-lg-12">
                                        <input type="text" name="" class="form-control"
                                            placeholder="Other Programme Type">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Accreditation Body</label>
                                    <div class="col-lg-12">
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="accreditation1" value="mqa" name="accreditation"
                                                class="custom-control-input">
                                            <label class="custom-control-label" for="accreditation1">Malaysian
                                                Qualifications
                                                Agency (MQA)</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="accreditation2" value="other" name="accreditation"
                                                class="custom-control-input">
                                            <label class="custom-control-label" for="accreditation2">Other</label>
                                        </div>


                                    </div>
                                </div>
                                <div class="form-group accreditation">
                                    <label class="col-lg-2 control-label"></label>
                                    <div class="col-lg-12">
                                        <input type="text" name="" class="form-control"
                                            placeholder="Other Accreditation">
                                    </div>
                                </div>
                            </div>
                        </div>





                    </fieldset>


                    <fieldset title="Step 4" class="step" id="default-step-3">
                        <legend></legend>
                        <div class="card">
                            <header class="card-header">
                                Section 4
                            </header>
                            <div class="card-body">
                                <div class="form-group">
                                    <label class="ontrol-label">Taylor's Lakeside Campus Address </label>
                                    <textarea name="" id="" cols="30" rows="2" class="form-control"></textarea>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Method of Learning</label>
                                    <select class="form-control select2-multiple" name="learning_mode" id="learning_mode"
                                        multiple>
                                        <option value="lecture">Lecture</option>
                                        <option value="tutorial">Tutorial</option>
                                        <option value="lab">Lab</option>
                                        <option value="field_work">Field Work</option>
                                        <option value="study_work">Study Work</option>
                                        <option value="seminar">Seminar</option>
                                        <option value="others">Others</option>
                                    </select>
                                </div>
                                <div class="form-group other-learning-mode">
                                    <label class="control-label" >Other Learning Mode </label> <input type="text"
                                        class="form-control" placeholder="Others">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Mode of Delivery</label>
                                    <select class="form-control select2-multiple" name="delivery_mode" id="delivery_mode"
                                        multiple>
                                        <option value="lecture">Conventional (traditional, online and blended learning
                                        </option>
                                        <option value="tutorial">Open and Distance Learning (ODL)</option>
                                    </select>

                                </div>
                                <div class="form-group ">
                                    <label class="control-label">Language of Instruction</label>
                                    <input type="text" readonly value="English" class="form-control"
                                        placeholder="Others">
                                    <button type="button" class="btn btn-info btn-sm mt-2">Editable by RAG</button>
                                </div>
                            </div>
                        </div>

                    </fieldset>

                    <fieldset title="Step 5" class="step" id="default-step-4">
                        <legend></legend>
                        <div class="card">
                            <header class="card-header">
                                Section 5
                            </header>
                            <div class="card-body">
                                <h4>Programme Educational Objectives</h4>
                                <div id="table">
                                    <span class="table-add float-right mb-3 mr-2"><a href="#!" class="text-success"><i
                                                class="fa fa-plus fa-2x" aria-hidden="true"></i></a></span>
                                    <table class="table table-bordered table-condensed tgc">
                                        <thead>
                                            <tr>
                                                <th width="10%" class="text-center">PEO ID</th>
                                                <th>PEO Description</th>
                                                <th width="15%" class="text-center">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="col-item">PEO 1</td>
                                                <td contenteditable="true">
                                                    Knowledge and Understanding:
                                                    At the end of the programme, students should be able to
                                                    demonstrate knowledge and understanding of the
                                                    terminology and the theoretical basis of performance
                                                </td>
                                                <td class="text-center">
                                                    <span class="table-remove text-center">
                                                        <button type="button"
                                                            class="btn btn-danger btn-rounded btn-sm my-0">
                                                            <i class="fa fa-trash mr-1"></i>
                                                            Remove
                                                        </button>
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col-item">PEO 2</td>
                                                <td contenteditable="true">
                                                    Application:
                                                    At the end of the programme, students should be able to
                                                    use art confidently as a form of expression and
                                                    communication while demonstrating a range of technical
                                                    skills in formal or informal performance and exhibition.
                                                </td>
                                                <td class="text-center">
                                                    <span class="table-remove text-center">
                                                        <button type="button"
                                                            class="btn btn-danger btn-rounded btn-sm my-0">
                                                            <i class="fa fa-trash mr-1"></i>
                                                            Remove
                                                        </button>
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr class="hide">
                                                <td contenteditable="true"></td>
                                                <td contenteditable="true"></td>
                                                <td class="text-center">
                                                    <span class="table-remove text-center">
                                                        <button type="button"
                                                            class="btn btn-danger btn-rounded btn-sm my-0">
                                                            <i class="fa fa-trash mr-1"></i>
                                                            Remove
                                                        </button>
                                                    </span>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>


                    </fieldset>

                    <fieldset title="Step 6" class="step" id="default-step-5">
                        <legend></legend>

                        <div class="card">
                            <header class="card-header">
                                Section 6
                            </header>
                            <div class="card-body">
                                <h3>Programme Educational Objectives</h3>
                                <div id="table2">
                                    <span class="table-add2 float-right mb-3 mr-2"><a href="#!" class="text-success"><i
                                                class="fa fa-plus fa-2x" aria-hidden="true"></i></a></span>
                                    <table class="table table-bordered table-condensed tgc text-center">
                                        <thead>
                                            <tr class="text-center">
                                                <th width="10%">PLO No</th>
                                                <th>PLO Description</th>
                                                <th>Set KPI/Target</th>
                                                <th>T & L Strategies</th>
                                                <th width="13%">Assessment Mode</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>PLO 1</td>
                                                <td contenteditable="true" class="text-left">
                                                    Demonstrate understanding of key theories of the field of theatre
                                                    permormance in analysis, critique, performance and production of
                                                    works.
                                                </td>
                                                <td>50%</td>
                                                <td class="text-left">
                                                    The facilitator will demonstrate the exercises and student will
                                                    learn
                                                    throught practical engament in these exercises
                                                </td>
                                                <td>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                    id="customCheck1" name="example1">
                                                                <label class="custom-control-label"
                                                                    for="customCheck1">Presentation</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                    id="customCheck2" name="example1">
                                                                <label class="custom-control-label"
                                                                    for="customCheck2">Journal Writing</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <span class="table-remove"><button type="button"
                                                            class="btn btn-danger btn-rounded btn-sm my-0"><i
                                                                class="fa fa-trash"></i></button></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>PLO 2</td>
                                                <td contenteditable="true" class="text-left">
                                                    Lorem ipsum, dolor sit amet consectetur adipisicing elit. Earum
                                                    accusamus
                                                    tempora cupiditate ipsam molestiae quas quos, asperiores ex nemo
                                                    sint,
                                                    libero sit quisquam maxime dicta dolorem. Quas minus deserunt odit.
                                                </td>
                                                <td>50%</td>
                                                <td class="text-left">Lorem ipsum dolor sit amet consectetur adipisicing
                                                    elit. Dicta
                                                    facere rerum
                                                    in qui accusamus, vitae totam dignissimos ullam a necessitatibus?
                                                    Dolor
                                                    tempora, dolore hic omnis veniam earum deserunt molestias est.</td>
                                                <td>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                    id="customCheck1" name="example1">
                                                                <label class="custom-control-label"
                                                                    for="customCheck1">Presentation</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                    id="customCheck2" name="example1">
                                                                <label class="custom-control-label"
                                                                    for="customCheck2">Journal Writing</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <span class="table-remove"><button type="button"
                                                            class="btn btn-danger btn-rounded btn-sm my-0"><i
                                                                class="fa fa-trash"></i></button></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>PLO 3</td>
                                                <td contenteditable="true" class="text-left">
                                                    Lorem ipsum, dolor sit amet consectetur adipisicing elit. Earum
                                                    accusamus
                                                    tempora cupiditate ipsam molestiae quas quos, asperiores ex nemo
                                                    sint,
                                                    libero sit quisquam maxime dicta dolorem. Quas minus deserunt odit.
                                                </td>
                                                <td>50%</td>
                                                <td class="text-left">Lorem ipsum dolor sit amet consectetur adipisicing
                                                    elit. Dicta
                                                    facere rerum
                                                    in qui accusamus, vitae totam dignissimos ullam a necessitatibus?
                                                    Dolor
                                                    tempora, dolore hic omnis veniam earum deserunt molestias est.</td>
                                                <td>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                    id="customCheck1" name="example1">
                                                                <label class="custom-control-label"
                                                                    for="customCheck1">Presentation</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                    id="customCheck2" name="example1">
                                                                <label class="custom-control-label"
                                                                    for="customCheck2">Journal Writing</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <span class="table-remove"><button type="button"
                                                            class="btn btn-danger btn-rounded btn-sm my-0"><i
                                                                class="fa fa-trash"></i></button></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>PLO 4</td>
                                                <td contenteditable="true" class="text-left">
                                                    Lorem ipsum, dolor sit amet consectetur adipisicing elit. Earum
                                                    accusamus
                                                    tempora cupiditate ipsam molestiae quas quos, asperiores ex nemo
                                                    sint,
                                                    libero sit quisquam maxime dicta dolorem. Quas minus deserunt odit.
                                                </td>
                                                <td>50%</td>
                                                <td class="text-left">Lorem ipsum dolor sit amet consectetur adipisicing
                                                    elit. Dicta
                                                    facere rerum
                                                    in qui accusamus, vitae totam dignissimos ullam a necessitatibus?
                                                    Dolor
                                                    tempora, dolore hic omnis veniam earum deserunt molestias est.</td>
                                                <td>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                    id="customCheck1" name="example1">
                                                                <label class="custom-control-label"
                                                                    for="customCheck1">Presentation</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                    id="customCheck2" name="example1">
                                                                <label class="custom-control-label"
                                                                    for="customCheck2">Journal Writing</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <span class="table-remove"><button type="button"
                                                            class="btn btn-danger btn-rounded btn-sm my-0"><i
                                                                class="fa fa-trash"></i></button></span>
                                                </td>
                                            </tr>
                                            <tr class="hide">
                                                <td contenteditable="true">
                                                    <textarea name="" class="form-control" id="" cols="30"
                                                        rows="2"></textarea>
                                                </td>
                                                <td contenteditable="true">
                                                    <textarea name="" class="form-control" id="" cols="30"
                                                        rows="2"></textarea>
                                                </td>
                                                <td contenteditable="true">
                                                    <select name="" id="" class="form-control">
                                                        <option value="40%">40%</option>
                                                        <option value="50%">50%</option>
                                                        <option value="60%">60%</option>
                                                        <option value="70%">70%</option>
                                                        <option value="80%">80%</option>
                                                        <option value="90%">90%</option>
                                                        <option value="100%">100%</option>
                                                    </select>
                                                </td>
                                                <td contenteditable="true">
                                                    <textarea name="" class="form-control" id="" cols="30"
                                                        rows="2"></textarea>
                                                </td>
                                                <td>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                    id="customCheck1" name="example1">
                                                                <label class="custom-control-label"
                                                                    for="customCheck1">Presentation</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                    id="customCheck2" name="example1">
                                                                <label class="custom-control-label"
                                                                    for="customCheck2">Journal Writing</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <span class="table-remove"><button type="button"
                                                            class="btn btn-danger btn-rounded btn-sm my-0"><i
                                                                class="fa fa-trash"></i></button></span>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>


                    </fieldset>

                    <fieldset title="Step 7" class="step" id="default-step-6">
                        <legend></legend>
                        <div class="card bg-light">
                            <header class="card-header">
                                Section 7
                            </header>
                            <div class="card-body">
                                <h3>Programme Standards</h3>
                                <div id="table3">
                                    <span class="table-add3 float-right mb-3 mr-2"><a href="#!" class="text-success"><i
                                                class="fa fa-plus fa-2x" aria-hidden="true"></i></a></span>
                                    <table class="table table-bordered table-condensed tgc">
                                        <thead>
                                            <tr>
                                                <th width="10%">PS ID</th>
                                                <th>PS Description</th>
                                                <th width="15%">Action</th>
                                            </tr>
                                            <tr>
                                                <td class="col-item">PS 1</td>
                                                <td>
                                                    Lorem ipsum dolor sit amet consectetur, adipisicing elit. Impedit
                                                    consectetur deleniti quia ut dolores! Laboriosam, quaerat
                                                    repellendus
                                                    placeat reiciendis possimus odio aliquid magni dolore vel quia at
                                                    dignissimos. Illo, voluptatum?
                                                </td>
                                                <td class="col-item">
                                                    <span class="table-remove"><button type="button"
                                                            class="btn btn-danger btn-rounded btn-sm my-0"><i
                                                                class="fa fa-trash mr-1"></i> Remove</button></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col-item">PS 2</td>
                                                <td>Lorem ipsum dolor sit amet consectetur adipisicing elit. Harum ea
                                                    accusantium accusamus quisquam velit! Eligendi placeat corporis ex
                                                    quibusdam
                                                    suscipit unde minima magni, quasi nam quis saepe odio dolores eius.
                                                </td>
                                                <td class="col-item">
                                                    <span class="table-remove"><button type="button"
                                                            class="btn btn-danger btn-rounded btn-sm my-0"><i
                                                                class="fa fa-trash mr-1"></i> Remove</button></span>
                                                </td>
                                            </tr>
                                            <tr class="hide">
                                                <td class="col-item"></td>
                                                <td contenteditable="true"></td>
                                                <td class="col-item">
                                                    <span class="table-remove"><button type="button"
                                                            class="btn btn-danger btn-rounded btn-sm my-0"><i
                                                                class="fa fa-trash mr-1"></i> Remove</button></span>
                                                </td>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </fieldset>

                    <fieldset title="Step 8" class="step" id="default-step-7">
                        <legend></legend>

                        <div class="card">
                            <header class="card-header">
                                Section 8
                            </header>
                            <div class="card-body">
                                <h3>Professional Body Requirements</h3>
                                <div id="table4">
                                    <span class="table-add4 float-right mb-3 mr-2"><a href="#!" class="text-success"><i
                                                class="fa fa-plus fa-2x" aria-hidden="true"></i></a></span>
                                    <table class="table table-bordered table-condensed tgc text-center">
                                        <thead>
                                            <tr>
                                                <th width="5%">No.</th>
                                                <th width="25%">Professional Body Main Requirements</th>
                                                <th width="40%" colspan="2">Professional Body Sub Requirements
                                                    (Optional)</th>
                                                <th width="15%">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <textarea name="" id="summernote" cols="10" rows="2"
                                                        class="custom-field">LAM 1</textarea>
                                                </td>
                                                <td>
                                                    <textarea name="" id="summernote" cols="50" rows="2" width="100%"
                                                        class="custom-field"></textarea>
                                                </td>
                                                <td><textarea name="" id="summernote" cols="10" rows="2"
                                                        class="custom-field">LAM 1.1</textarea></td>
                                                <td>
                                                    <textarea name="" id="summernote" cols="50" rows="2" width="100%"
                                                        class="custom-field"></textarea>
                                                </td>
                                                <td> <span class="table-remove"><button type="button"
                                                            class="btn btn-danger btn-rounded btn-sm my-0"><i
                                                                class="fa fa-trash mr-1" aria-hidden="true"></i>
                                                            Remove</button></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <textarea name="" id="summernote" cols="10" rows="2"
                                                        class="custom-field"></textarea>
                                                </td>
                                                <td contenteditable="true">
                                                    <textarea name="" id="summernote" cols="50" rows="2" width="100%"
                                                        class="custom-field"></textarea>
                                                </td>
                                                <td><textarea name="" id="summernote" cols="10" rows="2"
                                                        class="custom-field">LAM 1.2</textarea></td>
                                                <td>
                                                    <textarea name="" id="summernote" cols="50" rows="2" width="100%"
                                                        class="custom-field"></textarea>
                                                </td>
                                                <td> <span class="table-remove"><button type="button"
                                                            class="btn btn-danger btn-rounded btn-sm my-0"><i
                                                                class="fa fa-trash mr-1" aria-hidden="true"></i>
                                                            Remove</button></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><textarea name="" id="summernote" cols="10" rows="2"
                                                        class="custom-field">LAM 2</textarea></td>
                                                <td contenteditable="true">
                                                    <textarea name="" id="summernote" cols="50" rows="2" width="100%"
                                                        class="custom-field"></textarea>
                                                </td>
                                                <td><textarea name="" id="summernote" cols="10" rows="2"
                                                        class="custom-field">LAM 2.1</textarea></td>
                                                <td>
                                                    <textarea name="" id="summernote" cols="50" rows="2" width="100%"
                                                        class="custom-field"></textarea>
                                                </td>
                                                <td> <span class="table-remove"><button type="button"
                                                            class="btn btn-danger btn-rounded btn-sm my-0"><i
                                                                class="fa fa-trash mr-1" aria-hidden="true"></i>
                                                            Remove</button></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <textarea name="" id="summernote" cols="10" rows="2"
                                                        class="custom-field"></textarea>
                                                </td>
                                                <td contenteditable="true">
                                                    <textarea name="" id="summernote" cols="50" rows="2" width="100%"
                                                        class="custom-field"></textarea>
                                                </td>
                                                <td><textarea name="" id="summernote" cols="10" rows="2"
                                                        class="custom-field">LAM 2.2</textarea></td>
                                                <td>
                                                    <textarea name="" id="summernote" cols="50" rows="2" width="100%"
                                                        class="custom-field"></textarea>
                                                </td>
                                                <td> <span class="table-remove"><button type="button"
                                                            class="btn btn-danger btn-rounded btn-sm my-0"><i
                                                                class="fa fa-trash mr-1" aria-hidden="true"></i>
                                                            Remove</button></span>
                                                </td>

                                            </tr>
                                            <tr class="hide">
                                                <td>
                                                    <textarea name="" id="summernote" cols="10" rows="2"
                                                        class="custom-field"></textarea>
                                                </td>
                                                <td contenteditable="true">
                                                    <textarea name="" id="summernote" cols="50" rows="2" width="100%"
                                                        class="custom-field"></textarea>
                                                </td>
                                                <td><textarea name="" id="summernote" cols="10" rows="2"
                                                        class="custom-field">LAM 2.2</textarea></td>
                                                <td>
                                                    <textarea name="" id="summernote" cols="50" rows="2" width="100%"
                                                        class="custom-field"></textarea>
                                                </td>
                                                <td> <span class="table-remove"><button type="button"
                                                            class="btn btn-danger btn-rounded btn-sm my-0"><i
                                                                class="fa fa-trash mr-1" aria-hidden="true"></i>
                                                            Remove</button></span>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>


                    </fieldset>

                    <fieldset title="Step 9" class="step" id="default-step-8">
                        <legend></legend>
                        <div class="card bg-light">
                            <header class="card-header">
                                Section 9
                            </header>
                            <div class="card-body">
                                <a class="btn btn-space btn-sm btn-primary" href="{{ URL::route('mapping1') }}">Click here to proceed to TGC-PLO Mapping <i class="fa fa-chevron-right"></i></a>
                            </div>
                        </div>

                    </fieldset>

                    <fieldset title="Step 10" class="step" id="default-step-9">
                        <legend></legend>

                    </fieldset>
                    <fieldset title="Step 11" class="step" id="default-step-10">
                        <legend></legend>

                    </fieldset>
                    <fieldset title="Step 12" class="step" id="default-step-11">
                        <legend></legend>

                    </fieldset>
                    <fieldset title="Step 13" class="step" id="default-step-12">
                        <legend></legend>
                        <div class="card bg-light">
                            <header class="card-header">
                                Section 13
                            </header>
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="">Name of Specialization</label>
                                    <input type="text" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="">Specializtion Code</label>
                                    <input type="text" class="form-control" readonly>
                                    <button class="btn btn-xs btn-primary mt-2"><i class="fa fa-info-circle mr-2"
                                            aria-hidden="true"></i>To be keyed in by RAG</button>
                                </div>
                                <div class="form-group">
                                    <label for="">Specialization Synopsis</label>
                                    <textarea name="" id="" cols="30" rows="5" class="form-control"></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="">Minimum number of credits to be completed for specialization</label>
                                    <input type="number" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="">Exclusion Criteria</label>
                                    <textarea name="" id="" cols="30" rows="5" class="form-control"></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="">Graduation Criteria</label>
                                    <textarea name="" id="" cols="30" rows="5" class="form-control"></textarea>
                                </div>
                            </div>
                        </div>

                    </fieldset>

                    <fieldset title="Step 14" class="step" id="default-step-13">
                        <legend></legend>
                        <div class="card">
                            <header class="card-header">
                                Section 14
                            </header>
                            <div class="card-body">
                                <div id="table5">
                                    <span class="table-add5 float-right mb-3 mr-2"><a href="#!" class="text-success"><i
                                                class="fa fa-plus fa-2x" aria-hidden="true"></i></a></span>
                                    <table class="table table-bordered table-condensed tgc text-center">
                                        <thead>
                                            <tr>
                                                <th width="10%">ID</th>
                                                <th width="40%">Description</th>
                                                <th width="10%">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><textarea name="" id="" cols="30" rows="3"
                                                        class="form-control custom-field" placeholder="IA1"></textarea>
                                                </td>
                                                <td class="text-left">
                                                    <textarea name="" id="" cols="30" rows="3"
                                                        class="form-control custom-field"
                                                        placeholder="Lorem ipsum, dolor sit amet consectetur adipisicing elit. Vitae neque reiciendis voluptatibus illum! Impedit vitae ipsa a beatae, reprehenderittempora voluptates hic quos explicabo doloremque fugit eaque laudantium quibusdam exercitationem!"></textarea>

                                                </td>
                                                <td> <span class="table-remove"><button type="button"
                                                            class="btn btn-danger btn-rounded btn-sm my-0"><i
                                                                class="fa fa-trash mr-1" aria-hidden="true"></i>
                                                            Remove</button></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><textarea name="" id="" cols="30" rows="3"
                                                        class="form-control custom-field" placeholder="IA2"></textarea>
                                                </td>
                                                <td class="text-left">
                                                    <textarea name="" id="" cols="30" rows="3"
                                                        class="form-control custom-field"
                                                        placeholder="Lorem ipsum, dolor sit amet consectetur adipisicing elit. Vitae neque reiciendis voluptatibus illum! Impedit vitae ipsa a beatae, reprehenderittempora voluptates hic quos explicabo doloremque fugit eaque laudantium quibusdam exercitationem!"></textarea>

                                                </td>
                                                <td> <span class="table-remove"><button type="button"
                                                            class="btn btn-danger btn-rounded btn-sm my-0"><i
                                                                class="fa fa-trash mr-1" aria-hidden="true"></i>
                                                            Remove</button></span>
                                                </td>
                                            </tr>
                                            <tr class="hide">
                                                <td><textarea name="" id="" cols="30" rows="3"
                                                        class="form-control custom-field" placeholder="IA3"></textarea>
                                                </td>
                                                <td class="text-left">
                                                    <textarea name="" id="" cols="30" rows="3"
                                                        class="form-control custom-field"
                                                        placeholder="Lorem ipsum, dolor sit amet consectetur adipisicing elit. Vitae neque reiciendis voluptatibus illum! Impedit vitae ipsa a beatae, reprehenderittempora voluptates hic quos explicabo doloremque fugit eaque laudantium quibusdam exercitationem!"></textarea>

                                                </td>
                                                <td> <span class="table-remove"><button type="button"
                                                            class="btn btn-danger btn-rounded btn-sm my-0"><i
                                                                class="fa fa-trash mr-1" aria-hidden="true"></i>
                                                            Remove</button></span>
                                                </td>
                                            </tr>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>


                    </fieldset>

                    <fieldset title="Step 15" class="step" id="default-step-14">
                        <legend></legend>
                       <div class="card bg-light">
                        <header class="card-header">
                            Section 15
                        </header>
                        <div class="card-body">
                            <a class="btn btn-space btn-sm btn-primary" href="{{ URL::route('mapping3') }}"> Click here to proceed to PLO-Module Mapping <i class="fa fa-chevron-right"></i></a>
                        </div>
                    </div>
                    </fieldset>

                    <fieldset title="Step 16" class="step" id="default-step-15">
                        <legend></legend>
                        <div class="card">
                            <header class="card-header">
                                Section 16
                            </header>
                            <div class="card-body">
                                <div class="container">
                                    <div class="form-row">
                                        <div class="form-group col-lg-6">
                                            <label for="">Effective Date</label>
                                            {{-- note: to prevent the date from poping out i put this to direct the focus to this hidden dummy field  muahaha --}}
                                            <input style="height:0px; top:-1000px; position:absolute" type="text"
                                                value="">
                                            <div class="input-group date helper-datepicker"  id="helper-datepicker">
                                                <input type="text" class="form-control" value="02/25/2018">
                                                <div class="input-group-append">
                                                    <button class="btn btn-primary" type="button"><i
                                                            class="fa fa-calendar f14"></i></button>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="form-group col-lg-6">
                                            <label for="">Revision Number</label>
                                            <input type="text" class="form-control" readonly value="Rev.1.01">
                                        </div>
                                    </div>
                                    <div id="approval" class="mb-0">
                                        <div class="form-row">
                                            <div class="form-group col-lg-6">
                                                <label for="">Approval Date</label>

                                                <input style="height:0px; top:-1000px; position:absolute" type="text" value="">
                                                <div class="input-group date helper-datepicker" id="helper-datepicker2">
                                                    <input type="text" class="form-control" value="02/25/2018">
                                                    <div class="input-group-append">
                                                        <button class="btn btn-default" type="button"><i class="fa fa-calendar f14"></i></button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-6">
                                                <label for="">Approval By</label>
                                                <input type="text" class="form-control" value="">
                                            </div>
                                        </div>

                                    </div>
                                    <div id="approval-add">
                                    </div>
                                    <div class="form-row">
                                            <button type="button" class="add-more btn btn-danger btn-xs"><i class="fa fa-plus mr-2"></i>Add more approval</button>
                                    </div>
                                    <div class="divider"></div>
                                    <div class="form-row mt-4">
                                        <div class="form-group col-lg-6">
                                            <label for="">Effective Intake Month</label>
                                            <div class="input-group date dpMonths" data-date-viewmode="years"
                                                data-date-format="mm">
                                                <input type="text" class="form-control" aria-label="Right Icon"
                                                    aria-describedby="dp-mdo">
                                                <div class="input-group-append">
                                                    <button id="dp-mdo" class="btn btn-primary" type="button"><i
                                                            class="fa fa-calendar f14"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group col-lg-6">
                                            <label for="">Effective Intake Year</label>
                                            <div class="input-group date dpYears" data-date-viewmode="years"
                                                data-date-format="mm">
                                                <input type="text" class="form-control" aria-label="Right Icon"
                                                    aria-describedby="dp-mdo">
                                                <div class="input-group-append">
                                                    <button id="dp-mdo" class="btn btn-primary" type="button"><i
                                                            class="fa fa-calendar f14"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>



                    </fieldset>


                    <div  class="finish">
                        <a href="{{ URL::route('programme-info') }}" class="btn btn-success" >
                            <i class="fa fa-check"></i>
                            Finish !
                        </a>
                    </div>


                </form>

            </div>
        </section>
    </div>
</div>


@endsection
